package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.partita.Partita;

public class PartitaMutatioTest {

	PartitaMutatio mut;
	Partita part;
	
	@Test
	public void testChangeModelOK() throws CdQException {
		part= new Partita();
		mut= new PartitaMutatio(part,"Ciao");
		
		assertEquals(part,mut.changeModel(part));
	}
	
	@Test
	public void testGetModelOK() throws CdQException {
		part= new Partita();
		mut= new PartitaMutatio(part,"Ciao");
		
		assertEquals(part,mut.getModello());
	}

	@Test
	public void testToString() throws FileSyntaxException{
		Partita part= new Partita();		
		mut= new PartitaMutatio(part,"Ciao");
		assertEquals("Mutatio@Ciao",mut.toString());
	}
}
