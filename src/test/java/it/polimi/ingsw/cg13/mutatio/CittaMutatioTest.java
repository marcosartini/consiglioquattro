package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CittaMutatioTest {

	CittaMutatio mut;
	Citta citta;
	Emporio emp;
	
	@Test
	public void testGetter() {
		citta= Mockito.mock(Citta.class);
		Mockito.when(citta.getNome()).thenReturn("bho");
		emp= Mockito.mock(Emporio.class);
		Mockito.when(emp.getGiocatoreAppartenente()).thenReturn(null);
		mut= new CittaMutatio(citta,emp);
		
		assertEquals("bho",mut.getCitta().getNome());
		assertEquals(null,mut.getNuovoEmporio().getGiocatoreAppartenente());
	}
	
	@Test
	public void testChangeModel() throws CdQException {
		Partita part= new Partita();
		Giocatore g1= new Giocatore("Luca",new Colore("Rosso",255,0,0),1,10);
		emp= new Emporio(g1);
		citta= part.getNodoCitta("Arkon").getCittaNodo();
		mut= new CittaMutatio(citta,emp);
		
		part= mut.changeModel(part);
		assertEquals(10,g1.getEmpori().size());
	}
	
	@Test
	public void testToString() throws CdQException {
		Partita part= new Partita();
		Giocatore g1= new Giocatore("Luca",new Colore("Rosso",255,0,0),1,10);
		emp= new Emporio(g1);
		citta= part.getNodoCitta("Arkon").getCittaNodo();
		mut= new CittaMutatio(citta,emp);
		
		System.out.println(mut.toString());
		assertEquals("Mutatio@ Citta: Arkon\nEmporiPresenti: ()",mut.toString());
	}

}
