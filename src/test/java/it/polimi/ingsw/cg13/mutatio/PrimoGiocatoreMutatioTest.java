package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;

public class PrimoGiocatoreMutatioTest {

	PrimoGiocatoreMutatio mut= new PrimoGiocatoreMutatio();
	@Test
	public void testTextString(){		
		assertEquals("Sei il primo giocatore collegato !! Inizio creazione nuova partita\n",mut.toString());
	}
	
	@Test
	public void testChangeModelOK() throws CdQException {
		Partita part= new Partita();	
		assertEquals(part,mut.changeModel(part));
	}

}
