package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.CartaPoliticaJolly;
import it.polimi.ingsw.cg13.carte.CartaPoliticaSemplice;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.partita.Partita;

public class MazzoScartoMutatioTest {
	MazzoScartoMutatio mut;
	List<CartaPolitica> carte= new ArrayList<>();	
	
	@Test
	public void testChangeModel() throws CdQException {
		carte.add(new CartaPoliticaSemplice(new Colore("bianco",255,255,255)));
		carte.add(new CartaPoliticaSemplice(new Colore("bianco",255,255,255)));
		carte.add(new CartaPoliticaJolly());
		mut= new MazzoScartoMutatio(carte);
		Partita part= new Partita();
		assertEquals(part,mut.changeModel(part));
	}

	@Test
	public void testToString() throws FileSyntaxException{
		Partita part= new Partita();		
		mut= new MazzoScartoMutatio(carte);
		assertEquals("MazzoScartoMutatio carte scartate: []",mut.toString());
	}
}
