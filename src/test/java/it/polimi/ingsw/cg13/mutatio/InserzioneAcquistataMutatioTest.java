package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoInizio;
import it.polimi.ingsw.cg13.machinestate.StateMarketAcquisto;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class InserzioneAcquistataMutatioTest {
	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	List<Giocatore> giocatori= new ArrayList<>();
	CasellaRicchezza cas= new CasellaRicchezza(2);
	Inserzione i= new Inserzione(cas,g2,new Aiutante());
	InserzioneAcquistataMutatio mut= new InserzioneAcquistataMutatio(i,g1);
	Market m= new Market();
	
	@Test
	public void testChangeModelOK() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		part.setStatoPartita(new StateAcquistoInizio(part.getGiocatori(),g1,m));
		g2.addAiutanti(1);
		i=new Inserzione(cas,g2,g2.getAiutanti().get(0));
		g2.mettiVendita(m, i);
		assertEquals(part,mut.changeModel(part));
	}

	@Test
	public void testTextString(){		
		assertEquals("InserzioneAcquistataMutatio@ Inserzione 0:\n oggetto= Aiutante prezzo= 2 venduta da Luigi\nE' stata acquistata da Luca",mut.toString());
	}

	@Test
	public void testGetter(){		
		assertEquals("Luca",mut.getGiocatore().getNome());
		assertEquals(2,mut.getInserzione().getPrezzo().getPuntiCasella().intValue());
	}
}
