package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class ConsiglioMutatioTest {

	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	List<Giocatore> giocatori= new ArrayList<>();
	ConsiglioMutatio mut;
	Consiglio consiglio;
	Consigliere consigliereAggiunto;
	
	@Test
	public void testChangeModel() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		part.startTurnoPartita();
		consiglio=part.getRegione("mare").getConsiglio();
		consigliereAggiunto= new Consigliere(new Colore("viola",143,0,255));
		mut= new ConsiglioMutatio(consiglio,consigliereAggiunto);
		mut.changeModel(part);
	}

	@Test
	public void testGetter() throws CdQException {
		Partita part= new Partita();
		consiglio=part.getRegione("mare").getConsiglio();
		consigliereAggiunto= new Consigliere(new Colore("viola",143,0,255));
		mut= new ConsiglioMutatio(consiglio,consigliereAggiunto);
		assertEquals(consiglio,mut.getConsiglio());
		assertEquals(consigliereAggiunto,mut.getConsigliereAggiunto());
	}
	
	@Test
	public void testToString() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		part.startTurnoPartita();
		consiglio=part.getKing().getConsiglio();
		consigliereAggiunto= new Consigliere(new Colore("viola",143,0,255));
		mut= new ConsiglioMutatio(consiglio,consigliereAggiunto);
		System.out.println(mut.toString());
		assertEquals(String.class,mut.toString().getClass());
		
	}
}
