package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.punteggio.CalcolatorePunteggio;

public class ClassificaMutatioTest {

	ClassificaMutatio mut;
	CalcolatorePunteggio calc= new CalcolatorePunteggio();
	
	@Test
	public void testChangeModel() throws CdQException {
		mut= new ClassificaMutatio(calc);
		Partita part= new Partita();
		
		part=mut.changeModel(part);
		assertTrue(part.isEnded());
	}

	@Test
	public void testToString() throws CdQException{
		Partita part= new Partita();
		calc=Mockito.mock(CalcolatorePunteggio.class);
		Mockito.when(calc.toString()).thenReturn("Sono il calcolatore!");
		mut= new ClassificaMutatio(calc);		
		
		assertFalse(part.isEnded());
	}
	
	@Test
	public void testGetCalcolo(){
		mut= new ClassificaMutatio(calc);	
		
		assertEquals(calc,mut.getCalcolo());
	}
}
