package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

public class ConsiglieriBancoMutatioTest {

	ConsiglieriBancoMutatio mut;
	Consigliere cons1;
	Consigliere cons2;
	
	@Test
	public void testGetter() {
		mut= new ConsiglieriBancoMutatio(new Consigliere(new Colore("rosso",255,0,0)),new Consigliere(new Colore("verde",0,255,0)));
		assertEquals("verde",mut.getConsigliereAggiunto().getColor().getNome());
		assertEquals("rosso",mut.getConsigliereTolto().getColor().getNome());
	}

	@Test
	public void testToString() {
		mut= new ConsiglieriBancoMutatio(new Consigliere(new Colore("rosso",255,0,0)),new Consigliere(new Colore("verde",0,255,0)));
		assertEquals("ConsiglieriBancoMutatio consigliere tolto: rosso, consigliere aggiunto: verde",mut.toString());
		
	}
	
	@Test
	public void testChangeModel() throws CdQException {
		mut= new ConsiglieriBancoMutatio(new Consigliere(new Colore("viola",143,0,255)),new Consigliere(new Colore("ciano",0,255,255)));
		Partita part= new Partita();
		part=mut.changeModel(part);
		
		
	}
}
