package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.King;

public class KingMutatioTest {
	KingMutatio mut;
	King king;
	List<NodoCitta> percorsoRe= Mockito.mock(List.class);
	
	@Test
	public void testChangeModelOK() throws CdQException {
		Partita part= new Partita();
		mut= new KingMutatio(part.getKing(),percorsoRe);
		
		assertEquals(part,mut.changeModel(part));
	}

	@Test
	public void testToString() throws FileSyntaxException{
		Partita part= new Partita();		
		mut= new KingMutatio(part.getKing(),percorsoRe);
		assertEquals("Mutatio@King: [citta=Graden]",mut.toString());
	}
	
	@Test
	public void testGetter() throws FileSyntaxException{
		Partita part= new Partita();
		Mockito.when(percorsoRe.size()).thenReturn(2);
		mut= new KingMutatio(part.getKing(),percorsoRe);
		assertEquals(2,mut.getPercorsoRe().size());
		assertEquals("Graden",mut.getKing().getPosizione().getCittaNodo().getNome());
	}
}
