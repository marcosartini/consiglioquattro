package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.MazzoPolitico;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class PescaCartaMutatioTest {

	List<CartaPolitica> carte= Mockito.mock(List.class);
	MazzoPolitico mazzo= new MazzoPolitico(carte);
	PescaCartaMutatio mut=new PescaCartaMutatio(mazzo);
	
	@Test
	public void testTextString(){		
		assertEquals("Mutatio@MazzoMazzoPolitico: [carte=0]",mut.toString());
	}
	
	@Test
	public void testChangeModelOK() throws CdQException {
		Partita part= new Partita();	
		assertEquals(part,mut.changeModel(part));
	}

}
