package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.StateInizioVendita;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class InserzioneInVenditaMutatioTest {

	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	List<Giocatore> giocatori= new ArrayList<>();
	CasellaRicchezza cas= new CasellaRicchezza(2);
	Inserzione i= new Inserzione(cas,g1,new Aiutante());
	InserzioneInVenditaMutatio mut= new InserzioneInVenditaMutatio(i);
	Market m= new Market();
	
	@Test
	public void testChangeModelOK() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		part.setStatoPartita(new StateInizioVendita(part.getGiocatori(),g1,m));
		g1.addAiutanti(1);
			
		g1.mettiVendita(part.getStatoPartita().getMarket(), i);
		assertEquals(part,mut.changeModel(part));
	}

	@Test
	public void testTextString(){		
		assertEquals("InserzioneInVenditaMutatio@ Inserzione 0:\n oggetto= Aiutante prezzo= 2 venduta da Luca",mut.toString());
	}

	@Test
	public void testGetter(){		
		assertEquals(2,mut.getInserzione().getPrezzo().getPuntiCasella().intValue());
	}

}
