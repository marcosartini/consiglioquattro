package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class LineGiocatoreMutatioTest {

	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	List<Giocatore> giocatori= new ArrayList<>();
	LineGiocatoreMutatio mut= new LineGiocatoreMutatio(g1);
	
	@Test
	public void testTextString(){		
		assertEquals("Mutatio@LineGiocatore_Luca: [ online=false ]",mut.toString());
	}
	
	@Test
	public void testChangeModelOK() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		mut.changeModel(part);
	}

}
