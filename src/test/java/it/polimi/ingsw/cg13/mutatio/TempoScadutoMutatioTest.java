package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class TempoScadutoMutatioTest {

	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	TempoScadutoMutatio mut=new TempoScadutoMutatio(g1);	
	List<Giocatore> giocatori= new ArrayList<>();
	
	
	@Test
	public void testChangeModelOK() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);	
		assertEquals(part,mut.changeModel(part));
	}
	
	@Test
	public void testTextString(){		
		assertEquals("TempoScadutoMutatio: il tempo  per fare l'azione e' finito !!\n Passaggio stato successivo in corso...",mut.toString());
	}

}
