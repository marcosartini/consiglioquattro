package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.partita.Partita;

public class CambioPermessiMutatioTest {

	CambioPermessiMutatio mut;
	MazzoPermessi mazzo1= Mockito.mock(MazzoPermessi.class);
	Regione reg;
	
	
	@Test
	public void testChangeModelOk() throws CdQException {
		Mockito.when(mazzo1.sizeMazzo()).thenReturn(1);
		Partita part= new Partita();
		mut= new CambioPermessiMutatio(mazzo1,part.getRegione("mare") );
				
		
		part=mut.changeModel(part);
		assertEquals(1,part.getMappaPartita().getRegione("mare").getMazzoPermesso().sizeMazzo());
		
	}

	@Test
	public void testToStringOk() throws CdQException {
		Mockito.when(mazzo1.getMazzoCarte()).thenReturn(new ArrayList<>());
		Partita part= new Partita();
		mut= new CambioPermessiMutatio(mazzo1,part.getRegione("mare") );
		assertEquals("Mutatio@MazzoPermessi: [carte=0]" ,mut.toString());
		
	}
	
	@Test
	public void testGetterOk() throws CdQException {
		Mockito.when(mazzo1.getMazzoCarte()).thenReturn(new ArrayList<>());
		Partita part= new Partita();
		mut= new CambioPermessiMutatio(mazzo1,part.getRegione("mare") );
		
		assertEquals(mazzo1,mut.getMazzo());
		assertEquals("mare",mut.getRegione().getNome());
	}
	

}
