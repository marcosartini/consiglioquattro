package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;

public class InizioFaseMarketMutatioTest {

	InizioFaseMarketMutatio mut= new InizioFaseMarketMutatio();
	
	@Test
	public void testChangeModelOK() throws CdQException {
		Partita part= new Partita();
		assertEquals(part,mut.changeModel(part));
	}

	@Test
	public void testTextString() throws CdQException {		
		assertEquals("Inizio fase di Market",mut.toString());
	}

}
