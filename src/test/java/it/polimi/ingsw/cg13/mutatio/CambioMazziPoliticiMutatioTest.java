package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.MazzoPolitico;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;

public class CambioMazziPoliticiMutatioTest {

	CambioMazziPoliticiMutatio mut;
	MazzoPolitico mazzo1= Mockito.mock(MazzoPolitico.class);
	MazzoPolitico mazzo2= Mockito.mock(MazzoPolitico.class);
	
	
	@Test
	public void testChangeModelOk() throws CdQException {
		Mockito.when(mazzo1.sizeMazzo()).thenReturn(1);
		Mockito.when(mazzo2.sizeMazzo()).thenReturn(2);
		mut= new CambioMazziPoliticiMutatio(mazzo1, mazzo2);
		Partita part= new Partita();		
		
		part=mut.changeModel(part);
		assertEquals(1,part.getBanco().getMazzoPolitico().sizeMazzo());
		assertEquals(2,part.getBanco().getMazzoScarto().sizeMazzo());
	}

	@Test
	public void testToStringOk() throws CdQException {
		mut= new CambioMazziPoliticiMutatio(mazzo1, mazzo2);
		assertEquals("CambioMazziPoliticiMutatio: mazzo terminato, mescolamento mazzo scarti in corso" ,mut.toString());
		
	}

}
