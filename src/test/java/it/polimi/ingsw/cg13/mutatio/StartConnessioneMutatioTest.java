package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;

public class StartConnessioneMutatioTest {

	StartConnessioneMutatio mut= new StartConnessioneMutatio("Pippo");
	
	@Test
	public void testChangeModelOK() throws CdQException {
		Partita part= new Partita();	
		assertEquals(part,mut.changeModel(part));
	}
	
	@Test
	public void testTextString(){		
		assertEquals("Creata connessione Pippo con il server\nAttendi l'inizio della partita",mut.toString());
	}

}
