package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class EmporioMutatioTest {

	EmporioMutatio mut;
	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	List<Giocatore> giocatori= new ArrayList<>();
	Emporio emp= new Emporio(g1);
	
	
	@Test
	public void testChangeModel() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		emp.setCittaPosizionato(part.getMappaPartita().getNodoCitta("Arkon").getCittaNodo());
		mut= new EmporioMutatio(emp);
		assertEquals(part,mut.changeModel(part));
	}
	
	@Test
	public void testGetter() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		emp.setCittaPosizionato(part.getMappaPartita().getNodoCitta("Arkon").getCittaNodo());
		mut= new EmporioMutatio(emp);
		assertEquals(emp,mut.getEmporio());
		assertEquals("Luca",mut.getGiocatore().getNome());
	}
	
	@Test
	public void testToString() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		emp.setCittaPosizionato(part.getMappaPartita().getNodoCitta("Arkon").getCittaNodo());
		mut= new EmporioMutatio(emp);
		assertEquals("Mutatio@Emporio: [giocatore=Luca citta=Arkon]",mut.toString());
	
	}

}
