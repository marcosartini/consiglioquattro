package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class GiocatoreNuovoMutatioTest {

	Giocatore g1=new Giocatore("Luca",new Colore("Rosso",255,0,0),1,10);
	
	GiocatoreNuovoMutatio mut= new GiocatoreNuovoMutatio(g1);
	
	@Test
	public void testChangeModelOK() throws CdQException {
		Partita part= new Partita();
		assertEquals(part,mut.changeModel(part));
	}

	@Test
	public void testTextString() {		
		assertEquals("Mutatio@Giocatore loggato: Luca",mut.toString());
	}

	@Test
	public void testGetGiocatore() {		
		assertEquals("Luca",mut.getGiocatore().getNome());
	}
}
