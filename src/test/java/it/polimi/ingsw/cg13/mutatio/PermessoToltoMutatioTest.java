package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoInizio;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class PermessoToltoMutatioTest {

	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	List<Giocatore> giocatori= new ArrayList<>();
	PermessoToltoMutatio mut;
	Regione reg;
	PermessoCostruzione perm;
	
	@Test
	public void testChangeModelOK() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		reg=part.getRegione("mare");
		perm=reg.getMazzoPermesso().getMazzoCarte().get(0);
		mut= new PermessoToltoMutatio(reg,perm);
		assertEquals(part,mut.changeModel(part));
	}
	
	@Test
	public void testGetterOK() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		reg=part.getRegione("mare");
		perm=reg.getMazzoPermesso().getMazzoCarte().get(0);
		mut= new PermessoToltoMutatio(reg,perm);
		assertEquals(perm,mut.getPermesso());
		assertEquals(reg,mut.getRegione());
	}
	
	@Test
	public void testToString() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		reg=part.getRegione("mare");
		perm=reg.getMazzoPermesso().getMazzoCarte().get(0);
		mut= new PermessoToltoMutatio(reg,perm);
		assertEquals("PermessoToltoMutatio: tolto dal mazzo "+perm.toString(),mut.toString());
		
	}

}
