package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.partita.Partita;

public class AiutantiBancoMutatioTest {

	AiutantiBancoMutatio mut;
	
	@Test
	public void testInitOk() {
		mut= new AiutantiBancoMutatio(10,10);
		assertEquals(10,mut.getAiutantiAggiunti());
		assertEquals(10,mut.getAiutantiRimossi());
	}
	
	@Test
	public void testChangeModelOk() throws CdQException {
		mut= new AiutantiBancoMutatio(10,10);
		Partita part= new Partita();
		
		part=mut.changeModel(part);
		assertFalse(part.isEmptyAiutanti());
	}

	@Test
	public void testToStringOk() throws CdQException {
		mut= new AiutantiBancoMutatio(10,10);
		assertEquals("AiutantiBancoMutatio aiutantiRimossi: 10, aiutantiAggiunti: 10" ,mut.toString());
		
	}
}
