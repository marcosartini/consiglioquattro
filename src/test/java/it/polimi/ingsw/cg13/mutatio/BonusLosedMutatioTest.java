package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;

public class BonusLosedMutatioTest {

	BonusLosedMutatio mut= new BonusLosedMutatio();
	
	
	@Test
	public void testChangeModelOk() throws CdQException {		
		Partita part= new Partita();		
		
		assertEquals(part,mut.changeModel(part));
	}

	@Test
	public void testToStringOk() throws CdQException {
		
		assertEquals("BonusLosedMutatio: Non hai citta o permessi da riutilizzare" ,mut.toString());
		
	}

}
