package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.TesseraBonus;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class TesseraBonusMutatioTest {

	TesseraBonusMutatio mut;
	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	List<Giocatore> giocatori= new ArrayList<>();
	TesseraBonus mockBonus= Mockito.mock(TesseraBonus.class);
	

	@Test
	public void testChangeModel() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		Mockito.when(mockBonus.getGiocatore()).thenReturn(g1);
		mut= new TesseraBonusMutatio(mockBonus);
		assertEquals(part,mut.changeModel(part));
	}

	@Test
	public void testGetGiocatore() throws CdQException {		
		Mockito.when(mockBonus.getGiocatore()).thenReturn(g1);
		mut= new TesseraBonusMutatio(mockBonus);
		assertEquals(g1,mut.getTesseraBonus().getGiocatore());
	}
	
	
}
