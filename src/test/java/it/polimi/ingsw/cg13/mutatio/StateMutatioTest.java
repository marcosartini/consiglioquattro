package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class StateMutatioTest {

	StateMutatio mut;
	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	List<Giocatore> giocatori= new ArrayList<>();
	State state= Mockito.mock(State.class);
	
	@Test
	public void testChangeModelRichOk() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		Mockito.when(state.getGiocatoreCorrente()).thenReturn(g1);
		mut= new StateMutatio(state);
				
		assertEquals(part,mut.changeModel(part));
		
	}

	@Test
	public void testToString(){
		Mockito.when(state.toString()).thenReturn("test");
		mut= new StateMutatio(state);
		assertEquals("StateMutatio@ test",mut.toString());
	}
	
	@Test
	public void testGetState(){
		Mockito.when(state.toString()).thenReturn("test");
		mut= new StateMutatio(state);
		assertEquals("test",mut.getStato().toString());
	}
}
