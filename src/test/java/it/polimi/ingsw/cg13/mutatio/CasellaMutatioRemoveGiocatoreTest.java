package it.polimi.ingsw.cg13.mutatio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.Casella;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CasellaMutatioRemoveGiocatoreTest {

	CasellaMutatioRemoveGiocatore mut;
	Casella cas1;
	Giocatore g1=new Giocatore("Luca",new Colore("rosso",255,0,0),1,10);
	Giocatore g2=new Giocatore("Luigi",new Colore("verde",0,255,0),2,11);
	List<Giocatore> giocatori= new ArrayList<>();
	
	
	@Test
	public void testChangeModelRichOk() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		cas1= new CasellaRicchezza(1);
		mut= new CasellaMutatioRemoveGiocatore(cas1,g1);
				
		
		part=mut.changeModel(part);
		assertEquals(0,part.getPercorsoRicchezza().getPercorso().get(cas1.getPuntiCasella()).getGiocatoriPresenti().size());
		
	}
	
	@Test
	public void testChangeModelNobOk() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		cas1= new CasellaNobilta(1);
		mut= new CasellaMutatioRemoveGiocatore(cas1,g1);
				
		
		part=mut.changeModel(part);
		assertEquals(0,part.getPercorsoNobilta().getPercorso().get(cas1.getPuntiCasella()).getGiocatoriPresenti().size());
		
	}
	
	@Test
	public void testChangeModelWinOk() throws CdQException {
		giocatori.add(g1);
		giocatori.add(g2);
		Partita part= new Partita(giocatori);
		cas1= new CasellaVittoria(1);
		mut= new CasellaMutatioRemoveGiocatore(cas1,g1);
				
		
		part=mut.changeModel(part);
		assertEquals(0,part.getPercorsoVittoria().getPercorso().get(cas1.getPuntiCasella()).getGiocatoriPresenti().size());
		
	}
	
	@Test
	public void testToString(){
		cas1= new CasellaRicchezza(1);
		mut= new CasellaMutatioRemoveGiocatore(cas1,g1);
		assertEquals("CasellaMutatio: Luca rimosso dalla CasellaRicchezza 1",mut.toString());
	}

}
