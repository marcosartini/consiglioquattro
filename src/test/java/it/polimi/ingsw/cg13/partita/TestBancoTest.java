package it.polimi.ingsw.cg13.partita;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.GruppoBonusRe;
import it.polimi.ingsw.cg13.bonus.TesseraBonus;



import it.polimi.ingsw.cg13.carte.CartaPoliticaSemplice;
import it.polimi.ingsw.cg13.carte.MazzoPolitico;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

public class TestBancoTest {

	private void init() throws ColoreNotFoundException{
		MazzoPolitico mazzoPolitico = Mockito.mock(MazzoPolitico.class);
		mazzoPolitico.aggiungiCarta(new CartaPoliticaSemplice(new Colore("blu", 0, 0, 255)));
		mazzoPolitico.aggiungiCarta(new CartaPoliticaSemplice(new Colore("rosso", 255, 0, 0)));
		
		MazzoPolitico scarto= new MazzoPolitico();
		
		
		
	}

	@Test
	public void testControllaMazzoPolitico() {
		MazzoPolitico mazzoPolitico = Mockito.mock(MazzoPolitico.class);
		MazzoPolitico mazzoScarto= Mockito.mock(MazzoPolitico.class);
		Queue<Aiutante> aiutanti = Mockito.mock(Queue.class);
		Map<Colore, Queue<Consigliere>> consiglieri = Mockito.mock(Map.class);
		GruppoBonusRe gruppoBonusRe= Mockito.mock(GruppoBonusRe.class);
		List<TesseraBonus> tessereBonus = Mockito.mock(List.class);
		
		
		Banco banco = new Banco(mazzoPolitico, mazzoScarto, aiutanti, consiglieri, gruppoBonusRe, tessereBonus);
				
		banco.controllaMazzoPolitico();
		
		
	}

	@Test
	public void testGetConsigliere() {
		
		MazzoPolitico mazzoPolitico = Mockito.mock(MazzoPolitico.class);
		MazzoPolitico mazzoScarto= Mockito.mock(MazzoPolitico.class);
		Queue<Aiutante> aiutanti = Mockito.mock(Queue.class);
		Map<Colore, Queue<Consigliere>> consiglieri = Mockito.mock(Map.class);
		GruppoBonusRe gruppoBonusRe= Mockito.mock(GruppoBonusRe.class);
		List<TesseraBonus> tessereBonus = Mockito.mock(List.class);
		
		
		Banco banco = new Banco(mazzoPolitico, mazzoScarto, aiutanti, consiglieri, gruppoBonusRe, tessereBonus);
		//when(consiglieri.get((new Colore("verde",	 0, 255, 0))).remove()).thenReturn(new Consigliere(new Colore ("verde", 0, 255, 0)));
		//assertTrue(banco.getConsigliere(new Colore("verde",	 0, 255, 0)).equals(new Consigliere(new Colore ("verde", 0, 255, 0))));
	}

	
	@Test
	public void testAggiungiConsigliere() {
		Colore col= new Colore ("verde", 0, 255, 0);
		MazzoPolitico mazzoPolitico = Mockito.mock(MazzoPolitico.class);
		MazzoPolitico mazzoScarto= Mockito.mock(MazzoPolitico.class);
		Queue<Aiutante> aiutanti = Mockito.mock(Queue.class);
		Map<Colore, Queue<Consigliere>> consiglieri = new HashMap<>();
		GruppoBonusRe gruppoBonusRe= Mockito.mock(GruppoBonusRe.class);
		List<TesseraBonus> tessereBonus = Mockito.mock(List.class);
		
		
		Banco banco = new Banco(mazzoPolitico, mazzoScarto, aiutanti, consiglieri, gruppoBonusRe, tessereBonus);
		
		banco.aggiungiConsigliere(new Consigliere(col));
		assertTrue(banco.getConsiglieri().containsKey(col));
	}
	
}
