package it.polimi.ingsw.cg13.partita;

import static org.junit.Assert.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.carte.Carta;
import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.machinestate.StateInizio;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.mappa.ConsiglioRe;
import it.polimi.ingsw.cg13.mappa.ConsiglioRegione;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.ProprietarioConsiglio;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.personaggi.King;

public class TestPartitaTest {

	

	@Test
	public void testPartitaListOfGiocatoreForTwoPlayersShouldHaveEmporioInAtLeastACity() throws FileSyntaxException, ColoreNotFoundException {
		List<Giocatore> giocatori= new ArrayList<>();
		giocatori.add(new Giocatore("Primo", 0, new Colore(123), 0, 0, new CasellaVittoria(0), new CasellaNobilta(0), new CasellaRicchezza(0)));
		giocatori.add(new Giocatore("Secondo", 0, new Colore(58), 0, 0, new CasellaVittoria(0), new CasellaNobilta(0), new CasellaRicchezza(0)));
		
		Partita partita = new Partita(giocatori);
		partita.startTurnoPartita();
		assertTrue(partita.getMappaPartita().getGrafo().stream().filter(nc -> nc.getCittaNodo().getEmpori().size()>0).count()>0);
	}
	
	@Test
	public void testGetCostoStradeOK() throws FileSyntaxException{
		
		Partita partita = new Partita();
		assertEquals(2,partita.getCostoStrade());
	}
	
	@Test
	public void testGetKingOK() throws FileSyntaxException{
		
		Partita partita = new Partita();
		King re=partita.getKing();
		assertEquals("Graden",re.getPosizione().getCittaNodo().getNome());
	}

	@Test
	public void testGetNodoCittaOK() throws FileSyntaxException, CittaNotFoundException{
		
		Partita partita = new Partita();
		NodoCitta temp=partita.getNodoCitta("Juvelar");
		assertEquals("Juvelar",temp.getCittaNodo().getNome());
	}

	@Test(expected=CittaNotFoundException.class)
	public void testGetNodoCittaThrowEXC() throws FileSyntaxException, CittaNotFoundException{
		
		Partita partita = new Partita();
		NodoCitta temp=partita.getNodoCitta("Milano");
		
	}
	
	@Test(expected=ColoreNotFoundException.class)
	public void testGetConsigliereThrowEXC() throws FileSyntaxException, ColoreNotFoundException{
		
		Partita partita = new Partita();
		Colore colore= new Colore("pippo",1,2,3);
		partita.getConsigliere(colore);
	}
	
	@Test
	public void testGetConsigliereOK() throws FileSyntaxException, ColoreNotFoundException{
		
		Partita partita = new Partita();
		Colore colore= new Colore("bianco",255,255,255);
		Consigliere cons=partita.getConsigliere(colore);
		assertEquals("bianco",cons.getColor().getNome());
	}
	
	@Test
	public void testGetPartitaOK() throws FileSyntaxException{		
		Partita partita = new Partita();
		assertEquals(partita,partita.getPartita());		
	}
	
	@Test
	public void testScartaCartaOK() throws FileSyntaxException{		
		Partita partita = new Partita();
		Banco desk=partita.getBanco();
		assertEquals(0,desk.getMazzoScarto().sizeMazzo());
		CartaPolitica card=desk.pescaCartaPolitica();
		List<CartaPolitica> carte= new ArrayList<>();
		carte.add(card);
		partita.scartaCarta(carte);
		assertEquals(1,desk.getMazzoScarto().sizeMazzo());
	}
	
	@Test
	public void testGetAiutantiOK() throws CdQException{		
		Partita partita = new Partita();
		List<Aiutante> help=new ArrayList<>();
		help.add(partita.getAiutante());
		assertEquals(1,help.size());
		help.addAll(partita.getAiutanti(2));
		assertEquals(3,help.size());
	}
	
	@Test
	public void testToStringOK() throws CdQException{		
		Partita partita = new Partita();
		StateInizio state=Mockito.mock(StateInizio.class);
		Mockito.when(state.toString()).thenReturn("Inizio");
		partita.setStatoPartita(state);
		assertEquals(String.class,partita.toString().getClass());
		
	}
	
	@Test
	public void testAddGiocatoreOK() throws CdQException{		
		Partita partita = new Partita();
		Login log= new Login("Lorenzo");
		Giocatore g1=partita.addGiocatore(log);
		assertEquals("Lorenzo",g1.getNome());
		assertEquals(0,g1.getAiutanti().size());
		assertEquals(10,g1.getRicchezzaGiocatore());
		assertEquals(4,g1.getCartePolitiche().size());
	}

	@Test
	public void testGetConsigliereConsOK() throws CdQException{		
		Partita partita = new Partita();
		Consigliere cons= new Consigliere(new Colore("bianco",255,255,255));
		assertEquals(cons,partita.getConsigliere(cons));
		
	}
	@Test(expected=ColoreNotFoundException.class)
	public void testGetConsigliereConsThrowEXC() throws CdQException{		
		Partita partita = new Partita();
		Consigliere cons= new Consigliere(new Colore("pippo",1,2,3));
		partita.getConsigliere(cons);
	}
	
	@Test
	public void testGetRegioneOK() throws FileSyntaxException, RegioneNotFoundException{		
		Partita partita = new Partita();
		Regione reg=partita.getRegione("montagna");
		
		assertEquals(5,reg.getCitta().size());
		
		assertEquals(5,partita.getRegione(reg).getCitta().size());
		
	}
	
	@Test(expected=RegioneNotFoundException.class)
	public void testGetRegioneThrowEXCRegioneNotFoundException() throws FileSyntaxException, RegioneNotFoundException{		
		Partita partita = new Partita();
		Regione reg=partita.getRegione("pianura");			
	}
	
	@Test
	public void testIsEndedOK() throws FileSyntaxException{		
		Partita partita = new Partita();
		assertFalse(partita.isEnded());
		partita.setEnded(true);
		assertTrue(partita.isEnded());
	}
	
	@Test
	public void testGetGiocatoriOffline() throws FileSyntaxException{		
		Partita partita = new Partita();
		assertEquals(0,partita.getGiocatoriOffline().size());
		
	}
	
	/*@Test
	public void testGetConsiglioOK() throws FileSyntaxException, RegioneNotFoundException, ErrorUpdateServerException{		
		Partita partita = new Partita();
		
		Queue<Consigliere> codaTemp=new ArrayDeque<Consigliere>();	
		codaTemp.add(new Consigliere(new Colore("verde",0,255,0)));
		codaTemp.add(new Consigliere(new Colore("blu",0,0,255)));
		codaTemp.add(new Consigliere(new Colore("giallo",255,255,0)));
		codaTemp.add(new Consigliere(new Colore("viola",255,0,255)));
		
		ConsiglioRegione cons= new ConsiglioRegione(codaTemp,partita.getRegione("mare"));
		
		partita.getConsiglio(cons);
	}*/
	
	@Test
	public void testStatoPartita() throws FileSyntaxException{		
		Partita partita = new Partita();
		
		List<Giocatore> giocatoriOnline=new ArrayList<>();
		Giocatore giocatore=new Giocatore("Primo", 0, new Colore(123), 0, 0, new CasellaVittoria(0), new CasellaNobilta(0), new CasellaRicchezza(0));
		giocatoriOnline.add(giocatore);
		List<RicompensaSpecial> ricompense=Mockito.mock(List.class);
		
		StateInizio stato= new StateInizio(giocatoriOnline,giocatore,ricompense);
		partita.setStatoPartita(stato);
		assertFalse(partita.getStatoPartita().isEnd());
		
	}
	
	@Test(expected= IllegalArgumentException.class)
	public void testStatoPartitaThrowEXC() throws FileSyntaxException{		
		Partita partita = new Partita();
		
		partita.setStatoPartita(null);		
	}
	
	@Test
	public void testSetKingOK() throws FileSyntaxException, CittaNotFoundException{
		
		Partita partita = new Partita();
		King re=partita.getKing();
		assertEquals("Graden",re.getPosizione().getCittaNodo().getNome());
		ConsiglioRe mockCons=Mockito.mock(ConsiglioRe.class);
		
		King king= new King(partita.getNodoCitta("Arkon"),mockCons);
		partita.setKing(king);
		assertEquals("Arkon",partita.getKing().getPosizione().getCittaNodo().getNome());
		
	}
	
	@Test
	public void testInitPartitaParamOK() throws FileSyntaxException, CittaNotFoundException{
		
		Partita partita = new Partita("7",50,7);
		King re=partita.getKing();
		assertEquals("Juvelar",re.getPosizione().getCittaNodo().getNome());
		
		
	}
}
