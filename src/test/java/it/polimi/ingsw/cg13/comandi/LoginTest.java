package it.polimi.ingsw.cg13.comandi;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class LoginTest {

	Login login;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testLoginStringaCorretta() {
		login=new Login("Daniele");
		assertTrue(login!=null && login.getNomeGiocatore().equals("Daniele"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testLoginStringaVuota() {
		login=new Login("   ");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testLoginStringaNull() {
		login=new Login(null);
	}


	@Test
	public void testGetNomeGiocatore() {
		login=new Login("Marco");
		assertEquals("Marco",login.getNomeGiocatore());
	}

	@Test
	public void testSetNomeGiocatoreStringaOK() {
		login=new Login("Daniele");
		login.setNomeGiocatore("Marco");
		assertEquals("Marco",login.getNomeGiocatore());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetNomeGiocatoreStringaVuota() {
		login=new Login("Daniele");
		login.setNomeGiocatore("  ");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetNomeGiocatoreStringaNulla() {
		login=new Login("Daniele");
		login.setNomeGiocatore(null);
	}


}
