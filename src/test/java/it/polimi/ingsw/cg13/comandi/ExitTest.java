package it.polimi.ingsw.cg13.comandi;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateFine;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class ExitTest {

	static Partita modello;
	static Partita modello1;
	static Partita modello2;
	static Partita modello3;
	static Giocatore gioc=Mockito.mock(Giocatore.class);
	static Giocatore gReale= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(10));
	static ArrayList<Giocatore> giocatoriVuoto=new ArrayList<>();
	static ArrayList<Giocatore> giocatoriPieno=new ArrayList<>();
	static ArrayList<Giocatore> giocatoriPieno2=new ArrayList<>();
	static ArrayList<Giocatore> giocatoriPieno3=new ArrayList<>();
	Exit query=new Exit(gReale);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		giocatoriPieno.add(gReale);
		giocatoriPieno.add(gioc);
		giocatoriPieno3.add(gReale);
		giocatoriPieno3.add(gioc);
		giocatoriPieno2.add(gioc);
		giocatoriPieno2.add(gReale);
		modello=new Partita(giocatoriPieno);
		modello1=new Partita(giocatoriPieno2);
		modello2=new Partita(giocatoriVuoto);
		modello3=new Partita(giocatoriPieno3);
		modello.startTurnoPartita();
		modello1.startTurnoPartita();
		modello3.startTurnoPartita();
		}

	@Test
	public void testPerformNoException() {
		try {
			assertEquals("Giocatore: Luca logout\n",query.perform(modello3));
		} catch (AzioneNotPossibleException e) {
			fail();
		}
		
	}
	
	@Test
	public void testPerformCatchException() {
		try {
			assertEquals("Giocatore: Daniele logout",query.perform(modello2));
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
		
	}

	

	@Test
	public void testSetGiocatoreOfflineSenzaGiocatori() {
		try {
			query.setGiocatoreOffline(modello2);
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testSetGiocatoreOfflineConGiocatore() throws AzioneNotPossibleException {
			Giocatore g1=query.setGiocatoreOffline(modello);
			assertFalse(g1.getIsOnline());
			assertTrue(modello.getGiocatoriOffline().contains(g1));
			assertTrue(!modello.getGiocatori().contains(query.getGiocatore()));
	}

	@Test
	public void testUpdateStatoPartitaSeGiocatoreCorrente() {
		query.updateStatoPartita(modello, gReale);
		State stato=modello.getStatoPartita();
		assertEquals(stato,new StateFine(stato.getGiocatoriMustDoAction(),gReale,new ArrayList<RicompensaSpecial>()));
	}
	
	@Test
	public void testUpdateStatoPartitaSeNONGiocatoreCorrente() {
		query.updateStatoPartita(modello1, gReale);
		State statoDopo=modello1.getStatoPartita();
		assertFalse(statoDopo.getGiocatoriMustDoAction().contains(gReale));
	}
	
	
	@Test
	public void testCostruttoreExit() {
		assertTrue(query!=null && query.getGiocatore().equals(gReale));
	}

}
