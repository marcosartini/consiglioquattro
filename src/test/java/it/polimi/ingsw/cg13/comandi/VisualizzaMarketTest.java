package it.polimi.ingsw.cg13.comandi;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class VisualizzaMarketTest {
	
	static Partita modello=Mockito.mock(Partita.class);
	static Partita modello2=Mockito.mock(Partita.class);
	static State statoMarket=Mockito.mock(State.class);
	static State statoGioco=Mockito.mock(State.class);
	static Giocatore gioc=Mockito.mock(Giocatore.class);
	static Market market=Mockito.mock(Market.class);
	VisualizzaMarket query;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Mockito.when(modello.getStatoPartita()).thenReturn(statoMarket);
		Mockito.when(modello2.getStatoPartita()).thenReturn(statoGioco);
		Mockito.when(statoMarket.getMarket()).thenReturn(market);
		Mockito.when(statoGioco.getMarket()).thenThrow(new AzioneNotPossibleException("Non e' turno market"));
		Mockito.when(market.toString()).thenReturn("market");
	}

	@Test
	public void testPerformTurnoMarket() {
		query=new VisualizzaMarket(gioc);
		try {
			assertEquals("market",query.perform(modello));
		} catch (AzioneNotPossibleException e) {
			fail();
		}
	}
	
	@Test
	public void testPerformVerificaEccezioneDiTurnoNonCorretto() {
		query=new VisualizzaMarket(gioc);
		try {
			assertEquals("market",query.perform(modello2));
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCostruttoreVisualizzaMarket() {
		query=new VisualizzaMarket(gioc);
		assertTrue(query!=null && query.getGiocatore().equals(gioc));
	}

}
