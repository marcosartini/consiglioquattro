package it.polimi.ingsw.cg13.comandi;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class PrintTest {

	static Partita modello=Mockito.mock(Partita.class);
	static Giocatore gioc=Mockito.mock(Giocatore.class);
	Print query;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Mockito.when(gioc.toString()).thenReturn("Daniele");
		Mockito.when(modello.toString()).thenReturn("StampaModello");
	}

	@Test
	public void testPerformPartitaNull() throws AzioneNotPossibleException {
		query=new Print(gioc);
		assertEquals("Non e' possibile stampare il modello\n",query.perform(null));
	}
	
	@Test
	public void testPerformPartitaNotNull() throws AzioneNotPossibleException {
		query=new Print(gioc);
		String result="\n::: STATO DEL MODELLO :::"+"StampaModello"+
				"\n### FINE STATO MODELLO ###\n"+"StatoGiocatore:\n Daniele\n";
		assertEquals(result,query.perform(modello));
	}

	@Test
	public void testCostruttorePrint() {
		query=new Print(gioc);
		assertTrue(query!=null && query.getGiocatore().equals(gioc));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePrintConGiocatoreNull() {
		query=new Print(null);
	}

	@Test
	public void testGetGiocatore() {
		query=new Print(gioc);
		assertEquals(query.getGiocatore(),gioc);
	}

}
