package it.polimi.ingsw.cg13.comandi;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class HelpTest {

	static Partita modello=Mockito.mock(Partita.class);
	static Giocatore gioc=Mockito.mock(Giocatore.class);
	Help query;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Mockito.when(gioc.toString()).thenReturn("Daniele");
	}

	@Test
	public void testPerform() throws AzioneNotPossibleException {
		query=new Help(gioc);
		String help="";
		help+="\nSintassi mosse:    --> nomeMossa parametro1 parametroMultiplo2 parametro3 ...";
		help+="ParametroMultiplo: --> valore1-valore2-... \n\n";
		help+="Elenco delle mosse giocatore:\n";
		help+="AcquistoPermesso regione numeroPermesso coloreCartaPolitica1-coloreCartaPolitica2-..\n";
		help+="CambioConsiglioVeloce regione coloreConsigliereDaInserire\n";
		help+="CambioConsiglio regione coloreConsigliereDaInserire\n";
		help+="CostruisciConRe citta1-...-cittaDoveCostruire coloreCartaPolitica1-cartaPolitica2-..\n";
		help+="CostruisciConRe cittaDelRe coloreCartaPolitica1-cartaPolitica2-..\n";
		help+="CostruisciConPermesso  numeroPermesso citta\n";
		help+="CambioPermessi regione\n";
		help+="AcquistoAiutante\n";
		help+="AcquistoAzionePrincipale\n";
		help+="\nElenco mosse di market:\n";
		help+="Vendo tipoVendibile vendibile prezzo\n";
		help+="Acquisto idInserzione\n";
		help+="PrintMarket\n";
		help+="\nAltri comandi:\n";
		help+="Exit   		 			--> esci dalla partita\n";
		help+="Help   		 			--> stampa l'help\n";
		help+="Print  		 			--> stampa stato della Partita\n";
		help+="FinisciTurno  			--> finisce il tuo turno corrente\n";
		help+="BonusInput parametro  	--> Inserisci il bonus da riusare\n"
				+ "Dove il parametro può essere un permesso o una citta a seconda della richiesta\n";
		assertEquals(help,query.perform(modello));
	}

	@Test
	public void testCostruttoreHelp() {
		query=new Help(gioc);
		assertTrue(query!=null && query.getGiocatore().equals(gioc));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreHelpConGiocatoreNull() {
		query=new Help(null);
	}


}
