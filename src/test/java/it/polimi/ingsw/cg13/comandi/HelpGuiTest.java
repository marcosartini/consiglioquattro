package it.polimi.ingsw.cg13.comandi;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class HelpGuiTest {

	Giocatore g1=Mockito.mock(Giocatore.class);;
	Partita part=Mockito.mock(Partita.class);;
	
	@Test
	public void testHelpAcquistoAiutante() {
		Mockito.when(g1.getNome()).thenReturn("Pippo");
		HelpGui help= new HelpGui(g1);
		String temp= "-AcquistoAiutante: Azione Veloce\npermette di aggiungere un nuovo aiutante a quelli che si possiedono al costo di 3 punti ricchezza\n";
		
		assertEquals(temp,help.helpAcquistoAiutante());
	}
	
	@Test
	public void testHelpAcquistoAzionePrincipale() {
		Mockito.when(g1.getNome()).thenReturn("Pippo");
		HelpGui help= new HelpGui(g1);
		String temp="-AcquistoAzionePrincipale: Azione Veloce\nPermette di comprare una nuova azione principale da eseguire  al costo di 3 aiutanti\n";
		temp+="Selezionare gli aiutanti da quelli in proprio possesso per eseguire l'azione\n";
		assertEquals(temp,help.helpAcquistoAzionePrincipale());
	}
	
	@Test
	public void testHelpAcquistoPermesso() {
		Mockito.when(g1.getNome()).thenReturn("Pippo");
		HelpGui help= new HelpGui(g1);
		String temp="-AcquistoPermesso: Azione Principale\nPermette di comprare un permesso di costruzione corrompendo il consiglio della Regione a cui appartiene\n";
		temp+="Selezionare il consiglio da corrompere e le carte politiche da utilizzare\n";
		assertEquals(temp,help.helpAcquistoPermesso());
	}

	@Test
	public void testHelpCambioConsiglio() {
		Mockito.when(g1.getNome()).thenReturn("Pippo");
		HelpGui help= new HelpGui(g1);
		String temp="-CambioConsiglio: Azione Principale\nPermette di cambiare un consigliere di un consiglio sulla mappa e ricevere 4 punti ricchezza \n";
		temp+="Selezionare il consiglio da cambiare e il relativo consigliere che si vuole inserire dal banco\n";
		assertEquals(temp,help.helpCambioConsiglio());
	}
	
	@Test
	public void testHelpCambioConsiglioVeloce() {
		Mockito.when(g1.getNome()).thenReturn("Pippo");
		HelpGui help= new HelpGui(g1);
		String temp="-CambioConsiglioVeloce: Azione Veloce\nPermette di cambiare un consigliere di un consiglio sulla mappa al costo di 1 aiutante \n";
		temp+="Selezionare il consiglio da cambiare, il relativo consigliere che si vuole inserire dal banco e i consiglieri da utilizzare\n";
		assertEquals(temp,help.helpCambioConsiglioVeloce());
	}
	
	@Test
	public void testHelpCambioPermessi() {
		Mockito.when(g1.getNome()).thenReturn("Pippo");
		HelpGui help= new HelpGui(g1);
		String temp="-CambioPermessi: Azione Veloce\nPermette di cambiare i permessi di costruzione scoperti di una regione al costo di 1 aiutante \n";
		temp+="Selezionare i consiglieri da utilizzare e il mazzo da cui prendere le carte\n";
		assertEquals(temp,help.helpCambioPermessi());
	}
	
	@Test
	public void testHelpCostruzioneConPermesso() {
		Mockito.when(g1.getNome()).thenReturn("Pippo");
		HelpGui help= new HelpGui(g1);
		String temp="-CostruzioneConPermesso: Azione Principale\nPermette di costruire un emporio utilizzando un permesso di cosrtuzione \n";
		temp+="Selezionare la città in cui si vuole costruire e il permesso che si vuole utilizzare\n";
		assertEquals(temp,help.helpCostruzioneConPermesso());
	}
	
	@Test
	public void testHelpCostruzioneConKing() {
		Mockito.when(g1.getNome()).thenReturn("Pippo");
		HelpGui help= new HelpGui(g1);
		String temp="-CostruzioneConKing: Azione Principale\nPermette di costruire un emporio corrompendo il consiglio del re e spostando il King sulla città prescelta\n";
		temp+="Selezionare le carte politiche da utilizzare per la corruzione, il consiglio del re e spostare il re sulla città desiderata\n";
		assertEquals(temp,help.helpCostruzioneConKing());
	}
	
	@Test
	public void testHelpPerform() throws AzioneNotPossibleException {
		Mockito.when(g1.getNome()).thenReturn("Pippo");
		HelpGui help= new HelpGui(g1);
		String temp="Durante il proprio turno ogni giocatore può effettuare un' Azione Principale e un' Azione Veloce\n";
		temp+= "-AcquistoAiutante: Azione Veloce\npermette di aggiungere un nuovo aiutante a quelli che si possiedono al costo di 3 punti ricchezza\n";
		temp+="-AcquistoAzionePrincipale: Azione Veloce\nPermette di comprare una nuova azione principale da eseguire  al costo di 3 aiutanti\n";
		temp+="Selezionare gli aiutanti da quelli in proprio possesso per eseguire l'azione\n";
		temp+="-AcquistoPermesso: Azione Principale\nPermette di comprare un permesso di costruzione corrompendo il consiglio della Regione a cui appartiene\n";
		temp+="Selezionare il consiglio da corrompere e le carte politiche da utilizzare\n";
		temp+="-CambioConsiglio: Azione Principale\nPermette di cambiare un consigliere di un consiglio sulla mappa e ricevere 4 punti ricchezza \n";
		temp+="Selezionare il consiglio da cambiare e il relativo consigliere che si vuole inserire dal banco\n";
		temp+="-CambioConsiglioVeloce: Azione Veloce\nPermette di cambiare un consigliere di un consiglio sulla mappa al costo di 1 aiutante \n";
		temp+="Selezionare il consiglio da cambiare, il relativo consigliere che si vuole inserire dal banco e i consiglieri da utilizzare\n";
		temp+="-CambioPermessi: Azione Veloce\nPermette di cambiare i permessi di costruzione scoperti di una regione al costo di 1 aiutante \n";
		temp+="Selezionare i consiglieri da utilizzare e il mazzo da cui prendere le carte\n";
		temp+="-CostruzioneConPermesso: Azione Principale\nPermette di costruire un emporio utilizzando un permesso di cosrtuzione \n";
		temp+="Selezionare la città in cui si vuole costruire e il permesso che si vuole utilizzare\n";
		temp+="-CostruzioneConKing: Azione Principale\nPermette di costruire un emporio corrompendo il consiglio del re e spostando il King sulla città prescelta\n";
		temp+="Selezionare le carte politiche da utilizzare per la corruzione, il consiglio del re e spostare il re sulla città desiderata\n";
		
		assertEquals(temp,help.perform(part));
	}
}
