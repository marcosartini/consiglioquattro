package it.polimi.ingsw.cg13.percorsi;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

public class PercorsoRicchezzaTest {

	PercorsoRicchezza percorso;
	PercorsoRicchezza percorso2;
	static CasellaRicchezza casella1=new CasellaRicchezza(10);
	static CasellaRicchezza casella2=new CasellaRicchezza(5);
	static ArrayList<CasellaRicchezza> caselleNULL=null;
	static ArrayList<CasellaRicchezza> caselleVuoto=new ArrayList<>();
	static ArrayList<CasellaRicchezza> casellePieno=new ArrayList<>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		casellePieno.add(casella1);
		casellePieno.add(casella2);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePercorsoRicchezzaListOfCasellaVuota() {
		percorso=new PercorsoRicchezza(caselleVuoto);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePercorsoRicchezzaListOfCasellaNull() {
		percorso=new PercorsoRicchezza(caselleNULL);
	}
	
	@Test
	public void testCostruttorePercorsoRicchezzaListOfCasellaPiena() {
		percorso=new PercorsoRicchezza(casellePieno);
		assertEquals(percorso.getPercorso(),casellePieno);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPercorsoRicchezzaDatoPercorsoNull() {
		percorso2=null;
		percorso=new PercorsoRicchezza(percorso2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPercorsoRicchezzaDatoPercorsoVuoto() {
		percorso2=new PercorsoRicchezza(caselleVuoto);
		percorso=new PercorsoRicchezza(percorso2);
	}
	
	@Test
	public void testPercorsoRicchezzaDatoPercorsoPieno() {
		percorso2=new PercorsoRicchezza(casellePieno);
		percorso=new PercorsoRicchezza(percorso2);
		assertEquals(percorso.getPercorso(),percorso2.getPercorso());
	}

	@Test
	public void testToStringPercorsoPieno() {
		percorso=new PercorsoRicchezza(casellePieno);
		assertEquals("PercorsoRicchezza\n[Casella punti:10, Casella punti:5]",percorso.toString());
	}
}
