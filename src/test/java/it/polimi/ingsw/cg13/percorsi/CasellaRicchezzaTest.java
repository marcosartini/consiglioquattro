package it.polimi.ingsw.cg13.percorsi;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CasellaRicchezzaTest {

	CasellaRicchezza casella;
	CasellaRicchezza casella2;
	static Giocatore giocatore1=Mockito.mock(Giocatore.class);
	static Giocatore giocatore2=Mockito.mock(Giocatore.class);
	static HashSet<Giocatore> giocatoriVuoto=new HashSet<>();
	static HashSet<Giocatore> giocatoriSoloUno=new HashSet<>();
	static HashSet<Giocatore> giocatoriPieno=new HashSet<>();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		giocatoriSoloUno.add(giocatore1);
		giocatoriPieno.add(giocatore1);
		giocatoriPieno.add(giocatore2);
		Mockito.when(giocatore1.getNome()).thenReturn("Daniele");
	}

	@Test
	public void testCostruttoreCasellaRicchezza() {
		casella=new CasellaRicchezza(10);
		assertTrue(casella.getPuntiCasella()==10 && casella.getGiocatoriPresenti().isEmpty());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCasellaRicchezzaValoreNegativo() {
		casella=new CasellaRicchezza(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCasellaRicchezzaNullo() {
		casella=new CasellaRicchezza(null);
	}

	@Test
	public void testGetPuntiCasella() {
		casella=new CasellaRicchezza(10);
		assertTrue(10==casella.getPuntiCasella());
	}

	@Test
	public void testGetGiocatoriPresentiNessuno() {
		casella=new CasellaRicchezza(10);
		assertTrue(casella.getGiocatoriPresenti().isEmpty());
	}
	
	@Test
	public void testGetGiocatoriPresentiQualcuno() {
		casella=new CasellaRicchezza(10);
		casella.setGiocatoriPresenti(giocatoriPieno);
		assertTrue(giocatoriPieno==casella.getGiocatoriPresenti());
	}

	@Test
	public void testSetGiocatoriPresenti() {
		casella=new CasellaRicchezza(10);
		casella.setGiocatoriPresenti(giocatoriPieno);
		assertEquals(giocatoriPieno,casella.getGiocatoriPresenti());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetGiocatoriPresentiNull() {
		casella=new CasellaRicchezza(10);
		casella.setGiocatoriPresenti(null);
	}


	@Test
	public void testRemoveGiocatore() {
		casella=new CasellaRicchezza(10);
		casella.setGiocatoriPresenti(giocatoriPieno);
		casella.removeGiocatore(giocatore2);
		assertTrue(!casella.getGiocatoriPresenti().contains(giocatore2));
	}
	
	@Test
	public void testRemoveGiocatoreNull() {
		casella=new CasellaRicchezza(10);
		casella.setGiocatoriPresenti(giocatoriPieno);
		casella.removeGiocatore(null);
		assertEquals(giocatoriPieno,casella.getGiocatoriPresenti());
	}
	
	@Test
	public void testRemoveGiocatoreNonPresente() {
		casella=new CasellaRicchezza(10);
		casella.setGiocatoriPresenti(giocatoriVuoto);
		casella.removeGiocatore(giocatore2);
		assertEquals(giocatoriVuoto,casella.getGiocatoriPresenti());
	}

	@Test
	public void testAddGiocatore() {
		casella=new CasellaRicchezza(10);
		casella.setGiocatoriPresenti(giocatoriSoloUno);
		casella.addGiocatore(giocatore2);
		assertEquals(giocatoriPieno,casella.getGiocatoriPresenti());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddGiocatoreNull() {
		casella=new CasellaRicchezza(10);
		casella.setGiocatoriPresenti(giocatoriSoloUno);
		casella.addGiocatore(null);
	}


	@Test
	public void testToStringSenzaGiocatori() {
		casella=new CasellaRicchezza(10);
		assertEquals("Casella punti:10",casella.toString());
	}
	
	@Test
	public void testToStringConGiocatori() {
		casella=new CasellaRicchezza(10);
		casella.addGiocatore(giocatore1);
		assertEquals("Casella punti:10, giocatoripresenti=(Daniele, )",casella.toString());
	}
	
	@Test
	public void testHashCodeUguale() {
		casella=new CasellaRicchezza(10);
		casella2=new CasellaRicchezza(10);
		assertTrue(casella.hashCode()==casella2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi() {
		casella=new CasellaRicchezza(10);
		casella2=new CasellaRicchezza(12);
		assertFalse(casella.hashCode()==casella2.hashCode());
	}

	@Test
	public void testEqualsCaselleUguali() {
		casella=new CasellaRicchezza(10);
		casella2=new CasellaRicchezza(10);
		assertTrue(casella.equals(casella2));
	}
	
	@Test
	public void testEqualsCaselleDiversi() {
		casella=new CasellaRicchezza(10);
		casella2=new CasellaRicchezza(12);
		assertFalse(casella.equals(casella2));
	}
	
	@Test
	public void testEqualsCaselleStessoRiferimento() {
		casella=new CasellaRicchezza(10);
		assertTrue(casella.equals(casella));
	}

}
