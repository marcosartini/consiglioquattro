package it.polimi.ingsw.cg13.percorsi;

import static org.junit.Assert.*;

import org.junit.Test;

public class CasellaVittoriaTest {

	CasellaVittoria casella;
	
	@Test
	public void testCostruttoreCasellaVittoria() {
		casella=new CasellaVittoria(20);
		assertTrue(casella.getPuntiCasella()==20 && casella.getGiocatoriPresenti().isEmpty());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCasellaVittoriaValoreNegativo() {
		casella=new CasellaVittoria(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCasellaVittoriaNullo() {
		casella=new CasellaVittoria(null);
	}

	
}
