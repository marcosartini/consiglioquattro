package it.polimi.ingsw.cg13.percorsi;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

public class PercorsoNobiltaTest {

	PercorsoNobilta percorso;
	PercorsoNobilta percorso2;
	static CasellaNobilta casella1=new CasellaNobilta(10);
	static CasellaNobilta casella2=new CasellaNobilta(5);
	static ArrayList<CasellaNobilta> caselleNULL=null;
	static ArrayList<CasellaNobilta> caselleVuoto=new ArrayList<>();
	static ArrayList<CasellaNobilta> casellePieno=new ArrayList<>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		casellePieno.add(casella1);
		casellePieno.add(casella2);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePercorsoNobiltaListOfCasellaVuota() {
		percorso=new PercorsoNobilta(caselleVuoto);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePercorsoNobiltaListOfCasellaNull() {
		percorso=new PercorsoNobilta(caselleNULL);
	}
	
	@Test
	public void testCostruttorePercorsoNobiltaaListOfCasellaPiena() {
		percorso=new PercorsoNobilta(casellePieno);
		assertEquals(percorso.getPercorso(),casellePieno);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPercorsoNobiltaDatoPercorsoNull() {
		percorso2=null;
		percorso=new PercorsoNobilta(percorso2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPercorsoNobiltaDatoPercorsoVuoto() {
		percorso2=new PercorsoNobilta(caselleVuoto);
		percorso=new PercorsoNobilta(percorso2);
	}
	
	@Test
	public void testPercorsoNobiltaDatoPercorsoPieno() {
		percorso2=new PercorsoNobilta(casellePieno);
		percorso=new PercorsoNobilta(percorso2);
		assertEquals(percorso.getPercorso(),percorso2.getPercorso());
	}

	@Test
	public void testToStringPercorsoPieno() {
		percorso=new PercorsoNobilta(casellePieno);
		assertEquals("PercorsoNobilta\n[Casella punti:10, Casella punti:5]",percorso.toString());
	}

}
