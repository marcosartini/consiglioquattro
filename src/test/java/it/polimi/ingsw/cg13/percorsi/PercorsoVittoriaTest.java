package it.polimi.ingsw.cg13.percorsi;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

public class PercorsoVittoriaTest {
	
	PercorsoVittoria percorso;
	PercorsoVittoria percorso2;
	static CasellaVittoria casella1=new CasellaVittoria(10);
	static CasellaVittoria casella2=new CasellaVittoria(5);
	static ArrayList<CasellaVittoria> caselleNULL=null;
	static ArrayList<CasellaVittoria> caselleVuoto=new ArrayList<>();
	static ArrayList<CasellaVittoria> casellePieno=new ArrayList<>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		casellePieno.add(casella1);
		casellePieno.add(casella2);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePercorsoVittoriaListOfCasellaVuota() {
		percorso=new PercorsoVittoria(caselleVuoto);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePercorsoVittoriaListOfCasellaNull() {
		percorso=new PercorsoVittoria(caselleNULL);
	}
	
	@Test
	public void testCostruttorePercorsoVittoriaListOfCasellaPiena() {
		percorso=new PercorsoVittoria(casellePieno);
		assertEquals(percorso.getPercorso(),casellePieno);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPercorsoVittoriaDatoPercorsoNull() {
		percorso2=null;
		percorso=new PercorsoVittoria(percorso2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPercorsoVittoriaDatoPercorsoVuoto() {
		percorso2=new PercorsoVittoria(caselleVuoto);
		percorso=new PercorsoVittoria(percorso2);
	}
	
	@Test
	public void testPercorsoVittoriaDatoPercorsoPieno() {
		percorso2=new PercorsoVittoria(casellePieno);
		percorso=new PercorsoVittoria(percorso2);
		assertEquals(percorso.getPercorso(),percorso2.getPercorso());
	}

	@Test
	public void testToStringPercorsoPieno() {
		percorso=new PercorsoVittoria(casellePieno);
		assertEquals("PercorsoVittoria\n[Casella punti:10, Casella punti:5]",percorso.toString());
	}
	

	@Test
	public void testGetNumCaselle() {
		percorso=new PercorsoVittoria(casellePieno);
		assertTrue(2==percorso.getNumCaselle());
	}

	@Test
	public void testGetPercorso() {
		percorso=new PercorsoVittoria(casellePieno);
		assertEquals(percorso.getPercorso(),casellePieno);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testGetCasellaNegativo() {
		percorso=new PercorsoVittoria(casellePieno);
		percorso.getCasella(-1);
	}
	
	@Test
	public void testGetCasellaNelPercorso() {
		percorso=new PercorsoVittoria(casellePieno);
		assertEquals(percorso.getCasella(0),casella1);
	}
	
	@Test
	public void testGetCasellaFuoriPercorso() {
		percorso=new PercorsoVittoria(casellePieno);
		assertEquals(percorso.getCasella(3),casella2);
	}

}
