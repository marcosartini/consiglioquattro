package it.polimi.ingsw.cg13.percorsi;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.PuntoRicchezza;
import it.polimi.ingsw.cg13.bonus.PuntoVittoria;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CasellaNobiltaTest {

	CasellaNobilta casella;
	static Ricompensa ricompensa1=new PuntoVittoria(1);
	static Ricompensa ricompensa2=new PuntoRicchezza(2);
	static HashSet<Ricompensa> ricompenseVuoto=new HashSet<>();
	static HashSet<Ricompensa> ricompenseUnicoElem=new HashSet<>();
	static HashSet<Ricompensa> ricompensePieno=new HashSet<>();
	static Giocatore g1=Mockito.mock(Giocatore.class);
	static Giocatore g2=Mockito.mock(Giocatore.class);
	static Partita partita=Mockito.mock(Partita.class);
	static HashSet<Giocatore> giocatoriVuoto=new HashSet<>();
	static HashSet<Giocatore> giocatoriSoloUno=new HashSet<>();
	static Giocatore gReale= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(10));
	static PercorsoVittoria percorso;
	static CasellaVittoria casella1=new CasellaVittoria(0);
	static CasellaVittoria casella2=new CasellaVittoria(1);
	static ArrayList<CasellaVittoria> casellePieno=new ArrayList<>();
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ricompenseUnicoElem.add(ricompensa1);
		ricompensePieno.add(ricompensa1);
		ricompensePieno.add(ricompensa2);
		giocatoriSoloUno.add(g1);
		casellePieno.add(casella1);
		casellePieno.add(casella2);
		percorso=new PercorsoVittoria(casellePieno);
		Mockito.when(g1.getVittoriaGiocatore()).thenReturn(10);
		Mockito.when(partita.getPercorsoVittoria()).thenReturn(percorso);
		
	}

	@Test
	public void testCostruttoreCasellaNobilta() {
		casella=new CasellaNobilta(10);
		assertTrue(casella.getPuntiCasella()==10 && casella.getGiocatoriPresenti().isEmpty() 
				&& casella.getRicompense().isEmpty());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCasellaNobiltaValoreNegativo() {
		casella=new CasellaNobilta(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCasellaNobiltaNullo() {
		casella=new CasellaNobilta(null);
	}
	
	
	@Test
	public void testGetRicompenseVuoto(){
		casella=new CasellaNobilta(5);
		assertEquals(ricompenseVuoto,casella.getRicompense());
	}
	
	@Test
	public void testAddRicompensaUno(){
		casella=new CasellaNobilta(5);
		casella.addRicompensa(ricompensa1);
		assertEquals(ricompenseUnicoElem,casella.getRicompense());
	}
	
	@Test
	public void testAddRicompensaDue() {
		casella=new CasellaNobilta(5);
		casella.addRicompensa(ricompensa1);
		casella.addRicompensa(ricompensa2);
		assertEquals(ricompensePieno,casella.getRicompense());
	}
	
	@Test
	public void testAddGiocatore() {
		casella=new CasellaNobilta(10);
		casella.addGiocatore(g1);
		assertEquals(giocatoriSoloUno,casella.getGiocatoriPresenti());
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddGiocatoreNull() {
		casella=new CasellaNobilta(10);
		casella.setGiocatoriPresenti(giocatoriSoloUno);
		casella.addGiocatore(null);
	}
	
	@Test
	public void testAddGiocatoreConPartita() {
		casella=new CasellaNobilta(10);
		casella.addGiocatore(g1,partita);
		assertEquals(giocatoriSoloUno,casella.getGiocatoriPresenti());
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddGiocatoreNullConPartita() {
		casella=new CasellaNobilta(10);
		casella.setGiocatoriPresenti(giocatoriSoloUno);
		casella.addGiocatore(null,partita);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddGiocatoreNullConPartitaNull() {
		casella=new CasellaNobilta(10);
		casella.setGiocatoriPresenti(giocatoriSoloUno);
		casella.addGiocatore(null,null);
	}

	@Test
	public void testToStringSenzaRicompense() {
		casella=new CasellaNobilta(10);
		assertEquals("Casella punti:10",casella.toString());
	}
	
	@Test
	public void testToStringConRicompense() {
		casella=new CasellaNobilta(10);
		casella.addRicompensa(ricompensa1);
		assertEquals("Casella punti:10, ricompense=[PuntoVittoria (1)]",casella.toString());
	}
	
	


	@Test
	public void testAssegnaRicompense() {
		casella=new CasellaNobilta(10);
		casella.addRicompensa(ricompensa1);
		casella.assegnaRicompense(gReale, partita);
		assertTrue(1==gReale.getVittoriaGiocatore());
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddRicompenseThrowEXC() {
		casella=new CasellaNobilta(10);
		casella.addRicompensa(null);		
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAssegnaRicompenseThrowEXCGiocatore() {
		casella=new CasellaNobilta(10);		
		casella.assegnaRicompense(null, partita);		
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAssegnaRicompenseThrowEXCPartita() {
		casella=new CasellaNobilta(10);		
		casella.assegnaRicompense(gReale, null);		
		
	}

}
