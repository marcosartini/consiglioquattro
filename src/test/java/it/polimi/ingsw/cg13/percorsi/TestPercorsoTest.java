package it.polimi.ingsw.cg13.percorsi;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import org.junit.Test;

public class TestPercorsoTest {
	
	 Percorso<CasellaRicchezza> percorso;
	 CasellaRicchezza cas0 = new CasellaRicchezza(0);
	 CasellaRicchezza cas1 = new CasellaRicchezza(1);
	 CasellaRicchezza cas2 = new CasellaRicchezza(2);
	
	@Before
	public void setUpBefore(){
		List<CasellaRicchezza> caselle = new ArrayList<>();
		caselle.add(cas0);
		caselle.add(cas1);
		caselle.add(cas2);

		percorso = new PercorsoRicchezza(caselle);
	}

	@Test
	public void testGetCasellaShouldReturnTheRequestedCasellaOrTheLastIfTheParameterIsGreaterThanThePercorso() {
		assertTrue(percorso.getCasella(0).equals(cas0));
		assertTrue(percorso.getCasella(2).equals(cas2));
		assertTrue(percorso.getCasella(5).equals(cas2));
		assertFalse(percorso.getCasella(6).equals(cas1));
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetCasellaShouoldThrowIndexOutOfBoundExceptionIfParameterIsBelowZero(){
		percorso.getCasella(-1);		
	}
	

}
