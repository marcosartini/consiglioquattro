package it.polimi.ingsw.cg13.azionimarket;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoInizio;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class AcquistaInserzioneTest {

	AcquistaInserzione azione;
	
	static Market market=new Market();
	static StateAcquistoInizio statoIA;
	
	static Inserzione inserzione,inserzione2;
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	static Partita modello;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		giocatori.add(g1);
		giocatori.add(g2);
		
		inserzione=new Inserzione(new CasellaRicchezza(2),g1,new Aiutante());
		inserzione2=new Inserzione(new CasellaRicchezza(4),g1,new Aiutante());
		market.getBanchetto().add(inserzione);
		market.getBanchetto().add(inserzione2);
		statoIA=new StateAcquistoInizio(giocatori,g1,market);
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		modello.setStatoPartita(statoIA);
	}

	@Test
	public void testEseguiAzione() throws AzioneNotPossibleException {
		azione=new AcquistaInserzione(g1,inserzione);
		int nIns=market.getBanchetto().size();
		assertTrue(azione.eseguiAzione(modello));
		assertTrue(nIns-1==market.getBanchetto().size());
	}

	@Test
	public void testUpdateParametri() throws CdQException {
		azione=new AcquistaInserzione(g1,inserzione);
		azione.updateParametri(modello);
	}

	@Test
	public void testAcquistaInserzione() {
		azione=new AcquistaInserzione(g1,inserzione);
		assertTrue(azione!=null && azione.getGiocatore().equals(g1) 
				&& azione.getInserizione().getIdInserzione()==inserzione.getIdInserzione());
	}

	@Test
	public void testVisit() throws AzioneNotPossibleException {
		azione=new AcquistaInserzione(g1,inserzione);
		assertEquals(statoIA,azione.visit(statoIA, modello));
	}

}
