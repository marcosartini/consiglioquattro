package it.polimi.ingsw.cg13.mappa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.AiutanteAggiuntivo;
import it.polimi.ingsw.cg13.bonus.PuntoRicchezza;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.eccezioni.IllegalPercorsoException;
import it.polimi.ingsw.cg13.file.FactoryOggetti;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class NodoCIttaTest {
	
	@Test
	public void testBfsRicompense() {
		//preparazione
		Giocatore giocatore = new Giocatore("Gino",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Ricompensa aiutante = new AiutanteAggiuntivo();
		Ricompensa ricchezza = new PuntoRicchezza(5);
		Ricompensa ricchezza2 = new PuntoRicchezza(6);
		Set<Ricompensa> ric1 = new HashSet<>();
		ric1.add(ricchezza);
		Citta alba = new Citta(new Colore("pippo",123, 123, 123), "Alba", ric1 , new HashSet<>());
		
		Set<Ricompensa> ric2 = new HashSet<>();
		ric2.add(ricchezza2);
		ric2.add(aiutante);
		
		Citta scura = new Citta(new Colore("pluto",245, 78, 12), "Scura", ric2, new HashSet<>());
		
		NodoCitta nodoAlba = new NodoCitta(alba);
		NodoCitta nodoScura = new NodoCitta(scura);
		
		nodoAlba.addVicino(nodoScura);
		nodoScura.addVicino(nodoAlba);
		
		List<Ricompensa> ricEmpo;
		Emporio emporioA = new Emporio(giocatore);
		emporioA.setCittaPosizionato(alba);
		alba.costruisciEmporio(emporioA);
			
		//esecuzione
		ricEmpo=nodoAlba.bfsRicompense(giocatore);
		
		assertTrue(ricEmpo.contains(ricchezza));
		assertFalse(ricEmpo.contains(aiutante));
		
		
		List<Ricompensa> ricEmpo2;
		Emporio emporioB = new Emporio(giocatore);
		emporioB.setCittaPosizionato(scura);
		scura.costruisciEmporio(emporioB);
		ricEmpo2=nodoScura.bfsRicompense(giocatore);
		
		assertTrue(ricEmpo2.containsAll(ric2));
		assertTrue(ricEmpo2.contains(ricchezza));
		
		List<NodoCitta> nodiVisitati = new LinkedList<>();
		nodiVisitati.add(nodoScura);
		
		nodoScura.bfsRicompensa(giocatore, nodiVisitati);
		
		assertTrue(ricEmpo2.containsAll(ric2));
		assertTrue(ricEmpo2.contains(ricchezza));
	}
	
	@Test
	public void testInitNodoOK(){		
		Citta temp = new Citta(new Colore("rosso",255, 0, 0), "NewYork", new HashSet<>() , new HashSet<>());
		NodoCitta nodo1= new NodoCitta(temp);
		
		assertEquals("NewYork",nodo1.getCittaNodo().getNome());
		assertTrue(nodo1.getCittaVicine().isEmpty());
		
		Set<NodoCitta> mockNear= Mockito.mock(Set.class);
		Mockito.when(mockNear.isEmpty()).thenReturn(false);
		NodoCitta nodo2= new NodoCitta(temp,mockNear);
		assertEquals("NewYork",nodo2.getCittaNodo().getNome());
		assertTrue(!nodo2.getCittaVicine().isEmpty());
	}
	
	@Test
	public void testAddVicinoOK(){		
		Citta temp = new Citta(new Colore("rosso",255, 0, 0), "NewYork", new HashSet<>() , new HashSet<>());
		NodoCitta nodo1= new NodoCitta(temp);
		
		assertEquals("NewYork",nodo1.getCittaNodo().getNome());
		assertTrue(nodo1.getCittaVicine().isEmpty());
		
		Citta tempA = new Citta(new Colore("rosso",255, 0, 0), "Londra", new HashSet<>() , new HashSet<>());
		NodoCitta nodoA= new NodoCitta(tempA);
		
		nodo1.addVicino(nodoA);
		assertTrue(!nodo1.getCittaVicine().isEmpty());
		
	}

	@Test(expected=IllegalArgumentException.class)
	public void testAddVicinoThrowException(){		
		Citta temp = new Citta(new Colore("rosso",255, 0, 0), "NewYork", new HashSet<>() , new HashSet<>());
		NodoCitta nodo1= new NodoCitta(temp);
		
		nodo1.addVicino(null);		
	}
	
	@Test
	public void testSetCittaNodoOK(){		
		Citta temp = new Citta(new Colore("rosso",255, 0, 0), "NewYork", new HashSet<>() , new HashSet<>());
		NodoCitta nodo1= new NodoCitta(temp);
		
		assertEquals("NewYork",nodo1.getCittaNodo().getNome());
		Citta tempA = new Citta(new Colore("rosso",255, 0, 0), "Londra", new HashSet<>() , new HashSet<>());
		
		nodo1.setCittaNodo(tempA);
		assertEquals("Londra",nodo1.getCittaNodo().getNome());
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetCittaNodoThrowException(){		
		Citta temp = new Citta(new Colore("rosso",255, 0, 0), "NewYork", new HashSet<>() , new HashSet<>());
		NodoCitta nodo1= new NodoCitta(temp);
		
		nodo1.setCittaNodo(null);		
	}
	
	@Test
	public void testIsCittaVicinaTrue(){		
		Citta temp = new Citta(new Colore("rosso",255, 0, 0), "NewYork", new HashSet<>() , new HashSet<>());
		NodoCitta nodo1= new NodoCitta(temp);
		
		Citta tempA = new Citta(new Colore("rosso",255, 0, 0), "Londra", new HashSet<>() , new HashSet<>());
		NodoCitta nodoA= new NodoCitta(tempA);
		
		nodo1.addVicino(nodoA);
		assertTrue(nodo1.isCittaVicina(nodoA));
		
	}
	
	@Test
	public void testIsCittaVicinaFalse(){		
		Citta temp = new Citta(new Colore("rosso",255, 0, 0), "NewYork", new HashSet<>() , new HashSet<>());
		NodoCitta nodo1= new NodoCitta(temp);
		
		Citta tempA = new Citta(new Colore("rosso",255, 0, 0), "Londra", new HashSet<>() , new HashSet<>());
		NodoCitta nodoA= new NodoCitta(tempA);
		
		assertFalse(nodo1.isCittaVicina(nodoA));
		
	}
	
	@Test
	public void testIsCittaVicinaFalseExc(){		
		Citta temp = new Citta(new Colore("rosso",255, 0, 0), "NewYork", new HashSet<>() , new HashSet<>());
		NodoCitta nodo1= new NodoCitta(temp);		
		
		assertFalse(nodo1.isCittaVicina(null));
		
	}
	
	@Test
	public void testGetCittaVicineOK(){		
		Citta temp = new Citta(new Colore("rosso",255, 0, 0), "NewYork", new HashSet<>() , new HashSet<>());
		NodoCitta nodo1= new NodoCitta(temp);
		
		Citta tempA = new Citta(new Colore("rosso",255, 0, 0), "Londra", new HashSet<>() , new HashSet<>());
		NodoCitta nodoA= new NodoCitta(tempA);
		
		nodo1.addVicino(nodoA);
		assertEquals(1,nodo1.getCittaVicine().size());
		
	}
	
	@Test
	public void testPercorsoCorrettoTrue() throws FileSyntaxException{		
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		List<NodoCitta> grafo=fact.getCittaMappa();
		
		NodoCitta start=grafo.get(0);
		
		List<NodoCitta> percorso= new ArrayList<>();
		percorso.add(grafo.get(1));
		percorso.add(grafo.get(4));
		percorso.add(grafo.get(7));
		
		assertTrue(start.percorsoCorretto(percorso));
	}
	
	@Test
	public void testPercorsoCorrettoFalse() throws FileSyntaxException{		
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		List<NodoCitta> grafo=fact.getCittaMappa();
		
		NodoCitta start=grafo.get(0);
		
		List<NodoCitta> percorso= new ArrayList<>();
		percorso.add(grafo.get(1));
		percorso.add(grafo.get(4));
		percorso.add(grafo.get(14));
		
		assertFalse(start.percorsoCorretto(percorso));
	}
	
	@Test
	public void testPercorsoCorrettoFalseExc() throws FileSyntaxException{		
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		List<NodoCitta> grafo=fact.getCittaMappa();
		
		NodoCitta start=grafo.get(0);
		
		assertFalse(start.percorsoCorretto(null));
	}
	
	@Test
	public void testCostoPercorsoOK() throws FileSyntaxException, IllegalPercorsoException{		
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		List<NodoCitta> grafo=fact.getCittaMappa();
		
		NodoCitta start=grafo.get(0);
		
		List<NodoCitta> percorso= new ArrayList<>();
		percorso.add(grafo.get(1));
		percorso.add(grafo.get(4));
		percorso.add(grafo.get(7));
		
		assertEquals(6,start.costoPercorso(fact.getCostoStrade(), percorso));
	}
	
	@Test(expected=IllegalPercorsoException.class)
	public void testCostoPercorsoThrowException() throws FileSyntaxException, IllegalPercorsoException{		
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		List<NodoCitta> grafo=fact.getCittaMappa();
		
		NodoCitta start=grafo.get(0);
		
		List<NodoCitta> percorso= new ArrayList<>();
		percorso.add(grafo.get(1));
		percorso.add(grafo.get(4));
		percorso.add(grafo.get(14));
		
		start.costoPercorso(fact.getCostoStrade(), percorso);
	}
	
	@Test
	public void testToString() throws FileSyntaxException, IllegalPercorsoException{		
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		List<NodoCitta> grafo=fact.getCittaMappa();
		
		NodoCitta start=grafo.get(0);
		
		assertEquals("Citta nome=Arkon, ciano, ricompense=[],\nempori=[],\nCittaVicine=Burgen, Castrum, ",start.toString());		
		
	}
}
