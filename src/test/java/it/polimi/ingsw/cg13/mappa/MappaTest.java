package it.polimi.ingsw.cg13.mappa;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.file.FactoryOggetti;

public class MappaTest {

	Mappa map;
	List<NodoCitta> mockGrafo=Mockito.mock(List.class);
	Set<Regione> mockRegioni=Mockito.mock(Set.class);
	
	@Test
	public void testInitOK() {
		try {
			map=new Mappa(mockGrafo,mockRegioni);
			Mappa map2;
			FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
			map2= new Mappa(fact);
			
			map= new Mappa(map2);
			assertEquals(map2,map);
		} catch (FileSyntaxException e) {			
			e.printStackTrace();
		}		
	}
	
	@Test
	public void testGetGrafoOK() {
		Mockito.when(mockGrafo.size()).thenReturn(5);
		map=new Mappa(mockGrafo,mockRegioni);
		
		assertEquals(5,map.getGrafo().size());			
	}
	
	@Test
	public void testGetRegioniOK() {
		Mockito.when(mockRegioni.size()).thenReturn(3);
		map=new Mappa(mockGrafo,mockRegioni);
		
		assertEquals(3,map.getRegioni().size());			
	}
	
	
	@Test
	public void testGetNodoCittaOK() throws FileSyntaxException, CittaNotFoundException {
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		map=new Mappa(fact);
		
		NodoCitta search=fact.getCittaMappa().get(7);
		assertEquals(search,map.getNodoCitta(search.getCittaNodo()));
		
		assertEquals(search,map.getNodoCitta(search.getCittaNodo().getNome()));
	}
	
	@Test(expected=CittaNotFoundException.class)
	public void testGetNodoCittaThrowExcCity() throws FileSyntaxException, CittaNotFoundException {
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		map=new Mappa(fact);
		
		NodoCitta search=fact.getCittaMappa().get(7);
		assertEquals(search,map.getNodoCitta(new Citta(Mockito.mock(Colore.class),"Milano",new HashSet<>(),new HashSet<>())));		
		
	}
	
	@Test(expected=CittaNotFoundException.class)
	public void testGetNodoCittaThrowExcString() throws FileSyntaxException, CittaNotFoundException {
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		map=new Mappa(fact);
		
		NodoCitta search=fact.getCittaMappa().get(7);		
		
		assertEquals(search,map.getNodoCitta("Milano"));
	}

	@Test
	public void testGetRegioneOK() throws FileSyntaxException, RegioneNotFoundException {
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		map=new Mappa(fact);
		
		Iterator<Regione> iter=fact.getRegioni().iterator();
		while(iter.hasNext()){
			Regione temp=iter.next();
			assertEquals(temp,map.getRegione(temp));
			assertEquals(temp,map.getRegione(temp.getNome()));
		}
		
	}
	
	@Test(expected=RegioneNotFoundException.class)
	public void testGetRegioneThrowExcReg() throws FileSyntaxException, RegioneNotFoundException {
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		map=new Mappa(fact);
		
		map.getRegione(new Regione("Lombardia",Mockito.mock(Set.class),Mockito.mock(ConsiglioRegione.class),Mockito.mock(MazzoPermessi.class)));	
		
	}
	
	@Test(expected=RegioneNotFoundException.class)
	public void testGetRegioneThrowExcString() throws FileSyntaxException, RegioneNotFoundException {
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		map=new Mappa(fact);
		
		map.getRegione("Calabria");	
		
	}
	
	@Test
	public void testRegistraCittaOK() throws FileSyntaxException{
		FactoryOggetti fact= new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		map=new Mappa(fact);
		
		map.registraCitta();
		
	}
}
