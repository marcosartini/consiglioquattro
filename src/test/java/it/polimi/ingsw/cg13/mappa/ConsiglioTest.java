package it.polimi.ingsw.cg13.mappa;

import static org.junit.Assert.*;

import java.util.ArrayDeque;
import java.util.Queue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

public class ConsiglioTest {

	static ProprietarioConsiglio mockProp= Mockito.mock(ProprietarioConsiglio.class);
	static Consigliere cons1=new Consigliere(new Colore("verde",0,255,0));
	static Consigliere cons2= new Consigliere(new Colore("blu",0,0,255));
	static Consigliere cons3= new Consigliere(new Colore("giallo",255,255,0));
	static Consigliere cons4= new Consigliere(new Colore("viola",255,0,255));
	static Queue<Consigliere> codaTemp;	
	
	
	
	public static void setUpCoda(){
		codaTemp=new ArrayDeque<Consigliere>();
		codaTemp.add(cons1);
		codaTemp.add(cons2);
		codaTemp.add(cons3);
		codaTemp.add(cons4);
		
	}
	
	@Test
	public void testInitOk() {
		this.setUpCoda();
		Consiglio consiglio1= new Consiglio(codaTemp,mockProp);
		
		ConsiglioRegione consiglio2=new ConsiglioRegione(codaTemp,mockProp);
		
		ConsiglioRe consiglio3=new ConsiglioRe(codaTemp,mockProp);
	}
	
	@Test
	public void testSostituzioneConsigliereOk() {
		this.setUpCoda();
		Consiglio consiglio= new Consiglio(codaTemp,mockProp);
		
		Consigliere consRet=consiglio.sostituzioneConsigliere(cons3);		
		assertEquals("verde",consRet.getColor().getNome());
	}
	
	@Test
	public void testSostituzioneConsigliereRegioneOk() {
		this.setUpCoda();
		ConsiglioRegione consiglio= new ConsiglioRegione(codaTemp,mockProp);
		
		Consigliere consRet=consiglio.sostituzioneConsigliere(cons3);		
		assertEquals("verde",consRet.getColor().getNome());
	}
	
	@Test
	public void testSostituzioneConsigliereReOk() {
		this.setUpCoda();
		ConsiglioRe consiglio= new ConsiglioRe(codaTemp,mockProp);
		
		Consigliere consRet=consiglio.sostituzioneConsigliere(cons3);		
		assertEquals("verde",consRet.getColor().getNome());
	}
	
	@Test(expected= IllegalArgumentException.class)
	public void testSostituzioneConsigliereThrowException() {
		Consiglio consiglio= new Consiglio(codaTemp,mockProp);
		this.setUpCoda();
		Consigliere consRet=consiglio.sostituzioneConsigliere(null);		
		
	}
	
	@Test
	public void testGetBalconeOk() {
		this.setUpCoda();
		Consiglio consiglio= new Consiglio(codaTemp,mockProp);
		
		assertEquals(codaTemp,consiglio.getBalcone());
	}
	
	@Test
	public void testSetBalconeOk() {
		this.setUpCoda();
		Queue<Consigliere> mockCoda=Mockito.mock(Queue.class);
		Mockito.when(mockCoda.size()).thenReturn(4);
		Consiglio consiglio= new Consiglio(mockCoda,mockProp);
		
		
		assertEquals(4,consiglio.getBalcone().size());
		
		consiglio.setConsiglieri(codaTemp);
		
		assertEquals(codaTemp,consiglio.getBalcone());		
	}
	
	/*@Test  --> Da Sistemare
	public void testCopiaConsiglioOk() {
		this.setUpCoda();		
		Consiglio consiglio1= new Consiglio(codaTemp,mockProp);
		
		Consiglio consiglio2=consiglio1.copiaConsiglio();
		
		assertEquals(consiglio1.getBalcone(),consiglio2.getBalcone());		
	}*/

	
	@Test
	public void testProprietarioOk() {
		
		Mockito.when(mockProp.getNome()).thenReturn("Mario");
		Consiglio consiglio= new Consiglio(codaTemp,mockProp);	
		
		assertEquals("Mario",consiglio.getProprietario().getNome());
		
		ProprietarioConsiglio mockProp2= Mockito.mock(ProprietarioConsiglio.class);
		Mockito.when(mockProp2.getNome()).thenReturn("Luigi");
		
		consiglio.setProprietario(mockProp2);
		assertEquals("Luigi",consiglio.getProprietario().getNome());		
	}
	
	@Test
	public void testToStringOk() {
		this.setUpCoda();
		Mockito.when(mockProp.getNome()).thenReturn("Mario");
		ConsiglioRe consiglio= new ConsiglioRe(codaTemp,mockProp);	
		assertEquals("ConsiglioRe={([Consigliere verde, Consigliere blu, Consigliere giallo, Consigliere viola])}",consiglio.toString());
		ConsiglioRegione cons= new ConsiglioRegione(codaTemp,mockProp);
		assertEquals("ConsiglioRegione={([Consigliere verde, Consigliere blu, Consigliere giallo, Consigliere viola])}",cons.toString());
	}
}
