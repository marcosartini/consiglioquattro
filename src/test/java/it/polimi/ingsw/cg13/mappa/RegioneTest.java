package it.polimi.ingsw.cg13.mappa;

import static org.junit.Assert.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

public class RegioneTest {
	Colore mockCol=Mockito.mock(Colore.class);
	Set<Citta> mockCity=Mockito.mock(Set.class);
	ConsiglioRegione mockCons=Mockito.mock(ConsiglioRegione.class);
	MazzoPermessi mockMazzo=Mockito.mock(MazzoPermessi.class);	
	
	@Test
	public void testInitRegOK() {
		Regione reg= new Regione("Tundra",mockCity,mockCons,mockMazzo);
		Regione temp=new Regione(reg);
		
		assertEquals("Tundra",temp.getNome());
	}
	
	@Test
	public void testGetCittaOK() {
		Regione reg= new Regione("Tundra",mockCity,mockCons,mockMazzo);
		Mockito.when(mockCity.size()).thenReturn(5);
		
		assertEquals(5,reg.getCitta().size());
	}
	
	@Test
	public void testGetNomeOK() {
		Regione reg= new Regione("Tundra",mockCity,mockCons,mockMazzo);
		assertEquals("Tundra",reg.getNome());
	}
	
	@Test
	public void testGetConsiglioOK() {
		Regione reg= new Regione("Tundra",mockCity,mockCons,mockMazzo);
		Queue<Consigliere> tempCons =new ArrayDeque<Consigliere>();
		for(int i=0;i<4;i++)
			tempCons.add(new Consigliere(mockCol));		
		
		Mockito.when(mockCons.getBalcone()).thenReturn(tempCons);
		assertEquals(4,reg.getConsiglio().getBalcone().size());
	}
	
	@Test
	public void testSetConsiglioOK() {
		Regione reg= new Regione("Tundra",mockCity,mockCons,mockMazzo);
		Queue<Consigliere> tempCons =new ArrayDeque<Consigliere>();
		for(int i=0;i<4;i++)
			tempCons.add(new Consigliere(mockCol));		
		
		ConsiglioRegione cons= new ConsiglioRegione(tempCons,Mockito.mock(ProprietarioConsiglio.class));
		reg.setConsiglio(cons);
		
		assertEquals(4,reg.getConsiglio().getBalcone().size());
	}

	@Test
	public void testGetMazzoPermessoOK() {
		Regione reg= new Regione("Tundra",mockCity,mockCons,mockMazzo);
		Mockito.when(mockMazzo.sizeMazzo()).thenReturn(20);
		assertEquals(20,reg.getMazzoPermesso().sizeMazzo());
	}
	
	@Test
	public void testSetMazzoPermessoOK() {
		Regione reg= new Regione("Tundra",mockCity,mockCons,mockMazzo);
		MazzoPermessi tempMazzo=Mockito.mock(MazzoPermessi.class);		
		Mockito.when(tempMazzo.sizeMazzo()).thenReturn(30);
		
		Mockito.when(mockMazzo.sizeMazzo()).thenReturn(20);
		assertEquals(20,reg.getMazzoPermesso().sizeMazzo());
		
		reg.setMazzoPermesso(tempMazzo);
		assertEquals(30,reg.getMazzoPermesso().sizeMazzo());
	}
	
	@Test
	public void testGetStringPermessiDisponibiliOK(){
		Regione reg= new Regione("Tundra",mockCity,mockCons,mockMazzo);
		List<PermessoCostruzione> tempPerm= new ArrayList<>();
		for(int i=0;i<5;i++){
			PermessoCostruzione mockPerm=Mockito.mock(PermessoCostruzione.class);
			Mockito.when(mockPerm.toString()).thenReturn("Carta n°"+i);
			tempPerm.add(mockPerm);			
		}
		
		MazzoPermessi deck= new MazzoPermessi(tempPerm,1);
		reg.setMazzoPermesso(deck);
		
		reg.getStringPermessiDisponibili(2);
		assertEquals(5,reg.getMazzoPermesso().sizeMazzo());
		
	}
	
	@Test
	public void testToStringOK(){
		mockCity= new HashSet<>();
		Citta temp=Mockito.mock(Citta.class);
		Mockito.when(temp.getNome()).thenReturn("citta0");
		mockCity.add(temp);
		
		Mockito.when(mockCons.toString()).thenReturn("consiglio");
		Mockito.when(mockMazzo.toString()).thenReturn("mazzo");
		Regione reg= new Regione("Tundra",mockCity,mockCons,mockMazzo);
		
		assertEquals("Regione nome: Tundra\nconsiglio\nmazzo\nCittaAppartenenti:\ncitta0, ",reg.toString());
		
	}
}
