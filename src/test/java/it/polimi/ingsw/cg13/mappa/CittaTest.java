package it.polimi.ingsw.cg13.mappa;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.AiutanteAggiuntivo;
import it.polimi.ingsw.cg13.bonus.CartaPoliticaAggiuntiva;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CittaTest {
	
	Citta cityR= new Citta(new Colore("rosso",255,0,0),"Catania",new HashSet<>(),new HashSet<>());
	Giocatore g1= new Giocatore("Mario",new Colore("verde",0,255,0),1,10);
	Giocatore g2= new Giocatore("Luigi",new Colore("blu",0,0,255),1,10);
	
	@Test
	public void testGetNomeOK() {
		assertEquals("Catania",cityR.getNome());
	}
	
	@Test
	public void testCostruisciEmporioOK() {
		Emporio e= new Emporio(g1);
		cityR.costruisciEmporio(e);
		
		assertEquals(1,cityR.getEmpori().size());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruisciEmporioThrowException() {
		Emporio e= new Emporio(g1);
		cityR.costruisciEmporio(null);			
	}

	@Test
	public void testHasEmporioTrue() {
		Emporio e= new Emporio(g1);		
		cityR.costruisciEmporio(e);
		assertTrue(cityR.hasEmporio(g1));
	}
	
	@Test
	public void testHasEmporioFalse() {
		Emporio e= new Emporio(g1);		
		cityR.costruisciEmporio(e);
		assertFalse(cityR.hasEmporio(g2));
	}
	
	@Test
	public void testHasEmporioFalseEXC() {
		assertFalse(cityR.hasEmporio(null));
	}
	
	@Test
	public void testGetColorOK() {
		assertEquals("rosso",cityR.getColor().getNome());
	}
	
	@Test
	public void testRicompense() {
		assertEquals(0,cityR.getRicompense().size());
		
		Set<Ricompensa> mockRich=Mockito.mock(Set.class);
		Mockito.when(mockRich.size()).thenReturn(5);
		cityR.setRicompense(mockRich);
		
		assertEquals(5,cityR.getRicompense().size());
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetRicompenseThrowEXC() {		
		cityR.setRicompense(null);		
		
	}
	
	@Test
	public void testAssegnaBonusInput() throws FileSyntaxException {		
		Set<Ricompensa> rew= new HashSet<>();
		rew.add(new AiutanteAggiuntivo());
		rew.add(new CartaPoliticaAggiuntiva());
		Partita game= new Partita();
		
		cityR.setRicompense(rew);
		cityR.assegnaBonusInput(g1, game);
		assertEquals(1,g1.getAiutanti().size());
		assertEquals(1,g1.getCartePolitiche().size());
		
		
	}
}
