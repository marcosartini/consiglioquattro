package it.polimi.ingsw.cg13.punteggio;

import static org.junit.Assert.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.TesseraBonus;
import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class PunteggioTest {

	Punteggio punteggio,punteggio2;
	static Giocatore giocatore1=Mockito.mock(Giocatore.class);
	static Giocatore giocatore2=Mockito.mock(Giocatore.class);
	static Giocatore giocatore3=Mockito.mock(Giocatore.class);
	static TesseraBonus tessera1=Mockito.mock(TesseraBonus.class);
	static TesseraBonus tessera2=Mockito.mock(TesseraBonus.class);
	static HashSet<TesseraBonus> tessereVuote=new HashSet<TesseraBonus>();
	static HashSet<TesseraBonus> tesserePiene=new HashSet<TesseraBonus>();
	static ArrayList<Aiutante> aiutantiVuoti=new ArrayList<Aiutante>();
	static ArrayList<Aiutante> aiutantiPresenti=new ArrayList<Aiutante>();
	static Emporio emporio=Mockito.mock(Emporio.class);
	static ArrayDeque<Emporio> emporiVuoti=new ArrayDeque<Emporio>();
	static ArrayDeque<Emporio> emporiPresenti=new ArrayDeque<Emporio>();
	static CartaPolitica cartaPol=Mockito.mock(CartaPolitica.class);
	static ArrayList<CartaPolitica> cartePoliticheVuote=new ArrayList<CartaPolitica>();
	static ArrayList<CartaPolitica> cartePolitichePresenti=new ArrayList<CartaPolitica>();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		tesserePiene.add(tessera1);
		tesserePiene.add(tessera2);
		emporiPresenti.add(emporio);
		aiutantiPresenti.add(new Aiutante());
		cartePolitichePresenti.add(cartaPol);
		cartePolitichePresenti.add(cartaPol);
		//Giocatore1
		Mockito.when(giocatore1.getNome()).thenReturn("Daniele");
		Mockito.when(giocatore1.getVittoriaGiocatore()).thenReturn(12);
		Mockito.when(giocatore1.getEmpori()).thenReturn(emporiVuoti);
		Mockito.when(giocatore1.getAiutanti()).thenReturn(aiutantiVuoti);
		Mockito.when(giocatore1.getCartePolitiche()).thenReturn(cartePoliticheVuote);
		Mockito.when(giocatore1.getTessereBonus()).thenReturn(tessereVuote);
		//Giocatore2
		Mockito.when(giocatore2.getNome()).thenReturn("Marco");
		Mockito.when(giocatore2.getVittoriaGiocatore()).thenReturn(15);
		Mockito.when(giocatore2.getEmpori()).thenReturn(emporiVuoti);
		Mockito.when(giocatore2.getAiutanti()).thenReturn(aiutantiPresenti);
		Mockito.when(giocatore2.getCartePolitiche()).thenReturn(cartePolitichePresenti);
		Mockito.when(giocatore2.getTessereBonus()).thenReturn(tesserePiene);
		Mockito.when(tessera1.getPunteggioCaselleVittoria()).thenReturn(10);
		Mockito.when(tessera2.getPunteggioCaselleVittoria()).thenReturn(20);
		//Giocatore3
		Mockito.when(giocatore3.getNome()).thenReturn("Lori");
		Mockito.when(giocatore3.getVittoriaGiocatore()).thenReturn(10);
		Mockito.when(giocatore3.getEmpori()).thenReturn(emporiPresenti);
		Mockito.when(giocatore3.getAiutanti()).thenReturn(aiutantiVuoti);
		Mockito.when(giocatore3.getCartePolitiche()).thenReturn(cartePoliticheVuote);
		Mockito.when(giocatore3.getTessereBonus()).thenReturn(tesserePiene);
		Mockito.when(tessera1.getPunteggioCaselleVittoria()).thenReturn(10);
		Mockito.when(tessera2.getPunteggioCaselleVittoria()).thenReturn(20);
	}

	@Test
	public void testCostuttorePunteggio() {
		punteggio=new Punteggio(giocatore1);
		assertTrue(giocatore1.equals(punteggio.getGiocatore()) && 15==punteggio.getPunteggio());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostuttorePunteggioConGiocatoreNullo() {
		punteggio=new Punteggio(null);
	}

	@Test
	public void testAssegnaPuntiConNessunOggetto() {
		punteggio=new Punteggio(giocatore1);
		assertEquals(15,punteggio.getPunteggio());
		}
	
	@Test
	public void testAssegnaPuntiConTessereSenzaEmporio() {
		punteggio=new Punteggio(giocatore2);
		assertEquals(48,punteggio.getPunteggio());
		}
	
	@Test
	public void testAssegnaPuntiConTessereConEmporio() {
		punteggio=new Punteggio(giocatore3);
		assertEquals(40,punteggio.getPunteggio());
		}

	@Test
	public void testAddPunti() {
		punteggio=new Punteggio(giocatore1);
		punteggio.addPunti(5);
		assertEquals(20,punteggio.getPunteggio());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testAddPuntiNegativi() {
		punteggio=new Punteggio(giocatore1);
		punteggio.addPunti(-5);
	}
	
	@Test
	public void testGetPunteggio() {
		punteggio=new Punteggio(giocatore1);
		assertEquals(15,punteggio.getPunteggio());
	}

	@Test
	public void testSetPunteggio() {
		punteggio=new Punteggio(giocatore2);
		punteggio.setPunteggio(20);
		assertEquals(20,punteggio.getPunteggio());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetPunteggioNegativi() {
		punteggio=new Punteggio(giocatore2);
		punteggio.setPunteggio(-20);
	}

	@Test
	public void testGetGiocatore() {
		punteggio=new Punteggio(giocatore1);
		assertEquals(giocatore1,punteggio.getGiocatore());
	}

	@Test
	public void testAiutantiECarteUgualeAZero() {
		punteggio=new Punteggio(giocatore1);
		assertEquals(0,punteggio.aiutantiECarte());
	}
	
	@Test
	public void testAiutantiECarteUgualeValore(){
		punteggio=new Punteggio(giocatore2);
		assertEquals(3,punteggio.aiutantiECarte());
	}

	@Test
	public void testToString() {
		punteggio=new Punteggio(giocatore1);
		assertEquals("Giocatore: Daniele, punteggio: 15",punteggio.toString());
	}
	
	@Test
	public void testEqualsDiPunteggiDiversi(){
		punteggio=new Punteggio(giocatore1);
		punteggio2=new Punteggio(giocatore2);
		assertFalse(punteggio.equals(punteggio2));
	}
	
	@Test
	public void testEqualsDiPunteggiUguali(){
		punteggio=new Punteggio(giocatore1);
		punteggio2=new Punteggio(giocatore1);
		assertTrue(punteggio.equals(punteggio2));
	}
	
	@Test 
	public void testHashCodeStessiOggetti(){
		punteggio=new Punteggio(giocatore1);
		punteggio2=punteggio;
		assertTrue(punteggio.hashCode()==punteggio2.hashCode());
	}
	
	@Test 
	public void testHashCodeDiversiOggetti(){
		punteggio=new Punteggio(giocatore1);
		punteggio2=new Punteggio(giocatore2);
		assertFalse(punteggio.hashCode()==punteggio2.hashCode());
	}

}
