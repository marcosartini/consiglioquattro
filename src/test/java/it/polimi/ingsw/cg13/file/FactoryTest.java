package it.polimi.ingsw.cg13.file;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;

public class FactoryTest {

	FactoryOggetti fact;
	//= new FactoryOggetti("src/main/resources/CdQoriginalConfig.xml");
	
	@Test(expected=NullPointerException.class)
	public void testInitEXC() throws FileSyntaxException {		
		fact=new FactoryOggetti("");
		
	}
	
	@Test
	public void testGetFileReaderOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(FileReader.class,fact.getFileReader().getClass());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetNumeroMaxGiocatoriOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(4,fact.getNumeroMaxGiocatori());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetCostoStradeOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(2,fact.getCostoStrade());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetCostiAggiuntiviPerAzioniOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(4,fact.getCostiAggiuntiviPerAzioni().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetNumeroConsiglieriOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(4,fact.getNumeroConsiglieri());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetNumeroEmporiOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(10,fact.getNumeroEmpori());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetReOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals("Juvelar",fact.getRe().getPosizione().getCittaNodo().getNome());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetMappaPartitaNULL() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(null,fact.getMappaPartita());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetCittaMappaOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(15,fact.getCittaMappa().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetGiocatoriOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertTrue(fact.getGiocatori().isEmpty());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetTessereBonusRegioneOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(3,fact.getTessereBonusRegione().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}

	
	@Test
	public void testGetPercorsoNobiltaOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(21,fact.getPercorsoNobilta().getNumCaselle());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetPercorsoRicchezzaOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(21,fact.getPercorsoRicchezza().getNumCaselle());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetPercorsoVittoriaOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(100,fact.getPercorsoVittoria().getNumCaselle());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetConsiglieriOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertTrue(fact.getConsiglieri().isEmpty());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAiutantiOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(30,fact.getAiutanti().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetMazzoPoliticoOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(90,fact.getMazzoPolitico().sizeMazzo());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetGruppoBonusReOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(5,fact.getGruppoBonusRe().getGruppo().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetRegioniOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(3,fact.getRegioni().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetGiocatoriMustDoActionNull() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(null,fact.getGiocatoriMustDoAction());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetGiocatoreCorrenteNull() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(null,fact.getGiocatoreCorrente());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetColoriPoliticiOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(6,fact.getColoriPolitici().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetColoriGiocatoriOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(4,fact.getColoriGiocatori().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetTessereBonusCittaOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(4,fact.getTessereBonusCitta().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetColoriCittaOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(5,fact.getColoriCitta().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetGettoniOK() {
		try {
			fact=new FactoryOggetti("src/main/resources/CdQConfig2.xml");
			assertEquals(14,fact.getGettoni().size());
			
		} 
		catch (FileSyntaxException e) {			
			e.printStackTrace();
		}
	}
}
