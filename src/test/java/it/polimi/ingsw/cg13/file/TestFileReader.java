package it.polimi.ingsw.cg13.file;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.bonus.TesseraBonusGruppoCitta;
import it.polimi.ingsw.cg13.bonus.TesseraBonusRe;
import it.polimi.ingsw.cg13.bonus.TesseraBonusRegione;
import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.MazzoPolitico;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.percorsi.PercorsoNobilta;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.King;

public class TestFileReader {

	@Test
	public void testReadColorShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Colore> colori=file.readColor();
		assertEquals(colori.size(),10);
		boolean contiene=file.readColor().containsAll(colori);
		assertTrue(contiene);
	}
	
	@Test
	public void testGetNumMaxPlayerShuldBe10() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		int player=file.getNumMaxPlayer();
		assertEquals(player,10);		
	}
	
	
	
	@Test
	public void testGetVictoryPathShuldBe40() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		PercorsoVittoria winPath=file.getVictoryPath();
		assertEquals(winPath.getNumCaselle(),41);		
	}
	
	
	@Test
	public void testGetHelpersNumShuldBe30() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Aiutante>aiutanti=file.getHelpersNum();
		assertEquals(aiutanti.size(),30);		
	}
	
	@Test
	public void testGetCartePoliticaShuldBe50() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		MazzoPolitico deck=file.getCartePolitica();
		assertEquals(deck.getMazzoCarte().size(),50);		
	}	
	
	@Test
	public void testGetCityShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		
		List<String> controllare = new ArrayList<String>(Arrays.asList(new String[]{"Milano","Venezia","Napoli","Roma","Firenze","Ancona"}));
		
		for(Citta c:citta){
			assertTrue(controllare.contains(c.getNome()));
		}
		
	}
	
	@Test
	public void testGetCittaShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		Citta city=file.getCitta("Milano", citta);
		
		assertNotNull(city);
	}
	
	@Test
	public void testGetCittaShuldBeKo() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		Citta city=file.getCitta("Torino", citta);
		
		assertNull(city);
	}
	
	@Test
	public void testGetCittaColorateShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		
		boolean temp=false;
		
		if(!file.getCittaColorate(citta, new Colore("verde",0,255,0)).isEmpty())
			temp=true;
		
		assertTrue(temp);
		
	}
	
	@Test
	public void testGetCittaColorateShuldBeKo() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		
		boolean temp=false;
		
		if(!file.getCittaColorate(citta, new Colore("rosa",255,192,203)).isEmpty())
			temp=true;
		
		assertFalse(temp);
		
	}
	
	@Test
	public void testConvertiCittaShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		
		List<NodoCitta> controlla= file.convertiCitta(citta);
		
		assertEquals(citta.size(),controlla.size());
	}
	
	/*@Test
	public void testGetLinkACittaShuldBeKo() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		
		List<NodoCitta> controlla= file.convertiCitta(citta);
		
		file.getLinkACitta(controlla);
		
	
	}*/
	
	@Test
	public void testGetNodoCittaShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		
		List<NodoCitta> controlla= file.convertiCitta(citta);
		
		assertEquals(file.getNodoCitta("Milano", controlla).getCittaNodo().getNome(),"Milano");
	
	}
	
	@Test
	public void testGetKingShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		
		List<NodoCitta> controlla= file.convertiCitta(citta);
		
		King re=file.getKing(controlla);
		assertEquals(re.getPosizione().getCittaNodo().getNome(),"Roma");
	
	}
	
	@Test
	public void testGetNobilityPathShuldBe6() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		PercorsoNobilta nobPath=file.getNobilityPath();
		
		assertEquals(nobPath.getNumCaselle(),6);
	
	}
	
	@Test
	public void testGetBonusReShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<TesseraBonusRe> kingBonus=file.getBonusRe();
		int temp=0;
		boolean ok=true;
		
		for(TesseraBonusRe t:kingBonus){
			if(t.getPunteggioCaselleVittoria()!=kingBonus.get(temp).getPunteggioCaselleVittoria())
				ok=false;
			temp++;
		}
		assertTrue(ok);	
	
	}
	
	@Test
	public void testGetBonusCittaShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		List<TesseraBonusGruppoCitta> tessere=file.getBonusCitta(citta);	
		assertEquals(tessere.size(),3);
		
		for(TesseraBonusGruppoCitta t:tessere)
		{			
			if(t.getColore().getNome()=="rosso"){
				assertEquals(t.getPunteggioCaselleVittoria(),10);
			}
			if(t.getColore().getNome()=="verde"){
				assertEquals(t.getPunteggioCaselleVittoria(),12);
			}
			if(t.getColore().getNome()=="blu"){
				assertEquals(t.getPunteggioCaselleVittoria(),7);
			}
		}	
	}
	
	@Test
	public void testGetRegioniShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		List<Regione> regioni;
		try {
			regioni = file.getRegioni(citta);
			assertEquals(regioni.size(),3);	
		} catch (FileSyntaxException e) {			
			e.printStackTrace();
		}			
	}
	
	@Test
	public void testGetRegioniThrowException() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Citta> citta=file.getCity();
		List<Regione> regioni;
		Set<Ricompensa> mockRic= Mockito.mock(Set.class);
		Set<Emporio> mockEmp= Mockito.mock(Set.class);
		citta.add(new Citta(new Colore("rosso",255,0,0),"Lisbona",mockRic,mockEmp ));
		
		boolean thr=false;
		
		try {
			regioni = file.getRegioni(citta);
			assertEquals(regioni.size(),3);	
		} catch (FileSyntaxException e) {			
			thr=true;
		}
		
		assertTrue(thr);
	}
	
	@Test
	public void testGetBonusShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Set<Ricompensa>> bonus= file.getBonus();
			
		assertEquals(bonus.get(0).size(),2);
		assertEquals(bonus.get(1).size(),3);
		assertEquals(bonus.get(2).size(),2);
			
	}
	
	@Test
	public void testGetRichPathShuldBe21() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		PercorsoRicchezza richPath= file.getRichPath();
			
		assertEquals(21,richPath.getNumCaselle());
			
	}
	
	@Test
	public void testGetCittaShuldBeMilano() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		Citta city= file.getCitta("Milano", file.getCity());
			
		assertEquals("Milano",city.getNome());
		assertEquals("rosso",city.getColor().getNome());
			
	}
	
	@Test
	public void testGetRegioneShuldBeMontagna() throws RegioneNotFoundException {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Regione> temp;
		try {
			temp = file.getRegioni(file.getCity());
			Set<Regione> tempReg= new HashSet<>();
			for(Regione r:temp)
				tempReg.add(r);
			Regione region= file.getRegione("montagna", tempReg);
			assertEquals("montagna",region.getNome());		
		} catch (FileSyntaxException e) {			
			e.printStackTrace();
		}	
			
	}
	
	@Test
	public void testGetRegioneThrowException() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		boolean thr=false;
		
		List<Regione> temp;
		try {
			temp = file.getRegioni(file.getCity());
			Set<Regione> tempReg= new HashSet<>();
			for(Regione r:temp)
				tempReg.add(r);
			try{
				file.getRegione("pianura", tempReg);
			}
			catch(RegioneNotFoundException e){
				thr=true;
			}
				
			assertTrue(thr);
		} catch (FileSyntaxException e1) {			
			e1.printStackTrace();
		}
				
			
	}
	
	@Test
	public void testGetBonusRegioniShuldBeOk() throws FileSyntaxException {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Regione> temp=file.getRegioni(file.getCity());
		Set<Regione> tempReg= new HashSet<>();
		for(Regione r:temp)	tempReg.add(r);
		
		List<TesseraBonusRegione> bonus=file.getBonusRegione(tempReg);
		assertEquals(3,bonus.size());	
		
		for(TesseraBonusRegione t:bonus){
			if("mare".equals(t.getRegione().getNome())) assertEquals(10,t.getPunteggioCaselleVittoria());
			if("montagna".equals(t.getRegione().getNome())) assertEquals(5,t.getPunteggioCaselleVittoria());
			if("collina".equals(t.getRegione().getNome())) assertEquals(15,t.getPunteggioCaselleVittoria());
		}
	}
	
	@Test
	public void testGetCartePermessoRegioneShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		//test Mare
		MazzoPermessi mazzo=file.getCartePermessoRegione("mare",file.getCity());
		
		assertEquals(5,mazzo.sizeMazzo());
		
		//test Montagna
		mazzo=file.getCartePermessoRegione("montagna",file.getCity());
		
		assertEquals(2,mazzo.sizeMazzo());
		
		//test Collina
		mazzo=file.getCartePermessoRegione("collina",file.getCity());
		
		assertEquals(2,mazzo.sizeMazzo());
	}
	
	@Test
	public void testGetCostoLinkShuldBe2() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		int costo=file.getCostoLink();
		
		assertEquals(2,costo);
	}
	
	@Test
	public void testGetCostiAggiuntiviShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Integer> costi=file.getCostoAggiuntivo();
		int[] temp={0,4,7,10};
		
		for(int i=0;i<costi.size();i++)
			assertEquals(temp[i],costi.get(i).intValue());
	}
	
	@Test
	public void testGetNumeroEmporiShuldBe2() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		int num=file.getNumeroEmpori();
		
		assertEquals(2,num);
	}
	
	@Test
	public void testGetNumeroConsiglieriShuldBe10() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		int num=file.getNumeroConsiglieri();
		
		assertEquals(10,num);
	}
	
	@Test
	public void testGetColoreConsiglieriShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Colore> color=file.getColoreConsiglieri();
		
		assertEquals("rosso",color.get(0).getNome());
		assertEquals("verde",color.get(1).getNome());
		assertEquals("blu",color.get(2).getNome());
		assertEquals("nero",color.get(3).getNome());
		
	}
	
	@Test
	public void testGetColoreCittaShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<Colore> color=file.getColoriCitta();
		
		assertEquals("rosso",color.get(0).getNome());
		assertEquals("verde",color.get(1).getNome());
		assertEquals("blu",color.get(2).getNome());
		
		
	}
	
	@Test
	public void testCreateNodiCittaShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<NodoCitta> cities=file.createNodiCitta();
		assertEquals(6,cities.size());
		
	}
	
	@Test
	public void testGetCityLinkShuldBeOk() {
		String pathName="src/main/resources/CdQTestFile.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		List<NodoCitta> cities=file.createNodiCitta();
		assertEquals(6,cities.size());
		
	}
	
	@Test
	public void testGetCittaColorateOK() {
		String pathName="src/main/resources/CdQConfig0.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);
		
		Colore ciano= new Colore("ciano",0,255,255);
		
		List<Citta> tempCitta=file.getCittaColorate(ciano, file.getCity());
		assertEquals(2,tempCitta.size());
		
	}
	
	@Test
	public void testGetCittaColorateEmpty() {
		String pathName="src/main/resources/CdQConfig0.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);		
		
		Colore verde= new Colore("verde",0,255,0);
		
		List<Citta> tempCitta=file.getCittaColorate(verde, file.getCity());
		assertEquals(0,tempCitta.size());
		
		tempCitta=file.getCittaColorate(null, file.getCity());
		assertEquals(0,tempCitta.size());		
		
	}
	
	@Test
	public void testGetCittaReturnNull() {
		String pathName="src/main/resources/CdQConfig0.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);		
		
		assertNull(file.getCitta("Milano",null));
		assertNull(file.getCitta("Milano",new ArrayList<>()));				
		
	}
	
	@Test
	public void testGetCittaColorateReturnNull() {
		String pathName="src/main/resources/CdQConfig0.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);		
		Colore ciano= new Colore("ciano",0,255,255);
		
		assertEquals(0,file.getCittaColorate(ciano,null).size());
		assertEquals(0,file.getCittaColorate(ciano,new ArrayList<>()).size());				
		
	}
	
	@Test
	public void testGetCittaColorateSetReturnNull() {
		String pathName="src/main/resources/CdQConfig0.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);		
		Colore ciano= new Colore("ciano",0,255,255);
		
		assertEquals(0,file.getCittaColorate(null,ciano).size());
		assertEquals(0,file.getCittaColorate(new ArrayList<>(),ciano).size());				
		
	}
	
	@Test
	public void testGetCityLinkReturnNull() {
		String pathName="src/main/resources/CdQConfig0.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);		
		
		assertEquals(0,file.getCityLink(null).size());
		assertEquals(0,file.getCityLink(new ArrayList<>()).size());				
		
	}
	
	@Test
	public void testGetNodoCittaReturnNull() {
		String pathName="src/main/resources/CdQConfig0.xml";
		String path= FileSystems.getDefault().getPath(pathName).toString();
		FileReader file=new FileReader(path);		
		
		assertNull(file.getNodoCitta("", file.getCityLink(file.getCity())));
		assertNull(file.getNodoCitta(null, file.getCityLink(file.getCity())));
		assertNull(file.getNodoCitta("Hellar", null));
		assertNull(file.getNodoCitta("Hellar", new ArrayList<>()));				
		
	}
}
