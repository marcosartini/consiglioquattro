package it.polimi.ingsw.cg13.personaggi;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;

public class AiutantiTest {

	@Test
	public void testAcquistaDaMarketShuldBeOk() {
		Aiutante help= new Aiutante();
		Giocatore g1= new Giocatore("Mario",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		help.acquistaDaMarket(g1);
		assertEquals(g1.getAiutanti().size(),11);
	}
	
	@Test
	public void testRimuoviDaGiocatoreShuldBeOk() {
		Aiutante help= new Aiutante();
		Giocatore g1= new Giocatore("Mario",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		help.rimuoviDaGiocatore(g1);
		assertEquals(g1.getAiutanti().size(),9);
	}
	
	@Test
	public void testHashCodeOk() {
		Aiutante help= new Aiutante();		
		assertEquals(35,help.hashCode());
	}

	@Test
	public void testRiassegnaShuldBeOk() {
		Aiutante help= new Aiutante();
		Giocatore g1= new Giocatore("Mario",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		help.riassegnaAlProprietario(g1);
		assertEquals(11,g1.getAiutanti().size());
	}
}
