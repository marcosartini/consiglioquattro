package it.polimi.ingsw.cg13.personaggi;

import static org.junit.Assert.*;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.MazzoPolitico;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.ConsiglioRe;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.ProprietarioConsiglio;


public class KingTest {

	@Test
	public void testGetSetPosizioneShouldBeOk() {
		NodoCitta temp= new NodoCitta(new Citta(new Colore("rosso",255,0,0),"Taranto",new HashSet<>(),new HashSet<>()));
		Queue<Consigliere> tempQueue= new ArrayDeque<>();
		
		tempQueue.add(new Consigliere(new Colore("rosso",255,0,0)));
		tempQueue.add(new Consigliere(new Colore("verde",0,255,0)));
		tempQueue.add(new Consigliere(new Colore("blu",0,0,255)));
		tempQueue.add(new Consigliere(new Colore("rosso",255,0,0)));
			

		ProprietarioConsiglio tempProp= Mockito.mock(ProprietarioConsiglio.class);		
		ConsiglioRe cons= new ConsiglioRe(tempQueue,tempProp);				
		
		King re= new King(temp,cons);
		assertEquals("Taranto",re.getPosizione().getCittaNodo().getNome());
		
		re.setPosizione(new NodoCitta(new Citta(new Colore("rosso",255,0,0),"Londra",new HashSet<>(),new HashSet<>())));
		assertEquals("Londra",re.getPosizione().getCittaNodo().getNome());
	}
	
	@Test
	public void testSetPosizioneThrowException() {
		NodoCitta temp= new NodoCitta(new Citta(new Colore("rosso",255,0,0),"Taranto",new HashSet<>(),new HashSet<>()));
		Queue<Consigliere> tempQueue= new ArrayDeque<>();
		
		tempQueue.add(new Consigliere(new Colore("rosso",255,0,0)));
		tempQueue.add(new Consigliere(new Colore("verde",0,255,0)));
		tempQueue.add(new Consigliere(new Colore("blu",0,0,255)));
		tempQueue.add(new Consigliere(new Colore("rosso",255,0,0)));
			

		ProprietarioConsiglio tempProp= Mockito.mock(ProprietarioConsiglio.class);		
		ConsiglioRe cons= new ConsiglioRe(tempQueue,tempProp);				
		boolean thr=false;
		
		try{
			King re= new King(temp,cons);
			re.setPosizione(null);
		}
		catch(IllegalArgumentException e){
			thr=true;
		}
		assertTrue(thr);
	}
	
	@Test
	public void testInitParamShouldBeOk() {
		NodoCitta temp= Mockito.mock(NodoCitta.class);
		ConsiglioRe cons= Mockito.mock(ConsiglioRe.class);		
		
		King re= new King(temp,cons);
	}
	
	@Test
	public void testInitParamThrowException() {
		boolean thr=false;
		NodoCitta temp= null;
		ConsiglioRe cons= Mockito.mock(ConsiglioRe.class);		
		try{
			King re= new King(temp,cons);
		}
		catch(IllegalArgumentException e){
			thr=true;
		}
		assertTrue(thr);
	}
	
	@Test
	public void testInitKingShouldBeOk() {
		King temp= Mockito.mock(King.class);			
		
		King re= new King(temp);
	}
	
	@Test
	public void testInitKingThrowException() {
		King temp= null;			
		boolean thr=false;		
		
		try{
			King re= new King(temp);
		}
		catch(IllegalArgumentException e){
			thr=true;
		}
		assertTrue(thr);
	}
	
	
	
	@Test
	public void testGetSetConsiglioKingShouldBeOk() {
		NodoCitta temp= new NodoCitta(new Citta(new Colore("rosso",255,0,0),"Taranto",new HashSet<>(),new HashSet<>()));
		Queue<Consigliere> tempQueue= new ArrayDeque<>();
		
		tempQueue.add(new Consigliere(new Colore("rosso",255,0,0)));
		tempQueue.add(new Consigliere(new Colore("verde",0,255,0)));
		tempQueue.add(new Consigliere(new Colore("blu",0,0,255)));
		tempQueue.add(new Consigliere(new Colore("rosso",255,0,0)));		
		

		ProprietarioConsiglio tempProp= Mockito.mock(ProprietarioConsiglio.class);		
		ConsiglioRe cons= new ConsiglioRe(tempQueue,tempProp);		
		
		ConsiglioRe mockCons=Mockito.mock(ConsiglioRe.class);
		
		King re= new King(temp,mockCons);
		re.setConsiglioKing(cons);
		
		assertEquals(re.getConsiglio().getBalcone().size(),4);		
	}
	
/*	@Test //magari se cambiamo le cose modifichiamo anche i test neh! .......
	public void testGetSetConsiglioKingThrowException() {
		NodoCitta temp= new NodoCitta(new Citta(new Colore("rosso",255,0,0),"Taranto",new HashSet<>(),new HashSet<>()));
			
		boolean thr=false;

		ProprietarioConsiglio tempProp= Mockito.mock(ProprietarioConsiglio.class);			
		
		ConsiglioRe mockCons=Mockito.mock(ConsiglioRe.class);
		try{
			King re= new King(temp,mockCons);
			re.setConsiglioKing(null);
		}
		catch(IllegalArgumentException e){
			thr=true;
		}
		assertTrue(thr);
	}
*/
}
