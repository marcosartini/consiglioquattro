package it.polimi.ingsw.cg13.personaggi;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;

public class ConsigliereTest {

	@Test(expected=IllegalArgumentException.class)
	public void testInitThrowEXC() {
		Consigliere cons= new Consigliere(null);
		
	}
	
	@Test
	public void testGetColorShouldBeRed() {
		Consigliere cons;
		
		cons = new Consigliere(new Colore("Rosso",255,0,0));
		assertEquals(cons.getColor().getNome(),"Rosso");
		assertEquals(cons.getColor().getRed(),255);
		assertEquals(cons.getColor().getBlue(),0);
		assertEquals(cons.getColor().getGreen(),0);
		
		
		
	}
	
	@Test
	public void testToStringShouldBeOk() {
		Consigliere cons;
		
		cons = new Consigliere(new Colore("Rosso",255,0,0));
		assertEquals(cons.toString(),"Consigliere Rosso");
			
		
	}
	
	@Test
	public void testEqualsShouldBeOk() {
		Consigliere cons1;
		
		cons1 = new Consigliere(new Colore("Rosso",255,0,0));
		Consigliere cons2= new Consigliere(new Colore("Rosso",255,0,0));
		assertTrue(cons1.equals(cons2));
		
		
	}
	@Test
	public void testEqualsShouldBeKo() {
		Consigliere cons1;
		
		cons1 = new Consigliere(new Colore("Rosso",255,0,0));
		Consigliere cons2= new Consigliere(new Colore("Verde",0,255,0));
		assertFalse(cons1.equals(cons2));
		
		
		
	}
	@Test
	public void testEqualsThrowException() {
		Consigliere cons1;
		boolean thrown=false;
		Consigliere cons2= null;
		try {
			cons1 = new Consigliere(new Colore("Rosso",255,0,0));		
			cons1.equals(cons2);
		} 	
		catch(IllegalArgumentException e){
			thrown=true;
		}
		assertTrue(thrown);
		
	}
	

}
