package it.polimi.ingsw.cg13.personaggi;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.CartaPoliticaJolly;
import it.polimi.ingsw.cg13.carte.CartaPoliticaSemplice;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.eccezioni.PermessoNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;



public class GiocatoreTest {

	@Test
	public void testInitGiocatoreParamShouldBeOk() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore("lorenzo",1,mockCol, 10, 10,mockWin,mockNob,mockRich);
		
		assertEquals(g1.getNome(),"lorenzo");
		assertEquals(g1.getNobiltaGiocatore(),2);
		assertEquals(g1.getRicchezzaGiocatore(),3);
		assertEquals(g1.getVittoriaGiocatore(),1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreParamthrowExcNomeNull() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore(null,1,mockCol, 10, 10,mockWin,mockNob,mockRich);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreParamthrowExcNumeroNeg() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore("lorenzo",-1,mockCol, 10, 10,mockWin,mockNob,mockRich);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreParamthrowExcEmporiNeg() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore("lorenzo",1,mockCol, -1, 10,mockWin,mockNob,mockRich);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreParamthrowExcAiutantiNeg() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore("lorenzo",1,mockCol, 10, -1,mockWin,mockNob,mockRich);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreParamthrowExcNomeEmpty() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore("",1,mockCol, 10, 10,mockWin,mockNob,mockRich);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreParamthrowExcVittoriaNull() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore("Lorenzo",1,mockCol, 10, 10,null,mockNob,mockRich);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreParamthrowExcRicchezzaNull() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore("Lorenzo",1,mockCol, 10, 10,mockWin,mockNob,null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreParamthrowExcNobiltaNull() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore("Lorenzo",1,mockCol, 10, 10,mockWin,null,mockRich);
	}
	
	
	@Test
	public void testInitGiocatoreParamThrowExceptionColoreNull() {
		
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);		
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);	
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);		
		boolean thr=false;
		try{
			Giocatore g1= new Giocatore("lorenzo",1,null, 1, 10,mockWin,mockNob,mockRich);
		}
		catch(IllegalArgumentException e){
			thr=true;
		}
		assertTrue(thr);
	}
	
	@Test
	public void testInitGiocatoreRedShouldBeOk() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockCol, 10, 10);
		
		assertEquals(g1.getNome(),"lorenzo");		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreRedThrowExcNomeNull() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");	
		
		Giocatore g1= new Giocatore(null,mockCol, 1, 10);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreRedThrowExcNomeEmpty() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		
		
		Giocatore g1= new Giocatore("",mockCol, 1, 10);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreRedThrowExcNumNeg() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		
		
		Giocatore g1= new Giocatore("Lorenzo",mockCol, -1, 10);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitGiocatoreRedThrowExcEmporiNeg() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		
		
		Giocatore g1= new Giocatore("Lorenzo",mockCol, 1, -10);
		
	}
	
	@Test
	public void testInitGiocatoreRedThrowExcColoreNull() {
			
		boolean thr=false;
		try{
			Giocatore g1= new Giocatore("lorenzo",null, 1, 10);
		}
		catch(IllegalArgumentException e){
			thr=true;
		}
		assertTrue(thr);
	}
	
	@Test
	public void testInitGiocatoreGiocShouldBeOk() {
		Colore mockCol= Mockito.mock(Colore.class);
		Mockito.when(mockCol.getNome()).thenReturn("rosso");
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g2= new Giocatore("lorenzo",1,mockCol, 10, 10,mockWin,mockNob,mockRich);	
		
		Giocatore g1= new Giocatore(g2);
		assertEquals(g1.getNome(),"lorenzo");
		assertEquals(g1.getNobiltaGiocatore(),2);
		assertEquals(g1.getRicchezzaGiocatore(),3);
		assertEquals(g1.getVittoriaGiocatore(),1);
			
	}
	
	@Test
	public void testInitGiocatoreGiocThrowException() {
			
		boolean thr=false;
		try{
			Giocatore g1= new Giocatore(null);
		}
		catch(IllegalArgumentException e){
			thr=true;
		}
		assertTrue(thr);
	}
	
	@Test
	public void testHasCartePoliticheShouldBeFalseExc() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		Colore mockColG= Mockito.mock(Colore.class);
		Mockito.when(mockColG.getNome()).thenReturn("verde");
		Colore mockColB= Mockito.mock(Colore.class);
		Mockito.when(mockColB.getNome()).thenReturn("blu");
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		assertFalse(g1.hasCartePolitiche(new ArrayList<>()));
		assertFalse(g1.hasCartePolitiche(null));
		List<CartaPolitica> tempList = new ArrayList<>();
		tempList.add(null);
		assertFalse(g1.hasCartePolitiche(tempList));		
	}
	
	@Test
	public void testHasCartePoliticheShouldBeTrue() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);	
		
		List<CartaPolitica> tempList = new ArrayList<>();
		
		try {
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("blu",0,0,255)));
			tempList.add(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
			tempList.add(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));
		} catch (ColoreNotFoundException e) {
			
			e.printStackTrace();
		}
		
		assertTrue(g1.hasCartePolitiche(tempList));		
	}
	
	@Test
	public void testHasCartePoliticheShouldBeFalse() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);	
		
		List<CartaPolitica> tempList = new ArrayList<>();
		
		try {
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("blu",0,0,255)));
			tempList.add(new CartaPoliticaSemplice(new Colore("giallo",255,255,0)));
			tempList.add(new CartaPoliticaSemplice(new Colore("viola",255,0,255)));
		} catch (ColoreNotFoundException e) {
			
			e.printStackTrace();
		}
		
		assertFalse(g1.hasCartePolitiche(tempList));		
	}
	
	@Test
	public void testGetSetCartePoliticheShouldBeOk() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		try {
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
			g1.addCartaPolitica(new CartaPoliticaJolly());
			
			List<CartaPolitica> temp= g1.getCartePolitiche();
			assertEquals("(rosso)",temp.get(0).toString());
			assertEquals("(jolly)",temp.get(1).toString());
		} catch (ColoreNotFoundException e) {			
			e.printStackTrace();
		}		
	}
	
	
	@Test
	public void testGetSetCartePoliticheThrowException() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		boolean thr=false;
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		try {
			g1.addCartaPolitica(null);	
			
			
		} catch (IllegalArgumentException e) {			
			thr=true;
		}
		assertTrue(thr);
	}
	
	
	@Test
	public void testRimuoviCartaPoliticaShouldBeFalseEXC() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		assertFalse(g1.rimuoviCartaPolitica(null));
	}
	
	@Test
	public void testRimuoviCartaPoliticaShouldBeFalse() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		CartaPoliticaSemplice carta;
		
		try {
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));
			g1.addCartaPolitica(new CartaPoliticaJolly());	
			carta= new CartaPoliticaSemplice(new Colore("blu",0,0,255));
			assertFalse(g1.rimuoviCartaPolitica(carta));
			
		} catch (ColoreNotFoundException e) {			
			e.printStackTrace();
		}	
		
	}
	
	@Test
	public void testRimuoviCartaPoliticaShouldBeTrue() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		CartaPoliticaSemplice carta;
		
		try {
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));
			g1.addCartaPolitica(new CartaPoliticaJolly());	
			carta= new CartaPoliticaSemplice(new Colore("verde",0,255,0));
			assertTrue(g1.rimuoviCartaPolitica(carta));
			
		} catch (ColoreNotFoundException e) {			
			e.printStackTrace();
		}	
		
	}
	
	@Test
	public void testRimuoviCarteShouldBeTrue() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		List<CartaPolitica> carta= new ArrayList<>();
		
		try {
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));
			g1.addCartaPolitica(new CartaPoliticaJolly());	
			assertEquals(3,g1.getCartePolitiche().size());
			carta.add( new CartaPoliticaSemplice(new Colore("verde",0,255,0)));
			carta.add( new CartaPoliticaJolly());
			g1.removeCarte(carta);
			assertEquals(1,g1.getCartePolitiche().size());
			
		} catch (ColoreNotFoundException e) {			
			e.printStackTrace();
		}	
		
	}
	
	@Test
	public void testRimuoviCarteThrowException() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		List<CartaPolitica> carta= new ArrayList<>();
		boolean thr=false;
		try {
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
			g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));
			g1.addCartaPolitica(new CartaPoliticaJolly());	
			assertEquals(3,g1.getCartePolitiche().size());
			
			g1.removeCarte(null);
			
		} catch (ColoreNotFoundException e) {			
			e.printStackTrace();
		}
		catch (IllegalArgumentException e) {			
			thr=true;
		}
		
	}
	
	@Test
	public void testAddUseAiutantiOK() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		g1.addAiutanti(5);
		assertEquals(5,g1.getAiutanti().size());
		
		g1.usaAiutanti(5);;
		assertEquals(0,g1.numeroAiutanti());
	}
	
	@Test
	public void testEmporiOK() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		assertEquals(10,g1.getEmpori().size());
		
		g1.usaEmporio();
		assertEquals(9,g1.getEmpori().size());
	}
	
	
	@Test
	public void testAddPermessoCostruzioneOk() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		PermessoCostruzione perm1=new PermessoCostruzione(mockCitta,mockRicompense);
		PermessoCostruzione perm2=new PermessoCostruzione(mockCitta,mockRicompense);
		
		g1.addPermessoCostruzione(perm1, g1.getPermessiCostruzione());
		g1.addPermessoCostruzione(perm2, g1.getPermessiCostruzione());
		
		assertEquals(2,g1.getPermessiCostruzione().size());
		
	}
	
	@Test
	public void testAddPermessoCostruzioneThrowExcListaNull() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		PermessoCostruzione perm1=new PermessoCostruzione(mockCitta,mockRicompense);
		
		boolean thr=false;
		try	{
			g1.addPermessoCostruzione(perm1,null);
		}
		catch(IllegalArgumentException e){
			thr=true;
		}
		
		
		assertTrue(thr);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddPermessoCostruzioneThrowExcPermessoNull() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);	
		
		g1.addPermessoCostruzione(null, g1.getPermessiCostruzione());
		
		
	}
	
	@Test
	public void testrimuoviCartaPermessoShouldBeTrue() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		PermessoCostruzione perm1=new PermessoCostruzione(mockCitta,mockRicompense);
		
		
		g1.addPermessoCostruzione(perm1,g1.getPermessiCostruzione());		
		
		
		assertTrue(g1.rimuoviCartaPermesso(perm1, g1.getPermessiCostruzione()));
		
	}
	
	@Test
	public void testrimuoviCartaPermessoShouldBeFalseEXC() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		
		
		assertFalse(g1.rimuoviCartaPermesso(null, g1.getPermessiCostruzione()));
		
	}
	
	@Test
	public void testUsaPermessoOk() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		PermessoCostruzione perm1=new PermessoCostruzione(mockCitta,mockRicompense);
		
		
		try {
			g1.accumulaPermesso(perm1);
			g1.usaPermesso(perm1);
			assertEquals(1,g1.getCartePermessoUsate().size());
		} 
		catch (PermessoNotFoundException e) {
			e.printStackTrace();
		}		
	}
	
	@Test(expected = PermessoNotFoundException.class ) 
	public void testUsaPermessoThrowException() throws PermessoNotFoundException {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		PermessoCostruzione perm1=new PermessoCostruzione(mockCitta,mockRicompense);		
			
		g1.usaPermesso(perm1);			
		
	}
	
	@Test
	public void testPerdiPermessoOk() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		PermessoCostruzione perm1=new PermessoCostruzione(mockCitta,mockRicompense);
		
		
		try {
			g1.accumulaPermesso(perm1);
			assertEquals(1,g1.getPermessiCostruzione().size());
			g1.perdiPermesso(perm1);
			assertTrue(g1.getPermessiCostruzione().isEmpty());
		} 
		catch (PermessoNotFoundException e) {
			e.printStackTrace();
		}		
	}
	
	@Test (expected = PermessoNotFoundException.class )
	public void testPerdiPermessoThrowException() throws PermessoNotFoundException {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		PermessoCostruzione perm1=new PermessoCostruzione(mockCitta,mockRicompense);
		
		g1.perdiPermesso(perm1);
		
				
	}
	
	@Test (expected = IllegalArgumentException.class )
	public void testAccumulaPermessoThrowException() throws PermessoNotFoundException {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
				
		g1.accumulaPermesso(null);				
	}
	
	@Test
	public void testRicchezzaOk() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		List<CasellaRicchezza>	temp=new ArrayList<>();
		for(int i=0;i<10;i++)
			temp.add(new CasellaRicchezza(i));
		PercorsoRicchezza path=new PercorsoRicchezza(temp);
		
		g1.setCasellaRicchezza(2, path);
		assertEquals(2,g1.getRicchezzaGiocatore());
		
		g1.setCasellaRicchezza(5, path);
		assertEquals(5,g1.getRicchezzaGiocatore());
	}
	
	@Test(expected= IllegalArgumentException.class)
	public void testSetRicchezzaThrowExcPathNull() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);			
		
		g1.setCasellaRicchezza(2, null);
		
	}
	
	@Test(expected= IllegalArgumentException.class)
	public void testSetRicchezzaThrowExcCasellaNeg() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);			
		
		g1.setCasellaRicchezza(2, null);
		
	}
	
	@Test
	public void testSpendiRiceviMoneteOk() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		List<CasellaRicchezza>	temp=new ArrayList<>();
		for(int i=0;i<10;i++)
			temp.add(new CasellaRicchezza(i));
		PercorsoRicchezza path=new PercorsoRicchezza(temp);
		
		g1.riceviMonete(5, path);
		assertEquals(5,g1.getRicchezzaGiocatore());
		
		g1.spendiMonete(3, path);
		assertEquals(2,g1.getRicchezzaGiocatore());
	}
	
	@Test (expected = IllegalArgumentException.class )
	public void testSpendiMoneteThrowExcPathNull() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		g1.spendiMonete(3, null);
		
	}
	
	@Test (expected = IllegalArgumentException.class )
	public void testSpendiMoneteThrowExcMoneteNeg() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		Set<Ricompensa> mockRicompense=Mockito.mock(Set.class);
		List<Citta> mockCitta=Mockito.mock(List.class);
		
		List<CasellaRicchezza>	temp=new ArrayList<>();
		for(int i=0;i<10;i++)
			temp.add(new CasellaRicchezza(i));
		PercorsoRicchezza path=new PercorsoRicchezza(temp);
		
		
		g1.spendiMonete(-3, path);
		
	}
	
	@Test (expected = IllegalArgumentException.class )
	public void testRiceviMoneteThrowExcPathNull() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);		
		
		g1.riceviMonete(3, null);
		
	}
	
	@Test (expected = IllegalArgumentException.class )
	public void testRiceviMoneteThrowExcMoneteNeg() {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);			
		
		List<CasellaRicchezza>	temp=new ArrayList<>();
		for(int i=0;i<10;i++)
			temp.add(new CasellaRicchezza(i));
		PercorsoRicchezza path=new PercorsoRicchezza(temp);
		
		g1.riceviMonete(-3, path);
		
	}
	
	@Test  
	public void testNobiltaOk() throws FileSyntaxException {
		Colore mockColR= Mockito.mock(Colore.class);
		Mockito.when(mockColR.getNome()).thenReturn("rosso");		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		Partita partita= new Partita();
		


		g1.setCasellaNobilta(2, partita);
		assertEquals(2,g1.getNobiltaGiocatore());
		
	}
	
	@Test  
	public void testRiceviNobiltaOk() throws FileSyntaxException {
		Colore mockColR= Mockito.mock(Colore.class);				
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		Partita partita= new Partita();
		
		g1.riceviNobilta(2, partita);
		assertEquals(2,g1.getNobiltaGiocatore());
		
	}
	
	@Test (expected=IllegalArgumentException.class) 
	public void testSetCasellaNobiltaThrowexception() throws FileSyntaxException {
		Colore mockColR= Mockito.mock(Colore.class);		
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);		
		
		g1.setCasellaNobilta(3, null);
		
	}
	
	@Test  
	public void testVittoriaOk(){
		Colore mockColR= Mockito.mock(Colore.class);			
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		List<CasellaVittoria>	temp=new ArrayList<>();
		for(int i=0;i<10;i++)
			temp.add(new CasellaVittoria(i));
		PercorsoVittoria path=new PercorsoVittoria(temp);
		
		g1.setCasellaVittoria(5,path);
		
		assertEquals(5,g1.getVittoriaGiocatore());
		
		g1.setCasellaVittoria(9,path);
		
		assertEquals(9,g1.getVittoriaGiocatore());
		
	}
	
	@Test (expected=IllegalArgumentException.class) 
	public void testSetCasellaVittoriaThrowException() throws FileSyntaxException{
		Colore mockColR= Mockito.mock(Colore.class);			
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		g1.setCasellaVittoria(5,null);		
	}
	
	@Test  
	public void testRiceviVittoriaOk() {
		Colore mockColR= Mockito.mock(Colore.class);			
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		List<CasellaVittoria>	temp=new ArrayList<>();
		for(int i=0;i<10;i++)
			temp.add(new CasellaVittoria(i));
		PercorsoVittoria path=new PercorsoVittoria(temp);
		
		g1.riceviVittoria(5, path);
		
		assertEquals(5,g1.getVittoriaGiocatore());


		
	}
	
	@Test  
	public void testGetNumeroEmporiOk() {
		Colore mockColR= Mockito.mock(Colore.class);			
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		assertEquals(10,g1.getNumeroEmpori());
		
	}
	
	@Test (expected=IllegalArgumentException.class) 
	public void testAddBonusThrowExc() {
		Colore mockColR= Mockito.mock(Colore.class);			
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		g1.addBonus(null);
		
	}
	
	@Test (expected=IllegalArgumentException.class) 
	public void testMettiVenditaThrowExcMarket() {
		Colore mockColR= Mockito.mock(Colore.class);			
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		Inserzione mockIns= Mockito.mock(Inserzione.class);
		g1.mettiVendita(null, mockIns);
	}
	
	@Test (expected=IllegalArgumentException.class) 
	public void testMettiVenditaThrowExcIns() {
		Colore mockColR= Mockito.mock(Colore.class);			
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		Market mockMarket= Mockito.mock(Market.class);
		g1.mettiVendita(mockMarket,null);
	}
	
	@Test  
	public void testRimuoviAiutanteOK() {
		Colore mockColR= Mockito.mock(Colore.class);			
		
		Giocatore g1= new Giocatore("lorenzo",mockColR, 10, 10);
		
		g1.addAiutanti(5);
		Aiutante help= g1.getAiutanti().get(0);
		assertTrue(g1.rimuoviAiutante(help));		
		
	}
	
	@Test  
	public void testToStringOK() {					
		
		Colore col= new Colore("rosso",255,0,0);
		CasellaVittoria mockWin=Mockito.mock(CasellaVittoria.class);
		Mockito.when(mockWin.getPuntiCasella()).thenReturn(1);
		CasellaNobilta mockNob=Mockito.mock(CasellaNobilta.class);
		Mockito.when(mockNob.getPuntiCasella()).thenReturn(2);
		CasellaRicchezza mockRich=Mockito.mock(CasellaRicchezza.class);
		Mockito.when(mockRich.getPuntiCasella()).thenReturn(3);
		
		Giocatore g1= new Giocatore("lorenzo",1,col, 10, 10,mockWin,mockNob,mockRich);
		
		String str="{ nome=lorenzo, rosso, numeroGiocatore=1, idGiocatore="+g1.getIdGiocatore()+",\nempori=10, aiutanti=10, tessereBonus=[], \ncartePolitiche=[], \ncartePermesso=[], \ncartePermessoUsate=[], \nPuntiVittoria=1, PuntiNobilta=2, PuntiRicchezza=3}";
			
		assertEquals(str,g1.toString());		
		
	}
	
	@Test  
	public void testGetColoreOK() {
		Colore col= new Colore("rosso",255,0,0);			
		
		Giocatore g1= new Giocatore("lorenzo",col, 10, 10);
		
		assertEquals(col,g1.getColore());		
		
	}
}
