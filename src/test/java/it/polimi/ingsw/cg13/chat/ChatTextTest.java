package it.polimi.ingsw.cg13.chat;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.comandi.ChatMessage;

public class ChatTextTest {

	ChatText chat;
	
	static ChatMessage message=Mockito.mock(ChatMessage.class);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Mockito.when(message.toString()).thenReturn("chat");
	}

	@Test
	public void testChatText() {
		chat=new ChatText();
		assertTrue(chat.getText().equals(""));
	}

	@Test
	public void testGetText() {
		chat=new ChatText();
		chat.append("yeee");
		assertEquals(chat.getText(),"yeee\n");
	}

	@Test
	public void testAppend() {
		chat=new ChatText();
		chat.append("uooo");
		assertEquals(chat.getText(),"uooo\n");
	}

	@Test
	public void testUpdate() {
		chat=new ChatText();
		chat.update(message);
		assertTrue(chat.getText().equals("chat\n"));
	}

}
