package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import java.util.ArrayDeque;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.mutatio.TesseraBonusMutatio;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class GruppoBonusReTest {

	
	GruppoBonusRe gruppo;
	GruppoBonusRe gruppo2;
	
	static Giocatore g1;
	
	static TesseraBonusRe tessera1;
	static TesseraBonusRe tessera2;
	static ArrayDeque<TesseraBonusRe> array=new ArrayDeque<>();
	
	static TesseraBonusGruppoCitta tesseraCitta=Mockito.mock(TesseraBonusGruppoCitta.class);

	static TesseraBonusMutatio change=Mockito.mock(TesseraBonusMutatio.class);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		tessera1=new TesseraBonusRe(10,1);
		tessera2=new TesseraBonusRe(7,2);
		array.add(tessera1);
		array.add(tessera2);
		
		g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(0),new CasellaRicchezza(10));
		
		Mockito.when(change.getTesseraBonus()).thenReturn(tesseraCitta);
		
		Mockito.when(tesseraCitta.getGiocatore()).thenReturn(g1);
	}

	@Test
	public void testGruppoBonusReCostruttoreConCoda() {
		gruppo=new GruppoBonusRe(array);
		assertTrue(gruppo!=null && gruppo.getGruppo().equals(array));
	}

	@Test
	public void testGruppoBonusReGruppoCostruttoreConClasse() {
		gruppo=new GruppoBonusRe(array);
		gruppo2=new GruppoBonusRe(gruppo);
		assertTrue(gruppo2!=null && gruppo2.getGruppo().equals(array));
	}

	@Test
	public void testSetTesseraBonusRe() {
		gruppo=new GruppoBonusRe(new ArrayDeque<>());
		gruppo.setTesseraBonusRe(tessera1);
		gruppo.setTesseraBonusRe(tessera2);
		assertTrue(gruppo.getGruppo().poll().equals(tessera1) && gruppo.getGruppo().poll().equals(tessera2));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetTesseraBonusReNull() {
		gruppo=new GruppoBonusRe(array);
		gruppo.setTesseraBonusRe(null);
	}

	@Test
	public void testGetGruppo() {
		gruppo=new GruppoBonusRe(array);
		assertEquals(array,gruppo.getGruppo());
	}

	@Test
	public void testUpdate() {
		gruppo=new GruppoBonusRe(new ArrayDeque<>());
		gruppo.setTesseraBonusRe(tessera1);
		gruppo.update(change);
		assertTrue(!gruppo.getGruppo().contains(tessera1) && tessera1.getGiocatore().equals(g1)
				&& g1.getTessereBonus().contains(tessera1));
	}

	@Test
	public void testToString() {
		gruppo=new GruppoBonusRe(array);
		assertEquals("GruppoBonusRe {[TesseraBonusRe (ordine=1, puntiVittoria=10), "
				+ "TesseraBonusRe (ordine=2, puntiVittoria=7)]}",gruppo.toString());
	}

}
