package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.machinestate.StateCinque;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateSette;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class AzionePrincipaleAggiuntivaTest {

	AzionePrincipaleAggiuntiva ricompensa;
	AzionePrincipaleAggiuntiva ricompensa2;
	static Giocatore gReale;
	static Partita partita=Mockito.mock(Partita.class);
	static StateUno statoUno;
	static StateTre statoTre;
	static StateQuattro statoQuattro;
	static StateQuattro statoQuattro2;
	static ArrayList<RicompensaSpecial> ricompense1=new ArrayList<>();
	static ArrayList<RicompensaSpecial> ricompense2=new ArrayList<>();
	static ArrayList<RicompensaSpecial> ricompense3=new ArrayList<>();
	static ArrayList<RicompensaSpecial> dueRicompense=new ArrayList<>();
	static ArrayList<RicompensaSpecial> ricompenseDopo=new ArrayList<>();
	static ArrayList<Giocatore> giocatori=new ArrayList<Giocatore>();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gReale= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(10));
		ricompense1.add(new AzionePrincipaleAggiuntiva());
		ricompense2.add(new AzionePrincipaleAggiuntiva());
		ricompense3.add(new AzionePrincipaleAggiuntiva());
		dueRicompense.add(new AzionePrincipaleAggiuntiva(2));
		dueRicompense.add(new AzionePrincipaleAggiuntiva(2));
		statoUno=new StateUno(giocatori,gReale,ricompense1);
		statoTre=new StateTre(giocatori,gReale,ricompense2);
		statoQuattro=new StateQuattro(giocatori,gReale,ricompense3);
		statoQuattro2=new StateQuattro(giocatori,gReale,ricompenseDopo);
		Mockito.when(partita.getStatoPartita()).thenReturn(statoQuattro2);
	}

	@Test
	public void testHashCodeUguali() {
		ricompensa=new AzionePrincipaleAggiuntiva(3);
		ricompensa2=new AzionePrincipaleAggiuntiva(3);
		assertTrue(ricompensa.hashCode()==ricompensa2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi() {
		ricompensa=new AzionePrincipaleAggiuntiva(3);
		ricompensa2=new AzionePrincipaleAggiuntiva();
		assertFalse(ricompensa.hashCode()==ricompensa2.hashCode());
	}

	@Test
	public void testAssegnaRicompensa() {
		ricompensa=new AzionePrincipaleAggiuntiva(2);
		ricompensa.assegnaRicompensa(gReale, partita);
		assertEquals(statoQuattro2.getRicompense(),dueRicompense);
	}

	@Test
	public void testGetQuantita() {
		ricompensa=new AzionePrincipaleAggiuntiva(3);
		ricompensa2=new AzionePrincipaleAggiuntiva();
		assertTrue(3==ricompensa.getQuantita() && 1==ricompensa2.getQuantita());
	}

	@Test
	public void testEqualsUguali() {
		ricompensa=new AzionePrincipaleAggiuntiva(3);
		ricompensa2=new AzionePrincipaleAggiuntiva(3);
		assertTrue(ricompensa.equals(ricompensa2));
	}
	
	@Test
	public void testEqualsStessoReference() {
		ricompensa=new AzionePrincipaleAggiuntiva(3);
		assertTrue(ricompensa.equals(ricompensa));
	}
	
	@Test
	public void testEqualsDiversi() {
		ricompensa=new AzionePrincipaleAggiuntiva(3);
		ricompensa2=new AzionePrincipaleAggiuntiva();
		assertFalse(ricompensa.equals(ricompensa2));
	}

	@Test
	public void testCostruttoreAzionePrincipaleAggiuntivaBase() {
		ricompensa2=new AzionePrincipaleAggiuntiva();
		assertTrue(ricompensa2!=null && 1==ricompensa2.getQuantita());
	}


	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreAzionePrincipaleAggiuntivaConIntNegativo() {
		ricompensa=new AzionePrincipaleAggiuntiva(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreAzionePrincipaleAggiuntivaIntZero() {
		ricompensa=new AzionePrincipaleAggiuntiva(0);
	}
	
	@Test
	public void testCostruttoreAzionePrincipaleAggiuntivaConIntCorretto() {
		ricompensa=new AzionePrincipaleAggiuntiva(3);
		assertTrue(ricompensa!=null && 3==ricompensa.getQuantita());
	}


	@Test
	public void testVisitStateUnoPartita() {
		ricompensa=new AzionePrincipaleAggiuntiva();
		assertEquals(new StateDue(giocatori,gReale,ricompenseDopo),ricompensa.visit(statoUno, partita));
	}

	@Test
	public void testVisitStateTrePartita() {
		ricompensa=new AzionePrincipaleAggiuntiva();
		assertEquals(new StateSette(giocatori,gReale,ricompenseDopo),ricompensa.visit(statoTre, partita));
	}

	@Test
	public void testVisitStateQuattroPartita() {
		ricompensa=new AzionePrincipaleAggiuntiva();
		assertEquals(new StateCinque(giocatori,gReale,ricompenseDopo),ricompensa.visit(statoQuattro, partita));
	}

	@Test
	public void testToString() {
		ricompensa=new AzionePrincipaleAggiuntiva(3);
		assertEquals("AzionePrincipaleAggiuntiva (3)",ricompensa.toString());
	}

	@Test
	public void testCheckBonus() {
		ricompensa=new AzionePrincipaleAggiuntiva(3);
		assertTrue(ricompensa.checkBonus(gReale, partita));
	}

}
