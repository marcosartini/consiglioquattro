package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;


import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CartaPoliticaAggiuntivaTest {

	CartaPoliticaAggiuntiva ricompensa;
	CartaPoliticaAggiuntiva ricompensa2;
	static Giocatore gReale;
	static Partita partita=Mockito.mock(Partita.class);
	static CartaPolitica carta=Mockito.mock(CartaPolitica.class);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Mockito.when(partita.pescaCarta()).thenReturn(carta);
		gReale= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(10));
	}

	@Test
	public void testHashCodeUguali() {
		ricompensa=new CartaPoliticaAggiuntiva(3);
		ricompensa2=new CartaPoliticaAggiuntiva(3);
		assertTrue(ricompensa.hashCode()==ricompensa2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi() {
		ricompensa=new CartaPoliticaAggiuntiva(3);
		ricompensa2=new CartaPoliticaAggiuntiva();
		assertFalse(ricompensa.hashCode()==ricompensa2.hashCode());
	}

	
	@Test
	public void testAssegnaRicompensa() {
		ricompensa=new CartaPoliticaAggiuntiva(3);
		ricompensa.assegnaRicompensa(gReale, partita);
		assertTrue(3==gReale.getCartePolitiche().size());
	}

	@Test
	public void testGetQuantita() {
		ricompensa=new CartaPoliticaAggiuntiva(3);
		ricompensa2=new CartaPoliticaAggiuntiva();
		assertTrue(3==ricompensa.getQuantita() && 1==ricompensa2.getQuantita());
	}

	@Test
	public void testEqualsUguali() {
		ricompensa=new CartaPoliticaAggiuntiva(3);
		ricompensa2=new CartaPoliticaAggiuntiva(3);
		assertTrue(ricompensa.equals(ricompensa2));
	}
	
	@Test
	public void testEqualsStessoReference() {
		ricompensa=new CartaPoliticaAggiuntiva(3);
		assertTrue(ricompensa.equals(ricompensa));
	}
	
	@Test
	public void testEqualsDiversi() {
		ricompensa=new CartaPoliticaAggiuntiva(3);
		ricompensa2=new CartaPoliticaAggiuntiva();
		assertFalse(ricompensa.equals(ricompensa2));
	}

	@Test
	public void testCostruttoreCartaPoliticaAggiuntivaBase() {
		ricompensa=new CartaPoliticaAggiuntiva();
		assertTrue(ricompensa!=null && 1==ricompensa.getQuantita());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCartaPoliticaAggiuntivaConIntNegativo() {
		ricompensa=new CartaPoliticaAggiuntiva(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCartaPoliticaAggiuntivaConIntZero() {
		ricompensa=new CartaPoliticaAggiuntiva(0);
	}
	
	@Test
	public void testCostruttoreCartaPoliticaAggiuntivaConIntCorretto() {
		ricompensa=new CartaPoliticaAggiuntiva(3);
		assertTrue(ricompensa!=null && 3==ricompensa.getQuantita());
	}

	@Test
	public void testToString() {
		ricompensa=new CartaPoliticaAggiuntiva(2);
		assertEquals("CartaPoliticaAggiuntiva (2)",ricompensa.toString());
	}


}
