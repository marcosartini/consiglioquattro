package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class RiutilizzaBonusPermessoTest {

	RiutilizzaBonusPermesso ricompensa=new RiutilizzaBonusPermesso();
	RiutilizzaBonusPermesso ricompensa2=new RiutilizzaBonusPermesso();
	RiutilizzaBonusCitta ricompensaErrata=new RiutilizzaBonusCitta();
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	static ArrayList<Giocatore> giocatoriPartita=new ArrayList<>();
	static OggettoBonusInput cittaDaTrovare;
	static Partita modello2=Mockito.mock(Partita.class);
	static HashSet<Ricompensa> ricompense=new HashSet<>();
	static PermessoCostruzione permesso=Mockito.mock(PermessoCostruzione.class);
	static ArrayList<PermessoCostruzione> carte=new ArrayList<>();


	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(0),new CasellaRicchezza(10));
		g1.addPermessoCostruzione(permesso, g1.getCartePermessoUsate());
		Mockito.when(g2.getCartePermessoUsate()).thenReturn(carte);
		Mockito.when(permesso.getRicompense()).thenReturn(ricompense);
		
	}

	@Test
	public void testHashCodeUguale() {
		assertTrue(ricompensa.hashCode()==ricompensa2.hashCode());
	}
	
	@Test
	public void testHashCodeDiverso() {
		assertFalse(ricompensa.hashCode()==ricompensaErrata.hashCode());
	}

	@Test
	public void testEqualsObjectUguali() {
		assertTrue(ricompensa.equals(ricompensa2));
	}

	@Test
	public void testEqualsObjectDiversi() {
		assertFalse(ricompensa.equals(ricompensaErrata));
	}
	
	@Test
	public void testCheckBonusFalse() {
		assertFalse(ricompensa.checkBonus(g2, modello2));
	}
	
	
	@Test
	public void testCheckBonusTrue() {
		assertTrue(ricompensa.checkBonus(g1, modello2));
	}

	@Test
	public void testToString() {
		assertEquals("RiutilizzaBonusPermesso",ricompensa.toString());
	}

	@Test
	public void testAssegnaBonusSceltoAssegnato() {
		ricompensa.assegnaBonusScelto(permesso, g1, modello2);
	}
	

	@Test
	public void testUpdateFromServerWithExceptionNonHaEmpori() {
		try {
			assertEquals(permesso,ricompensa.updateFromServer(g2, modello2, permesso));
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	

	@Test
	public void testUpdateFromServerTrovato() throws CdQException {
		assertEquals(permesso,ricompensa.updateFromServer(g1, modello2, permesso));
	}
	
}
