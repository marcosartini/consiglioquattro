package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class AiutanteAggiuntivoTest {

	AiutanteAggiuntivo ricompensa;
	AiutanteAggiuntivo ricompensa2;
	static Giocatore gReale;
	static Partita partita=Mockito.mock(Partita.class);
	static ArrayList<Aiutante> list=new ArrayList<>();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		list.add(new Aiutante());
		list.add(new Aiutante());
		Mockito.when(partita.getAiutanti(list.size())).thenReturn(list);
		gReale= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(10));
	}

	@Test
	public void testHashCodeUguali() {
		ricompensa=new AiutanteAggiuntivo(3);
		ricompensa2=new AiutanteAggiuntivo(3);
		assertTrue(ricompensa.hashCode()==ricompensa2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi() {
		ricompensa=new AiutanteAggiuntivo(3);
		ricompensa2=new AiutanteAggiuntivo();
		assertFalse(ricompensa.hashCode()==ricompensa2.hashCode());
	}


	@Test(expected=IllegalArgumentException.class)
	public void testAssegnaRicompensaGiocatoreNull() {
		ricompensa=new AiutanteAggiuntivo(3);
		ricompensa.assegnaRicompensa(null, partita);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAssegnaRicompensaPartitaNull() {
		ricompensa=new AiutanteAggiuntivo(3);
		ricompensa.assegnaRicompensa(gReale,null);
	}
	
	@Test
	public void testAssegnaRicompensa() {
		ricompensa=new AiutanteAggiuntivo(2);
		ricompensa.assegnaRicompensa(gReale, partita);
		assertTrue(3==gReale.numeroAiutanti());
	}

	@Test
	public void testGetQuantita() {
		ricompensa=new AiutanteAggiuntivo(3);
		ricompensa2=new AiutanteAggiuntivo();
		assertTrue(3==ricompensa.getQuantita() && 1==ricompensa2.getQuantita());
	}

	@Test
	public void testEqualsUguali() {
		ricompensa=new AiutanteAggiuntivo(3);
		ricompensa2=new AiutanteAggiuntivo(3);
		assertTrue(ricompensa.equals(ricompensa2));
	}
	
	@Test
	public void testEqualsStessoReference() {
		ricompensa=new AiutanteAggiuntivo(3);
		assertTrue(ricompensa.equals(ricompensa));
	}
	
	@Test
	public void testEqualsDiversi() {
		ricompensa=new AiutanteAggiuntivo(3);
		ricompensa2=new AiutanteAggiuntivo();
		assertFalse(ricompensa.equals(ricompensa2));
	}

	@Test
	public void testCostruttoreAiutanteAggiuntivoBase() {
		ricompensa=new AiutanteAggiuntivo();
		assertTrue(ricompensa!=null && 1==ricompensa.getQuantita());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreAiutanteAggiuntivoConIntNegativo() {
		ricompensa=new AiutanteAggiuntivo(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreAiutanteAggiuntivoConIntZero() {
		ricompensa=new AiutanteAggiuntivo(0);
	}
	
	@Test
	public void testCostruttoreAiutanteAggiuntivoConIntCorretto() {
		ricompensa=new AiutanteAggiuntivo(3);
		assertTrue(ricompensa!=null && 3==ricompensa.getQuantita());
	}

	@Test
	public void testToString() {
		ricompensa=new AiutanteAggiuntivo(2);
		assertEquals("AiutanteAggiuntivo (2)",ricompensa.toString());
	}

	@Test
	public void testCheckGiveNobilta() {
		ricompensa=new AiutanteAggiuntivo();
		assertFalse(ricompensa.checkGiveNobilta());
	}

}
