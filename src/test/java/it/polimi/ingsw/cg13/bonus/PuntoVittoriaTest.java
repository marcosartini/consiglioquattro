package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class PuntoVittoriaTest {

	PuntoVittoria ricompensa;
	PuntoVittoria ricompensa2;
	static Giocatore gReale;
	static Partita partita=Mockito.mock(Partita.class);
	static PercorsoVittoria percorso=Mockito.mock(PercorsoVittoria.class);
	static CasellaVittoria casella1=new CasellaVittoria(0);
	static CasellaVittoria casella2=new CasellaVittoria(1);

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gReale= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(0),new CasellaRicchezza(10));
		Mockito.when(partita.getPercorsoVittoria()).thenReturn(percorso);
		Mockito.when(percorso.getCasella(0)).thenReturn(casella1);
		Mockito.when(percorso.getCasella(1)).thenReturn(casella2);
		
	}

	@Test
	public void testHashCodeUguali() {
		ricompensa=new PuntoVittoria(3);
		ricompensa2=new PuntoVittoria(3);
		assertTrue(ricompensa.hashCode()==ricompensa2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi() {
		ricompensa=new PuntoVittoria(3);
		ricompensa2=new PuntoVittoria(1);
		assertFalse(ricompensa.hashCode()==ricompensa2.hashCode());
	}


	@Test
	public void testAssegnaRicompensa() {
		ricompensa=new PuntoVittoria(1);
		ricompensa.assegnaRicompensa(gReale, partita);
		assertTrue(1==gReale.getVittoriaGiocatore());
	}

	@Test
	public void testGetQuantita() {
		ricompensa=new PuntoVittoria(3);
		assertTrue(3==ricompensa.getQuantita());
	}

	@Test
	public void testEqualsUguali() {
		ricompensa=new PuntoVittoria(3);
		ricompensa2=new PuntoVittoria(3);
		assertTrue(ricompensa.equals(ricompensa2));
	}
	
	@Test
	public void testEqualsStessoReference() {
		ricompensa=new PuntoVittoria(3);
		assertTrue(ricompensa.equals(ricompensa));
	}
	
	@Test
	public void testEqualsDiversi() {
		ricompensa=new PuntoVittoria(3);
		ricompensa2=new PuntoVittoria(2);
		assertFalse(ricompensa.equals(ricompensa2));
	}

	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePuntoVittoriaNegativo() {
		ricompensa=new PuntoVittoria(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePuntoVittoriaZero() {
		ricompensa=new PuntoVittoria(0);
	}
	
	@Test
	public void testCostruttorePuntoVittoriaCorretto() {
		ricompensa=new PuntoVittoria(3);
		assertTrue(ricompensa!=null && 3==ricompensa.getQuantita());
	}

	@Test
	public void testToString() {
		ricompensa=new PuntoVittoria(2);
		assertEquals("PuntoVittoria (2)",ricompensa.toString());
	}
	

}
