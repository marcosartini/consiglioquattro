package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.mutatio.EmporioMutatio;

public class TesseraBonusReTest {

	
	TesseraBonusRe tessera;
	TesseraBonusRe tessera2;
	
	static EmporioMutatio change=Mockito.mock(EmporioMutatio.class);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testHashCodeUguali() {
		tessera=new TesseraBonusRe(10,1);
		tessera2=new TesseraBonusRe(10,1);
		assertTrue(tessera.hashCode()==tessera2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi() {
		tessera=new TesseraBonusRe(10,1);
		tessera2=new TesseraBonusRe(5,2);
		assertFalse(tessera.hashCode()==tessera2.hashCode());
	}

	@Test
	public void testIsAssegnabile() {
		tessera=new TesseraBonusRe(10,1);
		assertFalse(tessera.isAssegnabile(change));
	}
	
	

	@Test(expected=UnsupportedOperationException.class)
	public void testUpdate() {
		tessera=new TesseraBonusRe(10,1);
		tessera.update(change);
	}

	@Test
	public void testToString() {
		tessera=new TesseraBonusRe(10,1);
		assertEquals("TesseraBonusRe (ordine=1, puntiVittoria=10)", this.tessera.toString());
	}

	@Test
	public void testCostruttoreTesseraBonusReOrdineOK() {
		tessera=new TesseraBonusRe(10,1);
		assertTrue(tessera!=null && tessera.getPunteggioCaselleVittoria()==10);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreTesseraBonusReOrdineNegativo() {
		tessera=new TesseraBonusRe(10,-1);
	}

	@Test
	public void testEqualsUguali(){
		tessera=new TesseraBonusRe(10,1);
		tessera2=new TesseraBonusRe(10,1);
		assertTrue(tessera.equals(tessera2));
	}
	
	@Test
	public void testEqualsStessoReference(){
		tessera=new TesseraBonusRe(10,1);
		assertTrue(tessera.equals(tessera));
	}
	
	
	@Test
	public void testEqualsDiversoPunteggio(){
		tessera=new TesseraBonusRe(10,1);
		tessera2=new TesseraBonusRe(8,1);
		assertFalse(tessera.equals(tessera2));
	}
	
	@Test
	public void testEqualsDiversoOrdine(){
		tessera=new TesseraBonusRe(10,1);
		tessera2=new TesseraBonusRe(10,2);
		assertFalse(tessera.equals(tessera2));
	}

}
