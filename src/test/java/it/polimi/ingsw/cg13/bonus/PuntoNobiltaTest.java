package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;


import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.percorsi.PercorsoNobilta;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class PuntoNobiltaTest {
	
	PuntoNobilta ricompensa;
	PuntoNobilta ricompensa2;
	static Giocatore gReale;
	static Partita partita=Mockito.mock(Partita.class);
	static PercorsoNobilta percorso=Mockito.mock(PercorsoNobilta.class);
	static CasellaNobilta casella1=new CasellaNobilta(0);
	static CasellaNobilta casella2=new CasellaNobilta(1);

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gReale= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(0),new CasellaRicchezza(10));
		Mockito.when(partita.getPercorsoNobilta()).thenReturn(percorso);
		Mockito.when(percorso.getCasella(0)).thenReturn(casella1);
		Mockito.when(percorso.getCasella(1)).thenReturn(casella2);
		
	}

	@Test
	public void testHashCodeUguali() {
		ricompensa=new PuntoNobilta(3);
		ricompensa2=new PuntoNobilta(3);
		assertTrue(ricompensa.hashCode()==ricompensa2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi() {
		ricompensa=new PuntoNobilta(3);
		ricompensa2=new PuntoNobilta(1);
		assertFalse(ricompensa.hashCode()==ricompensa2.hashCode());
	}


	@Test
	public void testAssegnaRicompensa() {
		ricompensa=new PuntoNobilta(1);
		ricompensa.assegnaRicompensa(gReale, partita);
		assertTrue(1==gReale.getNobiltaGiocatore());
	}

	@Test
	public void testGetQuantita() {
		ricompensa=new PuntoNobilta(3);
		assertTrue(3==ricompensa.getQuantita());
	}

	@Test
	public void testEqualsUguali() {
		ricompensa=new PuntoNobilta(3);
		ricompensa2=new PuntoNobilta(3);
		assertTrue(ricompensa.equals(ricompensa2));
	}
	
	@Test
	public void testEqualsStessoReference() {
		ricompensa=new PuntoNobilta(3);
		assertTrue(ricompensa.equals(ricompensa));
	}
	
	@Test
	public void testEqualsDiversi() {
		ricompensa=new PuntoNobilta(3);
		ricompensa2=new PuntoNobilta(2);
		assertFalse(ricompensa.equals(ricompensa2));
	}

	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePuntoNobiltaNegativo() {
		ricompensa=new PuntoNobilta(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePuntoNobiltaZero() {
		ricompensa=new PuntoNobilta(0);
	}
	
	@Test
	public void testCostruttorePuntoNobiltaCorretto() {
		ricompensa=new PuntoNobilta(3);
		assertTrue(ricompensa!=null && 3==ricompensa.getQuantita());
	}

	@Test
	public void testToString() {
		ricompensa=new PuntoNobilta(2);
		assertEquals("PuntoNobilta (2)",ricompensa.toString());
	}

	@Test
	public void testCheckGiveNobilta() {
		ricompensa=new PuntoNobilta(2);
		assertTrue(ricompensa.checkGiveNobilta());
	}

}
