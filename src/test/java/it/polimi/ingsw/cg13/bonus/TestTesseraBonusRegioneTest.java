package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;


import java.util.HashSet;
import java.util.Set;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.mutatio.EmporioMutatio;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class TestTesseraBonusRegioneTest {
	
	TesseraBonusRegione tessera;
	TesseraBonusRegione tessera2;
	static Giocatore g = new Giocatore("Marco", new Colore(123), 0, 0);
	static Regione regione;
	static Regione regione2;
	static EmporioMutatio change;
	static Citta citta1 = Mockito.mock(Citta.class);
	static Citta citta2 = Mockito.mock(Citta.class);
	static Set<Citta> cittas = new HashSet<>();
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		regione =  Mockito.mock(Regione.class);
		Mockito.when(regione.getNome()).thenReturn("Lombardia");
		
		regione2 =  Mockito.mock(Regione.class);
		
		change = Mockito.mock(EmporioMutatio.class);
		Mockito.when(change.getGiocatore()).thenReturn(g);
		
		cittas.add(citta1);
		cittas.add(citta2);
	}

	@Before
	public void setUp() throws Exception {
		tessera= new TesseraBonusRegione(1, regione);
		
	}

	@Test
	public void testIsAssegnabile() {
		
		Mockito.when(citta1.hasEmporio(g)).thenReturn(true);
		Mockito.when(citta2.hasEmporio(g)).thenReturn(true);

		assertTrue(tessera.isAssegnabile(change));
		
	}
	
	@Test
	public void testIsAssegnabileSHoulBeFalseIfThePlayerHasNotAnEmporioInAllTheCittaInTheRegione() {

		Mockito.when(citta1.hasEmporio(g)).thenReturn(false);
		Mockito.when(citta2.hasEmporio(g)).thenReturn(false);

		Mockito.when(regione.getCitta()).thenReturn(cittas);

		assertFalse(tessera.isAssegnabile(change));
		
	}

	@Test
	public void testToString() {
		assertEquals("TesseraBonusRegione (regione=Lombardia, puntiVittoria=1)", this.tessera.toString());
	}

	@Test
	public void testTesseraBonusRegione() {
		assertEquals("Lombardia", tessera.getRegione().getNome());
	}

	@Test
	public void testGetRegione() {
		assertEquals("Lombardia",tessera.getRegione().getNome());
	}
	
	@Test
	public void testTesseraBonus() {
		assertFalse("Assegnato e' falso", tessera.isAssegnato());
		assertNull(tessera.getGiocatore());
		assertEquals(1, tessera.getPunteggioCaselleVittoria());
	}

	@Test
	public void testAssegnaBonus() {		
		tessera.assegnaBonus(change);
		
		assertTrue(tessera.isAssegnato());
		assertEquals(g, tessera.getGiocatore());
		
	}

	@Test
	public void testIsAssegnato() {
		tessera.setAssegnato(true);
		assertTrue("Assegnato vero: assegnato",tessera.isAssegnato());
		tessera.setAssegnato(false);
		assertTrue("Assegnato falso: non assegnato",!tessera.isAssegnato());
	}

	@Test
	public void testSetAssegnato() {
		tessera.setAssegnato(false);
		assertTrue(!tessera.isAssegnato());
		tessera.setAssegnato(true);
		assertTrue(tessera.isAssegnato());
	}

	@Test
	public void testUpdate() {
		Mockito.when(citta1.hasEmporio(g)).thenReturn(true);
		Mockito.when(citta2.hasEmporio(g)).thenReturn(true);
	//	GruppoBonusRe gruppoBR = Mockito.mock(GruppoBonusRe.class);
	//	tessera.registerObserver(gruppoBR);
		
		
		tessera.update(change);
		testAssegnaBonus();
	//	TesseraBonusMutatio mutatio = new TesseraBonusMutatio(tessera);
	//	Mockito.verify(gruppoBR, Mockito.times(1)).update(mutatio);
		
	}

	@Test
	public void testGetGiocatore() {
		assertNull("Giocatore is null", tessera.getGiocatore());
	}

	@Test
	public void testSetGiocatore() {
		tessera.setGiocatore(g);
		assertEquals(g, tessera.getGiocatore());
		
	}

	@Test
	public void testGetPunteggioCaselleVittoria() {
		assertEquals(1, this.tessera.getPunteggioCaselleVittoria());
	}
	
	@Test
	public void testHashCodeUguale(){
		tessera2=new TesseraBonusRegione(1,regione);
		assertTrue(tessera.hashCode()==tessera2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi(){
		tessera2=new TesseraBonusRegione(4,regione);
		assertFalse(tessera.hashCode()==tessera2.hashCode());
	}

	@Test
	public void testEqualsUguali(){
		tessera2=new TesseraBonusRegione(1,regione);
		assertTrue(tessera.equals(tessera2));
	}
	
	@Test
	public void testEqualsStessoReference(){
		assertTrue(tessera.equals(tessera));
	}
	
	
	@Test
	public void testEqualsDiversoPunteggio(){
		tessera2=new TesseraBonusRegione(3,regione);
		assertFalse(tessera.equals(tessera2));
	}
	
	@Test
	public void testEqualsDiversaRegione(){
		tessera2=new TesseraBonusRegione(1,regione2);
		assertFalse(tessera.equals(tessera2));
	}
}
