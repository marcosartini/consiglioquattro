package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.machinestate.StateWBDue;
import it.polimi.ingsw.cg13.machinestate.StateWBQuattro;
import it.polimi.ingsw.cg13.machinestate.StateWBTre;
import it.polimi.ingsw.cg13.machinestate.StateWBUno;
import it.polimi.ingsw.cg13.mappa.Mappa;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class OttieniPermessoTest {

	OttieniPermesso ricompensa=new OttieniPermesso();
	OttieniPermesso ricompensa2=new OttieniPermesso();
	RiutilizzaBonusPermesso ricompensaErrata=new RiutilizzaBonusPermesso();
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	static ArrayList<Giocatore> giocatoriPartita=new ArrayList<>();
	static OggettoBonusInput permessoDaTrovare;
	static OggettoBonusInput permessoDaTrovare2;
	static Partita modelloSenzaCartePermesso=Mockito.mock(Partita.class);
	static ArrayList<PermessoCostruzione> arrayCarteVuoto=new ArrayList<>();
	static MazzoPermessi mazzoVuoto=Mockito.mock(MazzoPermessi.class);
	static Partita modello;
	static Mappa mappa=Mockito.mock(Mappa.class);
	static HashSet<Regione> regioni=new HashSet<>();
	static Regione regione=Mockito.mock(Regione.class);
	static StateUno statoUno;
	static StateDue statoDue;
	static StateTre statoTre;
	static StateQuattro statoQuattro;
	static StateQuattro statoQuattro2;
	static ArrayList<RicompensaSpecial> ricompense1=new ArrayList<>();
	static ArrayList<RicompensaSpecial> ricompense2=new ArrayList<>();
	static ArrayList<RicompensaSpecial> ricompensePrima=new ArrayList<>();
	static ArrayList<Giocatore> giocatori=new ArrayList<Giocatore>();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		regioni.add(regione);
		ricompense1.add(new OttieniPermesso());
		Mockito.when(modelloSenzaCartePermesso.getMappaPartita()).thenReturn(mappa);	
		Mockito.when(mappa.getRegioni()).thenReturn(regioni);
		Mockito.when(regione.getMazzoPermesso()).thenReturn(mazzoVuoto);
		Mockito.when(mazzoVuoto.getCarteInCima(0)).thenReturn(arrayCarteVuoto);
		Mockito.when(mazzoVuoto.getMazzoCarte()).thenReturn(arrayCarteVuoto);
		g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(0),new CasellaRicchezza(10));
		giocatoriPartita.add(g1);
		giocatoriPartita.add(g2);
		modello=new Partita(giocatoriPartita);
		modello.startTurnoPartita();
		statoUno=new StateUno(giocatori,g1,ricompense1);
		statoDue=new StateDue(giocatori,g1,ricompense1);
		statoTre=new StateTre(giocatori,g1,ricompense1);
		statoQuattro=new StateQuattro(giocatori,g1,ricompense1);
		statoQuattro2=new StateQuattro(giocatori,g1,ricompense2);
		Mockito.when(modelloSenzaCartePermesso.getStatoPartita()).thenReturn(statoQuattro2);
		permessoDaTrovare=modello.getRegione("mare").getMazzoPermesso().getCarta(0,2);
		permessoDaTrovare2=modello.getRegione("collina").getMazzoPermesso().getCarta(1,2);
	}

	@Test
	public void testHashCodeUguale() {
		assertTrue(ricompensa.hashCode()==ricompensa2.hashCode());
	}
	
	@Test
	public void testHashCodeDiverso() {
		assertFalse(ricompensa.hashCode()==ricompensaErrata.hashCode());
	}

	@Test
	public void testEqualsObjectUguali() {
		assertTrue(ricompensa.equals(ricompensa2));
	}

	@Test
	public void testEqualsObjectDiversi() {
		assertFalse(ricompensa.equals(ricompensaErrata));
	}
	
	@Test
	public void testCheckBonusTrue() {
		assertTrue(ricompensa.checkBonus(g1, modello));
	}
	
	@Test
	public void testCheckBonusFalse() {
		assertFalse(ricompensa.checkBonus(g1, modelloSenzaCartePermesso));
	}

	@Test
	public void testToString() {
		assertEquals("OttieniPermessoCostruzione",ricompensa.toString());
	}

	@Test
	public void testAssegnaBonusSceltoAssegnato() throws CdQException {
		ricompensa.assegnaBonusScelto(permessoDaTrovare2, g1, modello);
		assertTrue(g1.getPermessiCostruzione().contains(permessoDaTrovare2)
				&& !modello.getMappaPartita().getRegioni().stream()
				.anyMatch(regione->regione.getMazzoPermesso().getMazzoCarte().contains(permessoDaTrovare2)));
	}
	
	@Test
	public void testAssegnaBonusSceltoWithException(){
		try {
			ricompensa.assegnaBonusScelto(permessoDaTrovare2, g1, modelloSenzaCartePermesso);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testUpdateFromServerWithException() {
		try {
			assertNotEquals(permessoDaTrovare,ricompensa.updateFromServer(g1,modelloSenzaCartePermesso, permessoDaTrovare));
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testUpdateFromServerTrovato() throws ErrorUpdateServerException {
		assertEquals(permessoDaTrovare,ricompensa.updateFromServer(g1,modello, permessoDaTrovare));	
	}

	@Test
	public void testCercaByBonusInputException() {
		try {
			assertNotEquals(permessoDaTrovare,ricompensa.cercaByBonusInput(modelloSenzaCartePermesso, permessoDaTrovare));
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCercaByBonusInputPermessoTrovato() throws ErrorUpdateServerException {
		assertEquals(permessoDaTrovare,ricompensa.cercaByBonusInput(modello, permessoDaTrovare));
	}

	@Test
	public void testAssegnaRicompensa() {
		ricompensa.assegnaRicompensa(g1, modelloSenzaCartePermesso);
		assertEquals(statoQuattro2.getRicompense(),ricompense1);
	}

	@Test
	public void testGetQuantita() {
		assertTrue(ricompensa.getQuantita()==1);
	}

	@Test
	public void testVisitStateUnoPartita() {
		assertTrue(new StateWBUno(giocatori,g1,ricompense1).equals(ricompensa.visit(statoUno, modello)));
	}


	@Test
	public void testVisitStateDuePartita() {
		assertEquals(new StateWBDue(giocatori,g1,ricompense1),ricompensa.visit(statoDue, modello));
	}

	@Test
	public void testVisitStateTrePartita() {
		assertEquals(new StateWBTre(giocatori,g1,ricompense1),ricompensa.visit(statoTre, modello));
	}

	@Test
	public void testVisitStateQuattroPartita() {
		assertEquals(new StateWBQuattro(giocatori,g1,ricompense1),ricompensa.visit(statoQuattro, modello));
	}

}
