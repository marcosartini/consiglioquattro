package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.Mappa;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class RiutilizzaBonusCittaTest {

	RiutilizzaBonusCitta ricompensa=new RiutilizzaBonusCitta();
	RiutilizzaBonusCitta ricompensa2=new RiutilizzaBonusCitta();
	RiutilizzaBonusPermesso ricompensaErrata=new RiutilizzaBonusPermesso();
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	static ArrayList<Giocatore> giocatoriPartita=new ArrayList<>();
	static OggettoBonusInput cittaDaTrovare;
	static Partita modello2=Mockito.mock(Partita.class);
	static Partita modello3=Mockito.mock(Partita.class);
	static Partita modello;
	static Mappa mappa=Mockito.mock(Mappa.class);
	static Mappa mappa2=Mockito.mock(Mappa.class);
	static HashSet<Ricompensa> ricompense2=new HashSet<>();
	static HashSet<Ricompensa> ricompense3=new HashSet<>();
	static ArrayList<NodoCitta> listCitta=new ArrayList<>();
	static ArrayList<NodoCitta> listCitta2=new ArrayList<>();
	static NodoCitta nodo1=Mockito.mock(NodoCitta.class);
	static NodoCitta nodo2=Mockito.mock(NodoCitta.class);
	static Citta citta1=Mockito.mock(Citta.class);
	static Citta citta2=Mockito.mock(Citta.class);


	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(0),new CasellaRicchezza(10));
		Mockito.when(modello2.getMappaPartita()).thenReturn(mappa);
		listCitta.add(nodo1);
		Mockito.when(mappa.getGrafo()).thenReturn(listCitta);
		Mockito.when(nodo1.getCittaNodo()).thenReturn(citta1);
		Mockito.when(citta1.hasEmporio(g1)).thenReturn(true);
		Mockito.when(citta1.getRicompense()).thenReturn(ricompense2);
		
		ricompense3.add(new PuntoNobilta(2));
		Mockito.when(modello3.getMappaPartita()).thenReturn(mappa2);
		listCitta2.add(nodo2);
		Mockito.when(mappa2.getGrafo()).thenReturn(listCitta2);
		Mockito.when(nodo2.getCittaNodo()).thenReturn(citta2);
		Mockito.when(citta2.hasEmporio(g1)).thenReturn(true);
		Mockito.when(citta2.getRicompense()).thenReturn(ricompense3);
		giocatoriPartita.add(g1);
		giocatoriPartita.add(g2);
		modello=new Partita(giocatoriPartita);
		modello.startTurnoPartita();
		cittaDaTrovare=modello.getMappaPartita().getGrafo().get(0).getCittaNodo();
	}

	@Test
	public void testHashCodeUguale() {
		assertTrue(ricompensa.hashCode()==ricompensa2.hashCode());
	}
	
	@Test
	public void testHashCodeDiverso() {
		assertFalse(ricompensa.hashCode()==ricompensaErrata.hashCode());
	}

	@Test
	public void testEqualsObjectUguali() {
		assertTrue(ricompensa.equals(ricompensa2));
	}

	@Test
	public void testEqualsObjectDiversi() {
		assertFalse(ricompensa.equals(ricompensaErrata));
	}
	
	@Test
	public void testCheckBonusFalseCittaGiocatore() {
		assertFalse(ricompensa.checkBonus(g1, modello));
	}
	
	@Test
	public void testCheckBonusFalseCittaGiveNobilta() {
		assertFalse(ricompensa.checkBonus(g1, modello3));
	}
	
	@Test
	public void testCheckBonusTrue() {
		assertTrue(ricompensa.checkBonus(g1, modello2));
	}

	@Test
	public void testToString() {
		assertEquals("RiutilizzaBonusCitta",ricompensa.toString());
	}

	@Test
	public void testAssegnaBonusSceltoAssegnato() {
		ricompensa.assegnaBonusScelto(citta1, g1, modello2);
	}
	
	@Test
	public void testUpdateFromServerWithExceptionNobilta() {
		try {
			assertEquals(citta2,ricompensa.updateFromServer(g1, modello3, citta2));
		} catch (CdQException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testUpdateFromServerWithExceptionNonHaEmpori() {
		try {
			assertEquals(cittaDaTrovare,ricompensa.updateFromServer(g1, modello, cittaDaTrovare));
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	

	@Test
	public void testUpdateFromServerTrovato() throws CdQException {
		assertEquals(citta1,ricompensa.updateFromServer(g1, modello2, citta1));
	}
	
	
}
