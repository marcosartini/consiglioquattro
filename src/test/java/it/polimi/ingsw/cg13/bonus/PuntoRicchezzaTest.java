package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class PuntoRicchezzaTest {

	PuntoRicchezza ricompensa;
	PuntoRicchezza ricompensa2;
	static Giocatore gReale;
	static Partita partita=Mockito.mock(Partita.class);
	static PercorsoRicchezza percorso=Mockito.mock(PercorsoRicchezza.class);
	static CasellaRicchezza casella1=new CasellaRicchezza(0);
	static CasellaRicchezza casella2=new CasellaRicchezza(1);

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gReale= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,new CasellaVittoria(0),new CasellaNobilta(0),new CasellaRicchezza(0));
		Mockito.when(partita.getPercorsoRicchezza()).thenReturn(percorso);
		Mockito.when(percorso.getCasella(0)).thenReturn(casella1);
		Mockito.when(percorso.getCasella(1)).thenReturn(casella2);
		
	}

	@Test
	public void testHashCodeUguali() {
		ricompensa=new PuntoRicchezza(3);
		ricompensa2=new PuntoRicchezza(3);
		assertTrue(ricompensa.hashCode()==ricompensa2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi() {
		ricompensa=new PuntoRicchezza(3);
		ricompensa2=new PuntoRicchezza(1);
		assertFalse(ricompensa.hashCode()==ricompensa2.hashCode());
	}


	@Test
	public void testAssegnaRicompensa() {
		ricompensa=new PuntoRicchezza(1);
		ricompensa.assegnaRicompensa(gReale, partita);
		assertTrue(1==gReale.getRicchezzaGiocatore());
	}

	@Test
	public void testGetQuantita() {
		ricompensa=new PuntoRicchezza(3);
		assertTrue(3==ricompensa.getQuantita());
	}

	@Test
	public void testEqualsUguali() {
		ricompensa=new PuntoRicchezza(3);
		ricompensa2=new PuntoRicchezza(3);
		assertTrue(ricompensa.equals(ricompensa2));
	}
	
	@Test
	public void testEqualsStessoReference() {
		ricompensa=new PuntoRicchezza(3);
		assertTrue(ricompensa.equals(ricompensa));
	}
	
	@Test
	public void testEqualsDiversi() {
		ricompensa=new PuntoRicchezza(3);
		ricompensa2=new PuntoRicchezza(2);
		assertFalse(ricompensa.equals(ricompensa2));
	}

	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePuntoRicchezzaNegativo() {
		ricompensa=new PuntoRicchezza(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttorePuntoRicchezzaZero() {
		ricompensa=new PuntoRicchezza(0);
	}
	
	@Test
	public void testCostruttorePuntoRicchezzaCorretto() {
		ricompensa=new PuntoRicchezza(3);
		assertTrue(ricompensa!=null && 3==ricompensa.getQuantita());
	}

	@Test
	public void testToString() {
		ricompensa=new PuntoRicchezza(2);
		assertEquals("PuntoRicchezza (2)",ricompensa.toString());
	}

	


}
