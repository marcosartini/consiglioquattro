package it.polimi.ingsw.cg13.bonus;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mutatio.EmporioMutatio;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class TesseraBonusGruppoCittaTest {
	
	TesseraBonusGruppoCitta tessera;
	TesseraBonusGruppoCitta tessera2;
	
	static Giocatore g1=Mockito.mock(Giocatore.class);
	
	static EmporioMutatio change=Mockito.mock(EmporioMutatio.class);
	
	static Colore colore1=Mockito.mock(Colore.class);
	static Colore colore2=Mockito.mock(Colore.class);
	
	static HashSet<Citta> gruppoCitta1=new HashSet<>();
	static Citta citta1=Mockito.mock(Citta.class);
	static Citta citta2=Mockito.mock(Citta.class);
	static HashSet<Citta> gruppoCitta2=new HashSet<>();
	static Citta citta3=Mockito.mock(Citta.class);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Mockito.when(citta1.hasEmporio(g1)).thenReturn(true);
		Mockito.when(citta2.hasEmporio(g1)).thenReturn(true);
		
		Mockito.when(citta3.hasEmporio(g1)).thenReturn(false);
		
		Mockito.when(change.getGiocatore()).thenReturn(g1);
		
		Mockito.when(colore1.toString()).thenReturn("rosso");
		
		gruppoCitta1.add(citta1);
		gruppoCitta1.add(citta2);
	
		gruppoCitta2.add(citta3);
	}

	@Test
	public void testHashCodeUguali() {
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		tessera2=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		assertTrue(tessera.hashCode()==tessera2.hashCode());
	}
	
	@Test
	public void testHashCodeDiversi() {
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		tessera2=new TesseraBonusGruppoCitta(3,colore2,gruppoCitta1);
		assertFalse(tessera.hashCode()==tessera2.hashCode());
	}

	@Test
	public void testIsAssegnabileTrue() {
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		assertTrue(tessera.isAssegnabile(change));
	}
	
	@Test
	public void testIsAssegnabileFalse() {
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta2);
		assertFalse(tessera.isAssegnabile(change));
	}

	@Test
	public void testToString() {
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		assertEquals("TesseraBonusGruppoCitta (rosso, puntiVittoria=1)", this.tessera.toString());
	}

	@Test
	public void testGetColore() {
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		assertEquals(colore1,tessera.getColore());
	}
	
	@Test
	public void testEqualsUguali(){
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		tessera2=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		assertTrue(tessera.equals(tessera2));
	}
	
	@Test
	public void testEqualsStessoReference(){
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		assertTrue(tessera.equals(tessera));
	}
	
	
	@Test
	public void testEqualsDiversoPunteggio(){
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		tessera2=new TesseraBonusGruppoCitta(3,colore1,gruppoCitta1);
		assertFalse(tessera.equals(tessera2));
	}
	
	@Test
	public void testEqualsDiversoColore(){
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		tessera2=new TesseraBonusGruppoCitta(1,colore2,gruppoCitta1);
		assertFalse(tessera.equals(tessera2));
	}
	
	

	@Test
	public void testCostruttoreTesseraBonusGruppoCittaCorretto() {
		tessera=new TesseraBonusGruppoCitta(1,colore1,gruppoCitta1);
		assertTrue(tessera!=null && tessera.getPunteggioCaselleVittoria()==1 && tessera.getColore().equals(colore1));
	}
	

	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreTesseraBonusGruppoCittaPunteggioNegativo() {
		tessera=new TesseraBonusGruppoCitta(-1,colore1,gruppoCitta1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreTesseraBonusGruppoCittaColoreNull() {
		tessera=new TesseraBonusGruppoCitta(1,null,gruppoCitta1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreTesseraBonusGruppoCittaCittaNull() {
		tessera=new TesseraBonusGruppoCitta(1,colore1,null);
	}


}
