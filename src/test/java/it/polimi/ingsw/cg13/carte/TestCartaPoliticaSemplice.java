package it.polimi.ingsw.cg13.carte;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class TestCartaPoliticaSemplice {

	@Test
	public void testMatchConsigliereShouldBeTrue() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		Consigliere consigliere= new Consigliere(new Colore("Rosso",255,0,0));
		assertTrue(carta.match(consigliere));		
	}
	
	@Test
	public void testMatchConsigliereShouldBeFalse() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		Consigliere consigliere= new Consigliere(new Colore("Verde",0,255,0));
		assertFalse(carta.match(consigliere));		
	}
		
	@Test
	public void testEqualsShouldBeFalse() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		CartaPoliticaSemplice carta2= new CartaPoliticaSemplice(new Colore("Verde",0,255,0));
		assertFalse(carta1.equals(carta2));				
	}
	
	@Test
	public void testEqualsShouldBeTrue() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		CartaPoliticaSemplice carta2= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		assertTrue(carta1.equals(carta2));
				
	}
	
	@Test
	public void testEqualsThrowException() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		CartaPoliticaSemplice carta2=null;
		boolean thrown=false;
		try{
			carta1.equals(carta2);
		}
		catch(IllegalArgumentException e){
			thrown=true;
		}
		assertTrue(thrown);				
	}
	
	@Test
	public void testEqualsCartaPoliticaJolly() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		CartaPoliticaJolly carta2= new CartaPoliticaJolly();
		assertFalse(carta1.equals(carta2));				
	}
	
	@Test
	public void testToStringCartaPoliticaSemplice() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		
		assertEquals(carta1.toString(),"(Rosso)");				
	}

	@Test(expected=ColoreNotFoundException.class)
	public void testInitThrowEXC() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(null);
		
						
	}

	@Test
	public void testGetCostoOK() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		
		assertEquals(0,carta1.getCosto());				
	}
	
	/*@Test(expected=IllegalArgumentException.class)
	public void testEqualsThrowEXC() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		
		carta1.equals(null);				
	}*/
	@Test
	public void testRiassegnaProprietarioOK() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		Giocatore g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		
		assertEquals(0,g1.getCartePolitiche().size());
		carta1.riassegnaAlProprietario(g1);			
		assertEquals(1,g1.getCartePolitiche().size());
	}
	
	@Test
	public void testRimuoviGiocatoreOK() throws ColoreNotFoundException{
		CartaPoliticaSemplice carta1= new CartaPoliticaSemplice(new Colore("Rosso",255,0,0));
		Giocatore g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		g1.addCartaPolitica(carta1);
	
		assertEquals(1,g1.getCartePolitiche().size());
		carta1.rimuoviDaGiocatore(g1);
		assertEquals(0,g1.getCartePolitiche().size());
	}
	
}
