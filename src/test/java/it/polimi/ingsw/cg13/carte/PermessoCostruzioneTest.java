package it.polimi.ingsw.cg13.carte;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.AiutanteAggiuntivo;
import it.polimi.ingsw.cg13.bonus.CartaPoliticaAggiuntiva;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.eccezioni.PermessoNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class PermessoCostruzioneTest {
	PermessoCostruzione perm;
	List<Citta> cittaCostruibili=Mockito.mock(List.class);;
	Set<Ricompensa> ricompense=Mockito.mock(Set.class);;
	
	@Test
	public void testInitOK() {
		Mockito.when(cittaCostruibili.size()).thenReturn(2);
		Mockito.when(ricompense.size()).thenReturn(3);
		perm= new PermessoCostruzione(cittaCostruibili,ricompense);
		assertEquals(2,perm.getListaCitta().size());
		assertEquals(3,perm.getRicompense().size());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitThrowEXCCittaNull() {		
		perm= new PermessoCostruzione(null,ricompense);		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitThrowEXCRewardNull() {		
		perm= new PermessoCostruzione(cittaCostruibili,null);		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitThrowEXCCittaEmpty() {		
		perm= new PermessoCostruzione(new ArrayList<>(),ricompense);		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitThrowEXCREwardEmpty() {		
		perm= new PermessoCostruzione(cittaCostruibili,new HashSet<>());		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitThrowEXCCittaContNull() {	
		List<Citta> temp=new ArrayList<>();
		temp.add(null);
		perm= new PermessoCostruzione(temp,ricompense);		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInitThrowEXCREwardContNull() {	
		Set<Ricompensa> temp=new HashSet<>();
		temp.add(null);
		perm= new PermessoCostruzione(cittaCostruibili,temp);		
	}

	@Test
	public void testGetCittaOK() throws CittaNotFoundException {
		cittaCostruibili=new ArrayList<>();
		Citta mockCity1=Mockito.mock(Citta.class);
		Mockito.when(mockCity1.getNome()).thenReturn("Milano");
		Citta mockCity2=Mockito.mock(Citta.class);
		Mockito.when(mockCity2.getNome()).thenReturn("Napoli");
		
		cittaCostruibili.add(mockCity1);
		cittaCostruibili.add(mockCity2);
		perm= new PermessoCostruzione(cittaCostruibili,ricompense);
		assertEquals(2,perm.getListaCitta().size());
		assertEquals("Milano",perm.getCitta(0).getNome());
		assertEquals("Napoli",perm.getCitta("Napoli").getNome());
	}
	
	@Test(expected=CittaNotFoundException.class)
	public void testGetCittaThrowEXC() throws CittaNotFoundException {
		cittaCostruibili=new ArrayList<>();
		Citta mockCity1=Mockito.mock(Citta.class);
		Mockito.when(mockCity1.getNome()).thenReturn("Milano");
		Citta mockCity2=Mockito.mock(Citta.class);
		Mockito.when(mockCity2.getNome()).thenReturn("Napoli");
		
		cittaCostruibili.add(mockCity1);
		cittaCostruibili.add(mockCity2);
		perm= new PermessoCostruzione(cittaCostruibili,ricompense);
		
		perm.getCitta("Livorno");
	}
	
	@Test
	public void testAssegnaRicompenseOK() throws FileSyntaxException{
		ricompense= new HashSet<>();
		ricompense.add(new AiutanteAggiuntivo());
		ricompense.add(new CartaPoliticaAggiuntiva());
		Partita mockPart=new Partita();
		Giocatore g1= new Giocatore("Mario",Mockito.mock(Colore.class),10,10);
		
		perm= new PermessoCostruzione(cittaCostruibili,ricompense);
		perm.assegnaRicompense(g1, mockPart);
		assertEquals(1,g1.getCartePolitiche().size());
		assertEquals(1,g1.getAiutanti().size());
	}
	
	@Test
	public void testToStringOK(){	
		cittaCostruibili= new ArrayList<>();
		cittaCostruibili.add(new Citta(new Colore("rosso",255,0,0),"Milano",new HashSet<>(),new HashSet<>()));
		ricompense= new HashSet<>();
		ricompense.add(new AiutanteAggiuntivo());
		perm= new PermessoCostruzione(cittaCostruibili,ricompense);		
		
		assertEquals("PermessoCostruzione { cittaCostruibili=(Milano, ), ricompense=[AiutanteAggiuntivo (1)]",perm.toString());
		
	}
	
	@Test
	public void testAcquistoPerdiOK() throws PermessoNotFoundException{	
		Giocatore g1= new Giocatore("Mario",Mockito.mock(Colore.class),10,10);
		perm= new PermessoCostruzione(cittaCostruibili,ricompense);		
		perm.acquistaDaMarket(g1);
		assertEquals(1,g1.getPermessiCostruzione().size());
		perm.rimuoviDaGiocatore(g1);
		assertEquals(0,g1.getPermessiCostruzione().size());
		
	}
	
	@Test
	public void testRiassegnaAlProprietarioOK(){	
		Giocatore g1= new Giocatore("Mario",Mockito.mock(Colore.class),10,10);
		int numCarteP=g1.getCartePermessoUsate().size()+g1.getPermessiCostruzione().size();
		perm= new PermessoCostruzione(cittaCostruibili,ricompense);		
		perm.riassegnaAlProprietario(g1);
		assertEquals(numCarteP+1,g1.getCartePermessoUsate().size()+g1.getPermessiCostruzione().size());
		
	}
	
	@Test
	public void testAssegnaBonusInputOK() throws FileSyntaxException{
		ricompense= new HashSet<>();
		ricompense.add(new AiutanteAggiuntivo());
		ricompense.add(new CartaPoliticaAggiuntiva());
		Partita mockPart=new Partita();
		Giocatore g1= new Giocatore("Mario",Mockito.mock(Colore.class),10,10);
		
		perm= new PermessoCostruzione(cittaCostruibili,ricompense);
		perm.assegnaBonusInput(g1, mockPart);
		assertEquals(1,g1.getCartePolitiche().size());
		assertEquals(1,g1.getAiutanti().size());
	}
	
	@Test
	public void testGetRicompenseOK() throws FileSyntaxException{
		ricompense= new HashSet<>();
		ricompense.add(new AiutanteAggiuntivo());
		ricompense.add(new CartaPoliticaAggiuntiva());
		Partita mockPart=new Partita();
		Giocatore g1= new Giocatore("Mario",Mockito.mock(Colore.class),10,10);
		
		perm= new PermessoCostruzione(cittaCostruibili,ricompense);
		
		assertEquals(2,perm.getRicompense().size());
		}
	
}
