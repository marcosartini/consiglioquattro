package it.polimi.ingsw.cg13.carte;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;

public class MazzoTest {

	@Test
	public void testInitMazzo() {
		
		List<PermessoCostruzione> mockCard=new ArrayList<>();
		for(int i=0;i<10;i++)
			mockCard.add(Mockito.mock(PermessoCostruzione.class));
		MazzoPermessi deckPerm=new MazzoPermessi(mockCard,2);
		
		assertEquals(10,deckPerm.sizeMazzo());
		
		List<CartaPolitica> mockPol=new ArrayList<>();
		for(int i=0;i<10;i++)
			mockPol.add(Mockito.mock(CartaPolitica.class));
		MazzoPolitico deckPol=new MazzoPolitico(new MazzoPolitico(mockPol));	
		
		assertEquals(10,deckPol.getMazzoCarte().size());
	}
	
	@Test
	public void testGetCarteInCimaOk() throws ColoreNotFoundException {
		
		List<CartaPolitica> mockPol=new ArrayList<>();
		mockPol.add(new CartaPoliticaJolly());
	
		mockPol.add(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
		mockPol.add(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));	
		
		
		MazzoPolitico deckPol=new MazzoPolitico(mockPol);
		List<CartaPolitica> tempPol=deckPol.getCarteInCima(2);		
		List<CartaPolitica> tempPol1=deckPol.getCarteInCima(3);
		
		assertEquals(2,tempPol.size());
		assertEquals(3,tempPol1.size());
		assertEquals(3,deckPol.sizeMazzo());
	}
	
	@Test
	public void testPescaCartaOk(){
		List<CartaPolitica> mockPol=new ArrayList<>();
		for(int i=0;i<10;i++){
			CartaPolitica temp=Mockito.mock(CartaPolitica.class);
			Mockito.when(temp.getColor()).thenReturn(new Colore("rosso",255,0,0));
			mockPol.add(temp);
			
		}
		MazzoPolitico deckPol=new MazzoPolitico(mockPol);
		
		assertEquals(10,deckPol.sizeMazzo());
		
		CartaPolitica tempCard=deckPol.pescaCarta();
		assertEquals("rosso",tempCard.getColor().getNome());
		
		assertEquals(9,deckPol.sizeMazzo());
		
	}
	
	@Test
	public void testSetGetMazzoOk(){
		List<CartaPolitica> mockPol=new ArrayList<>();
		for(int i=0;i<10;i++){
			CartaPolitica temp=Mockito.mock(CartaPolitica.class);
			Mockito.when(temp.getColor()).thenReturn(new Colore("rosso",255,0,0));
			mockPol.add(temp);
			
		}
		MazzoPolitico deckPol=new MazzoPolitico();
		
		assertEquals(0,deckPol.getMazzoCarte().size());
		
		deckPol.setMazzoCarte(mockPol);
		
		assertEquals(10,deckPol.getMazzoCarte().size());
	}
	
	@Test
	public void testAggiungiCartaOk(){
		List<CartaPolitica> mockPol=new ArrayList<>();
		for(int i=0;i<10;i++){
			CartaPolitica temp=Mockito.mock(CartaPolitica.class);
			Mockito.when(temp.getColor()).thenReturn(new Colore("rosso",255,0,0));
			mockPol.add(temp);
			
		}
		
		MazzoPolitico deckPol=new MazzoPolitico();	
		
		deckPol.aggiungiCarta(Mockito.mock(CartaPolitica.class));
		assertEquals(1,deckPol.sizeMazzo());
		
		deckPol.aggiungiCarta(mockPol);
		assertEquals(11,deckPol.sizeMazzo());
		
	}
	
	@Test
	public void testMazzoPermessiOk(){
		List<PermessoCostruzione> mockPol=new ArrayList<>();
		for(int i=0;i<10;i++){
			PermessoCostruzione temp=Mockito.mock(PermessoCostruzione.class);			
			mockPol.add(temp);
			
		}
		
		MazzoPermessi deckPerm=new MazzoPermessi();		
		assertEquals("MazzoPermesso:\ncarteMazzo=0",deckPerm.toString());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testMazzoPermessiThrowEXC(){
		List<PermessoCostruzione> mockPol=new ArrayList<>();
		for(int i=0;i<10;i++){
			PermessoCostruzione temp=Mockito.mock(PermessoCostruzione.class);			
			mockPol.add(temp);			
		}
		
		MazzoPermessi deckPerm=new MazzoPermessi(mockPol,-1);		
		
	}
	
	@Test
	public void testMazzoPermessiVisibiliOk(){		
		
		MazzoPermessi deckPerm=new MazzoPermessi();		
		deckPerm.setPermessiVisibili(1);
		assertEquals(1,deckPerm.getPermessiVisibili());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testMazzoPermessiVisibiliThrowEXC(){		
		
		MazzoPermessi deckPerm=new MazzoPermessi();		
		deckPerm.setPermessiVisibili(0);
		
	}	

	/*@Test
	public void testRemoveCarteOk() throws ColoreNotFoundException{		
		List<CartaPolitica> mockPol=new ArrayList<>();
		mockPol.add(new CartaPoliticaJolly());
	
		mockPol.add(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
		mockPol.add(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));	
		
		
		MazzoPolitico deckPol=new MazzoPolitico(mockPol);
		
		List<CartaPolitica> removePol=new ArrayList<>();
		removePol.add(new CartaPoliticaSemplice(new Colore("rosso",255,0,0)));
		removePol.add(new CartaPoliticaSemplice(new Colore("verde",0,255,0)));
		
		deckPol.removeCarte(removePol);
		
		assertEquals(1,deckPol.sizeMazzo());
	}*/
}
