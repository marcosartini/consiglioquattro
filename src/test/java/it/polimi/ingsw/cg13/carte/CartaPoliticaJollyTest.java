package it.polimi.ingsw.cg13.carte;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

public class CartaPoliticaJollyTest {

	CartaPoliticaJolly jolly= new CartaPoliticaJolly();
	CartaPoliticaSemplice c1;
	Colore rosso= new Colore("rosso",255,0,0);
	
	@Test
	public void testMatchConsigliere() {
		Consigliere mockCons= Mockito.mock(Consigliere.class);
		assertTrue(jolly.match(mockCons));
	}

	@Test
	public void testEqualsCartaPolitica() throws ColoreNotFoundException {
		CartaPoliticaJolly temp= new CartaPoliticaJolly();
		assertTrue(jolly.equals(temp));
		c1= new CartaPoliticaSemplice(rosso);
		
		assertFalse(jolly.equals(c1));
	}
	
	@Test
	public void testToString(){		
		assertEquals("(jolly)",jolly.toString());
	}
	
	@Test
	public void testGetCosto(){		
		assertEquals(1,jolly.getCosto());
	}
	
	@Test
	public void testGetColore(){	
		Colore col= new Colore("jolly",0,0,0);
		assertEquals(col,jolly.getColor());
	}
}
