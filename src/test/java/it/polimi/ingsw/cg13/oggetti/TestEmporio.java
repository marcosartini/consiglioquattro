package it.polimi.ingsw.cg13.oggetti;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Test;

import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class TestEmporio {

	@Test
	public void testSetGetGiocatoreAppartenenteShouldBeOk() {
		Giocatore g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Giocatore g2= new Giocatore("Marco",2,new Colore("Blu",0,0,255),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Emporio e = new Emporio(g1);
		assertEquals(e.getGiocatoreAppartenente().getNome(),"Luca");
		e.setGiocatoreAppartenente(g2);
		assertEquals(e.getGiocatoreAppartenente().getNome(),"Marco");
	}
	
	@Test
	public void testCittaPosizionatoShouldBeNull() {
		Giocatore g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		
		Emporio e = new Emporio(g1);
		assertNull(e.getCittaPosizionato());
		
	}
	
	@Test
	public void testCittaPosizionatoShouldBeok() {
		Giocatore g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Emporio e = new Emporio(g1);
		e.setCittaPosizionato(new Citta(new Colore("Verde",0,255,0),"Ancona",new HashSet<>(),new HashSet<>()));
		assertEquals(e.getCittaPosizionato().getNome(),"Ancona");
	}

	@Test
	public void testToStringShouldBeok() {
		Giocatore g1= new Giocatore("Lorenzo",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Emporio e = new Emporio(g1);
		;
		assertEquals("(giocatoreAppartenente=Lorenzo)",e.toString());
	}
	
	@Test
	public void testGetColoreShouldBeok() throws CdQException {
		Giocatore g1= new Giocatore("Lorenzo",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Emporio e = new Emporio(g1);
		;
		assertEquals(g1.getColore(),e.getColore());
	}
	
	@Test(expected=ColoreNotFoundException.class)
	public void testGetColoreThrowEXC() throws ColoreNotFoundException {
		
		Emporio e = new Emporio(null);
		
		e.getColore();
	}
}
