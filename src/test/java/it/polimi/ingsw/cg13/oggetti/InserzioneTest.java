package it.polimi.ingsw.cg13.oggetti;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg13.carte.CartaPoliticaJolly;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class InserzioneTest {

	@Test
	public void testSetGetGiocatoreShouldBeOk() {
		Giocatore g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Giocatore g2= new Giocatore("Marco",2,new Colore("Blu",0,0,255),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Inserzione i=new Inserzione(new CasellaRicchezza(10),g1,new Aiutante());
		assertEquals("Luca",i.getProprietario().getNome());
		i.setProprietario(g2);
		assertEquals("Marco",i.getProprietario().getNome());
		
	}
	
	@Test
	public void testGetPrezzoShouldBeOk() {
		Giocatore g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Inserzione i=new Inserzione(new CasellaRicchezza(10),g1,new Aiutante());
		
		assertEquals(10,i.getPrezzo().getPuntiCasella().intValue());
		
	}
	
	@Test
	public void testOggettoInVendita() {
		Giocatore g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Aiutante a=new Aiutante();
		Inserzione i=new Inserzione(new CasellaRicchezza(10),g1,new Aiutante());		
		assertEquals(a,i.getOggettoInVendita());
		
		CartaPoliticaJolly carta= new CartaPoliticaJolly();
		i.setOggettoInVendita(new CartaPoliticaJolly());
		assertEquals(carta,i.getOggettoInVendita());
		
	}
	
	@Test
	public void testIdInserzione() {
		Giocatore g1= new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Aiutante a=new Aiutante();
		Inserzione i=new Inserzione(new CasellaRicchezza(10),g1,new Aiutante());		
		
		i.setIdInserzione(10);
		assertEquals(10,i.getIdInserzione());
		
	}

}
