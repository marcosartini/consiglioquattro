package it.polimi.ingsw.cg13.oggetti;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import it.polimi.ingsw.cg13.bonus.AiutanteAggiuntivo;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.carte.CartaPoliticaSemplice;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.PermessoNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;

public class MarketTest {

	@Test
	public void testMarketAddInserzione() {
		Giocatore g1= new Giocatore("Mario",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),new CasellaRicchezza(10));
		Market m= new Market();
		g1.mettiVendita(m, new Inserzione(new CasellaRicchezza(10),g1,g1.getAiutanti().get(0)));
		assertEquals(m.getBanchetto().size(),1);
	}
	
	@Test
	public void testMarketCompraAiutante() throws PermessoNotFoundException {
		List<CasellaRicchezza> caselle=new ArrayList<>();
		for(int i=0;i<15;i++)
			caselle.add(new CasellaRicchezza(i));
		PercorsoRicchezza richPath= new PercorsoRicchezza(caselle);
		Giocatore g1= new Giocatore("Mario",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),caselle.get(1));
		Giocatore g2= new Giocatore("Luca",2,new Colore("Verde",0,255,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),caselle.get(10));
		
		Market m= new Market();
		g1.mettiVendita(m, new Inserzione( caselle.get(9),g1,g1.getAiutanti().get(1)));
		m.compraInserzione(g2, m.getBanchetto().get(0), richPath);
		
		assertEquals(g2.getRicchezzaGiocatore(), 1);
	}
	
	@Test
	public void testMarketCompraCartaPolitica() throws PermessoNotFoundException, ColoreNotFoundException {
		List<CasellaRicchezza> caselle=new ArrayList<>();
		for(int i=0;i<15;i++)
			caselle.add(new CasellaRicchezza(i));
		PercorsoRicchezza richPath= new PercorsoRicchezza(caselle);
		Giocatore g1= new Giocatore("Mario",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),caselle.get(1));
		Giocatore g2= new Giocatore("Luca",2,new Colore("Verde",0,255,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),caselle.get(10));
		g1.addCartaPolitica(new CartaPoliticaSemplice(new Colore("Verde",0,255,0)));
		Market m= new Market();
		g1.mettiVendita(m, new Inserzione(caselle.get(9),g1,g1.getCartePolitiche().get(0)) );
		m.compraInserzione(g2, m.getBanchetto().get(0), richPath);
		
		assertEquals(g2.getCartePolitiche().size(), 1);
	}
	
	@Test
	public void testMarketCompraPermessoCostruzione() throws PermessoNotFoundException {
		List<CasellaRicchezza> caselle=new ArrayList<>();
		for(int i=0;i<15;i++)
			caselle.add(new CasellaRicchezza(i));
		PercorsoRicchezza richPath= new PercorsoRicchezza(caselle);
		Giocatore g1= new Giocatore("Mario",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),caselle.get(1));
		Giocatore g2= new Giocatore("Luca",2,new Colore("Verde",0,255,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),caselle.get(10));
		
		Set<Ricompensa> tempRew= new HashSet<>();
		tempRew.add(new AiutanteAggiuntivo());
		List<Citta> tempCitta= new ArrayList<>();
		tempCitta.add(new Citta(new Colore("Rosso",255,0,0),"Milano",tempRew,new HashSet<>()));
		
		g1.accumulaPermesso(new PermessoCostruzione(tempCitta,tempRew));
		Market m= new Market();
		g1.mettiVendita(m,new Inserzione(caselle.get(9),g1, g1.getPermessiCostruzione().get(0)));
		m.compraInserzione(g2, m.getBanchetto().get(0), richPath);
		
		assertEquals(g2.getPermessiCostruzione().size(), 1);
	}
	
	@Test
	public void testRiassegnaVendibili() throws PermessoNotFoundException {
		List<CasellaRicchezza> caselle=new ArrayList<>();
		for(int i=0;i<15;i++)
			caselle.add(new CasellaRicchezza(i));
		PercorsoRicchezza richPath= new PercorsoRicchezza(caselle);
		Giocatore g1= new Giocatore("Mario",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),caselle.get(1));
		
		Set<Ricompensa> tempRew= new HashSet<>();
		tempRew.add(new AiutanteAggiuntivo());
		List<Citta> tempCitta= new ArrayList<>();
		tempCitta.add(new Citta(new Colore("Rosso",255,0,0),"Milano",tempRew,new HashSet<>()));
		
		g1.accumulaPermesso(new PermessoCostruzione(tempCitta,tempRew));
		Market m= new Market();
		g1.mettiVendita(m,new Inserzione(caselle.get(9),g1, g1.getPermessiCostruzione().get(0)));
		int numPermessi=g1.getCartePermessoUsate().size()+g1.getPermessiCostruzione().size();
		m.riassegnaVendibili();	
		assertEquals(numPermessi+1,g1.getCartePermessoUsate().size()+g1.getPermessiCostruzione().size());
	}
	
	@Test
	public void testToString(){
		List<CasellaRicchezza> caselle=new ArrayList<>();
		for(int i=0;i<15;i++)
			caselle.add(new CasellaRicchezza(i));
		PercorsoRicchezza richPath= new PercorsoRicchezza(caselle);
		Giocatore g1= new Giocatore("Mario",1,new Colore("Rosso",255,0,0),1,10,new CasellaVittoria(10),new CasellaNobilta(10),caselle.get(1));
		
		Set<Ricompensa> tempRew= new HashSet<>();
		tempRew.add(new AiutanteAggiuntivo());
		List<Citta> tempCitta= new ArrayList<>();
		tempCitta.add(new Citta(new Colore("Rosso",255,0,0),"Milano",tempRew,new HashSet<>()));
		
		g1.accumulaPermesso(new PermessoCostruzione(tempCitta,tempRew));
		Market m= new Market();
		
		assertEquals("Market: lista inserzioni\n",m.toString());
		Inserzione inser1=new Inserzione(caselle.get(1),g1, g1.getAiutanti().get(0));
		g1.mettiVendita(m,inser1);
		assertEquals("Market: lista inserzioni\nInserzione "+inser1.getIdInserzione()+":\n oggetto= Aiutante prezzo= 1 venduta da Mario\n",m.toString());
		
	}

}
