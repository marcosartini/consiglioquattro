package it.polimi.ingsw.cg13.colore;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

public class ColoreTest {
	Colore col1;	
	
	@Test
	public void testInitOK() {
		col1= new Colore("rosso",255,0,0);
		assertEquals("rosso",col1.getNome());		
	}
	
	@Test
	public void testNomeOK() {
		col1= new Colore("rosso",255,0,0);
		assertEquals("rosso",col1.getNome());	
		
		col1.setNome("pippo");
		assertEquals("pippo",col1.getNome());
	}

	
	@Test(expected=IllegalArgumentException.class)
	public void testNomeThrowExceptionEmpty() {	
		col1= new Colore("rosso",255,0,0);
		col1.setNome("");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNomeThrowExceptionNull() {	
		col1= new Colore("rosso",255,0,0);
		col1.setNome(null);
	}
}
