package it.polimi.ingsw.cg13.azioni;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayDeque;
import java.util.ArrayList;


import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.CartaPoliticaJolly;
import it.polimi.ingsw.cg13.carte.CartaPoliticaSemplice;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.ConsiglioRe;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CostruzioneConKingTest {

	CostruzioneConKing azione;
	
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	static ArrayList<CartaPolitica> carte=new ArrayList<>();
	static ArrayList<CartaPolitica> carteNo=new ArrayList<>();
	static Partita modello;
	
	static ArrayList<NodoCitta> percorsoConCittaErrata=new ArrayList<>();
	static ArrayList<NodoCitta> percorsoPiuCitta=new ArrayList<>();
	static ArrayList<NodoCitta> percorsoUnaCitta=new ArrayList<>();
	static ArrayList<NodoCitta> percorsoStessaCittaRe=new ArrayList<>();
	static Consigliere cons1;
	static Consigliere cons2;
	static Consigliere cons3;
	static Consigliere cons4;
	static Consigliere consDaInserire;
	static ArrayDeque<Consigliere> balcone=new ArrayDeque<>();
	
	static ConsiglioRe consiglio;
	
	static int costoStrade;
	
	static NodoCitta cittaMock=Mockito.mock(NodoCitta.class);
	static Citta citta=Mockito.mock(Citta.class);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),15,15,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		Mockito.when(g2.getRicchezzaGiocatore()).thenReturn(0);
		giocatori.add(g1);
		giocatori.add(g2);

		Mockito.when(cittaMock.getCittaNodo()).thenReturn(citta);
		Mockito.when(citta.getNome()).thenReturn("WAAA");
		percorsoConCittaErrata.add(cittaMock);
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
		cons1=new Consigliere(modello.getColorePolitico("ciano"));
		cons2=new Consigliere(modello.getColorePolitico("nero"));
		cons3=new Consigliere(modello.getColorePolitico("rosa"));
		cons4=new Consigliere(modello.getColorePolitico("bianco"));
		balcone.add(cons1);
		balcone.add(cons2);
		balcone.add(cons3);
		balcone.add(cons4);
		
		carteNo.add(new CartaPoliticaSemplice(modello.getColorePolitico("arancione")));
		carte.add(new CartaPoliticaSemplice(modello.getColorePolitico("rosa")));
		carte.add(new CartaPoliticaSemplice(modello.getColorePolitico("ciano")));
		carte.add(new CartaPoliticaSemplice(modello.getColorePolitico("nero")));
		carte.add(new CartaPoliticaJolly());
		g1.addCartaPolitica(new CartaPoliticaJolly());
		
		consiglio=new ConsiglioRe(balcone,modello.getKing());
		modello.getKing().setConsiglioKing(consiglio);
		
		percorsoStessaCittaRe.add(modello.getKing().getPosizione());
		percorsoUnaCitta.add(modello.getMappaPartita().getNodoCitta("Indur"));
		percorsoPiuCitta.add(modello.getMappaPartita().getNodoCitta("Indur"));
		percorsoPiuCitta.add(modello.getMappaPartita().getNodoCitta("Framek"));
		
		
		
		costoStrade=modello.getCostoStrade();
	}

	@Test
	public void testEseguiAzioneNoCorruzione() {
		azione=new CostruzioneConKing(g1, percorsoUnaCitta, carteNo);
		try {
			azione.eseguiAzione(modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testEseguiAzioneNoCash() {
		azione=new CostruzioneConKing(g2, percorsoUnaCitta, carte);
		try {
			azione.eseguiAzione(modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testEseguiAzioneCostruisciCittaOKNellaCittaDelRe() throws RemoteException, CdQException {
		azione=new CostruzioneConKing(g1,percorsoStessaCittaRe, carte);
		assertTrue(azione.eseguiAzione(modello));
		assertTrue(modello.getKing().getPosizione().equals(percorsoStessaCittaRe.get(0)));
		
	}
	
	@Test
	public void testEseguiAzioneCostruisciCittaOKPercorsoLungo() throws RemoteException, CdQException {
		azione=new CostruzioneConKing(g1,percorsoPiuCitta, carte);
		assertTrue(azione.eseguiAzione(modello));
		assertTrue(modello.getKing().getPosizione().equals(percorsoPiuCitta.get(1)));
		
	}

	@Test
	public void testUpdateParametriOK() throws ErrorUpdateServerException {
		azione=new CostruzioneConKing(g1, percorsoUnaCitta, carte);
		azione.updateParametri(modello);
	}
	
	@Test
	public void testUpdateParametriPercorsoErrato()  {
		azione=new CostruzioneConKing(g1, percorsoConCittaErrata, carte);
		try {
			azione.updateParametri(modello);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testCostruttoreCostruzioneConKingOK() {
		azione=new CostruzioneConKing(g1, percorsoUnaCitta, carte);
		assertTrue(azione!=null && azione.getCartePolitiche().equals(carte) 
				&& azione.getKingPath().equals(percorsoUnaCitta));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCostruzioneConKingPercorsoNull() {
		azione=new CostruzioneConKing(g1, null, carte);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCostruzioneConKingCarteNull() {
		azione=new CostruzioneConKing(g1, percorsoUnaCitta, null);
	}

	@Test
	public void testRisultatoAzione() throws RemoteException, CdQException {
		azione=new CostruzioneConKing(g1, percorsoUnaCitta, carte);
		int empori=g1.getNumeroEmpori();
		azione.risultatoAzione(percorsoUnaCitta.get(0), 1, modello.getPercorsoRicchezza(), 
				modello.getKing(),modello, carte);
		assertTrue(g1.getNumeroEmpori()==empori-1);
		assertTrue(percorsoUnaCitta.get(0).getCittaNodo().hasEmporio(g1));
		assertTrue(modello.getKing().getPosizione().equals(percorsoUnaCitta.get(0)));
	}

	

}
