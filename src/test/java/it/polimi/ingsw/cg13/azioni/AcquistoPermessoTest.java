package it.polimi.ingsw.cg13.azioni;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayDeque;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.OttieniPermesso;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.CartaPoliticaJolly;
import it.polimi.ingsw.cg13.carte.CartaPoliticaSemplice;
import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.machinestate.StateCinque;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateInizio;
import it.polimi.ingsw.cg13.machinestate.StateOtto;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateSei;
import it.polimi.ingsw.cg13.machinestate.StateSette;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.mappa.ConsiglioRegione;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class AcquistoPermessoTest {

	AcquistoPermesso azione;
	
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	
	static ArrayList<CartaPolitica> carte=new ArrayList<>();
	static ArrayList<CartaPolitica> carteVuote=new ArrayList<>();
	static ArrayList<CartaPolitica> carteNo=new ArrayList<>();
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	
	static ArrayList<RicompensaSpecial> ricompense=new ArrayList<>();
	
	static StateUno statoUno;
	static StateDue statoDue;
	static StateTre statoTre;
	static StateSei statoSei;
	static StateOtto statoOtto;
	static StateCinque statoCinque;
	static StateSette statoSette;
	static StateInizio statoInizio;
	
	static Consigliere cons1;
	static Consigliere cons2;
	static Consigliere cons3;
	static Consigliere cons4;
	static Consigliere consDaInserire;
	static ArrayDeque<Consigliere> balcone=new ArrayDeque<>();
	
	static Partita modello;
	
	static Regione mare;
	static ConsiglioRegione consiglio;
	static MazzoPermessi mazzo;
	static PermessoCostruzione permesso;
	static PermessoCostruzione permesso2;
	static PermessoCostruzione permessoMock=Mockito.mock(PermessoCostruzione.class);
	
	static Regione regioneMock=Mockito.mock(Regione.class);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,3,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		Mockito.when(g2.getRicchezzaGiocatore()).thenReturn(0);
		giocatori.add(g1);
		giocatori.add(g2);
		ricompense.add(new OttieniPermesso());
		statoUno=new StateUno(giocatori,g1,ricompense);
		statoDue=new StateDue(giocatori,g1,ricompense);
		statoTre=new StateTre(giocatori,g1,ricompense);
		statoCinque=new StateCinque(giocatori,g1,ricompense);
		statoSei=new StateSei(giocatori,g1,ricompense);
		statoSette=new StateSette(giocatori,g1,ricompense);
		statoOtto=new StateOtto(giocatori,g1,ricompense);
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
		cons1=new Consigliere(modello.getColorePolitico("ciano"));
		cons2=new Consigliere(modello.getColorePolitico("nero"));
		cons3=new Consigliere(modello.getColorePolitico("rosa"));
		cons4=new Consigliere(modello.getColorePolitico("bianco"));
		balcone.add(cons1);
		balcone.add(cons2);
		balcone.add(cons3);
		balcone.add(cons4);
		
		carteNo.add(new CartaPoliticaSemplice(modello.getColorePolitico("arancione")));
		carte.add(new CartaPoliticaSemplice(modello.getColorePolitico("rosa")));
		carte.add(new CartaPoliticaSemplice(modello.getColorePolitico("ciano")));
		carte.add(new CartaPoliticaSemplice(modello.getColorePolitico("nero")));
		carte.add(new CartaPoliticaJolly());
		g1.addCartaPolitica(new CartaPoliticaJolly());
		
		
		mare=modello.getRegione("mare");
		consiglio=new ConsiglioRegione(balcone,mare);
		modello.getRegione("mare").setConsiglio(consiglio);
		mare.setConsiglio(consiglio);
		mazzo=mare.getMazzoPermesso();
		permesso=mazzo.getCarteInCima(2).get(0);
		permesso2=mazzo.getCarteInCima(2).get(1);
		
		Mockito.when(regioneMock.getConsiglio()).thenReturn(consiglio);
		Mockito.when(regioneMock.getMazzoPermesso()).thenReturn(mazzo);
		
	}

	@Test
	public void testEseguiAzione() {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		try {
			assertTrue(azione.eseguiAzione(modello));
		} catch (RemoteException | CdQException e) {
		
		}
	}
	
	@Test
	public void testEseguiAzioneCarteNonVannoBene() {
		azione=new AcquistoPermesso(g1,carteNo,mare,permesso);
		try {
			azione.eseguiAzione(modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testEseguiAzioneCarteNonHaiSoldi() {
		azione=new AcquistoPermesso(g2,carte,mare,permesso);
		try {
			azione.eseguiAzione(modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testUpdateParametriRegioneNotFound() {
		azione=new AcquistoPermesso(g1,carte,regioneMock,permesso2);
		try {
			azione.updateParametri(modello);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testUpdateParametripermessoNotFound() {
		azione=new AcquistoPermesso(g1,carte,mare,permessoMock);
		try {
			azione.updateParametri(modello);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testUpdateParametri() throws ErrorUpdateServerException {
		azione=new AcquistoPermesso(g1,carte,mare,permesso2);
		azione.updateParametri(modello);
		assertEquals(azione.getRegione(),mare);
		assertEquals(azione.getPermessoScelto(),permesso2);
	}

	@Test
	public void testCostruttoreAcquistoPermessoOK() {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertTrue(azione!=null && azione.getCartePolitiche().equals(carte)
				&& azione.getRegione().equals(mare) && azione.getPermessoScelto().equals(permesso));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreAcquistoPermessoCarteNull(){
		azione=new AcquistoPermesso(g1,null,mare,permesso);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreAcquistoPermessRegioneNull(){
		azione=new AcquistoPermesso(g1,carte,null,permesso);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreAcquistoPermessoPermessoNull(){
		azione=new AcquistoPermesso(g1,carte,mare,null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreAcquistoPermessoCarteVuote(){
		azione=new AcquistoPermesso(g1,carteVuote,mare,permesso);
	}
	
	@Test
	public void testRisultatoAzione() {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		ArrayList<CartaPolitica> carteRiscontrate=new ArrayList<>();
		carteRiscontrate.add(new CartaPoliticaJolly());
		azione.risultatoAzione(modello, 0, carteRiscontrate);
		assertTrue(g1.getPermessiCostruzione().contains(permesso));
		assertTrue(!mazzo.getMazzoCarte().contains(permesso));
		
	}

	@Test
	public void testCorrompiConsiglio() {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		ArrayList<CartaPolitica> carteRiscontrate=new ArrayList<>();
		int costo=azione.corrompiConsiglio(carteRiscontrate);
		assertTrue(costo==1);
		assertTrue(carteRiscontrate.equals(carte));
	}

	@Test
	public void testMatchCartaConsigliere() {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		ArrayList<CartaPolitica> carteRiscontrate=new ArrayList<>();
		assertTrue(azione.matchCartaConsigliere(carte.get(0), carteRiscontrate));
		assertEquals(carte.get(0),carteRiscontrate.get(0));
	}
	
	@Test
	public void testNessunMatchCartaConsigliere() throws ColoreNotFoundException {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		ArrayList<CartaPolitica> carteRiscontrate=new ArrayList<>();
		CartaPolitica carta=new CartaPoliticaSemplice(new Colore("rosso",255,0,0));
		assertFalse(azione.matchCartaConsigliere(carta, carteRiscontrate));
		assertTrue(carteRiscontrate.isEmpty());
		
	}

	@Test
	public void testGetCartePolitiche() {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(azione.getCartePolitiche(),carte);
	}

	@Test
	public void testGetRegione() {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(azione.getRegione(),mare);
	}

	@Test
	public void testGetPermessoScelto() {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(azione.getPermessoScelto(),permesso);
	}

	@Test
	public void testGetMazzoPermessi() {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(azione.getMazzoPermessi(),mazzo);
	}

	@Test
	public void testVisitStateInizioPartita() throws RemoteException, CdQException {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(new StateUno(giocatori,g1,ricompense),azione.visit(statoInizio, modello));
	}

	@Test
	public void testVisitStateOttoPartita() throws RemoteException, CdQException {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(new StateQuattro(giocatori,g1,ricompense),azione.visit(statoOtto, modello));
	}

	@Test
	public void testVisitStateDuePartita() throws RemoteException, CdQException {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(new StateUno(giocatori,g1,ricompense),azione.visit(statoDue, modello));
	}

	@Test
	public void testVisitStateSeiPartita() throws RemoteException, CdQException {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(new StateTre(giocatori,g1,ricompense),azione.visit(statoSei, modello));
	}

	@Test
	public void testVisitStateTrePartita() throws RemoteException, CdQException {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(new StateQuattro(giocatori,g1,ricompense),azione.visit(statoTre, modello));
	}

	@Test
	public void testVisitStateCinquePartita() throws RemoteException, CdQException {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(new StateQuattro(giocatori,g1,ricompense),azione.visit(statoCinque, modello));
	}

	@Test
	public void testVisitStateSettePartita() throws RemoteException, CdQException {
		azione=new AcquistoPermesso(g1,carte,mare,permesso);
		assertEquals(new StateTre(giocatori,g1,ricompense),azione.visit(statoSette, modello));
	}

}
