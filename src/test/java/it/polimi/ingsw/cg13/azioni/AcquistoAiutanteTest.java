package it.polimi.ingsw.cg13.azioni;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.OttieniPermesso;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoFine;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoInizio;
import it.polimi.ingsw.cg13.machinestate.StateCinque;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateFine;
import it.polimi.ingsw.cg13.machinestate.StateFineVendita;
import it.polimi.ingsw.cg13.machinestate.StateInizio;
import it.polimi.ingsw.cg13.machinestate.StateInizioVendita;
import it.polimi.ingsw.cg13.machinestate.StateOtto;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateSei;
import it.polimi.ingsw.cg13.machinestate.StateSette;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.machinestate.StateWBDue;
import it.polimi.ingsw.cg13.machinestate.StateWBQuattro;
import it.polimi.ingsw.cg13.machinestate.StateWBTre;
import it.polimi.ingsw.cg13.machinestate.StateWBUno;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class AcquistoAiutanteTest {
	
	AcquistoAiutante azione;
	
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	static Giocatore g3=Mockito.mock(Giocatore.class);
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	static Partita modello;
	
	static Partita modelloSenzaAiutanti=Mockito.mock(Partita.class);
	
	static ArrayList<RicompensaSpecial> ricompense=new ArrayList<>();
	
	static StateUno statoUno;
	static StateDue statoDue;
	static StateInizio statoInizio;
	
	static Market market=new Market();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		giocatori.add(g1);
		giocatori.add(g2);
		
		ricompense.add(new OttieniPermesso());
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
		Mockito.when(modelloSenzaAiutanti.isEmptyAiutanti()).thenReturn(true);
		Mockito.when(g2.getRicchezzaGiocatore()).thenReturn(1);
		Mockito.when(g2.getNome()).thenReturn("Dani");
		
		statoUno=new StateUno(giocatori,g1,ricompense);
		statoDue=new StateDue(giocatori,g1,ricompense);
		statoInizio=new StateInizio(giocatori,g1,ricompense);
	}

	@Test
	public void testAcquistoAiutanteOK() {
		azione=new AcquistoAiutante(g1);
		assertTrue(azione!=null && azione.getGiocatore().equals(g1));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAcquistoAiutanteNull() {
		azione=new AcquistoAiutante(null);	
	}
	
	@Test
	public void testUpdateParametri() throws ErrorUpdateServerException{
		azione=new AcquistoAiutante(g1);
		azione.updateGiocatore(modello);
		assertEquals(g1,azione.getGiocatore());
	}
	
	@Test
	public void testUpdateParametriGiocatoreNonTrovato(){
		azione=new AcquistoAiutante(g3);
		try {
			azione.updateGiocatore(modello);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testUpdateGiocatore() throws ErrorUpdateServerException {
		azione=new AcquistoAiutante(g1);
		azione.updateGiocatore(modello);
		assertEquals(g1,azione.getGiocatore());
	}
	
	@Test
	public void testUpdateGiocatoreNonTrovato()  {
		azione=new AcquistoAiutante(g3);
		try {
			azione.updateGiocatore(modello);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
		assertEquals(g3,azione.getGiocatore());
	}
	
	
	@Test
	public void testEseguiAzioneOK() throws RemoteException, CdQException {
		azione=new AcquistoAiutante(g1);
		int numAiutanti=g1.numeroAiutanti();
		int ricchezza=g1.getRicchezzaGiocatore();
		assertTrue(azione.eseguiAzione(modello) && 
				numAiutanti+1==g1.numeroAiutanti() && ricchezza-3==g1.getRicchezzaGiocatore());
	}

	@Test
	public void testEseguiAzioneGiocatoriNoRicco() {
		azione=new AcquistoAiutante(g2);
		try {
			azione.eseguiAzione(modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testEseguiAzioneGiocatoriSenzaAiutanti() {
		azione=new AcquistoAiutante(g1);
		try {
			azione.eseguiAzione(modelloSenzaAiutanti);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testVisitStateInizioPartita() throws RemoteException, CdQException {
		azione=new AcquistoAiutante(g1);
		assertEquals(new StateOtto(giocatori,g1,ricompense),azione.visit(statoInizio, modello));
	}

	@Test
	public void testVisitStateDuePartita() throws RemoteException, CdQException {
		azione=new AcquistoAiutante(g1);
		assertEquals(new StateTre(giocatori,g1,ricompense),azione.visit(statoDue, modello));
	}

	@Test
	public void testVisitStateUnoPartita() throws RemoteException, CdQException {
		azione=new AcquistoAiutante(g1);
		assertEquals(new StateFine(giocatori,g1,ricompense),azione.visit(statoUno, modello));
	}


	@Test
	public void testGetGiocatore() {
		azione=new AcquistoAiutante(g1);
		assertEquals(g1,azione.getGiocatore());
	}

	@Test
	public void testSetGiocatore() {
		azione=new AcquistoAiutante(g1);
		azione.setGiocatore(g2);
		assertEquals(g2,azione.getGiocatore());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetGiocatoreNull() {
		azione=new AcquistoAiutante(g1);
		azione.setGiocatore(null);
		assertEquals(g2,azione.getGiocatore());
	}
	
	// Test per le azioni veloci non possibili in un certo stato
	
	@Test
	public void testVisitStatoImpossibileTre(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateTre(giocatori,g2,ricompense), modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileQuattro(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateQuattro(giocatori,g2,ricompense), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileCinque(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateCinque(giocatori,g2,ricompense), modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileSei(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateSei(giocatori,g2,ricompense), modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileSette(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateSette(giocatori,g2,ricompense), modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileOtto(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateOtto(giocatori,g2,ricompense), modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileFine(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateFine(giocatori,g2,ricompense), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileWBUno(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateWBUno(giocatori,g2,ricompense), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileWBDue(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateWBDue(giocatori,g2,ricompense), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileWBTre(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateWBTre(giocatori,g2,ricompense), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileWBQuattro(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateWBQuattro(giocatori,g2,ricompense), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileInizioVendita(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateInizioVendita(giocatori,g2,market), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileInizioAcquisto(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateAcquistoInizio(giocatori,g2,market), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileFineAcquisto(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateAcquistoFine(giocatori,g2,market), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStatoImpossibileFineVendita(){
		azione=new AcquistoAiutante(g2);
		try {
			azione.visit(new StateFineVendita(giocatori,g2,market), modello);
		} catch (CdQException e) {
			assertTrue(true);
		}
	}
	
}
