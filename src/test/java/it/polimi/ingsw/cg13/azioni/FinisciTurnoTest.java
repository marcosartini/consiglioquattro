package it.polimi.ingsw.cg13.azioni;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.OttieniPermesso;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoFine;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoInizio;
import it.polimi.ingsw.cg13.machinestate.StateFine;
import it.polimi.ingsw.cg13.machinestate.StateFineVendita;
import it.polimi.ingsw.cg13.machinestate.StateInizioVendita;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class FinisciTurnoTest {

	FinisciTurno azione;
	
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	static Giocatore g3=Mockito.mock(Giocatore.class);
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	static Partita modello;
	
	
	static ArrayList<RicompensaSpecial> ricompense=new ArrayList<>();
	
	static StateUno statoUno;
	static StateInizioVendita statoIV;
	static StateAcquistoInizio statoIA;
	
	static Market market=new Market();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		giocatori.add(g1);
		giocatori.add(g2);
		
		ricompense.add(new OttieniPermesso());
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
		Mockito.when(g2.numeroAiutanti()).thenReturn(1);
		
		statoUno=new StateUno(giocatori,g1,ricompense);
		statoIV=new StateInizioVendita(giocatori,g1,market);
		statoIA=new StateAcquistoInizio(giocatori,g1,market);
	}

	@Test
	public void testEseguiAzione() throws AzioneNotPossibleException {
		azione=new FinisciTurno(g1);
		assertTrue(azione.eseguiAzione(modello));
	}
	
	@Test
	public void testEseguiAzioneNonGiocatoreCorrente() throws AzioneNotPossibleException {
		azione=new FinisciTurno(g2);
		assertFalse(azione.eseguiAzione(modello));
	}

	@Test
	public void testUpdateParametri() {
		azione=new FinisciTurno(g1);
		assertEquals(g1,azione.getGiocatore());
	}

	@Test
	public void testFinisciTurno() {
		azione=new FinisciTurno(g1);
		assertTrue(azione!=null && g1.equals(azione.getGiocatore()));
	}

	@Test
	public void testVisitStateUnoPartita() throws AzioneNotPossibleException {
		azione=new FinisciTurno(g1);
		assertEquals(new StateFine(giocatori,g1,ricompense),azione.visit(statoUno, modello));
	}

	@Test
	public void testVisitStateInizioVenditaPartita() throws AzioneNotPossibleException {
		azione=new FinisciTurno(g1);
		assertEquals(new StateFineVendita(giocatori,g1,market),azione.visit(statoIV, modello));
	}

	@Test
	public void testVisitStateAcquistoInizioPartita() throws AzioneNotPossibleException {
		azione=new FinisciTurno(g1);
		assertEquals(new StateAcquistoFine(giocatori,g1,market),azione.visit(statoIA, modello));
	}
	
	@Test
	public void testVisitStateUnoPartitaNonGiocatoreCorrente() {
		azione=new FinisciTurno(g2);
		try {
			azione.visit(statoUno, modello);
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStateVenditaPartitaNonGiocatoreCorrente() {
		azione=new FinisciTurno(g2);
		try {
			azione.visit(statoIV, modello);
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testVisitStateAcquistoPartitaNonGiocatoreCorrente() {
		azione=new FinisciTurno(g2);
		try {
			azione.visit(statoIA, modello);
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}

}
