package it.polimi.ingsw.cg13.azioni;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.PuntoVittoria;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CostruzioneConPermessoTest {

	CostruzioneConPermesso azione;
	
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	
	static Partita modello;
	
	static ArrayList<Ricompensa> ricompense=new ArrayList<>();
	static PuntoVittoria punto=new PuntoVittoria(3);
	
	static PermessoCostruzione permesso1=Mockito.mock(PermessoCostruzione.class);
	static PermessoCostruzione permesso2=Mockito.mock(PermessoCostruzione.class);
	static PermessoCostruzione permessoMare;
	static NodoCitta nodoMare;
	
	static Set<Emporio> empori1=new HashSet<>();
	static Set<Emporio> empori2=new HashSet<>();
	static Emporio emp=Mockito.mock(Emporio.class);
	
	static List<Citta> listaCitta1=new ArrayList<>();
	static List<Citta> listaCitta2=new ArrayList<>();
	static NodoCitta nodoCitta=Mockito.mock(NodoCitta.class);
	static Citta citta=Mockito.mock(Citta.class);
	static Citta citta2=Mockito.mock(Citta.class);
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),15,15,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		
		giocatori.add(g1);
		giocatori.add(g2);
		ricompense.add(punto);
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
		permessoMare=modello.getRegione("mare").getMazzoPermesso().getCarteInCima(2).get(0);
		nodoMare=modello.getNodoCitta("Esti");
		
		g1.addPermessoCostruzione(permessoMare, g1.getPermessiCostruzione());
		g1.addPermessoCostruzione(permesso1, g1.getPermessiCostruzione());
		
		Mockito.when(g2.getNumeroEmpori()).thenReturn(0);
		Mockito.when(g2.numeroAiutanti()).thenReturn(0);
		Mockito.when(citta.hasEmporio(g2)).thenReturn(true);
		Mockito.when(citta.hasEmporio(g1)).thenReturn(false);
		empori2.add(emp);
		Mockito.when(citta.getEmpori()).thenReturn(empori1);
		Mockito.when(citta2.getEmpori()).thenReturn(empori2);
		listaCitta1.add(citta);
		listaCitta1.add(nodoMare.getCittaNodo());
		Mockito.when(permesso1.getListaCitta()).thenReturn(listaCitta1);
		Mockito.when(permesso2.getListaCitta()).thenReturn(listaCitta2);
		Mockito.when(nodoCitta.getCittaNodo()).thenReturn(citta);
		
		
	}

	@Test
	public void testEseguiAzioneSenzaRiuscireACostruire() {
		azione=new CostruzioneConPermesso(g2,nodoCitta,permesso1);
		try {
			azione.eseguiAzione(modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testEseguiAzioneConSuccesso() throws RemoteException, CdQException {
		azione=new CostruzioneConPermesso(g1,nodoMare,permesso1);
		assertTrue(azione.eseguiAzione(modello));
		assertTrue(g1.getCartePermessoUsate().contains(permesso1));
		assertTrue(!g1.getPermessiCostruzione().contains(permesso1));
		assertTrue(nodoMare.getCittaNodo().hasEmporio(g1));
		
	}

	@Test
	public void testUpdateParametriOK() throws ErrorUpdateServerException {
		azione=new CostruzioneConPermesso(g1,nodoMare,permessoMare);
		azione.updateParametri(modello);
		assertTrue(azione.getNodoCitta().equals(nodoMare) && azione.getPermesso().equals(permessoMare));
	}
	
	@Test
	public void testUpdateParametriPermessoNonTrovato(){
		azione=new CostruzioneConPermesso(g1,nodoMare,permesso1);
		try {
			azione.updateParametri(modello);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testUpdateParametriCittaNonTrovata(){
		azione=new CostruzioneConPermesso(g1,nodoCitta,permessoMare);
		try {
			azione.updateParametri(modello);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCostruzioneConPermessoCittaNull() {
		azione=new CostruzioneConPermesso(g1,null,permesso1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCostruzioneConPermessoPermessoNull() {
		azione=new CostruzioneConPermesso(g1,nodoCitta,null);
	}
	
	@Test
	public void testCostruttoreCostruzioneConPermessoOK() {
		azione=new CostruzioneConPermesso(g1,nodoCitta,permesso1);
		assertTrue(azione!=null && azione.getGiocatore().equals(g1) &&
				nodoCitta.equals(azione.getNodoCitta()) && permesso1.equals(azione.getPermesso()));
	}

	@Test
	public void testCittaGiustaContenutaNelPermesso() {
		azione=new CostruzioneConPermesso(g1,nodoCitta,permesso1);
		assertTrue(azione.cittaGiusta());
	}
	
	@Test
	public void testCittaNonContenutaNelPermesso() {
		azione=new CostruzioneConPermesso(g1,nodoCitta,permesso2);
		assertFalse(azione.cittaGiusta());
	}


	@Test
	public void testGiocatoreHaEmporiOK() {
		azione=new CostruzioneConPermesso(g1,nodoCitta,permesso1);
		assertTrue(azione.giocatoreHaEmpori());
	}
	
	@Test
	public void testGiocatoreNonHaEmporiPerCostruire() {
		azione=new CostruzioneConPermesso(g2,nodoCitta,permesso1);
		assertFalse(azione.giocatoreHaEmpori());
	}

	@Test
	public void testAssegnaRicompense() {
		azione=new CostruzioneConPermesso(g1,nodoCitta,permesso1);
		int vittoria=g1.getVittoriaGiocatore();
		azione.assegnaRicompense(ricompense, modello);
		assertTrue(g1.getVittoriaGiocatore()==vittoria+3);
	}

	@Test
	public void testGiaCostruitoYes() {
		azione=new CostruzioneConPermesso(g2,nodoCitta,permesso1);
		assertTrue(azione.giaCostruito(citta));
	}
	
	@Test
	public void testGiaCostruitoNo() {
		azione=new CostruzioneConPermesso(g1,nodoCitta,permesso1);
		assertFalse(azione.giaCostruito(citta));
	}

	@Test
	public void testCostoAggiuntivoNonCiSonoAltriEmpori() throws AzioneNotPossibleException {
		azione=new CostruzioneConPermesso(g1,nodoCitta,permesso1);
		assertTrue(azione.costoAggiuntivo(citta));
	}
	
	@Test
	public void testCostoAggiuntivoHaAbbastanzaAiutanti() throws AzioneNotPossibleException {
		azione=new CostruzioneConPermesso(g1,nodoCitta,permesso1);
		assertTrue(azione.costoAggiuntivo(citta2));
	}
	
	@Test
	public void testCostoAggiuntivoNonHaAbbastanzaAiutantiRispettoAgliEmpori(){
		azione=new CostruzioneConPermesso(g2,nodoCitta,permesso1);
		try {
			azione.costoAggiuntivo(citta2);
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testPuoCostruireOK() throws AzioneNotPossibleException {
		azione=new CostruzioneConPermesso(g1,nodoCitta,permesso1);
		assertTrue(azione.puoCostruire(citta));
	}
	
	@Test
	public void testNonPuoCostruireInQuestaCitta() {
		azione=new CostruzioneConPermesso(g2,nodoCitta,permesso1);
		try {
			assertFalse(azione.puoCostruire(citta));
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}

}
