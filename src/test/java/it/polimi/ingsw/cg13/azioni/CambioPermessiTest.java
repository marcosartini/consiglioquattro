package it.polimi.ingsw.cg13.azioni;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CambioPermessiTest {
	
	CambioPermessi azione;
	
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	
	static MazzoPermessi mazzoMock=Mockito.mock(MazzoPermessi.class);
	static MazzoPermessi mazzoMock2=Mockito.mock(MazzoPermessi.class);
	static MazzoPermessi mazzoPartita;
	static Partita modello;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,3,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(12));
	
		Mockito.when(mazzoMock.getPermessiVisibili()).thenReturn(-1);
		Mockito.when(mazzoMock2.getPermessiVisibili()).thenReturn(2);
		Mockito.when(g2.numeroAiutanti()).thenReturn(0);
		
		giocatori.add(g1);
		giocatori.add(g2);
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
		mazzoPartita=modello.getRegione("mare").getMazzoPermesso();
	}

	@Test
	public void testEseguiAzioneOK() throws RemoteException, CdQException {
		azione=new CambioPermessi(mazzoPartita,g1);
		int aiutanti=g1.numeroAiutanti();
		List<PermessoCostruzione> permessiPrima=mazzoPartita.getCarteInCima(2);
		azione.eseguiAzione(modello);
		List<PermessoCostruzione> permessiDopo=new ArrayList<>();
		permessiDopo.add(mazzoPartita.getMazzoCarte().get(1));
		permessiDopo.add(mazzoPartita.getMazzoCarte().get(0));
		assertTrue(g1.numeroAiutanti()==aiutanti-1);
		assertTrue(permessiPrima.equals(permessiDopo));
	}
	
	@Test
	public void testEseguiAzioneSenzaAiutanti() {
		azione=new CambioPermessi(mazzoPartita,g2);
		try {
			azione.eseguiAzione(modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testUpdateParametriOK() throws ErrorUpdateServerException {
		azione=new CambioPermessi(mazzoPartita,g1);
		azione.updateParametri(modello);
		assertEquals(mazzoPartita,azione.getMazzo());
		assertTrue(azione.getNumeroPermessi()==2);
	}
	
	@Test
	public void testUpdateParametriErrati(){
		azione=new CambioPermessi(mazzoMock2,g1);
		try {
			azione.updateParametri(modello);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testCostruttoreCambioPermessiOK() {
		azione=new CambioPermessi(mazzoPartita,g1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCambioPermessiMazzoNull() {
		azione=new CambioPermessi(null,g1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCambioPermessiNegativi() {
		azione=new CambioPermessi(mazzoMock,g1);
	}

	@Test
	public void testGetRegioneMazzoTrovata() throws RegioneNotFoundException {
		azione=new CambioPermessi(mazzoPartita,g1);
		assertEquals(modello.getRegione("mare"),azione.getRegioneMazzo(modello));
	}
	
	@Test
	public void testGetRegioneMazzoNonTrovata() {
		azione=new CambioPermessi(mazzoMock2,g1);
		try {
			azione.getRegioneMazzo(modello);
		} catch (RegioneNotFoundException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testGetMazzo() {
		azione=new CambioPermessi(mazzoPartita,g1);
		assertEquals(azione.getMazzo(),mazzoPartita);
	}

	@Test
	public void testGetNumeroPermessi() {
		azione=new CambioPermessi(mazzoPartita,g1);
		assertTrue(azione.getNumeroPermessi()==2);
	}

}
