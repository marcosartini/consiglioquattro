package it.polimi.ingsw.cg13.azioni;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.OggettoBonusInput;
import it.polimi.ingsw.cg13.bonus.OttieniPermesso;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.machinestate.StateWBDue;
import it.polimi.ingsw.cg13.machinestate.StateWBQuattro;
import it.polimi.ingsw.cg13.machinestate.StateWBTre;
import it.polimi.ingsw.cg13.machinestate.StateWBUno;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class BonusInputTest {

	BonusInput azione;
	
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	static Partita modello;
	
	static ArrayList<RicompensaSpecial> ricompense=new ArrayList<>();
	
	static StateWBUno statoWB1;
	static StateWBDue statoWB2;
	static StateWBTre statoWB3;
	static StateWBQuattro statoWB4;
	
	static List<PermessoCostruzione> mazzo;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,1,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		giocatori.add(g1);
		giocatori.add(g2);
		
		ricompense.add(new OttieniPermesso());
		ricompense.add(new OttieniPermesso());
		ricompense.add(new OttieniPermesso());
		ricompense.add(new OttieniPermesso());
		ricompense.add(new OttieniPermesso());
		
		statoWB1=new StateWBUno(giocatori,g1,ricompense);
		statoWB2=new StateWBDue(giocatori,g1,ricompense);
		statoWB3=new StateWBTre(giocatori,g1,ricompense);
		statoWB4=new StateWBQuattro(giocatori,g1,ricompense);
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
		mazzo=modello.getRegione("mare").getMazzoPermesso().getMazzoCarte();
	}

	
	@Test
	public void testEseguiAzione() throws CdQException {
		OggettoBonusInput oggetto=mazzo.get(mazzo.size()-1);
		azione=new BonusInput(g1,oggetto);
		azione.setRicompensa(new OttieniPermesso());
		assertTrue(azione.eseguiAzione(modello));
		assertTrue(g1.getPermessiCostruzione().contains(oggetto));
		assertTrue(!mazzo.contains(oggetto));
	}

	@Test
	public void testVisitStateWBUnoPartita() throws CdQException {
		OggettoBonusInput oggetto=mazzo.get(mazzo.size()-1);
		azione=new BonusInput(g1,oggetto);
		assertEquals(new StateUno(giocatori,g1,ricompense),azione.visit(statoWB1, modello));
	}

	@Test
	public void testVisitStateWBDuePartita() throws CdQException {
		OggettoBonusInput oggetto=mazzo.get(mazzo.size()-1);
		azione=new BonusInput(g1,oggetto);
		assertEquals(new StateDue(giocatori,g1,ricompense),azione.visit(statoWB2, modello));
	}

	@Test
	public void testVisitStateWBTrePartita() throws CdQException {
		OggettoBonusInput oggetto=mazzo.get(mazzo.size()-1);
		azione=new BonusInput(g1,oggetto);
		assertEquals(new StateTre(giocatori,g1,ricompense),azione.visit(statoWB3, modello));
	}

	@Test
	public void testVisitStateWBQuattroPartita() throws CdQException {
		OggettoBonusInput oggetto=mazzo.get(mazzo.size()-1);
		azione=new BonusInput(g1,oggetto);
		assertEquals(new StateQuattro(giocatori,g1,ricompense),azione.visit(statoWB4, modello));
	}

}
