package it.polimi.ingsw.cg13.azioni;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.bonus.OttieniPermesso;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateInizio;
import it.polimi.ingsw.cg13.machinestate.StateSei;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CompraAzionePrincipaleTest {

	CompraAzionePrincipale azione;
	
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	static Giocatore g3=Mockito.mock(Giocatore.class);
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	static Partita modello;
	
	
	static ArrayList<RicompensaSpecial> ricompense=new ArrayList<>();
	
	static StateUno statoUno;
	static StateDue statoDue;
	static StateInizio statoInizio;
	
	static Market market=new Market();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		giocatori.add(g1);
		giocatori.add(g2);
		
		ricompense.add(new OttieniPermesso());
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
		Mockito.when(g2.numeroAiutanti()).thenReturn(1);
		
		statoUno=new StateUno(giocatori,g1,ricompense);
		statoDue=new StateDue(giocatori,g1,ricompense);
		statoInizio=new StateInizio(giocatori,g1,ricompense);
	}

	@Test
	public void testCompraAzionePrincipale() {
		azione=new CompraAzionePrincipale(g1);
		assertTrue(azione!=null && g1.equals(azione.getGiocatore()));
	}

	@Test
	public void testEseguiAzione() throws RemoteException, CdQException {
		azione=new CompraAzionePrincipale(g1);
		int aiutanti=g1.numeroAiutanti();
		assertTrue(azione.eseguiAzione(modello));
		assertTrue(g1.numeroAiutanti()==aiutanti-3);
	}
	
	@Test
	public void testEseguiAzioneSenzaAbbastanzaAiutanti() {
		azione=new CompraAzionePrincipale(g2);
		try {
			azione.eseguiAzione(modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testUpdateParametriOK() throws ErrorUpdateServerException {
		azione=new CompraAzionePrincipale(g1);
		azione.updateParametri(modello);
		assertTrue(g1.equals(azione.getGiocatore()));
	}
	
	@Test
	public void testUpdateParametriGiocatoreErrato(){
		azione=new CompraAzionePrincipale(g2);
		try {
			azione.updateParametri(modello);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testVisitStateInizioPartita() throws RemoteException, CdQException {
		azione=new CompraAzionePrincipale(g1);
		assertEquals(new StateSei(giocatori,g1,ricompense),azione.visit(statoInizio, modello));
	}

	@Test
	public void testVisitStateDuePartita() throws RemoteException, CdQException {
		azione=new CompraAzionePrincipale(g1);
		assertEquals(new StateSei(giocatori,g1,ricompense),azione.visit(statoDue, modello));
	}

	@Test
	public void testVisitStateUnoPartita() throws RemoteException, CdQException {
		azione=new CompraAzionePrincipale(g1);
		assertEquals(new StateTre(giocatori,g1,ricompense),azione.visit(statoUno, modello));
	}

	

}
