package it.polimi.ingsw.cg13.azioni;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayDeque;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.mappa.ConsiglioRegione;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CambioConsiglioVeloceTest {

	CambioConsiglioVeloce azione;
	
	static Partita modello;
	static Partita modello2=Mockito.mock(Partita.class);
	static Partita modello3=Mockito.mock(Partita.class);
	
	static Consigliere cons1;
	static Consigliere cons2;
	static Consigliere cons3;
	static Consigliere cons4;
	static Consigliere consDaInserire;
	static ArrayDeque<Consigliere> balcone=new ArrayDeque<>();
	static Consiglio consiglio;
	static Consiglio consiglio2=Mockito.mock(Consiglio.class);
	static Consiglio consiglioNull=null;
	static Consigliere consNull=null;
	static Regione regione=Mockito.mock(Regione.class);
	static Giocatore g1;
	static Giocatore g2=Mockito.mock(Giocatore.class);
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	
	g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),2,3,
			new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));	
	giocatori.add(g1);
	
	Mockito.when(modello2.getGiocatori()).thenReturn(giocatori);
	Mockito.when(modello3.getGiocatori()).thenReturn(giocatori);
	Mockito.when(g2.numeroAiutanti()).thenReturn(0);
	
	modello=new Partita();
	cons1=new Consigliere(modello.getColorePolitico("ciano"));
	cons2=new Consigliere(modello.getColorePolitico("nero"));
	cons3=new Consigliere(modello.getColorePolitico("rosa"));
	cons4=new Consigliere(modello.getColorePolitico("bianco"));
	balcone.add(cons1);
	balcone.add(cons2);
	balcone.add(cons3);
	balcone.add(cons4);
	consDaInserire=new Consigliere(modello.getColorePolitico("viola"));
	consiglio=new ConsiglioRegione(balcone,regione);
	
	Mockito.when(modello3.getConsiglio(consiglio)).thenThrow(new ErrorUpdateServerException());
	Mockito.when(modello2.getConsiglio(consiglio)).thenReturn(consiglio2);
	Mockito.when(modello2.getConsigliere(consDaInserire)).thenReturn(cons1);
	}

	@Test
	public void testEseguiAzioneOK() throws RemoteException, CdQException {
		azione=new CambioConsiglioVeloce(g1,consiglio,consDaInserire);
		int numAiutanti=g1.numeroAiutanti();
		assertTrue(azione.eseguiAzione(modello));
		assertTrue(g1.numeroAiutanti()==numAiutanti-1);
		assertTrue(consiglio.getBalcone().toArray()[3].equals(consDaInserire));
	}
	

	@Test
	public void testEseguiAzioneNoAiutanti() throws RemoteException, CdQException {
		azione=new CambioConsiglioVeloce(g2,consiglio,consDaInserire);
		try {
			azione.eseguiAzione(modello2);
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testUpdateParametriError(){
		azione=new CambioConsiglioVeloce(g1,consiglio,consDaInserire);
		try {
			azione.updateParametri(modello3);
		} catch (ErrorUpdateServerException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testUpdateParametri() throws ErrorUpdateServerException {
		azione=new CambioConsiglioVeloce(g1,consiglio,consDaInserire);
		azione.updateParametri(modello2);
		assertTrue(consiglio2.equals(azione.getConsiglio()) 
				&& cons1.equals(azione.getConsigliereDaInserire()));
	}

	@Test
	public void testGetConsiglio(){
		azione=new CambioConsiglioVeloce(g1,consiglio,consDaInserire);
		assertEquals(consiglio,azione.getConsiglio());
	}
	
	@Test
	public void testGetConsigliere(){
		azione=new CambioConsiglioVeloce(g1,consiglio,consDaInserire);
		assertEquals(consDaInserire,azione.getConsigliereDaInserire());
	}
	
	@Test
	public void testCostruttoreCambioConsiglioVeloce() {
		azione=new CambioConsiglioVeloce(g1,consiglio,consDaInserire);
		assertTrue(azione!=null && azione.getGiocatore().equals(g1) && azione.getConsiglio().equals(consiglio)
				&& azione.getConsigliereDaInserire().equals(consDaInserire));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCambioConsiglioVeloceConsiglioNull() {
		azione=new CambioConsiglioVeloce(g1,consiglioNull,consDaInserire);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCostruttoreCambioConsiglioVeloceConsigliereNull() {
		azione=new CambioConsiglioVeloce(g1,consiglio,consNull);
	}

}
