package it.polimi.ingsw.cg13.machinestate;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import it.polimi.ingsw.cg13.bonus.OttieniPermesso;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class StateVenditaTest {

	static StateInizioVendita inizio;
	static StateFineVendita fine;
	
	static Giocatore g1;
	static Giocatore g2;
	static Giocatore g3;
	static ArrayList<RicompensaSpecial> ricompense=new ArrayList<>();
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	static ArrayList<Giocatore> giocatoriFinePartita=new ArrayList<>();
	static ArrayList<Giocatore> giocatoriFiniti=new ArrayList<>();
	static Partita modello;

	
	static Market market;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		g2=new Giocatore("Marco",0,new Colore("Nero",0,0,0),0,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		g3=new Giocatore("Ueee",2,new Colore("Nero",0,0,0),2,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		
		market=new Market();
		giocatoriFinePartita.add(g1);
		giocatoriFinePartita.add(g2);
		giocatori.add(g1);
		giocatori.add(g3);
		
		//ricompense.add(new OttieniPermesso());
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
	}

	@Test
	public void testToStringInizioVendita() {
		inizio=new StateInizioVendita(giocatori,g1,market);
		assertEquals(inizio.toString(),"StateMarketInizioVendita: "+g1.getNome()+"\n");
	}
	
	@Test
	public void testToStringFineVendita() {
		fine=new StateFineVendita(giocatori,g1,market);
		assertEquals(fine.toString(),"StateMarketFineVendita: "+g1.getNome()+"\n");
	}


	@Test
	public void testNextStateContinuaVendita() throws RemoteException, CdQException {
		inizio=new StateInizioVendita(giocatori,g1,market);
		State stato=inizio.nextState(modello);
		assertEquals(inizio,stato);
	}
	
	@Test
	public void testNextStateGiocatoreSuccessivoVendita() throws RemoteException, CdQException {
		fine=new StateFineVendita(giocatori,g1,market);
		State stato=fine.nextState(modello);
		assertEquals(new StateInizioVendita(stato.getGiocatoriMustDoAction(),
					stato.getGiocatoreCorrente(),market),stato);
	}
	
	@Test
	public void testNextStatePassaAdAcquisto() throws RemoteException, CdQException {
		fine=new StateFineVendita(giocatoriFiniti,g1,market);
		State stato=fine.nextState(modello);
		assertEquals(new StateAcquistoInizio(stato.getGiocatoriMustDoAction(),
						stato.getGiocatoreCorrente(),market),stato);
	}

	@Test
	public void testSetEndState() {
		inizio=new StateInizioVendita(giocatori,g1,market);
		assertEquals(inizio.setEndState(),new StateFineVendita(giocatori,g1,market));
	}


	@Test
	public void testAddRicompensa() {
		inizio=new StateInizioVendita(giocatori,g1,market);
		try {
			inizio.addRicompensa(new OttieniPermesso());
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testGetMarket() {
		inizio=new StateInizioVendita(giocatori,g1,market);
		assertEquals(market,inizio.getMarket());
	}


	@Test
	public void testSetMarket() {
		inizio=new StateInizioVendita(giocatori,g1,new Market());
		inizio.setMarket(market);
		assertEquals(market,inizio.getMarket());
	}


}
