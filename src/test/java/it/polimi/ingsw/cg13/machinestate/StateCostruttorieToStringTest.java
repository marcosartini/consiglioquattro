package it.polimi.ingsw.cg13.machinestate;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import it.polimi.ingsw.cg13.bonus.OttieniPermesso;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class StateCostruttorieToStringTest {

	static StateUno uno;
	static StateDue due;
	static StateTre tre;
	static StateQuattro quattro;
	static StateCinque cinque;
	static StateSei sei;
	static StateSette sette;
	static StateOtto otto;
	static StateFine fine;
	static StateWBUno wb1;
	static StateWBDue wb2;
	static StateWBTre wb3;
	static StateWBQuattro wb4;
	
	static Giocatore g1;
	static Giocatore g2;

	static ArrayList<RicompensaSpecial> ricompense=new ArrayList<>();
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	
	static Partita modello;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		g2=new Giocatore("Marco",0,new Colore("Nero",0,0,0),0,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		
		
		giocatori.add(g1);
		giocatori.add(g2);
		
		//ricompense.add(new OttieniPermesso());
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
	}
	
	@Test
	public void testCostruttoreAndStringStateUno(){
		uno=new StateUno(giocatori,g1,ricompense);
		assertTrue(uno!=null && uno.getGiocatoreCorrente().equals(g1));
		assertEquals(uno.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un'azione veloce");
	}
	
	@Test
	public void testCostruttoreAndStringStateDue(){
		due=new StateDue(giocatori,g1,ricompense);
		assertTrue(due!=null && due.getGiocatoreCorrente().equals(g1));
		assertEquals(due.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> "
						+ "In attesa di un'azione principale o veloce");
	}
	
	@Test
	public void testCostruttoreAndStringStateTre(){
		tre=new StateTre(giocatori,g1,ricompense);
		assertTrue(tre!=null && tre.getGiocatoreCorrente().equals(g1));
		assertEquals(tre.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un'azione principale");
	}
	
	@Test
	public void testCostruttoreAndStringStateQuattro(){
		quattro=new StateQuattro(giocatori,g1,ricompense);
		assertTrue(quattro!=null && quattro.getGiocatoreCorrente().equals(g1));
		assertEquals(quattro.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> Assegnamento Bonus in corso");
	}
	
	@Test
	public void testCostruttoreAndStringStateCinque(){
		cinque=new StateCinque(giocatori,g1,ricompense);
		assertTrue(cinque!=null && cinque.getGiocatoreCorrente().equals(g1));
		assertEquals(cinque.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un'azione principale");
	}
	
	@Test
	public void testCostruttoreAndStringStateSei(){
		sei=new StateSei(giocatori,g1,ricompense);
		assertTrue(sei!=null && sei.getGiocatoreCorrente().equals(g1));
		assertEquals(sei.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un'azione principale");
	}
	
	@Test
	public void testCostruttoreAndStringStateSette(){
		sette=new StateSette(giocatori,g1,ricompense);
		assertTrue(sette!=null && sette.getGiocatoreCorrente().equals(g1));
		assertEquals(sette.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un'azione principale");
	}
	
	@Test
	public void testCostruttoreAndStringStateOtto(){
		otto=new StateOtto(giocatori,g1,ricompense);
		assertTrue(otto!=null && otto.getGiocatoreCorrente().equals(g1));
		assertEquals(otto.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un'azione principale");
	}

	@Test
	public void testCostruttoreAndStringStateWBuno(){
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		ricompensetemp.add(new OttieniPermesso());
		wb1=new StateWBUno(giocatori,g1,ricompensetemp);
		assertTrue(wb1!=null && wb1.getGiocatoreCorrente().equals(g1));
		assertEquals(wb1.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un input bonus "+
							ricompensetemp.get(0));
	}
	
	@Test
	public void testCostruttoreAndStringStateWBdue(){
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		ricompensetemp.add(new OttieniPermesso());
		wb2=new StateWBDue(giocatori,g1,ricompensetemp);
		assertTrue(wb2!=null && wb2.getGiocatoreCorrente().equals(g1));
		assertEquals(wb2.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un input bonus "+
							ricompensetemp.get(0));
	}
	
	@Test
	public void testCostruttoreAndStringStateWBtre(){
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		ricompensetemp.add(new OttieniPermesso());
		wb3=new StateWBTre(giocatori,g1,ricompensetemp);
		assertTrue(wb3!=null && wb3.getGiocatoreCorrente().equals(g1));
		assertEquals(wb3.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un input bonus "+
							ricompensetemp.get(0));
	}
	
	@Test
	public void testCostruttoreAndStringStateWBquattro(){
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		ricompensetemp.add(new OttieniPermesso());
		wb4=new StateWBQuattro(giocatori,g1,ricompensetemp);
		assertTrue(wb4!=null && wb4.getGiocatoreCorrente().equals(g1));
		assertEquals(wb4.toString(),
				"StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> In attesa di un input bonus "+
							ricompensetemp.get(0));
	}
	
	
	

}
