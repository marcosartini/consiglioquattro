package it.polimi.ingsw.cg13.machinestate;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;


import it.polimi.ingsw.cg13.bonus.OttieniPermesso;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.bonus.RiutilizzaBonusCitta;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class StateGiocoTest {

	static StateInizio statoInizio;
	static StateQuattro statoQuattro;
	
	static Giocatore g1;
	static Giocatore g2;
	static Giocatore g3;
	static ArrayList<RicompensaSpecial> ricompense=new ArrayList<>();
	
	static ArrayList<Giocatore> giocatori=new ArrayList<>();
	static ArrayList<Giocatore> giocatoriFinePartita=new ArrayList<>();
	static ArrayList<Giocatore> giocatoriFiniti=new ArrayList<>();
	static Partita modello;
	static Partita modello2;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		g1=new Giocatore("Luca",1,new Colore("Rosso",255,0,0),1,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		g2=new Giocatore("Marco",0,new Colore("Nero",0,0,0),0,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		g3=new Giocatore("Ueee",2,new Colore("Nero",0,0,0),2,25,
				new CasellaVittoria(0),new CasellaNobilta(10),new CasellaRicchezza(18));
		
		giocatoriFinePartita.add(g1);
		giocatoriFinePartita.add(g2);
		giocatori.add(g1);
		giocatori.add(g3);
		
		//ricompense.add(new OttieniPermesso());
		
		modello=new Partita(giocatori);
		modello.startTurnoPartita();
		
		modello2=new Partita(giocatoriFinePartita);
		modello2.startTurnoPartita();
		
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		
	}

	@Test
	public void testToString() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		assertEquals("StatoPartita\ngiocatoreCorrente: "+g1.getNome()+ " -> Inizio del Turno",
				statoInizio.toString());
		
	}

	@Test
	public void testStateInizio() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		assertTrue(statoInizio!=null && statoInizio.getGiocatoreCorrente().equals(g1));
	}


	@Test
	public void testHashCodeUguali() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		StateInizio statoInizio2=new StateInizio(giocatori,g1,ricompense);
		assertTrue(statoInizio.hashCode()==statoInizio2.hashCode());
		
	}

	@Test
	public void testAddRicompensa() {
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		statoInizio=new StateInizio(giocatori,g1,ricompensetemp);
		RicompensaSpecial spec=new OttieniPermesso();
		statoInizio.addRicompensa(spec);
		assertTrue(statoInizio.getRicompense().get(0).equals(spec));
	}

	@Test
	public void testGetMarketFaseErrata() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		try {
			statoInizio.getMarket();
		} catch (AzioneNotPossibleException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testNextStateConRicompenseAccetabiliMaStatoSbagliato() {
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		RicompensaSpecial spec=new OttieniPermesso();
		ricompensetemp.add(spec);
		statoInizio=new StateInizio(giocatori,g1,ricompensetemp);
		try {
			statoInizio.nextState(modello);
		} catch (RemoteException | CdQException e) {
			assertTrue(true);
		}
		
		
	}
	
	@Test
	public void testNextStateConRicompenseNonAccettabile() throws RemoteException, CdQException {
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		RicompensaSpecial spec=new RiutilizzaBonusCitta();
		ricompensetemp.add(spec);
		statoInizio=new StateInizio(giocatori,g1,ricompensetemp);
		State stato=statoInizio.nextState(modello);
		assertTrue(stato.equals(statoInizio));
		
	}
	
	@Test
	public void testNextStateConRicompenseAccettabili() throws RemoteException, CdQException {
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		RicompensaSpecial spec=new RiutilizzaBonusCitta();
		ricompensetemp.add(spec);
		statoQuattro=new StateQuattro(giocatoriFinePartita,g2,ricompensetemp);
		State stato=statoQuattro.nextState(modello2);
		ArrayList<Giocatore> giocatoritemp=new ArrayList<>();
		giocatoritemp.add(g2);
		assertTrue(stato.equals(new StateInizio(giocatoritemp,g1,ricompensetemp)));
		
	}
	
	@Test
	public void testNextStateVenditaMarket() throws RemoteException, CdQException {
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		statoQuattro=new StateQuattro(giocatoriFiniti,g1,ricompensetemp);
		State stato=statoQuattro.nextState(modello);
		ArrayList<Giocatore> giocatoritemp=new ArrayList<>();
		giocatoritemp.add(g3);
		assertTrue(stato.equals(new StateInizioVendita(giocatoritemp,g1,new Market())));
		
	}
	

	@Test
	public void testNextStateStartLastTurn() throws RemoteException, CdQException {
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		statoQuattro=new StateQuattro(giocatoriFiniti,g2,ricompensetemp);
		State stato=statoQuattro.nextState(modello);
		ArrayList<Giocatore> giocatoritemp=new ArrayList<>();
		giocatoritemp.add(g3);
		assertTrue(stato.equals(new StateInizio(giocatoritemp,g1,ricompensetemp)));
		
	}
	
	@Test
	public void testNextStateFinePartita() throws RemoteException, CdQException {
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		statoQuattro=new StateQuattro(giocatoriFiniti,g1,ricompensetemp);
		State stato=statoQuattro.nextState(modello2);
		assertTrue(stato.equals(new StateFinePartita(giocatoriFinePartita,g1)));
		
	}


	@Test
	public void testSetEndState() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		assertEquals(new StateFine(giocatori,g1,ricompense),statoInizio.setEndState());
	}

	@Test
	public void testEqualsObject() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		StateInizio statoInizio2=new StateInizio(giocatori,g1,ricompense);
		assertTrue(statoInizio.equals(statoInizio2));
	}


	@Test
	public void testGetRicompense() {
		ArrayList<RicompensaSpecial> ricompensetemp=new ArrayList<>();
		RicompensaSpecial spec=new OttieniPermesso();
		ricompensetemp.add(spec);
		statoInizio=new StateInizio(giocatori,g1,ricompensetemp);
		assertEquals(ricompensetemp,statoInizio.getRicompense());
		
	}

	@Test
	public void testIsNotLastTurnBegin() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		assertFalse(statoInizio.isLastTurnBegin(modello));
	}
	
	@Test
	public void testIsLastTurnBegin() {
		statoInizio=new StateInizio(giocatori,g2,ricompense);
		assertTrue(statoInizio.isLastTurnBegin(modello));
	}


	@Test
	public void testIsEnd() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		assertFalse(statoInizio.isEnd());
	}

	@Test
	public void testGetGiocatoriMustDoAction() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		assertEquals(giocatori,statoInizio.getGiocatoriMustDoAction());
		
	}

	@Test
	public void testGetGiocatoreCorrente() {
		statoInizio=new StateInizio(giocatori,g1,ricompense);
		assertEquals(g1,statoInizio.getGiocatoreCorrente());
		
	}

	@Test
	public void testCalcolaPunteggioFinale() {
		
	}

}
