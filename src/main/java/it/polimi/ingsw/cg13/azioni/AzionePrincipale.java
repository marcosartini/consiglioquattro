/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;

import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateCinque;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateInizio;
import it.polimi.ingsw.cg13.machinestate.StateOtto;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateSei;
import it.polimi.ingsw.cg13.machinestate.StateSette;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public abstract class AzionePrincipale extends Azione{

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -1847260758512245258L;
	private static final String AZIONE_NON_CONCLUSA = "ERROR: Azione non conclusa";
	/**
	 * Costruttore di un azione Principale
	 * rochiama la super di azione
	 * @param giocatore : Giocatore che esegue la mossa
	 * @throws IllegalArgumentException--> Giocatore nullo
	 */
	public AzionePrincipale(Giocatore giocatore) {
		super(giocatore);
	}

	/**
	 * {@inheritDoc}
	 * Sta eseguendo un azione Principale
	 */
	@Override
	public abstract boolean eseguiAzione(Partita partita) throws CdQException, RemoteException;

	
	/**
	 * {@inheritDoc}
	 * Eseguita una azionePrincipale porta dallo stateInizio allo stateUno
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateInizio stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateUno(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita una azionePrincipale porta dallo stateOtto allo stateQuattro
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateOtto stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateQuattro(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	
	/**
	 * {@inheritDoc}
	 * Eseguita una azionePrincipale porta dallo stateDue allo stateUno
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateDue stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateUno(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	
	/**
	 * {@inheritDoc}
	 * Eseguita una azionePrincipale porta dallo stateSei allo stateTre
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateSei stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateTre(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita una azionePrincipale porta dallo stateTre allo stateQuattro
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateTre stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateQuattro(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	
	/**
	 * {@inheritDoc}
	 * Eseguita una azionePrincipale porta dallo stateCinque allo stateQuattro
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateCinque stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateQuattro(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	
	/**
	 * {@inheritDoc}
	 * Eseguita una azionePrincipale porta dallo stateSette allo stateTre
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException 
	 */
	@Override
	public State visit(StateSette stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateTre(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
}
