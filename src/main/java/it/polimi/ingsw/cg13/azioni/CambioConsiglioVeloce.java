/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;



import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.mutatio.AiutantiBancoMutatio;
import it.polimi.ingsw.cg13.mutatio.ConsiglieriBancoMutatio;
import it.polimi.ingsw.cg13.partita.Partita;

import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class CambioConsiglioVeloce extends AzioneVeloce{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1136564775878937878L;
	/**
	 * consiglio da modificare
	 */
	private Consiglio consiglio;
	/**
	 * consigliere da inserire nel consiglio
	 */
	private Consigliere consigliereDaInserire;

	/**
	 * Costruttore della CambioConsiglioVeloce
	 * @param gioc-->giocatore che compie l' azione
	 * @param consiglio-->consiglio da modificare
	 * @param consigliere-->nuovo consigliere da inserire
	 * @throws IllegalArgumentException--> valori nulli
	 */
	public CambioConsiglioVeloce(Giocatore gioc, Consiglio consiglio, Consigliere consigliere) {
		super(gioc);
		if(consiglio!=null)
			this.consiglio=consiglio;
		else
			throw new IllegalArgumentException("Consiglio passato non valido!");
		if(consigliere!=null)
			this.consigliereDaInserire=consigliere;
		else
			throw new IllegalArgumentException("Consigliere passato non valido!");
	}
	
	/**
	 * {@inheritDoc}
	 * Se il giocatore ha almeno un aiutante, Esegue la CambioConsiglio Veloce
	 * Toglie il consigliere piu' "vecchio" e aggiunge il consigliere passato come parametro
	 * Notifica l'azione alla view
	 * @throws AzioneNotPossibleException: se il giocatore non ha almeno un aiutante
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws  CdQException, RemoteException {
		if(this.giocatore.numeroAiutanti()>=1){
			Consigliere consigliereTolto=this.consiglio.sostituzioneConsigliere(this.consigliereDaInserire);
			giocatore.usaAiutanti(1);
			partita.accumulaConsigliere(consigliereTolto);
			partita.accumulaAiutante();
			partita.notifyObservers(new AiutantiBancoMutatio(0,1));
			partita.notifyObservers(new ConsiglieriBancoMutatio(consigliereDaInserire,consigliereTolto));
			return true;
		}
		else{
			//Non hai abbastanza aiutanti per fare la mossa
			throw new AzioneNotPossibleException("Non hai abbastanza aiutanti per fare l'Azione");
		}

	}
	
	/**
	 * {@inheritDoc}
	 * Aggiorna in base al modello passato, le istanze di giocatore
	 * consiglio e consigliere
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		super.updateGiocatore(partita);
		this.consiglio=partita.getConsiglio(consiglio);
		try {
			this.consigliereDaInserire=partita.getConsigliere(consigliereDaInserire);
		} catch (ColoreNotFoundException e) {
			throw new ErrorUpdateServerException(e);
		}	
		
	}

	/**
	 * Ritorna il consiglio scelto per l'azione
	 * @return consiglio su cui viene effettuata l'azione
	 */
	public Consiglio getConsiglio() {
		return consiglio;
	}
	
	/**
	 * Ritorna il consigliere scelto da inserire
	 * @return il consigliere da cambiare e mettere nel nuovo consiglio
	 */
	public Consigliere getConsigliereDaInserire() {
		return consigliereDaInserire;
	}
	

}
