package it.polimi.ingsw.cg13.azioni;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoFine;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoInizio;
import it.polimi.ingsw.cg13.machinestate.StateFine;
import it.polimi.ingsw.cg13.machinestate.StateFineVendita;
import it.polimi.ingsw.cg13.machinestate.StateInizioVendita;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class FinisciTurno extends Azione{

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 162399749844882221L;
	private static final String AZIONE_NON_CONCLUSA = "ERROR: Azione non conclusa";
	/**
	 * Costruttore della finisciTurno
	 * @param giocatore: giocatore che vuole eseguire l'azione
	 * @throws IllegalArgumentException se il giocatore e' null
	 */
	public FinisciTurno(Giocatore giocatore) {
		super(giocatore);
	}

	/**
	 * {@inheritDoc}
	 * Se e' il giocatore corrente, esegue la finisce turno
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws AzioneNotPossibleException {
		return this.giocatore.equals(partita.getStatoPartita().getGiocatoreCorrente());
	}

	/**
	 * {@inheritDoc}
	 * Aggiorna il giocatore in base al modello passato
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		super.updateGiocatore(partita);		
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita la finisciTurno porta dallo stateUno allo stateFine
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateUno stato,Partita partita) throws AzioneNotPossibleException{
		if(this.eseguiAzione(partita))
			return new StateFine(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita la finisciTurno porta dallo stateInizioVendita allo stateFineVendita
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateInizioVendita stato,Partita partita) throws AzioneNotPossibleException{
		if(this.eseguiAzione(partita))
			return new StateFineVendita(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getMarket());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita la finisciTurno porta dallo stateInizioAcquisto allo stateFineAcquisto
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateAcquistoInizio stato,Partita partita) throws AzioneNotPossibleException{
		if(this.eseguiAzione(partita))
			return new StateAcquistoFine(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getMarket());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	

	
	
}
