/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;



import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mutatio.AiutantiBancoMutatio;
import it.polimi.ingsw.cg13.mutatio.MazzoScartoMutatio;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.personaggi.King;

/**
 * @author Marco
 *
 */
public class CostruzioneConKing extends Costruzione{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3856150878232435153L;
	/**
	 * carte poltiche per corrompere il consiglio del re
	 */
	private List<CartaPolitica> cartePolitiche;
	/**
	 * percorso delle citta che il re deve attraversare
	 */
	private List<NodoCitta> kingPath;
	
	
	/**
	 * Costruttore  della costruisciConRe
	 * @param gioc--> giocatore che effettua la mossa
	 * @param percorsoRe--> percorso che deve effettuare il re per posizionarsi sulla citta
	 * @param carte--> carte politiche utilizzate per l' azione
	 * @throws IllegalArgumentException--> Valori nulli
	 */
	public CostruzioneConKing(Giocatore gioc,List<NodoCitta> percorsoRe,List<CartaPolitica> carte) {
		super(gioc);
		if(carte!=null && !carte.isEmpty() && !carte.contains(null))
			this.cartePolitiche=carte;
		else
			throw new IllegalArgumentException("Carte passate non valide!");
		if(percorsoRe!=null)
			this.kingPath=percorsoRe;
		else
			throw new IllegalArgumentException("Il Percorso del Re non è valido!");		
		
	}

	
	
	/**
	 * {@inheritDoc}
	 * Se soddisfa le condizione di Costruzione e costo, esegue l'azione di costruzione del re
	 * Altrimenti torna un'eccezione che indica il motivo per cui l'azione non puo' essere eseguita
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws CdQException, RemoteException {
		//Inizializzazione azione
		King re=partita.getKing();
		int costoStrade=partita.getCostoStrade();
		PercorsoRicchezza richPath=partita.getPercorsoRicchezza();
		Consiglio consiglioRe=re.getConsiglio().copiaConsiglio();
		NodoCitta partenza=re.getPosizione();
		NodoCitta arrivo=this.kingPath.get(this.kingPath.size()-1);
		List<CartaPolitica> carteRiscontrate=new ArrayList<>();
		//Inizio esecuzione azione
		int costoSpostaRe=this.corruzioneRe(partenza, arrivo, carteRiscontrate,costoStrade,consiglioRe);
		int consiglieriNonRiscontrati=consiglioRe.getBalcone().size();
		if(consiglieriNonRiscontrati>=4){
			// Non puoi corrompere il re con queste carte
			throw new AzioneNotPossibleException(
			"Non hai nessuna carta per corrompere il consiglio del Re");
			}
		else{
			int costoMossa=partita.getCostoAggiuntivoAzione(carteRiscontrate.size())+costoSpostaRe;
			if(giocatore.getRicchezzaGiocatore()>=costoMossa ){
				this.risultatoAzione(arrivo, costoMossa, richPath, re, partita, carteRiscontrate);
				return true;
			}
			else{
				// Non hai abbastanza monete per concludere questa mossa
				throw new AzioneNotPossibleException(
				"Costo Mossa:"+costoMossa+"; Ricchezza Giocatore:"+giocatore.getRicchezzaGiocatore()+
				"\nNon hai abbastanza punti Ricchezza per fare questa mossa");
			}
		}
	}
	
	
	/**
	 * Applica il risultato dell'azione al modello passato 
	 * Cambia la posizione del re, costruisce l'emporio, spende le monete del giocatore
	 * e assegna le ricompense della citta di arrivo
	 * @param arrivo: citta di destinazione del re
	 * @param costoMossa: costo totale della costruzione
	 * @param richPath: percorso ricchezza della partita
	 * @param re: 
	 * @param partita: modello su cui agire
	 * @param carteRiscontrate: carte riscontrate nella corruzione
	 * @throws RemoteException: si verifica un errore di chiamata in remoto
	 * @throws CdQException: errore nel eseguire l'azione o trovre parametri
	 */
	public void risultatoAzione(NodoCitta arrivo,int costoMossa,PercorsoRicchezza richPath,
			King re, Partita partita,List<CartaPolitica> carteRiscontrate) throws RemoteException, CdQException{
		
		int numeroEmpori=arrivo.getCittaNodo().getEmpori().size();
		giocatore.spendiMonete(costoMossa, richPath);
		giocatore.removeCarte(carteRiscontrate);
		if(numeroEmpori!=0)
			giocatore.usaAiutanti(numeroEmpori);
		Emporio emporio=this.giocatore.usaEmporio();
		arrivo.getCittaNodo().costruisciEmporio(emporio);		
		re.setCambiamentoRe(arrivo,kingPath);
		partita.scartaCarta(carteRiscontrate);
		assegnaRicompense(arrivo.bfsRicompense(giocatore),partita);
		if(numeroEmpori!=0)
			partita.notifyObservers(new AiutantiBancoMutatio(0,numeroEmpori ));
		partita.notifyObservers(new MazzoScartoMutatio(carteRiscontrate));

	}
	
	/**
	 * Data una carta politica, cerca il riscontro con il consiglio del re
	 * e la aggiunge alle carte risontrate 
	 * @param carta da riscontrare con il consiglio	
	 * @param carteRiscontrate : carteRiscontrate con il consiglio
	 * @param consiglioRe: consiglio del re
	 * @return true se la carta viene riscontrata con un consigliere
	 */
	private boolean matchCartaConsigliere(CartaPolitica carta,List<CartaPolitica> carteRiscontrate,Consiglio consiglioRe){
		boolean trovato=false;
		Queue<Consigliere> balcone=consiglioRe.getBalcone();
		Consigliere consigliere=null;
		Iterator<Consigliere> iteratorBalcone=balcone.iterator();
		while(iteratorBalcone.hasNext() && !trovato){
			consigliere=iteratorBalcone.next();
			if(carta.match(consigliere)){
				carteRiscontrate.add(carta);
				trovato=true;
			}
		}	
		if(trovato && consigliere!=null){
			consiglioRe.getBalcone().remove(consigliere);
		}
		return trovato;
		
	}
	
	
	/**
	 * Costo del riscontro delle carte passate con il consiglio
	 * @param carteRiscontrate: carte riscontrate con il consiglio
	 * @param consiglioRe: consiglio del re
	 * @return la somma del costo del riscontro delle carte(jolly:1 altre 0)
	 */
	private int costoMathConsiglio(List<CartaPolitica> carteRiscontrate,Consiglio consiglioRe){
		int costoUsoCarte=0;
		for(CartaPolitica carta:cartePolitiche){
			if(this.matchCartaConsigliere(carta, carteRiscontrate,consiglioRe)){
				costoUsoCarte+=carta.getCosto();
			}	
		}
		return costoUsoCarte;
	}
	
	/**
	 * Torna il costo dello spostamento del re piu il costo del riscontro delle carte
	 * @param partenza: citta di partenza del re
	 * @param arrivo: citta di destinazione del movimento del re
	 * @param carteRiscontrate: carte riscontrate nel match col consiglio
	 * @param costoStrade: costo dello spostamento attraverso una strada
	 * @param consiglioRe: consiglio del re
	 * @return costo totale dello spostamento del re
	 * @throws AzioneNotPossibleException: se il percorso e' sbagliato o non puoi costruire nella citta di arrivo
	 */
	private int corruzioneRe(NodoCitta partenza,NodoCitta arrivo,List<CartaPolitica> carteRiscontrate,int costoStrade,Consiglio consiglioRe) throws AzioneNotPossibleException{
		if(this.puoCostruire(arrivo.getCittaNodo())){
			if(partenza.percorsoCorretto(kingPath)){
				return partenza.costoPercorso(costoStrade, this.kingPath)+this.costoMathConsiglio(carteRiscontrate,consiglioRe);
			}
			else if(partenza.equals(arrivo) && this.kingPath.size()==1){
				return this.costoMathConsiglio(carteRiscontrate,consiglioRe);
			}
			else{
				// il percorso inserito e' sbagliato
				throw new AzioneNotPossibleException(
						"Il percorso delle citta' e' errato");
			}
		}
		else {
			// Non puo costruire nella citta di destinazione
			throw new AzioneNotPossibleException(
					"Non puo' costruire nella citta' di destinazione");
		}
	}
	
	/**
	 * {@inheritDoc}
	 * Aggiorna il giocatore e il percorso del re in base al modello passato
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		super.updateGiocatore(partita);
		// Carte non serve implementare l'update
		List<NodoCitta> pathRiscontrato=new ArrayList<>();
		for(NodoCitta citta:this.kingPath){
			NodoCitta nodo=partita.getMappaPartita().getGrafo().stream().
					filter(cittaPartita -> cittaPartita.equals(citta)).findFirst().orElseThrow(
					()-> new ErrorUpdateServerException("citta "+citta.getCittaNodo().getNome()+" non trovato"));
			pathRiscontrato.add(nodo);
		}
		this.kingPath=pathRiscontrato;
		
	}



	/**
	 * 
	 * @return carte usate per corrompere il consiglio del re
	 */
	public List<CartaPolitica> getCartePolitiche() {
		return cartePolitiche;
	}


	/**
	 * 
	 * @return la lista delle citta che il re deve attraversare
	 */
	public List<NodoCitta> getKingPath() {
		return kingPath;
	}
	
	

}
