package it.polimi.ingsw.cg13.azioni;

import it.polimi.ingsw.cg13.bonus.OggettoBonusInput;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.machinestate.StateWBDue;
import it.polimi.ingsw.cg13.machinestate.StateWBQuattro;
import it.polimi.ingsw.cg13.machinestate.StateWBTre;
import it.polimi.ingsw.cg13.machinestate.StateWBUno;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class BonusInput extends Azione {
	

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -2217916779307616155L;
	private static final String AZIONE_NON_CONCLUSA = "ERROR: Azione non conclusa";
	/**
	 * oggetto scelto come input
	 */
	private OggettoBonusInput oggetto;
	/**
	 * ricompensa ricevuta dal giocatore
	 */
	private RicompensaSpecial ricompensa;
	
	/**
	 * Costruttore della bonusInput
	 * @param giocatore: giocatore che sta eseguendo l'azione
	 * @param oggetto: oggetto scelto per ottenere il bonus
	 */
	public BonusInput(Giocatore giocatore,OggettoBonusInput oggetto) {
		super(giocatore);
		this.oggetto=oggetto;
	}

	/**
	 * {@inheritDoc}
	 * Assegna il bonus dell'oggetto scelto come ricompensa
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws CdQException {
		this.ricompensa.assegnaBonusScelto(oggetto, giocatore, partita);
		return true;
	}

	/**
	 * {@inheritDoc}
	 * aggiorna il giocatore in base al modello
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		super.updateGiocatore(partita);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita la BonusInput porta dallo stateWBUno allo stateUno
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override 
	public State visit(StateWBUno stato,Partita partita) throws CdQException{
		ricompensa=stato.getRicompense().get(0);
		ricompensa.updateFromServer(giocatore, partita, oggetto);
		if(this.eseguiAzione(partita)){
			stato.getRicompense().remove(0);
			return new StateUno(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());
		}
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita la BonusInput porta dallo stateWBDue allo stateDue
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override 
	public State visit(StateWBDue stato,Partita partita)throws CdQException{
		ricompensa=stato.getRicompense().get(0);
		ricompensa.updateFromServer(giocatore, partita, oggetto);
		if(this.eseguiAzione(partita)){
			stato.getRicompense().remove(0);
			return new StateDue(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());
		}
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita la BonusInput porta dallo stateWBTre allo stateTre
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override 
	public State visit(StateWBTre stato,Partita partita)throws CdQException{
		ricompensa=stato.getRicompense().get(0);
		ricompensa.updateFromServer(giocatore, partita, oggetto);
		if(this.eseguiAzione(partita)){
			stato.getRicompense().remove(0);
			return new StateTre(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());
		}
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita la BonusInput porta dallo stateWBQuattro allo stateQuattro
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override 
	public State visit(StateWBQuattro stato,Partita partita)throws CdQException{
		ricompensa=stato.getRicompense().get(0);
		ricompensa.updateFromServer(giocatore, partita, oggetto);
		if(this.eseguiAzione(partita)){
			stato.getRicompense().remove(0);
			return new StateQuattro(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());
		}
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}

	/**
	 * 
	 * @param ricompensa: setta la ricompensa da assegnare
	 */
	public void setRicompensa(RicompensaSpecial ricompensa) {
		this.ricompensa = ricompensa;
	}



}
