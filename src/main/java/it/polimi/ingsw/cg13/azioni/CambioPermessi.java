/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;


import java.rmi.RemoteException;
import java.util.List;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.mutatio.AiutantiBancoMutatio;
import it.polimi.ingsw.cg13.mutatio.CambioPermessiMutatio;
import it.polimi.ingsw.cg13.partita.Partita;

import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class CambioPermessi extends AzioneVeloce {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -2131124128780183439L;
	/**
	 * mazzo di cui si vogliono cambiare i permessi in cima
	 */
	private MazzoPermessi mazzo;
	/**
	 * numero dei permessi in cima visibili
	 */
	private int numeroPermessi;
	
	/**
	 * Costruttore della cambio permessi
	 * @param gioc-->giocatore che compie l' azione
	 * @param mazzo-->mazzo dei permessi
	 * @throws IllegalArgumentException--> valori nulli
	 */
	public CambioPermessi(MazzoPermessi mazzo,Giocatore giocatore) {
		super(giocatore);
		if(mazzo!=null && mazzo.getPermessiVisibili()>0){
			this.mazzo=mazzo;
			this.numeroPermessi=mazzo.getPermessiVisibili();
		}
		else
			throw new IllegalArgumentException("Mazzo passato non valido!");
	}

	
	/**
	 * {@inheritDoc}
	 * Se il giocatore ha almeno un aiutante, esegue la CambiaPermessi
	 * Prende i permessi in cima, li mette in fondo e toglie un aiutante al giocatore
	 * @throws AzioneNotPossibleException: se non si hanno abbastanza aiutanti
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws CdQException, RemoteException {
		if(giocatore.numeroAiutanti()>=1){
			List<PermessoCostruzione> carte;
			carte=mazzo.getCarteInCima(numeroPermessi);	
			mazzo.getMazzoCarte().removeAll(carte);
			carte.forEach(carta->mazzo.getMazzoCarte().add(0, carta));
			giocatore.usaAiutanti(1);
			partita.accumulaAiutante();
			partita.notifyObservers(new AiutantiBancoMutatio(0,1));
			partita.notifyObservers(new CambioPermessiMutatio(mazzo,this.getRegioneMazzo(partita)));
			return true;
		}
		else{
			//Non hai abbastanza aiutanti per fare la mossa
			throw new AzioneNotPossibleException("Non hai abbastanza aiutanti per fare l'Azione");
		}
	}

	/**
	 * {@inheritDoc}
	 * Aggiorna in base al modello passato, il giocatore dell'azione
	 * il mazzo dei permessi e il numero di permessi
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		this.updateGiocatore(partita);
		this.mazzo=partita.getMazzoPermessi(mazzo);
		this.numeroPermessi=mazzo.getPermessiVisibili();
		
	}
	
	/**
	 * Cerca e ritorna la regione del mazzo
	 * @param partita: modello in cui cercare
	 * @return regione: regionen contenente il mazzo corrente
	 * @throws RegioneNotFoundException se non trova la regione
	 */
	public Regione getRegioneMazzo(Partita partita) throws RegioneNotFoundException{
		return partita.getMappaPartita().getRegioni().stream().
				filter(regione->regione.getMazzoPermesso().equals(mazzo)).
				findFirst().orElseThrow(()->new RegioneNotFoundException("regione del mazzo non trovata"));
	}


	/**
	 * Ritorna mazzo a cui si vogliono cambiare i permessi
	 * @return mazzo a cui cambiare i permessi 
	 */
	public MazzoPermessi getMazzo() {
		return mazzo;
	}

	/**
	 * Ritorna il numero di carte permesso visibili al giocatore
	 * @return numero di permessi visibili
	 */
	public int getNumeroPermessi() {
		return numeroPermessi;
	}


}
