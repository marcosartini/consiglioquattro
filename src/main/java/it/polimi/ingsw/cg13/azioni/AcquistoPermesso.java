/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.mutatio.MazzoScartoMutatio;
import it.polimi.ingsw.cg13.mutatio.PermessoToltoMutatio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class AcquistoPermesso extends AzionePrincipale {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -7716017545138769020L;
	/**
	 * lista delle carta politiche per corrompere il consiglio
	 */
	private List<CartaPolitica> cartePolitiche;
	/**
	 * regione da dove prendere il permesso e contenente il consiglio da corrompere
	 */
	private Regione regione;
	/**
	 * copia del consiglio da corrompere
	 */
	private Consiglio consiglio;
	/**
	 * permesso scelto da acquistare
	 */
	private PermessoCostruzione permessoScelto;
	/**
	 * mazzo dei permessi da dove prendere il permesso
	 */
	private MazzoPermessi mazzoPermessi;
	
	/**
	 * Costruttore dell'acquista permesso
	 * @param gioc-->giocatore che compie l' azione
	 * @param carte-->CartePolitiche da utilizzate
	 * @param regione-->regione contenente mazzo e consiglio
	 * @param permesso--> permesso costruzione da acquistare
	 * @throws IllegalArgumentException--> valori nulli
	 */
	public AcquistoPermesso(Giocatore gioc,List<CartaPolitica> carte, Regione regione, PermessoCostruzione permesso) {
		super(gioc);
		if(carte!=null && !carte.isEmpty() && !carte.contains(null) )
			this.cartePolitiche=carte;
		else
			throw new IllegalArgumentException("Devono essere passate delle carte!");
		
		if(regione!=null)
			this.regione=regione;
		else
			throw new IllegalArgumentException("La regione non deve essere nulla!");
		
		if(regione.getConsiglio()!=null)
			this.consiglio=regione.getConsiglio().copiaConsiglio();
		else
			throw new IllegalArgumentException("La regione ha un consiglio nullo o non valido!");
		
		if(permesso!=null)
			this.permessoScelto=permesso;
		else
			throw new IllegalArgumentException("Permesso passato non valido!");
		
		if(regione.getMazzoPermesso()!=null)
			this.mazzoPermessi=regione.getMazzoPermesso();
		else
			throw new IllegalArgumentException("Il MazzoPermessi passato non è valido!");
		
	}

	/**
	 * {@inheritDoc}
	 * Esegue l'azione di AcquistoPermesso
	 * Corrompe il consiglio scelto con le carte passate
	 * Calcola il costo dell'azione in base alle carte e i consiglieri riscontrati
	 * Aggiorna il tutto assegnando il permesso, rimuovendo le carte
	 * @throws AzioneNotPossibleException: se non si riscontra alcun consigliere oppure non si ha abbastanza monete per la mossa
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws  CdQException, RemoteException {
		
		Integer costoAggiuntivo;
		List<CartaPolitica> carteRiscontrate=new ArrayList<>();
		
		costoAggiuntivo=this.corrompiConsiglio(carteRiscontrate);
		int consiglieriNonRiscontrati=this.consiglio.getBalcone().size();
		
		if(consiglieriNonRiscontrati>=4)
			throw new AzioneNotPossibleException(
					"Nessuna delle carte politiche corrompe il consiglio ");
		
		int costoMossa=partita.getCostoAggiuntivoAzione(carteRiscontrate.size())+costoAggiuntivo;
		if(giocatore.getRicchezzaGiocatore()>=costoMossa){	
			this.risultatoAzione(partita, costoMossa, carteRiscontrate);
			partita.notifyObservers(new MazzoScartoMutatio(carteRiscontrate));
			partita.notifyObservers(new PermessoToltoMutatio(this.regione,this.permessoScelto));
			return true;	
		}
		else{
			//Non ci sono carte corrispondenti al balcone o non hai abbastanza monete
			throw new AzioneNotPossibleException(
			"Non hai abbastanza punti Ricchezza per corrompere il consiglio");
		}
	}
	
	
	/**
	 * Aggiorna il modello e il giocatore con i risultati dell'azione
	 * assegna il permesso e rimuove le carte usate
	 * @param partita: modello su cui apllicare l'azione
	 * @param costoMossa: costo dell'azione che si sta eseguendo
	 * @param carteRiscontrate: carte riscontrate nel match con il consiglio
	 */
	public void risultatoAzione(Partita partita,int costoMossa,List<CartaPolitica> carteRiscontrate){
		giocatore.spendiMonete(costoMossa, partita.getPercorsoRicchezza());
		giocatore.accumulaPermesso(this.permessoScelto);
		giocatore.removeCarte(carteRiscontrate);
		permessoScelto.assegnaRicompense(giocatore, partita);
		this.mazzoPermessi.getMazzoCarte().remove(permessoScelto);
		partita.scartaCarta(carteRiscontrate);
	}
	
	/**
	 * Corrompe il consiglio matchando ogni carta con un consigliere
	 * @param carteRiscontrate: carte riscontrate nella corruzione del consiglio
	 * @return costoAggiuntivo: somma del costo dell'uso delle carte ( 1 per ogni jolly, 0 per le altre )
	 */
	public int corrompiConsiglio(List<CartaPolitica> carteRiscontrate){
		int costoAggiuntivo=0;
		for(CartaPolitica carta:cartePolitiche){
			if(this.matchCartaConsigliere(carta, carteRiscontrate)){
				costoAggiuntivo+=carta.getCosto();
			}
		}
		return costoAggiuntivo;
	}

	/**
	 * Controlla che la carta matchi con uno dei consiglieri
	 * Se matcha, la carta viene aggiunta alle carte riscontrate
	 * e il consigliere corrispondente viene tolto dal consiglio 
	 * @param carta: cartapolitca usata per corrompere un consigliere
	 * @param carteRiscontrate: carte che hanno trovato riscontro nel consiglio
	 * @return true se la carta corrompe un consigliere del consiglio
	 */
	public boolean matchCartaConsigliere(CartaPolitica carta,List<CartaPolitica> carteRiscontrate){
		boolean trovato=false;
		Queue<Consigliere> balcone=this.consiglio.getBalcone();
		Consigliere consigliere=null;
		Iterator<Consigliere> iteratorBalcone=balcone.iterator();
		while(iteratorBalcone.hasNext() && !trovato){
			consigliere=iteratorBalcone.next();
			if(carta.match(consigliere)){
				carteRiscontrate.add(carta);
				trovato=true;
			}
		}	
		if(trovato && consigliere!=null){
			this.consiglio.getBalcone().remove(consigliere);
		}
		
		return trovato;	
	}

	/**
	 * {@inheritDoc}
	 * Aggiorna rispetto al modello passato il giocatore,
	 * la regione, il consiglio, il mazzo e il permesso scelti
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		this.updateGiocatore(partita);
		//Carte non da aggiornare sono create nuove e uguali, gia' gestite 
		//Aggiorno Regione
		Regione regioneRiscontrata;
		try {
			regioneRiscontrata = partita.getRegione(regione);
		} catch (RegioneNotFoundException e) {
			throw new ErrorUpdateServerException(e);
		}
			//Aggiorno Consiglio
			this.consiglio=regioneRiscontrata.getConsiglio().copiaConsiglio();
			
			//Aggiorno Permesso
			MazzoPermessi mazzo=regioneRiscontrata.getMazzoPermesso();
			this.mazzoPermessi=mazzo;
			
			//Aggiorno permesso
			PermessoCostruzione permessoRiscontrato;
			permessoRiscontrato=mazzo.getCarteInCima(mazzo.getPermessiVisibili()).stream().
									filter(permesso -> permesso.equals(this.permessoScelto)).
									findFirst().orElse(null);
			if(permessoRiscontrato==null){
				throw new ErrorUpdateServerException("Permesso non trovato");
			}
			this.permessoScelto=permessoRiscontrato;
			
	}

	/**
	 * Ritorna la lista delle carte politiche usate per corrompere il consiglio
	 * @return lista delle carte politiche 
	 */
	public List<CartaPolitica> getCartePolitiche() {
		return cartePolitiche;
	}

	/**
	 * Ritorna la regione dove si e' scelto di acquistare il permesso
	 * @return regione scelta
	 */
	public Regione getRegione() {
		return regione;
	}

	/**
	 * Ritorna il consiglio da corrompere
	 * @return il consiglio scelto
	 */
	public Consiglio getConsiglio() {
		return consiglio;
	}

	/**
	 * Ritorna il permesso che si vuole acquistare
	 * @return permesso da acquistare
	 */
	public PermessoCostruzione getPermessoScelto() {
		return permessoScelto;
	}

	/**
	 * Ritorna il mazzo da cui si sta prendendo il permesso
	 * @return mazzo da cui estrarre il permesso
	 */
	public MazzoPermessi getMazzoPermessi() {
		return mazzoPermessi;
	}
	
	
	
	

}
