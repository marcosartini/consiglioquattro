/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;




import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.mutatio.ConsiglieriBancoMutatio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class CambioConsiglio extends AzionePrincipale {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -3399549056700740715L;
	/**
	 * consiglio da cambiare
	 */
	private Consiglio consiglio;
	/**
	 * consigliere da inserire nel consiglio 
	 */
	private Consigliere consigliereDaInserire;
	
	/**
	 * Costruttore della CambioConsiglio
	 * @param gioc-->giocatore che compie l' azione
	 * @param consiglio-->consiglio da modificare
	 * @param consigliere-->nuovo consigliere da inserire
	 * @throws IllegalArgumentException--> valori nulli
	 */
	public CambioConsiglio(Giocatore gioc, Consiglio consiglio, Consigliere consigliere) {
		super(gioc);
		if(consiglio!=null)
			this.consiglio=consiglio;
		else
			throw new IllegalArgumentException("Consiglio passato non valido!");
		if(consigliere!=null)
			this.consigliereDaInserire=consigliere;
		else
			throw new IllegalArgumentException("Consigliere passato non valido!");
	}
	
	/**
	 * {@inheritDoc}
	 * Esegue la CambioConsiglio Principale
	 * Toglie il consigliere piu' "vecchio" e aggiunge il consigliere passato come parametro
	 * Notifica l'azione alla view e aggiunge 4 monete al giocatore
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws CdQException, RemoteException {
		Consigliere consigliereTolto=this.consiglio.sostituzioneConsigliere(this.consigliereDaInserire);
		this.giocatore.riceviMonete(4, partita.getPercorsoRicchezza());	
		partita.accumulaConsigliere(consigliereTolto);
		partita.notifyObservers(new ConsiglieriBancoMutatio(consigliereDaInserire,consigliereTolto));
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * Aggiorna in base al modello passato, le istanze di giocatore
	 * consiglio e consigliere
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		super.updateGiocatore(partita);
		this.consiglio=partita.getConsiglio(consiglio);
		try {
			this.consigliereDaInserire=partita.getConsigliere(consigliereDaInserire);
		} catch (ColoreNotFoundException e) {
			throw new ErrorUpdateServerException(e);
		}	
		
	}

	/**
	 * Ritorna il consiglio che si sta cambiando
	 * @return consiglio in cambiamento
	 */
	public Consiglio getConsiglio() {
		return consiglio;
	}

	/**
	 * Ritorna il consigliere da inserire
	 * @return consigliere nuovo del consiglio
	 */
	public Consigliere getConsigliereDaInserire() {
		return consigliereDaInserire;
	}

	

}
