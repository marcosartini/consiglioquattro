/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;


import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.comandi.Comando;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;

import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.visitor.Visitor;

/**
 * @author Marco
 *
 */
public abstract class Azione implements Comando,Visitor{
	
	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -4683825781758583147L;
	/**
	 * giocatore che sta effettuando l'azione
	 */
	protected Giocatore giocatore;
	
	
	
	/**
	 * Costruttore di azione 
	 * Inserisci il giocatore come giocatore della mossa
	 * @param giocatore che sta eseguendo l'azione
	 * @throws IllegalArgumentException se il giocatore e' null
	 */
	public Azione(Giocatore giocatore) {
		if(giocatore!=null)
			this.giocatore = giocatore;
		else
			throw new IllegalArgumentException("Il giocatore che esegue l' azione non deve essere nullo!");
	}

	/**
	 * Esegue l'azione a seconda del tipo di azione
	 * @param partita: modello su cui viene applicata l'azione
	 * @return true se l'azione viene portata a termine altrimenti viene lanciata un'eccezione
	 * @throws CdQException: se le condizioni per l'azione non vengono soddisfatte
	 * @throws RemoteException : problema di chiamate in remoto dell'azione
	 */
	public abstract boolean eseguiAzione(Partita partita) throws CdQException, RemoteException;
	
	/**
	 * Aggiorna i parametri dell'azione con le istanze del modello passato come parametro
	 * @param partita: modello su cui applicare l'azione
	 * @throws CdQException: se non vengono trovate istanze uguali sul modello passato, non c'e' corrispodenza
	 */
	public abstract void updateParametri(Partita partita) throws CdQException;
	
	/**
	 * Aggiorna l'istanza del giocatore dell'azione con quella presente nel modello
	 * @param partita: modello dove cercare il giocatore e applicare l'azione
	 * @throws ErrorUpdateServerException: se il giocatore non viene trovato nel modello
	 */
	public void updateGiocatore(Partita partita) throws ErrorUpdateServerException{
			Giocatore giocatoreRiscontrato=partita.getGiocatori().stream().
				filter(player -> player.equals(this.giocatore)).
				findFirst().orElse(null);
			if(giocatoreRiscontrato==null){
				throw new ErrorUpdateServerException(" giocatore"+this.giocatore.getNome()+" non trovato");
			}
			this.setGiocatore(giocatoreRiscontrato);
	}
	
	

	/**
	 * Ritorna il giocatore che sta eseguendo l'azione
	 * @return giocatore
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}

	/**
	 * Setta il giocatore dell'azione con quello passato
	 * @param giocatore : giocatore da settare
	 * @throws IllegalArgumentException: se il giocatore passato e' null
	 */
	public void setGiocatore(Giocatore giocatore) {
		if(giocatore!=null)
			this.giocatore = giocatore;
		else
			throw new IllegalArgumentException("Il giocatore che esegue l' azione non deve essere nullo!");
	}
	
	
	
}
