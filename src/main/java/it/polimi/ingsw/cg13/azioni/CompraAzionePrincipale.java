/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;


import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateInizio;
import it.polimi.ingsw.cg13.machinestate.StateSei;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.mutatio.AiutantiBancoMutatio;
import it.polimi.ingsw.cg13.partita.Partita;

import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class CompraAzionePrincipale extends AzioneVeloce {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2231879070828511993L;
	private static final String AZIONE_NON_CONCLUSA = "ERROR: Azione non conclusa";
	/**
	 * Costruttore della CompraAzionePrincipale
	 * @param azioni : Lista delle azioni possibili del giocatore
	 * @param gioc : Giocatore che esegue la mossa
	 * @throws IllegalArgumentException--> Giocatore nullo
	 */
	public CompraAzionePrincipale(Giocatore gioc) {
		super(gioc);
	}
	
	/**
	 * L'azione puo' essere eseguita se il giocatore ha piu' di 3 aiutanti 
	 * @return true se e' possibile fare la seguente mossa CompraAzionePrincipale
	 */
	private boolean isPossibileFareMossa(){
		return giocatore.numeroAiutanti()>=3;
	}

	/**
	 * {@inheritDoc}
	 * Se il e' possibile fare la mossa, esegue la compraAzioneVeloce
	 * Cambia lo stato corrente del giocatore, e rimuove i tre aiutanti
	 * @throws AzioneNotPossibleException: se non viene soddisfatta la condizione
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws CdQException, RemoteException {
		if(this.isPossibileFareMossa()){
			this.giocatore.usaAiutanti(3);
			for(int i=0;i<3;i++){
				partita.accumulaAiutante();
			}
			partita.notifyObservers(new AiutantiBancoMutatio(0,3));
			return true;
		}	
		else{
			// Non hai abbastanza aiutanti per fare la mossa
			throw new AzioneNotPossibleException("Non hai abbastanza aiutanti per fare la mossa");
		}
	}

	/**
	 *{@inheritDoc}
	 *Aggiorna il giocatore 
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		this.updateGiocatore(partita);
		
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita una compraAzioneVeloce porta dallo stateInizio allo stateSei
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateInizio stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateSei(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita una compraAzioneVeloce porta dallo stateUno allo stateTre
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateUno stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateTre(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita una compraAzioneVeloce porta dallo stateDue allo stateSei
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateDue stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateSei(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	
	

}
