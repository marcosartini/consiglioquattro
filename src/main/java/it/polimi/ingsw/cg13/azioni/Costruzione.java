package it.polimi.ingsw.cg13.azioni;


import java.util.Collection;

import it.polimi.ingsw.cg13.bonus.Ricompensa;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public abstract class Costruzione extends AzionePrincipale{

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -1919881883597608695L;
	
	/**
	 * Costruttore della Costruzione
	 * @param giocatore : Giocatore che esegue la mossa
	 * @throws IllegalArgumentException--> Giocatore nullo
	 */
	public Costruzione(Giocatore giocatore) {
		super(giocatore);		
	}

	/**
	 * Ritorna se il giocatore ha ancora empori
	 * @return true se il numero di empori del giocatore e' maggiore di 0
	 */
	public boolean giocatoreHaEmpori(){
		return this.giocatore.getNumeroEmpori()>0;
	}
	
	/**
	 * Riceve una lista di ricompense e le assegna al giocatore che ha effettuato l'azione
	 * @param ricompense lista di ricompense su cui intervenire, anche vuota
	 */
	protected void assegnaRicompense(Collection<Ricompensa> ricompense,Partita partita){
		for(Ricompensa ricompensa: ricompense){
			ricompensa.assegnaRicompensa(this.giocatore, partita);
		}
	}
	
	/**
	 * Controlla se il giocatore ha gia' costruito nella citta passata
	 * @param citta da controllare 
	 * @return true se ci sono empori di quel giocatore
	 */
	public boolean giaCostruito(Citta citta){
		return citta.hasEmporio(this.giocatore);
	}
	
	/**
	 * Ritorna se il giocatore puo' pagare in aiutanti la presenza di empori 
	 * nella citta passata
	 * @param citta in cui controllare la presenza di empori
	 * @return true se il giocatore ha piu' aiutanti rispetto agli empori presenti
	 * @throws AzioneNotPossibleException se il giocatore non ha abbstanza aiutanti
	 */
	public boolean costoAggiuntivo(Citta citta) throws AzioneNotPossibleException{
		if(citta.getEmpori().isEmpty()){
			return true;
		}
		else if(citta.getEmpori().size()<=this.giocatore.numeroAiutanti()){
			return true;
		}
		else throw new AzioneNotPossibleException("Non hai abbastanza aiutanti per costruire in questa citta");
	}
	
	/**
	 * Verifica se nella citta passata, il giocatore puo' costruire oppure no
	 * Puo' costruire se non ha gia' costruito nella citta, se ha almeno un emporio
	 * e se ha abbastanza aiutanti per coprire gli empori gia' presenti
	 * Se non si verificano tutte e tre, non puo' costruire
	 * @param citta da verificare
	 * @return true se puo' costruire nella citta, altrimenti false
	 * @throws AzioneNotPossibleException: se non ha abbastanza aiutanti
	 */
	public boolean puoCostruire(Citta citta) throws AzioneNotPossibleException{
		return !giaCostruito(citta)&&this.giocatoreHaEmpori() && this.costoAggiuntivo(citta);
	}
	
	
	

}
