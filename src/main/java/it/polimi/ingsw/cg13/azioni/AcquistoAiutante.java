/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;


import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mutatio.AiutantiBancoMutatio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class AcquistoAiutante extends AzioneVeloce {
	

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -3787429953266845342L;


	/**
	 * 
	 * @param gioc-->giocatore che effettua la mossa
	 * @throws IllegalArgumentException--> Giocatore nullo
	 */
	public AcquistoAiutante(Giocatore gioc) {
		super(gioc);
	}
	
	/**
	 * {@inheritDoc}
	 * Azione AcquistoAiutante:
	 * se il giocatore ha una ricchezza maggiore di 3 e nella partita ci sono ancora aiutanti,
	 * il giocatore spende tre monete e guadagna un aiutante e notifica alla view l'avvenuto acquisto
	 * @throws RemoteException 
	 * @throws AzioneNotPossibleException: se le condizioni sopra non sono soddisfatte
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws CdQException, RemoteException {
		if(this.giocatore.getRicchezzaGiocatore()>=3 && !partita.isEmptyAiutanti()){
			giocatore.spendiMonete(3, partita.getPercorsoRicchezza());
			giocatore.addAiutanti(1);
			partita.getAiutante();
			partita.notifyObservers(new AiutantiBancoMutatio(1,0));
			return true;
		}
		else {
			throw new AzioneNotPossibleException(
			"Non hai abbastanza monete o non ci sono aiutanti disponibili in Partita");	
		}
	}


	/**
	 * {@inheritDoc}
	 * Aggiorna il giocatore con l'istanza del modello locale
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		this.updateGiocatore(partita);
	}

	

	

}
