/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;


import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateFine;
import it.polimi.ingsw.cg13.machinestate.StateInizio;
import it.polimi.ingsw.cg13.machinestate.StateOtto;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public abstract class AzioneVeloce extends Azione {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -306336897965502980L;
	private static final String AZIONE_NON_CONCLUSA = "ERROR: Azione non conclusa";
	/**
	 * Costruttore di un azione Veloce
	 * richiama la super di azione
	 * @param giocatore : Giocatore che esegue la mossa
	 * @throws IllegalArgumentException--> Giocatore nullo
	 */
	public AzioneVeloce(Giocatore giocatore) {
		super(giocatore);
	}

	/**
	 *  {@inheritDoc}
	 *  Sta eseguendo un azione veloce
	 */
	@Override
	public abstract  boolean eseguiAzione(Partita partita) throws CdQException, RemoteException;
	
	/**
	 * {@inheritDoc}
	 * Eseguita una azioneVeloce porta dallo stateInizio allo stateOtto
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateInizio stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateOtto(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita una azioneVeloce porta dallo stateDue allo stateTre
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateDue stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateTre(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}
	
	/**
	 * {@inheritDoc}
	 * Eseguita una azioneVeloce porta dallo stateUno allo stateFine
	 * se l'eseguiAzione viene portata a termine altrimenti lancia un'AzioneNotPossibleException
	 */
	@Override
	public State visit(StateUno stato,Partita partita) throws CdQException, RemoteException{
		if(this.eseguiAzione(partita))
			return new StateFine(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(),stato.getRicompense());
		else throw new AzioneNotPossibleException(AZIONE_NON_CONCLUSA);
	}

}
