/**
 * 
 */
package it.polimi.ingsw.cg13.azioni;







import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mutatio.AiutantiBancoMutatio;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class CostruzioneConPermesso extends Costruzione{

	
	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -6874745408910066288L;
	/**
	 * citta in cui si vuole costruire
	 */
	NodoCitta nodoCitta;
	/**
	 * permesso da usare per costruire
	 */
	PermessoCostruzione permesso;
	/**
	 * Costruttore della costruisciConPermesso
	 * @param gioc--> giocatore che esegue l' azione
	 * @param cit--> nodoCitta in cui costruire
	 * @param permesso--> permesso utilizzato
	 * @throws IllegalArgumentException--> valori nulli o non validi
	 */
	public CostruzioneConPermesso(Giocatore gioc,NodoCitta cit,PermessoCostruzione permesso) {
		super(gioc);
		if(cit!=null && cit.getCittaNodo()!=null)
			this.nodoCitta=cit;
		else
			throw new IllegalArgumentException("NodoCittà non valido!");
		if(permesso!=null)
			this.permesso=permesso;
		else
			throw new IllegalArgumentException("PermessoCostruzione non valido!");
		
	}
	
	/**
	 * {@inheritDoc}
	 * Se soddisfa la condizione di citta corretta e puo' costrurie, 
	 * viene eseguita l'azione di costruisciConPermesso altrimenti viene
	 * lanciata un'eccezione che indica il motivo per cui non si puo' costruire
	 * @throws RemoteException 
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws CdQException, RemoteException {
		if(this.cittaGiusta() && this.puoCostruire(this.nodoCitta.getCittaNodo())){
			int numeroEmpori=nodoCitta.getCittaNodo().getEmpori().size();
			if(numeroEmpori!=0)
				giocatore.usaAiutanti(numeroEmpori);
			Emporio emporio=this.giocatore.usaEmporio();
			giocatore.usaPermesso(permesso);
			nodoCitta.getCittaNodo().costruisciEmporio(emporio);	
			assegnaRicompense(nodoCitta.bfsRicompense(giocatore),partita);
			if(numeroEmpori!=0)
				partita.notifyObservers(new AiutantiBancoMutatio(0,numeroEmpori ));
			return true;
		}
		else{
			//Non puo costruire 
			throw new AzioneNotPossibleException("Non hai i requisiti per costruire in questa citta con questo permesso");
		}
	}
	

	/**
	 * Torna se la citta scelta e' contenuta nella citta del permesso
	 * @return true il permesso permette la costruzione della citta
	 */
	public boolean cittaGiusta(){
		return this.permesso.getListaCitta().contains(this.nodoCitta.getCittaNodo());
	}
	
	/**
	 * {@inheritDoc}
	 * Aggiorna il permesso e la citta in base al parametro passato come parametro 
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		//Update giocatore
		super.updateGiocatore(partita);
		//Aggiorno permesso
		PermessoCostruzione permessoRiscontrato=giocatore.getPermessiCostruzione().stream().
				filter(cartaperm -> cartaperm.equals(this.permesso)).
				findFirst().orElseThrow(()->new ErrorUpdateServerException("permesso non trovato"));
		this.permesso=permessoRiscontrato;
		NodoCitta nodoCitta1;
		try {
			nodoCitta1 = partita.getMappaPartita().getNodoCitta(this.nodoCitta.getCittaNodo());
		} catch (CittaNotFoundException e) {
			throw new ErrorUpdateServerException(e);
		} //modificata ricerca nodo
		if(nodoCitta1==null){
			throw new ErrorUpdateServerException("citta "+nodoCitta.getCittaNodo().getNome()+" non trovata");
		}
		this.nodoCitta=nodoCitta1;
		

	}

	/**
	 * 
	 * @return citta in cui si vuole costruire
	 */
	public NodoCitta getNodoCitta() {
		return nodoCitta;
	}

	/**
	 * 
	 * @return permesso che si vuole usare per costruire
	 */
	public PermessoCostruzione getPermesso() {
		return permesso;
	}
	
}
