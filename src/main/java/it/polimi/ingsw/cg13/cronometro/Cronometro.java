package it.polimi.ingsw.cg13.cronometro;

import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.mutatio.LineGiocatoreMutatio;
import it.polimi.ingsw.cg13.mutatio.TempoScadutoMutatio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;


public class Cronometro implements Runnable{
	
	private static Cronometro istanza=null;
	private int tempo;
	private Partita modello;
	
	private static final Logger logger = LoggerFactory.getLogger(Cronometro.class);
	
	/**
	 * Crea un cronometro per la gestione del tempo di azione
	 * @param partita: modello con cui interagisce
	 */
	public Cronometro(Partita partita) {
		modello=partita;
		tempo=modello.getParametri().getDurataMossa();
	}
	
	
	/**
	 * Ritorna l'istanza se gia creata altrimenti ne crea una nuova (Singleton)
	 * @param partita: modello con cui interagisce
	 * @return istanza del cronometro
	 */
	public static synchronized Cronometro getIstanza(Partita partita){
		if(istanza==null){
			istanza=new Cronometro(partita);
		}
		return istanza;
	}

	
	@Override
	public void run() {
		while(!finePartita()){
			if(!tempoScaduto()){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					synchronized(this){
						logger.error(e.getMessage(),e);
						modello.notifyObservers(e);
						Cronometro.getIstanza(modello);
					}	
				}
				this.decrementaTempo();
			}
			else{
				this.setGiocatoreOffline();
			}
		}
		
	}
	
	
	private synchronized void decrementaTempo() {
		this.tempo--;
	}


	private synchronized boolean finePartita() {
		return modello.isEnded();
	}


	private synchronized boolean tempoScaduto() {
		return tempo==0;
	}


	private synchronized void setGiocatoreOffline() {
		State stato=modello.getStatoPartita();
		Giocatore giocatoreUscente=stato.getGiocatoreCorrente();	
		giocatoreUscente.setIsOnline(false);
		modello.getGiocatoriOffline().add(giocatoreUscente);
		modello.getGiocatori().remove(giocatoreUscente);
		stato=stato.setEndState();
		try {
			modello.notifyObservers(new TempoScadutoMutatio(giocatoreUscente));
			modello.notifyObservers(new LineGiocatoreMutatio(giocatoreUscente));
			stato=stato.nextState(modello);
			modello.setStatoPartita(stato);
		} catch (RemoteException | CdQException e) {
			modello.notifyObservers(e);
		}
		istanza.setTempo(modello.getParametri().getDurataMossa());
	}


	public synchronized int getTempo() {
		return tempo;
	}


	public synchronized void setTempo(int tempo) {
		this.tempo = tempo;
	}

}
