package it.polimi.ingsw.cg13.visitorforgui;


import it.polimi.ingsw.cg13.mutatio.BonusLosedMutatio;
import it.polimi.ingsw.cg13.mutatio.CambioPermessiMutatio;
import it.polimi.ingsw.cg13.mutatio.CasellaMutatioAddGiocatore;
import it.polimi.ingsw.cg13.mutatio.CasellaMutatioRemoveGiocatore;
import it.polimi.ingsw.cg13.mutatio.ChatMutatio;
import it.polimi.ingsw.cg13.mutatio.CittaMutatio;
import it.polimi.ingsw.cg13.mutatio.ClassificaMutatio;
import it.polimi.ingsw.cg13.mutatio.ConsiglieriBancoMutatio;
import it.polimi.ingsw.cg13.mutatio.ConsiglioMutatio;
import it.polimi.ingsw.cg13.mutatio.FineFaseMarketMutatio;
import it.polimi.ingsw.cg13.mutatio.InizioFaseMarketMutatio;
import it.polimi.ingsw.cg13.mutatio.InserzioneAcquistataMutatio;
import it.polimi.ingsw.cg13.mutatio.InserzioneInVenditaMutatio;
import it.polimi.ingsw.cg13.mutatio.KingMutatio;
import it.polimi.ingsw.cg13.mutatio.LineGiocatoreMutatio;
import it.polimi.ingsw.cg13.mutatio.MescolatoMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.mutatio.NomeNonDisponibileMutatio;
import it.polimi.ingsw.cg13.mutatio.PartitaMutatio;
import it.polimi.ingsw.cg13.mutatio.PermessoToltoMutatio;
import it.polimi.ingsw.cg13.mutatio.PrimoGiocatoreMutatio;
import it.polimi.ingsw.cg13.mutatio.QueryMutatio;
import it.polimi.ingsw.cg13.mutatio.StartConnessioneMutatio;
import it.polimi.ingsw.cg13.mutatio.StateMutatio;
import it.polimi.ingsw.cg13.mutatio.TempoScadutoMutatio;
import it.polimi.ingsw.cg13.mutatio.TesseraBonusMutatio;
import it.polimi.ingsw.cg13.mutatio.UltimoTurnoMutatio;

public interface VisitMutatio {

	public void visit(BonusLosedMutatio change);
	public void visit(CambioPermessiMutatio change);
	public void visit(CasellaMutatioAddGiocatore change);
	public void visit(CasellaMutatioRemoveGiocatore change);
	public void visit(ChatMutatio change);
	public void visit(CittaMutatio change);
	public void visit(ClassificaMutatio change);
	public void visit(ConsiglieriBancoMutatio change);
	public void visit(ConsiglioMutatio change);
	public void visit(InserzioneAcquistataMutatio change);
	public void visit(InserzioneInVenditaMutatio change);
	public void visit(KingMutatio change);
	public void visit(LineGiocatoreMutatio change);
	public void visit(Mutatio change);
	public void visit(PartitaMutatio change);
	public void visit(PermessoToltoMutatio change);
	public void visit(QueryMutatio change);
	public void visit(StartConnessioneMutatio change);
	public void visit(StateMutatio change);
	public void visit(TesseraBonusMutatio change);
	public void visit(UltimoTurnoMutatio change);
	public void visit(TempoScadutoMutatio change);
	public void visit(InizioFaseMarketMutatio change);
	public void visit(FineFaseMarketMutatio change);
	public void visit(PrimoGiocatoreMutatio mutatio);
	public void visit(NomeNonDisponibileMutatio mutatio);
	public void visit(MescolatoMutatio change);
	
}
