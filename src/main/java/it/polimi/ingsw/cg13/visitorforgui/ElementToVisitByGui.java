package it.polimi.ingsw.cg13.visitorforgui;

import java.io.Serializable;

import it.polimi.ingsw.cg13.gui.ClientGUIController;

public interface ElementToVisitByGui extends Serializable{
	
	public void accept(ClientGUIController gui);

}
