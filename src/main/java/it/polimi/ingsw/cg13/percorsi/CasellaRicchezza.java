/**
 * 
 */
package it.polimi.ingsw.cg13.percorsi;

/**
 * @author Marco
 *
 */
public class CasellaRicchezza extends Casella {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 4807179317708032259L;

	/**
	 * Costruttore della classe CasellaRicchezza
	 * Richiama il costruttore della superclasse
	 * Crea una casellaRicchezza con punti i valori passati e 
	 * con giocatore presenti un Set vuoto
	 * @param valore da assegnare alla casella creata
	 * @throws IllegalArgumentException : se assegno un valore nullo o negativo alla casella
	 */
	public CasellaRicchezza(Integer valore) {
		super(valore);
		
	}

}
