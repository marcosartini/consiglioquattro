/**
 * 
 */
package it.polimi.ingsw.cg13.percorsi;

import java.util.List;

/**
 * @author Marco
 *
 */
public class PercorsoNobilta extends Percorso<CasellaNobilta> {

	/**
	 * id per la seriliazzazione
	 */
	private static final long serialVersionUID = -4594372363670250559L;

	/**
	 * Costruttore che crea un percorso nobilta data una lista
	 * di caselle vittoria come parametro
	 * @param caselle con cui voglio creare il percorso nobilta
	 * @throws IllegalArgumentException: se lista di caselle nulla o vuota
	 */
	public PercorsoNobilta(List<CasellaNobilta> caselle) {
		super(caselle);
	}

	/** 
	 * Crea un percorso uguale al percorso passato
	 * come parametro
	 * @param percorso
	 * @throws IllegalArumentException: se passo un percorso nullo o contenente una lista vuota di caselle
	 */
	public PercorsoNobilta(PercorsoNobilta percorsoNobilta) {
		super(percorsoNobilta);
	}

	/**
	 * Ritorna la lista delle caselle come stringa 
	 * @return stringa del percorso
	 */
	@Override
	public String toString() {
		return "PercorsoNobilta\n" + caselle + "";
	}

}
