/**
 * 
 */
package it.polimi.ingsw.cg13.percorsi;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;;

/**
 * @author Marco
 *
 */
public class CasellaNobilta extends Casella {
	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -6152052049269183748L;
	
	/**
	 * Insieme delle ricompense assegnabili ai giocatori di questa casella 
	 */
	private final Set<Ricompensa> ricompense;
	
	/**
	 * Costruttore della classe CasellaNobilta
	 * Richiama il costruttore della superclasse
	 * Crea una casellaNobilta con punti i valori passati e 
	 * con giocatore presenti un Set vuoto
	 * e infine inizializza il Set di ricompense con un 
	 * Set vuoto da riempire
	 * @param valore da assegnare alla casella creata
	 * @throws IllegalArgumentException : se assegno un valore nullo o negativo alla casella
	 */
	public CasellaNobilta(Integer valore) {
		super(valore);
		this.ricompense=new HashSet<>();		
	}
	
	/**
	 * Aggiunge la ricompensa r al Set delle ricompense
	 * @param r: ricompensa da aggiungere alla casellaNobilta
	 * @throws IllegalArgumentException se si cerca di aggiungere una ricompensa null
	 */
	public void addRicompensa(Ricompensa r)
	{
		if(r==null)
			throw new IllegalArgumentException("Non puoi aggiungere una ricompensa nulla");
		this.ricompense.add(r);
	}
	
	/**
	 * Assegna tutte le ricompense presenti sulla casella
	 * al giocatore passato come parametro
	 * @param giocatore: giocatore a cui assegnare la ricompensa
	 * @param partita: il modello per modificare i risultati della ricompensa
	 * @throws IllegalArgumentException: se si passa un giocatore o partita null
	 */
	public void assegnaRicompense(Giocatore giocatore, Partita partita){
		if(giocatore==null || partita==null)
			throw new IllegalArgumentException("Non posso assegnare un giocatore o una partita nulli");
		Iterator<Ricompensa> iterator = ricompense.iterator();
		while(iterator.hasNext()){
			iterator.next().assegnaRicompensa(giocatore, partita);
		}
	}
	
	/**
	 * Aggiunge il giocatore alla seguente casella e
	 * assegna tutte le ricompense presenti sulla casella
	 * al giocatore aggiunto 
	 * @param giocatore: giocatore da aggiungere alla casella
	 * @param partita: il modello per modificare i risultati della ricompensa
	 * @throws IllegalArgumentException: se si passa un giocatore o partita null
	 */
	@Override
	public void addGiocatore(Giocatore giocatoreDaAggiungere, Partita partita) {
		if(giocatoreDaAggiungere==null || partita==null)
			throw new IllegalArgumentException("Non posso assegnare un giocatore o una partita nulli");
		super.addGiocatore(giocatoreDaAggiungere);
		this.assegnaRicompense(giocatoreDaAggiungere, partita);
	}
	
	
	/**
	 * Ritorna la stringa di casella e in piu' concatena le ricompense presenti sulla casella
	 * @return stringa della casellaNobilta
	 */
	@Override
	public String toString() {
		String stringa= super.toString();
		if(!this.ricompense.isEmpty()){
			stringa+=", ricompense="+ricompense+"";
		}
		return stringa;
	}

	/**
	 * Ritorna l'insieme delle ricompense presenti su questa casella
	 * @return Set delle ricompense della casella
	 */
	public Set<Ricompensa> getRicompense() {
		return ricompense;
	}
}
