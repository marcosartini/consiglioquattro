/**
 * 
 */
package it.polimi.ingsw.cg13.percorsi;

import java.util.List;

/**
 * @author Marco
 *
 */
public class PercorsoVittoria extends Percorso<CasellaVittoria> {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 7001552620786274629L;

	/**
	 * Costruttore che crea un percorso vittoria data una lista
	 * di caselle vittoria come parametro
	 * @param caselle con cui voglio creare il percorso vittoria
	 * @throws IllegalArgumentException: se lista di caselle nulla o vuota
	 */
	public PercorsoVittoria(List<CasellaVittoria> caselle) {
		super(caselle);
	}

	/** 
	 * Crea un percorso uguale al percorso passato
	 * come parametro
	 * @param percorso
	 * @throws IllegalArumentException: se passo un percorso nullo o contenente una lista vuota di caselle
	 */
	public PercorsoVittoria(PercorsoVittoria percorsoVittoria) {
		super(percorsoVittoria);
	}

	/**
	 * Ritorna la lista delle caselle come stringa
	 * @return stringa corrispondente al percorso
	 */
	@Override
	public String toString() {
		return "PercorsoVittoria\n" + caselle + "";
	}

}
