/**
 * 
 */
package it.polimi.ingsw.cg13.percorsi;

/**
 * @author Marco
 *
 */
public class CasellaVittoria extends Casella {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -1947592052828660679L;

	/**
	 * Costruttore della classe CasellaVittoria
	 * Richiama il costruttore della superclasse
	 * Crea una casellaVittoria con punti i valori passati e 
	 * con giocatore presenti un Set vuoto
	 * @param valore da assegnare alla casella creata
	 * @throws IllegalArgumentException : se assegno un valore nullo o negativo alla casella
	 */
	public CasellaVittoria(Integer valore) {
		super(valore);
	}


}
