/**
 * 
 */
package it.polimi.ingsw.cg13.percorsi;

import java.io.Serializable;
import java.util.List;

/**
 * @author Marco
 *
 */
public abstract class Percorso<C extends Casella> implements Serializable{

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 1032175448642341456L;
	/**
	 * Lista delle caselle generiche di cui è composto il percorso generico
	 */
	protected final List<C> caselle;
	/**
	 * 
	 */

	/**
	 * Costruisce un percorso data come parametro
	 * la lista di caselle
	 * @param caselle con cui voglio costruire il percorso
	 * @throws IlllegalArgumentException se la lista di caselle è nulla o vuota
	 */
	public Percorso(List<C> caselle) {
		super();
		if(caselle==null)
			throw new IllegalArgumentException("Non puoi passare una lista vuota");
		if(caselle.isEmpty())
			throw new IllegalArgumentException("Non puoi creare un percose senza alcuna casella");
		this.caselle = caselle;
	}
	
	/**
	 * Crea un percorso uguale al percorso passato
	 * come parametro
	 * @param percorso
	 * @throws IllegalArumentException: se passo un percorso nullo o contenente una lista vuota di caselle
	 */
	public Percorso(Percorso<C> percorso) {
		if(percorso==null)
			throw new IllegalArgumentException("Non puoi passare un percorso vuoto");
		if(percorso.getPercorso().isEmpty())
			throw new IllegalArgumentException("Non puoi passare un percorso senza caselle");
		this.caselle=percorso.caselle;
	}

	/**
	 * Ritorna il numero di caselle di cui è composta la lista
	 * @return il numero di caselle da cui e' composta la lista
	 */
	public int getNumCaselle()	{
		return this.caselle.size();
	}
	
	/**
	 * Ritorna la lista delle caselle di cui e' fatto il percorso
	 * @return la lista di caselle di cui è composto il percorso
	 */
	public  List<C> getPercorso(){
		return caselle;
	}
	
	/**
	 * Ritorna la casella corrispondente al numero richiesto 
	 * oppure l'ultima casella del percorso se il numero supera la dimensione del percorso
	 * @param numero intero positivo o zero
	 * @return la Casella presente al numero ricevuto, o l'ultima se il numero supera il limite
	 * @throws IllegalArgumentException se passo un numero negativo che esce appunto dal percorso
	 */
	public  C getCasella(int numero){
		if(numero<0)
			throw new IllegalArgumentException("Il parametro deve essere non negativo");
		if(numero<caselle.size())
			return caselle.get(numero);
		return caselle.get(caselle.size()-1);
	}

}
