/**
 * 
 */
package it.polimi.ingsw.cg13.percorsi;

import java.util.List;

/**
 * @author Marco
 *
 */
public class PercorsoRicchezza extends Percorso<CasellaRicchezza> {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -4394669253739476603L;

	/**
	 * Costruttore che crea un percorso Ricchezza data una lista
	 * di caselle vittoria come parametro
	 * @param caselle con cui voglio creare il percorso ricchezza
	 * @throws IllegalArgumentException: se lista di caselle nulla o vuota
	 */
	public PercorsoRicchezza(List<CasellaRicchezza> caselle) {
		super(caselle);
	}

	/** 
	 * Crea un percorso uguale al percorso passato
	 * come parametro
	 * @param percorso
	 * @throws IllegalArumentException: se passo un percorso nullo o contenente una lista vuota di caselle
	 */
	public PercorsoRicchezza(PercorsoRicchezza percorsoRicchezza) {
		super(percorsoRicchezza);
	}

	/**
	 * Ritorna la lista delle caselle come stringa 
	 * @return stringa del percorso
	 */
	@Override
	public String toString() {
		return "PercorsoRicchezza\n" + caselle + "";
	}

	
	
}
