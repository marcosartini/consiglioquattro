package it.polimi.ingsw.cg13.percorsi;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import it.polimi.ingsw.cg13.mutatio.CasellaMutatioAddGiocatore;
import it.polimi.ingsw.cg13.mutatio.CasellaMutatioRemoveGiocatore;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public abstract class Casella extends ObservableC<Mutatio> implements Serializable{

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -5759692485861115797L;
	
	/**
	 * punti della casella
	 */
	private final Integer puntiCasella;
	
	/**
	 * giocatori presenti su quella casella
	 */
	private Set<Giocatore> giocatoriPresenti;

	/**
	 * Costruisce una casella con punti equivalenti al valore passato
	 * e per il Set<Giocatori> alloca un nuovo hashSet vuoto
	 * @param valore: intero equivalente al valore della casella
	 * @throws IllegalArgumentException se inserisco un valore negativo o nullo
	 */
	public Casella(Integer valore) {
		if(valore==null)
			throw new IllegalArgumentException("Non è possibile creare una casella con valore null");
		if(valore<0)
			throw new IllegalArgumentException("Non è possibile creare una casella con punti negativi");
		this.puntiCasella = valore;
		this.setGiocatoriPresenti(new HashSet<Giocatore>());
	}
	
	/**
	 * Ritorna i punti della casella
	 * @return i punti equivalenti della casella
	 */

	public Integer getPuntiCasella() {
		return puntiCasella;
	}

	/**
	 * Ritorna il Set di giocatori presenti nella casella
	 * @return Set di giocatori della casella
	 */
	public Set<Giocatore> getGiocatoriPresenti() {
		return giocatoriPresenti;
	}

	/**
	 * Modifica il Set di giocatori presenti nella casella con quello passato per parametro
	 * @param giocatoriPresenti: nuovo Set di giocatori che si vuole associare alla casella
	 * @throws IllegalArgumentException: se cerco di assegnare un Set null
	 */
	public void setGiocatoriPresenti(Set<Giocatore> giocatoriPresenti) {
		if(giocatoriPresenti==null)
			throw new IllegalArgumentException("Non puoi passare un Set nullo di giocatori");
		this.giocatoriPresenti = giocatoriPresenti;
	}
	
	/**
	 * Eliminazione del giocatore passato come parametro
	 * dal Set di giocatori presenti nella casella e notifica il cambiamento
	 * @param giocatoreDaTogliere : Giocatore da togliere da una casella 
	 */
	public void removeGiocatore(Giocatore giocatoreDaTogliere){
		giocatoriPresenti.remove(giocatoreDaTogliere);
		this.notifyObservers(new CasellaMutatioRemoveGiocatore(this,giocatoreDaTogliere));
	}
	
	/**
	 * Aggiunge il giocatore passato per parametro al Set dei giocatori presenti
	 * della casella e notifica il cambiamento
	 * @param giocatoreDaAggiungere : Giocatore da aggiungere alla casella
	 * @throws IllegalArgumentException: se si aggiunge un giocatore null
	 */
	public void addGiocatore(Giocatore giocatoreDaAggiungere){
		if(giocatoreDaAggiungere==null)
			throw new IllegalArgumentException("Non puoi aggiungere un giocatore null");
		giocatoriPresenti.add(giocatoreDaAggiungere);
		this.notifyObservers(new CasellaMutatioAddGiocatore(this,giocatoreDaAggiungere));
	}
	
	/**
	 * Aggiunge il giocatore passato per parametro al Set dei giocatori presenti
	 * della casella e notifica il cambiamento
	 * @param giocatoreDaAggiungere : Giocatore da aggiungere alla casella
	 * @throws IllegalArgumentException: se si aggiunge un giocatore null
	 */
	public void addGiocatore (Giocatore giocatoreDaAggiungere, Partita partita){
		this.addGiocatore(giocatoreDaAggiungere);
	}

	/**Ritorna stringa della casella:
	 * Casella punti: ... ,giocatoriPresenti=(.. ,.. ,..)
	 * @return stringa equivalente della casella
	 */
	@Override
	public String toString() {
		String punto="Casella punti:" + puntiCasella;
		if(!this.giocatoriPresenti.isEmpty()){
			punto+=", giocatoripresenti=(";
			for(Giocatore gioc:this.giocatoriPresenti){
				punto+=gioc.getNome()+", ";
			}
			punto+=")";
		}
		
		return punto;
	}

	/**
	 * @return hashCode della classe casella
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((giocatoriPresenti == null) ? 0 : giocatoriPresenti.hashCode());
		result = prime * result + ((puntiCasella == null) ? 0 : puntiCasella.hashCode());
		return result;
	}

	/**
	 * Confronto due caselle guardando se sono della stessa classe
	 * e poi i punti della casella
	 * @return true se le caselle sono uguali altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Casella other = (Casella) obj;
		if (puntiCasella == null) {
			if (other.puntiCasella != null)
				return false;
		} else if (!puntiCasella.equals(other.puntiCasella))
			return false;
		return true;
	}
	
	
	
}
