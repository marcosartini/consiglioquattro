/**
 * 
 */
package it.polimi.ingsw.cg13.oggetti;

import java.io.Serializable;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mutatio.EmporioMutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class Emporio extends ObservableC<EmporioMutatio> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4285406298648541170L;

	


	private Giocatore giocatoreAppartenente;
	private Citta cittaPosizionato;
	
	/**
	 * Costruttore della classe
	 * @param g--> giocatore a cui appartiene l' emporio
	 */
	public Emporio(Giocatore g){
		this.giocatoreAppartenente=g;
		this.cittaPosizionato=null;
	}
	
	/**
	 * getter della citt� in cui � posizionato
	 * @return cittaPosizionato
	 */
	public Citta getCittaPosizionato() {
		return cittaPosizionato;
	}
	
	/**
	 * setter della citt� in cui � posizionato
	 * notifica osservatore
	 * @param cittaPosizionato
	 */
	public void setCittaPosizionato(Citta cittaPosizionato) {
		this.cittaPosizionato = cittaPosizionato;
		this.notifyObservers(new EmporioMutatio(this));
	}
	
	/**
	 * getter del giocatore a cui appartiene
	 * @return cittaPosizionato
	 */
	public Giocatore getGiocatoreAppartenente() {
		return giocatoreAppartenente;
	}
	
	/**
	 * setter del giocatore a cui appartiene
	 * notifica osservatore
	 * @param giocatoreAppartenente
	 */
	public void setGiocatoreAppartenente(Giocatore giocatoreAppartenente) {
		this.giocatoreAppartenente = giocatoreAppartenente;

	}

	
	
	
	/**
	 * toString della classe
	 * @return String
	 */
	@Override
	public String toString() {
		return "(giocatoreAppartenente=" + giocatoreAppartenente.getNome()+ ")";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	/**
	 * Ritorna l' HashCode della classe
	 * @return result--> hashCode
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cittaPosizionato == null) ? 0 : cittaPosizionato.hashCode());
		result = prime * result + ((giocatoreAppartenente == null) ? 0 : giocatoreAppartenente.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	/**
	 * Metodo equals della classe--> confronta con altro oggetto
	 * @param obj--> oggeto con cui confrontare
	 * @return boolean value
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Emporio other = (Emporio) obj;
		if (cittaPosizionato == null) {
			if (other.cittaPosizionato != null)
				return false;
		} else if (!cittaPosizionato.equals(other.cittaPosizionato))
			return false;
		if (giocatoreAppartenente == null) {
			if (other.giocatoreAppartenente != null)
				return false;
		} else if (!giocatoreAppartenente.equals(other.giocatoreAppartenente))
			return false;
		return true;
	}
	
	public Colore getColore() throws ColoreNotFoundException{
		if(giocatoreAppartenente != null){
			return giocatoreAppartenente.getColore();
		}
		else throw new ColoreNotFoundException("Non c'e' ancora un giocatore colorato proprietario di questo emporio");
	}
}
