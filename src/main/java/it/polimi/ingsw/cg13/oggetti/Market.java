/**
 * 
 */
package it.polimi.ingsw.cg13.oggetti;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg13.eccezioni.PermessoNotFoundException;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.personaggi.Giocatore;


/**
 * @author lorenzo
 *
 */
public class Market implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 555994768749681592L;
	private static int numeroInserzioni=0;
	private List<Inserzione> inserzioniInVendita;
	
	public Market(){
		inserzioniInVendita=new ArrayList<>();
	}
	
	@Override
	public String toString(){
		String inserzioni="Market: lista inserzioni\n";
		for(Inserzione i :inserzioniInVendita){
			inserzioni+=i.toString()+"\n";
		}
		return inserzioni;
	}
	
	public List<Inserzione> getBanchetto(){
		return this.inserzioniInVendita;	
	}
	
	/**
	 * richiamata quando viene effettuata compravendita: cambio proprietario e pagamento 
	 * @param Giocatore : Acquirente dell' inserzione
	 * @param Inserzione : Inserzione di riferimento
	 * @param richPath: percorso ricchezza della mappa
	 * @throws PermessoNotFoundException se non viene trovato il permesso
	 */
	public void compraInserzione(Giocatore g,Inserzione i,PercorsoRicchezza richPath) throws PermessoNotFoundException{
		if(g.getRicchezzaGiocatore()>=i.getPrezzo().getPuntiCasella()){
			i.isSell(g,richPath);
			this.inserzioniInVendita.remove(i);
		}		
	}
	
	public void inserisciInserzione(Inserzione i)
	{
		incrementaNumeroInserzioni();
		i.setIdInserzione(numeroInserzioni);
		inserzioniInVendita.add(i);	
	}
	
	public void riassegnaVendibili(){
		if(!this.inserzioniInVendita.isEmpty()){
			this.inserzioniInVendita.forEach(i->i.getOggettoInVendita().riassegnaAlProprietario(i.getProprietario()));
		}
	}
	
	private static void incrementaNumeroInserzioni(){
		numeroInserzioni++;
	}
}
