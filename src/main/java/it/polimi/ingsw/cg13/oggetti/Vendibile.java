/**
 * 
 */
package it.polimi.ingsw.cg13.oggetti;

import java.io.Serializable;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.PermessoNotFoundException;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author lorenzo
 *
 */
public interface Vendibile extends Serializable{
	
	public void acquistaDaMarket(Giocatore g);
	public void rimuoviDaGiocatore(Giocatore g) throws PermessoNotFoundException;
	public void updateFromServer(Giocatore g,Inserzione inserzione) throws CdQException;
	public void riassegnaAlProprietario(Giocatore g);
}
