/**
 * 
 */
package it.polimi.ingsw.cg13.oggetti;

import java.io.Serializable;

import it.polimi.ingsw.cg13.eccezioni.PermessoNotFoundException;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author lorenzo
 *
 */
public class Inserzione implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8754235946054426644L;
	private int idInserzione;
	private CasellaRicchezza prezzo;
	private Giocatore proprietario;
	private Vendibile oggettoInVendita;
	
	/**
	 * Costruttore della classe
	 * @param prezzo : prezzo dell' inserzione
	 * @param venditore: giocatore che mette in vendita l' inserzione
	 * @param oggettoInVendita:oggetto dell' inserzione (Aiutante,CartaPolitica,PermessoCostruzione)
	 */
	public Inserzione(CasellaRicchezza prezzo, Giocatore venditore,Vendibile oggettoInVendita){
		this.oggettoInVendita=oggettoInVendita;
		this.proprietario=venditore;
		this.prezzo=prezzo;
	}
	
	/**
	 * Setta il proprietario dell'inserzione
	 * @param g : nuovo proprietario dell' inserzione
	 */
	public void setProprietario(Giocatore g){
		this.proprietario=g;		
	}
	
	/**
	 * Getter del proprietario dell'inserzione
	 */
	public Giocatore getProprietario(){
		return this.proprietario;		
	}
	
	/**
	 * Getter del prezzo dell'inserzione
	 */
	public CasellaRicchezza getPrezzo()
	{
		return this.prezzo;
	}		
		
	/**
	 * richiamata quando viene effettuata vendita: cambio proprietario e pagament 
	 * @param Acquirente : Acquirente dell' inserzione
	 * @param richPath: percorso ricchezza della mappa
	 * @throws PermessoNotFoundException se non trova il permesso richiesto
	 */
	public void isSell(Giocatore acquirente,PercorsoRicchezza richPath) throws PermessoNotFoundException
	{
		this.proprietario.riceviMonete(this.prezzo.getPuntiCasella(),richPath);
		acquirente.spendiMonete(this.prezzo.getPuntiCasella(), richPath);
		this.oggettoInVendita.acquistaDaMarket(acquirente);
		//this.oggettoInVendita.rimuoviDaGiocatore(this.proprietario);
	}

	
	/**
	 * Restituisce l' oggetto in vendita dell' inserzione
	 * @return oggettoInVendita
	 */
	public Vendibile getOggettoInVendita() {
		return oggettoInVendita;
	}

	/**
	 * Permette di settare l' oggetto in vendita dell' inserzione
	 * @param oggettoInVendita
	 */
	public void setOggettoInVendita(Vendibile oggettoInVendita) {
		this.oggettoInVendita = oggettoInVendita;
	}

	/**
	 * Restituisce l' ID dell' inserzione
	 * @return idInserzione
	 */
	public int getIdInserzione() {
		return idInserzione;
	}
	
	/**
	 * Permette di settare l' ID dell' inserzione
	 * @param idInserzione
	 */
	public void setIdInserzione(int idInserzione) {
		this.idInserzione = idInserzione;
	}
	
	/**
	 * ToString della classe
	 */
	@Override
	public String toString() {
		return "Inserzione "+this.idInserzione+":\n oggetto= "+ oggettoInVendita+" prezzo= " + prezzo.getPuntiCasella() + " venduta da " + proprietario.getNome() ;
	}

	/**
	 * Restituisce l' hashCode della classe
	 * @return result
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idInserzione;
		result = prime * result + ((proprietario == null) ? 0 : proprietario.hashCode());
		return result;
	}

	/**
	 * Permette di confrontare l' oggetto con altri
	 * @return true-> uguali \ false-> diversi
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inserzione other = (Inserzione) obj;
		if (idInserzione != other.idInserzione)
			return false;
		if (proprietario == null) {
			if (other.proprietario != null)
				return false;
		} else if (!proprietario.equals(other.proprietario))
			return false;
		return true;
	}
	
	
}
