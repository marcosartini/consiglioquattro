package it.polimi.ingsw.cg13.file;

import java.util.*;
import java.io.File;
import java.io.IOException;


import org.jdom2.*;
import org.jdom2.input.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.bonus.AiutanteAggiuntivo;
import it.polimi.ingsw.cg13.bonus.AzionePrincipaleAggiuntiva;
import it.polimi.ingsw.cg13.bonus.CartaPoliticaAggiuntiva;
import it.polimi.ingsw.cg13.bonus.OttieniPermesso;
import it.polimi.ingsw.cg13.bonus.PuntoNobilta;
import it.polimi.ingsw.cg13.bonus.PuntoRicchezza;
import it.polimi.ingsw.cg13.bonus.PuntoVittoria;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.bonus.RiutilizzaBonusCitta;
import it.polimi.ingsw.cg13.bonus.RiutilizzaBonusPermesso;
import it.polimi.ingsw.cg13.bonus.TesseraBonusGruppoCitta;
import it.polimi.ingsw.cg13.bonus.TesseraBonusRe;
import it.polimi.ingsw.cg13.bonus.TesseraBonusRegione;
import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.CartaPoliticaJolly;
import it.polimi.ingsw.cg13.carte.CartaPoliticaSemplice;
import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.MazzoPolitico;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.Regione;

import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.percorsi.PercorsoNobilta;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.King;

public class FileReader {
	private SAXBuilder saxBuilder;  
	private Document doc;
	
	private static final Logger logger = LoggerFactory.getLogger(FileReader.class);
	
	private static final String NUMBER = "number";  
	private static final String JOLLY = "jolly";  
	private static final String BONUS = "bonus";  
	private static final String REWARD = "reward";  
	private static final String TESSEREBONUS = "tessereBonus";   
	private static final String PUNTI = "punti"; 
	private static final String COLOR = "color";
	
	private static final String AIUTANTEAGGIUNTIVO = "AiutanteAggiuntivo";
	private static final String AZIONEPRINCIPALEAGGIUNTIVA = "AzionePrincipaleAggiuntiva";
	private static final String CARTAPOLITICAAGGIUNTIVA = "CartaPoliticaAggiuntiva";
	private static final String PUNTONOBILTA = "PuntoNobilta";
	private static final String PUNTORICCHEZZA = "PuntoRicchezza";
	private static final String PUNTOVITTORIA = "PuntoVittoria";
	private static final String RIUTILIZZABONUSPERMESSO = "RiutilizzaBonusPermesso";
	private static final String RIUTILIZZABONUSCITTA = "RiutilizzaBonusCitta";
	private static final String OTTIENIPERMESSO = "OttieniPermesso";
	private static final String CITYLINK = "cityLink";
	/**
	 * costruttore lettore di file Xml
	 * @param pathName --> percorso del file
	 * @throws IOException,JDOMException
	 */
	public FileReader(String pathName)
	{
		this.saxBuilder = new SAXBuilder();	
		
		
		try{
			File file =new File(pathName);
			this.doc = saxBuilder.build(file);			
		}
		catch(IOException io)
		{
			logger.error(io.getMessage(),io);
		}
		catch(JDOMException e){
			logger.error(e.getMessage(),e);
		}		
	}	
	
	/**
	 * legge l' elenco di colori presente nel file xml
	 * @return lista di colori letti
	 * @throws NullPointerException
	 */
	public List<Colore> readColor()
	{
		List<Colore> colori= new ArrayList<>();
		
		
		try{
			Element root = doc.getRootElement().getChild("colors");				
			List<Element> coloriList=root.getChildren(COLOR);
			Iterator<Element> coloriIter=coloriList.iterator();
			
			while(coloriIter.hasNext())
			{
				Element temp=coloriIter.next();	
				colori.add(tornaColore(temp));
			}	
		}
		catch(NullPointerException e)
		{		
			logger.error("FileError: correggere il campo dei colors inseriti",e);
		}
		
		return colori;
	}
	
	/**
	 * restituisce il numero massimo di giocatori per una determinata partita
	 * @return numero Max giocatori
	 * @throws NullPointerException
	 */
	public int getNumMaxPlayer()	{
		Element root = doc.getRootElement();
		
		String temp=null;
		try{
			Element player =root.getChild("players");
			temp=player.getChildText(NUMBER);
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo dei players inseriti",e);
		}
		return Integer.parseInt(temp);
	}
		
	/**
	 * Restituisce il percorso della ricchezza presente nel file XML
	 * @return richPath
	 * @throws NullPointerException
	 */
	public PercorsoRicchezza getRichPath()
	{
		Element root = doc.getRootElement();
		PercorsoRicchezza richPath=null;
		
		int temp=0;
		try{
			Element rich =root.getChild("richnesspath");
			temp=Integer.parseInt(rich.getAttributeValue("dimension"));
			List<CasellaRicchezza> percorso= new ArrayList<>();
			for(int i=0;i<=temp;i++){
				percorso.add(new CasellaRicchezza(i));
			}
			richPath=new PercorsoRicchezza(percorso);
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo relativo al richnesspath",e);
		}
		return richPath;
	}
	
	/**
	 * Restituisce il percorso della vittoria presente nel file XML
	 * @return winPath
	 * @throws NullPointerException
	 */
	public PercorsoVittoria getVictoryPath()
	{
		Element root = doc.getRootElement();
		
		int temp=0;
		PercorsoVittoria winPath= null;
		try{
			Element victory =root.getChild("victorypath");
			temp=Integer.parseInt(victory.getAttributeValue("dimension"));
			List<CasellaVittoria> percorso= new ArrayList<>();
			for(int i=0;i<=temp;i++){
				percorso.add(new CasellaVittoria(i));
			}
			winPath=new PercorsoVittoria(percorso);
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo relativo al victorypath",e);
		}
		return winPath;
	}
	
	/**
	 * Restituisce una lista di tutti gli aiutanti presente nel file XML
	 * @return helpers
	 * @throws NullPointerException
	 */		
	public List<Aiutante> getHelpersNum()
	{
		Element root = doc.getRootElement();		
		int temp=0;
		List<Aiutante> helpers=new ArrayList<>();
		
		try{
			Element helper =root.getChild("helpers");
			temp=Integer.parseInt(helper.getChildText(NUMBER));
			for(int i=0;i<temp;i++){
				helpers.add(new Aiutante());
			}
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo relativo agli helpers",e);
		}
		return helpers;
	}
	
	
	/**
	 * Restituisce il mazzo di carte politiche presente nel file XML
	 * @return PoliticDeck
	 * @throws NullPointerException
	 */
	public MazzoPolitico getCartePolitica() 	
	{		
		List<CartaPolitica>cartePol= new ArrayList<>();
		int num=0;
		Colore colore=null;
		try{
			Element root = doc.getRootElement().getChild("deckPolitico");
			List<Element> politica=root.getChildren("card");
			Iterator<Element> iter=politica.iterator();
			
			while(iter.hasNext())
			{
				Element temp=iter.next();	
				
				if(temp.getChild(COLOR).getAttributeValue("name").equals(JOLLY))
				{
					num=Integer.parseInt(temp.getChildText(NUMBER));
					for(int i=0;i<num;i++){
						cartePol.add(new CartaPoliticaJolly());
					}
				}
				else
				{
					num=Integer.parseInt(temp.getChildText(NUMBER));
					colore=tornaColore(temp.getChild(COLOR));
					for(int i=0;i<num;i++){
						cartePol.add(new CartaPoliticaSemplice(colore));
					}
				}
			}	
		}
		catch(NullPointerException | ColoreNotFoundException e)
		{
			logger.error("FileError: correggere il deckPolitico nel file",e);
		}		
		return new MazzoPolitico(cartePol);
	}
	
	/**
	 * Restituisce un colore dell' elemento TEMP dato
	 * TEMP � un ramo dell'albero XML nel file
	 * @param temp-->campo da cui partire per leggere il colore
	 * @return Colore 
	 */
	private Colore tornaColore(Element temp){
		int r=Integer.parseInt(temp.getAttributeValue("r"));
		int g=Integer.parseInt(temp.getAttributeValue("g"));
		int b=Integer.parseInt(temp.getAttributeValue("b"));
		String nome=temp.getAttributeValue("name");
		return new Colore(nome,r,g,b);
	}
	
	
	/**
	 * Restituisce le citt� presenti nel file XML
	 * gli empori e i bonus sono NULL
	 * @return cities
	 * @throws NullPointerException
	 */
	public List<Citta> getCity(){		
		List<Citta> cities=new ArrayList<>();
		Colore tempColor=null;//regione			
		String tempName=null;//bonus
		
		try{
			Element root = doc.getRootElement().getChild("cities");
			List<Element> citta=root.getChildren("city");
			Iterator<Element> iter=citta.iterator();
				
			
			while(iter.hasNext())
			{
				Element temp=iter.next();
				tempColor= tornaColore(temp.getChild(COLOR));
				tempName= temp.getChildText("name");
				
				cities.add(new Citta(tempColor,tempName,new HashSet<>(),new HashSet<>()));
			}	
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo delle cities nel file",e);
		}
		return cities;
		
	}
	
	/**
	 * Dato il nome di una citt� verifica se essa appartiene ad una lista di oggetti Citt� e
	 * ritorna l'oggetto cercato,Null se non trovato
	 * @param nomeCitta --> nome da cercare
	 * @param cittaMappa -->lista di citt� in cui cercare
	 * @return citta 
	 */
	public Citta getCitta(String nomeCitta,List<Citta> cittaMappa){ 
		if(cittaMappa==null || cittaMappa.isEmpty())
			return null;
		for(Citta citta:cittaMappa){
			if(citta.getNome().equals(nomeCitta)){
				return citta;
			}
		}		
		return null;
	}
	
	
	/**
	 * Restituisce i lista delle citt� colorate 
	 * @return colorate
	 */
	public List<Citta> getCittaColorate(Colore colore,List<Citta> citta){ 
		if(citta==null||citta.isEmpty())
			return new ArrayList<>();
		List<Citta> colorate=new ArrayList<>();
		for(Citta temp:citta){
			if(temp.getColor().equals(colore)){
				colorate.add(temp);
			}
		}
		return colorate;
	}
	
	/**
	 * Restituisce i link delle citt� presenti nel file XML
	 * @return linkCity
	 * @throws NullPointerException
	 */
	public List<NodoCitta> getCityLink(List<Citta> city){
		if(city==null||city.isEmpty())
			return new ArrayList<>();
		List<NodoCitta> linkCity=new ArrayList<>();
		String tempName=null;
		NodoCitta tempNodo=null;

		try{	
			Element root = doc.getRootElement().getChild(CITYLINK);
			List<Element> citta=root.getChildren("city");
			Iterator<Element> iter=citta.iterator();
			
			while(iter.hasNext())
			{
				Element temp=iter.next();
				tempName=temp.getAttributeValue("name");				 
				tempNodo= new NodoCitta(getCitta(tempName,city));
				List<Element> tempLink=temp.getChildren("linkTo");
				Iterator<Element> iterLink=tempLink.iterator();
				while(iterLink.hasNext())
				{
					Element tempEl=iterLink.next();
					tempNodo.addVicino(getNodoCitta(tempEl.getText(),convertiCitta(city)));
				}
				linkCity.add(tempNodo);
			}	
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo dei cityLink",e);
		}
		return linkCity;
	}
	
	public NodoCitta getNodoCitta(String nomeCitta,List<NodoCitta> nodi){
		if(nodi==null||nodi.isEmpty())
			return null;
		for(NodoCitta citta:nodi){
			if(citta.getCittaNodo().getNome().equals(nomeCitta)){
				return citta;
			}
		}
		return null; 
	}
	

	/**
	 * Restituisce il re con la citt� di partenza presenti nel file XML
	 * il consiglio assegnato � NULL
	 * @return re
	 * @throws NullPointerException
	 */
	public King getKing(List<NodoCitta> nodi)
	{
		Element root = doc.getRootElement();		
		String temp=null;
		King re=null;
		
		try{
			Element king =root.getChild("king");
			temp=king.getChildText("start");
			re=new King(getNodoCitta(temp,nodi),null);
			
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo del king",e);
		}
		return re;
	}
	
	/**
	 * Restituisce percorso della nobilt� con i bonus nel file XML
	 * @return PercorsoNobilta
	 * @throws NullPointerException
	 */
	public PercorsoNobilta getNobilityPath()
	{
		List<CasellaNobilta>caselle= new ArrayList<>();
		
		try{
			Element root = doc.getRootElement().getChild("nobilitypath");		
			List<Element> caselleNob=root.getChildren(BONUS);
			Iterator<Element> iter=caselleNob.iterator();
			if(iter.hasNext()){
				while(iter.hasNext())
				{
					Element temp=iter.next();				
					List<Element> tempBonus=temp.getChildren(REWARD);
					Iterator<Element> iterBonus=tempBonus.iterator(); 
					int num=Integer.parseInt(temp.getAttributeValue("numCasella"));
					CasellaNobilta casella=new CasellaNobilta(num);
					while(iterBonus.hasNext()){
						Element tempReward=iterBonus.next();
						
						if(AIUTANTEAGGIUNTIVO.equals(tempReward.getAttributeValue("tipo")))
						{
							casella.addRicompensa(new AiutanteAggiuntivo(Integer.parseInt(tempReward.getChildText(PUNTI))));						
						}
						else if(AZIONEPRINCIPALEAGGIUNTIVA.equals(tempReward.getAttributeValue("tipo")))
						{
							casella.addRicompensa(new AzionePrincipaleAggiuntiva(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(CARTAPOLITICAAGGIUNTIVA.equals(tempReward.getAttributeValue("tipo")))
						{
							casella.addRicompensa(new CartaPoliticaAggiuntiva(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(PUNTONOBILTA.equals(tempReward.getAttributeValue("tipo")))
						{
							casella.addRicompensa(new PuntoNobilta(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(PUNTORICCHEZZA.equals(tempReward.getAttributeValue("tipo")))
						{
							casella.addRicompensa(new PuntoRicchezza(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(PUNTOVITTORIA.equals(tempReward.getAttributeValue("tipo")))
						{
							casella.addRicompensa(new PuntoVittoria(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(RIUTILIZZABONUSPERMESSO.equals(tempReward.getAttributeValue("tipo")))
						{
							casella.addRicompensa(new RiutilizzaBonusPermesso());
						}
						else if(RIUTILIZZABONUSCITTA.equals(tempReward.getAttributeValue("tipo")))
						{
							casella.addRicompensa(new RiutilizzaBonusCitta());
						}
						else if(OTTIENIPERMESSO.equals(tempReward.getAttributeValue("tipo")))
						{
							casella.addRicompensa(new OttieniPermesso());
						}
						
					}
					caselle.add(casella);
				}
			}
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo del nobilitypath",e);
		}
		return new PercorsoNobilta(caselle);
	}
	
	/**
	 * Restituisce le tessere bonus del re nel file XML
	 * @return tessere
	 * @throws NullPointerException
	 */
	public List<TesseraBonusRe> getBonusRe(){
		List<TesseraBonusRe>tessere= new ArrayList<>();
		
		try{
			Element root = doc.getRootElement().getChild(TESSEREBONUS);		
			List<Element> bonusRe=root.getChildren("bonusRe");
			Iterator<Element> iter=bonusRe.iterator();
			int num=1;
			while(iter.hasNext())
			{
				Element temp=iter.next();				
				tessere.add(new TesseraBonusRe(Integer.parseInt(temp.getChildText(PUNTI)),num));
				num++;
			}	
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo del bonusRe",e);
		}
		return tessere;
	}
	
	/**
	 * Restituisce un set di citta del colore specificato
	 * @return tessere	 *
	 */
	public Set<Citta> getCittaColorate(List<Citta> citta,Colore colore)
	{
		if(citta==null||citta.isEmpty())
			return new HashSet<>();
		Set<Citta> cittaCol=new HashSet<>();		
		for(Citta city:citta)
		{
			if(city.getColor().equals(colore))
				cittaCol.add(city);
		}
		return cittaCol;
	}
	
	/**
	 * Restituisce le tessere bonus del gruppo colorato di citt� nel file XML
	 * @return tessere
	 * @throws NullPointerException
	 */
	public List<TesseraBonusGruppoCitta> getBonusCitta(List<Citta> citta){
		List<TesseraBonusGruppoCitta>tessere= new ArrayList<>();
		
		try{
			Element root = doc.getRootElement().getChild(TESSEREBONUS);		
			List<Element> bonusCitta=root.getChildren("bonusCitta");
			Iterator<Element> iter=bonusCitta.iterator();			
			while(iter.hasNext())
			{				
				Element temp=iter.next();	
				Colore tempColor= tornaColore(temp.getChild(COLOR));
				tessere.add(new TesseraBonusGruppoCitta(Integer.parseInt(temp.getChildText(PUNTI)),tempColor,getCittaColorate(citta,tempColor)));
				
			}	
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo del bonusCitta",e);
		}
		return tessere;
	}
	
	/**
	 * Cerca una regione in un set di regioni e ritorna quella cercata
	 * @param nomeRegione --> nome della regione da cui cercare
	 * @param regioni --> SET di regioni in cui cercare
	 * @return regione
	 * @throws RegioneNotFoundException quando non trova la regione nell'elenco
	 */
	public Regione getRegione (String nomeRegione,Set<Regione> regioni) throws RegioneNotFoundException {
		for(Regione regione:regioni){
			if(regione.getNome().equals(nomeRegione)){
				return regione;
			}
		}
		throw new RegioneNotFoundException(nomeRegione+" non trovata");
	}
	
	/**
	 * Restituisce le tessere bonus della regione nel file XML
	 * @return tessere
	 * @throws FileSyntaxException quando non trova la regione del file nell'elenco regioni
	 * @throws NullPointerException
	 */
	public List<TesseraBonusRegione> getBonusRegione(Set<Regione> regioni) throws FileSyntaxException{
		
		List<TesseraBonusRegione>tessere= new ArrayList<>();
		
		try{
			Element root = doc.getRootElement().getChild(TESSEREBONUS);		
			List<Element> bonusReg=root.getChildren("bonusRegione");
			Iterator<Element> iter=bonusReg.iterator();

			while(iter.hasNext())
			{				
				Element temp=iter.next();	
				Regione tempReg= getRegione(temp.getChildText("region"),regioni);
				tessere.add(new TesseraBonusRegione(Integer.parseInt(temp.getChildText(PUNTI)),tempReg));				
			}	
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo del bonusRegione",e);
		} catch (RegioneNotFoundException e) {
			throw new FileSyntaxException(e);
		}
		return tessere;
		
	}
	
	/**
	 * Restituisce il mazzo di carte permesso della regione presente nel file XML
	 * @return MazzoPermessi
	 * @throws NullPointerException
	 */
	public MazzoPermessi getCartePermessoRegione(String reg,List<Citta> citta){
		List<PermessoCostruzione>carteperm= new ArrayList<>();
		int visibili=0;
		
		try{
			Element root = doc.getRootElement().getChild("deckPermesso");
			visibili=Integer.parseInt(root.getAttributeValue("visibili"));
			List<Element> permessiReg=root.getChildren("card");
			Iterator<Element> iter=permessiReg.iterator();
			while(iter.hasNext())
			{				
				Element temp=iter.next();	
				if(temp.getAttributeValue("regione").equals(reg)){
					List<Element> tempValue=temp.getChild("citta").getChildren("name");		//prima iterazione per citt�
					Iterator<Element> valueIter=tempValue.iterator();
					List<Citta> cittaIns=new ArrayList<>();
					while(valueIter.hasNext()){
						Element insCity=valueIter.next();
						cittaIns.add(getCitta(insCity.getValue(),citta));
					}
					
					tempValue=temp.getChild(BONUS).getChildren(REWARD);	//seconda iterazione per bonus
					valueIter=tempValue.iterator();
					Set<Ricompensa> rewardIns=new HashSet<>();
					while(valueIter.hasNext()){
						Element tempReward=(valueIter.next());
						
						if(AIUTANTEAGGIUNTIVO.equals(tempReward.getAttributeValue("tipo")))
						{
							rewardIns.add(new AiutanteAggiuntivo(Integer.parseInt(tempReward.getChildText(PUNTI))));						
						}
						else if(AZIONEPRINCIPALEAGGIUNTIVA.equals(tempReward.getAttributeValue("tipo")))
						{
							rewardIns.add(new AzionePrincipaleAggiuntiva(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(CARTAPOLITICAAGGIUNTIVA.equals(tempReward.getAttributeValue("tipo")))
						{
							rewardIns.add(new CartaPoliticaAggiuntiva(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(PUNTONOBILTA.equals(tempReward.getAttributeValue("tipo")))
						{
							rewardIns.add(new PuntoNobilta(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(PUNTORICCHEZZA.equals(tempReward.getAttributeValue("tipo")))
						{
							rewardIns.add(new PuntoRicchezza(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(PUNTOVITTORIA.equals(tempReward.getAttributeValue("tipo")))
						{
							rewardIns.add(new PuntoVittoria(Integer.parseInt(tempReward.getChildText(PUNTI))));
						}
						else if(RIUTILIZZABONUSPERMESSO.equals(tempReward.getAttributeValue("tipo")))
						{
							rewardIns.add(new RiutilizzaBonusPermesso());
						}
						else if(RIUTILIZZABONUSCITTA.equals(tempReward.getAttributeValue("tipo")))
						{
							rewardIns.add(new RiutilizzaBonusCitta());
						}
						else if(OTTIENIPERMESSO.equals(tempReward.getAttributeValue("tipo")))
						{
							rewardIns.add(new OttieniPermesso());
						}
					}
					carteperm.add(new PermessoCostruzione(cittaIns,rewardIns));
				}
			}	
		}
		catch(NullPointerException e){
			logger.error("FileError: correggere il campo del deckPermesso",e);
		}
		
		return new MazzoPermessi(carteperm, visibili);
	}
	
	/**
	 * @throws CittaNotFoundException 
	 * Restituisce le regioni presente nel file XML
	 * @return Regioni
	 * @throws NullPointerException
	 */
	public List<Regione> getRegioni(List<Citta> citta) throws FileSyntaxException{		
		List<Regione> regioni=new ArrayList<>();
		try{
			Element root = doc.getRootElement().getChild("regioni");		
			List<Element> region=root.getChildren("region");
			Iterator<Element> iter=region.iterator();
			while(iter.hasNext())
			{	
				Set <Citta> addCity=new HashSet<>();
				Element temp=iter.next();	
				String nome= temp.getAttributeValue("name");
				List<Element> city= temp.getChildren("city");
				Iterator<Element> iterCity=city.iterator();
				while(iterCity.hasNext()){		//iterazione per citt�della regione
					Element tempCity=iterCity.next();
					addCity.add(getCitta(tempCity.getAttributeValue("name"),citta));
				}
				regioni.add(new Regione(nome,addCity,null,getCartePermessoRegione(nome, citta)));
			}	
		}
		catch(NullPointerException e){
			logger.error("FileError: correggere il campo delle regioni",e);
		}
		
		if(!this.controllaCitta(citta, regioni))
			throw new FileSyntaxException(new CittaNotFoundException("FileError: citta non trovata o troppe presenti nel file!"));
		return regioni;
	}
	
	/**
	 * Restituisce il costo dei link presenti nel file XML
	 * @return costo
	 * @throws NullPointerException
	 */
	public int getCostoLink(){
		int costo=0;
		try{
			Element root = doc.getRootElement().getChild(CITYLINK);		
			costo=Integer.parseInt(root.getAttributeValue("costo"));				
		}
		catch(NullPointerException e){
			logger.error("FileError: correggere il campo del costo dei cityLink",e);
		}
		return costo;
	}
	
	/**
	 * Restituisce i costi aggiuntivi per le azioni presenti nel file XML
	 * @return Regioni
	 * @throws NullPointerException
	 */
	public List<Integer> getCostoAggiuntivo(){
		List<Integer> costi=new ArrayList<>();
		try{
			Element root = doc.getRootElement().getChild("CostiAggiuntivi");
			List<Element> costo=root.getChildren("costo");
			Iterator<Element> iter= costo.iterator();
			
			while(iter.hasNext()){
				Element temp=iter.next();
				costi.add(Integer.parseInt(temp.getText()));
			}
		}
		catch(NullPointerException e){
			logger.error("FileError: correggere il campo dei CostiAggiuntivi",e);
		}
		return costi;
	}
	
	/**
	 * Restituisce il numero di empori che ogni giocatore pu� avere
	 * @return num
	 * @throws NullPointerException
	 */
	public int getNumeroEmpori()
	{
		int num=0;
		try{
			Element root = doc.getRootElement().getChild("NumeroEmpori");
			Element costo=root.getChild("numero");
			num=Integer.parseInt(costo.getText());			
		}
		catch(NullPointerException e){
			logger.error("FileError: correggere il campo del NumeroEmpori",e);
		}
		return num;
	}
	
	/**
	 * Restituisce il numero dei consiglieri che ogni giocatore pu� avere
	 * @return num
	 * @throws NullPointerException
	 */
	public int getNumeroConsiglieri()
	{
		int num=0;
		try{
			Element root = doc.getRootElement().getChild("NumeroConsiglieri");
			Element costo=root.getChild("numero");
			num=Integer.parseInt(costo.getText());			
		}
		catch(NullPointerException e){
			logger.error("FileError: correggere il campo del NumeroConsiglieri",e);
		}
		return num;
	}
	
	/**
	 * Restituisce la lista dei colori dei consiglieri 
	 * @return colori
	 * @throws NullPointerException
	 */
	public List<Colore> getColoreConsiglieri(){
		List<Colore> colori= new ArrayList<>();
		try{
			Element root = doc.getRootElement().getChild("deckPolitico");
			List<Element> costo=root.getChildren("card");
			Iterator<Element> iter= costo.iterator();			
			
			while(iter.hasNext()){
				Element temp=iter.next();
				String name=temp.getChild(COLOR).getAttributeValue("name");
				if(!JOLLY.equals(name))
				{					
					colori.add(tornaColore(temp.getChild(COLOR)));
				}
			}
		}
		catch(NullPointerException e){
			logger.error("FileError: correggere il campo del colore del deckPolitico",e);
		}
		return colori;
	}
	
	/**
	 * Restituisce la lista dei colori delle citta
	 * @return coloriCitta
	 * @throws NullPointerException
	 */
	public List<Colore> getColoriCitta(){
		List<Colore> coloriCitta= new ArrayList<>();
	
		try{
			Element root = doc.getRootElement().getChild("cities");
			List<Element> city=root.getChildren("city");
			Iterator<Element> iter=city.iterator();
			Colore tempCol;		
				
			while(iter.hasNext()){
				Element temp=iter.next();
				tempCol=tornaColore(temp.getChild(COLOR));
				if(!coloriCitta.contains(tempCol))
				{
					coloriCitta.add(tempCol);
				}
			}
		}
		catch(NullPointerException e){
			logger.error("FileError: correggere il campo del colore delle cities",e);
		}
		return coloriCitta;
	}
	/**
	 * Converte una lista di citt� in nodi citt� senza link
	 * @param citta
	 * @return tempNodi
	 */
	public List<NodoCitta> convertiCitta(List<Citta> citta)
	{
		List<NodoCitta> tempNodi= new ArrayList<>();
		for(Citta c :citta)
		{
			tempNodi.add(new NodoCitta(c));
		}
		return tempNodi;
	}
	
	/**
	 * Restituisce i link delle citt� presenti nel file XML
	 * @return linkCity
	 * @throws NullPointerException
	 */
	public void getLinkACitta(List<NodoCitta> city){
		
		String tempName=null;
		NodoCitta tempNodo=null;

		try{	
			Element root = doc.getRootElement().getChild(CITYLINK);
			List<Element> citta=root.getChildren("city");
			Iterator<Element> iter=citta.iterator();
			
			while(iter.hasNext())
			{
				Element temp=iter.next();
				tempName=temp.getAttributeValue("name");				 
				
				tempNodo=getNodoCitta(tempName,city);
				
				List<Element> tempLink=temp.getChildren("linkTo");
				Iterator<Element> iterLink=tempLink.iterator();
				while(iterLink.hasNext())
				{
					Element tempEl=iterLink.next();
					tempNodo.addVicino(getNodoCitta(tempEl.getText(),city));
				}				
			}	
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo dei cityLink",e);
		}
		
	}
	
	/**
	 * Legge da file i nomi delle citt� e restituisce la mappa
	 * @return tempNodi --> mappa
	 */
	public List<NodoCitta> createNodiCitta(){
		List<Citta> tempCitta;
		List<NodoCitta> tempNodi;
		
		tempCitta=this.getCity();
		tempNodi=this.convertiCitta(tempCitta);
		this.getLinkACitta(tempNodi);
		
		return tempNodi;
	}
	
	/**
	 * Crea i bonus da assegnare alle citt�
	 * @return bonuses
	 */
	public List<Set<Ricompensa>> getBonus(){
		
		List<Set<Ricompensa>> bonuses=new ArrayList<>();
		
		try{	
			Element root = doc.getRootElement().getChild("bonuses");
			List<Element> bonus=root.getChildren(BONUS);
			Iterator<Element> iter=bonus.iterator();
			
			
			while(iter.hasNext())
			{
				Element temp=iter.next();						 
				
				List<Element> tempBonus=temp.getChildren("reward");
				Iterator<Element> iterBonus=tempBonus.iterator();
				Set<Ricompensa>reward=new HashSet<>();
				while(iterBonus.hasNext())
				{
					Element tempEl=iterBonus.next();
					
					if(AIUTANTEAGGIUNTIVO.equals(tempEl.getAttributeValue("tipo")))
					{
						reward.add(new AiutanteAggiuntivo(Integer.parseInt(tempEl.getChildText(PUNTI))));
					}
					else if(AZIONEPRINCIPALEAGGIUNTIVA.equals(tempEl.getAttributeValue("tipo")))
					{
						reward.add(new AzionePrincipaleAggiuntiva(Integer.parseInt(tempEl.getChildText(PUNTI))));
					}
					else if(CARTAPOLITICAAGGIUNTIVA.equals(tempEl.getAttributeValue("tipo")))
					{
						reward.add(new CartaPoliticaAggiuntiva(Integer.parseInt(tempEl.getChildText(PUNTI))));
					}
					else if(PUNTONOBILTA.equals(tempEl.getAttributeValue("tipo")))
					{
						reward.add(new PuntoNobilta(Integer.parseInt(tempEl.getChildText(PUNTI))));
					}
					else if(PUNTORICCHEZZA.equals(tempEl.getAttributeValue("tipo")))
					{
						reward.add(new PuntoRicchezza(Integer.parseInt(tempEl.getChildText(PUNTI))));
					}
					else if(PUNTOVITTORIA.equals(tempEl.getAttributeValue("tipo")))
					{
						reward.add(new PuntoVittoria(Integer.parseInt(tempEl.getChildText(PUNTI))));
					}
					else if(RIUTILIZZABONUSPERMESSO.equals(tempEl.getAttributeValue("tipo")))
					{
						reward.add(new RiutilizzaBonusPermesso());
					}
					else if(RIUTILIZZABONUSCITTA.equals(tempEl.getAttributeValue("tipo")))
					{
						reward.add(new RiutilizzaBonusCitta());
					}
					else if(OTTIENIPERMESSO.equals(tempEl.getAttributeValue("tipo")))
					{
						reward.add(new OttieniPermesso());
					}
				}
				bonuses.add(reward);
				
			}	
		}
		catch(NullPointerException e)
		{
			logger.error("FileError: correggere il campo dei bonus",e);
		}	
		
		return bonuses;
	}
	
	/**
	 * controlla che una citta sia presente nelle regioni
	 * @param city
	 * @param regioni
	 * @return true--> trovata, false-->se non trovata o trovate troppe
	 */
	public boolean controllaCittaRegione(Citta city,List<Regione> regioni){		
		int numPresenti=0;
		
		for(Regione r:regioni){
			if(r.getCitta().contains(city))
				numPresenti++;				 
		}		
		return numPresenti==1; 
	}
	
	/**
	 * controlla che tutti le citta di una lista siano presenti nelle regioni
	 * @param citta
	 * @param regioni
	 * @return true--> tutte presenti, false-->errore: non trovata o trovate troppe
	 */
	public boolean controllaCitta(List<Citta> citta,List<Regione> regioni){
		for(Citta nc:citta){
			if(!this.controllaCittaRegione(nc, regioni))
				return false;
		}
		return true;
	}
	
	/**
	 * controlla che tutti i nodiCittà di una lista siano presenti nelle regioni
	 * @param nodi
	 * @param regioni
	 * @return true--> tutti presenti, false-->errore nell'albero
	 */
	public boolean controllaAlbero(List<NodoCitta> nodi,List<Regione> regioni){
		for(NodoCitta nc:nodi){
			if(!this.controllaCittaRegione(nc.getCittaNodo(), regioni))
				return false;
		}
		return true;
	}
	
	
}
