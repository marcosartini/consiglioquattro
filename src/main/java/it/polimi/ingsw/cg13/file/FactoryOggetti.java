package it.polimi.ingsw.cg13.file;

import java.nio.file.FileSystems;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import java.util.Set;

import it.polimi.ingsw.cg13.bonus.GruppoBonusRe;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.bonus.TesseraBonusGruppoCitta;
import it.polimi.ingsw.cg13.bonus.TesseraBonusRe;
import it.polimi.ingsw.cg13.bonus.TesseraBonusRegione;
import it.polimi.ingsw.cg13.carte.MazzoPolitico;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.Mappa;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.percorsi.PercorsoNobilta;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.personaggi.King;

public class FactoryOggetti {

	FileReader fileReader;
	private final int numeroMaxGiocatori;
	private final int costoStrade;
	private final List<Integer> costiAggiuntiviPerAzioni;
	private final int numeroConsiglieri;
	private final int numeroEmpori;
	private King re;
	private Mappa mappaPartita;
	private List<NodoCitta> cittaMappa;
	private List<Giocatore> giocatori;
	private List<TesseraBonusRegione> tessereBonusRegione;
	private PercorsoNobilta percorsoNobilta;
	private PercorsoRicchezza percorsoRicchezza;
	private PercorsoVittoria percorsoVittoria;
	private List<Consigliere> consiglieri;
	private List<Aiutante> aiutanti;
	private MazzoPolitico mazzoPolitico; 
	private GruppoBonusRe gruppoBonusRe;
	private Set<Regione> regioni;

	private List<Giocatore> giocatoriMustDoAction; 
	private Giocatore giocatoreCorrente;
	private List<TesseraBonusGruppoCitta> tessereBonusCitta;

	private List<Colore> coloriPolitici;
	private List<Colore> coloriGiocatori;
	private List<Colore> coloriCitta;
	
	private final List<Set<Ricompensa>> gettoni;
	
	/**
	 * Costruttore
	 * @param pathName--> percorso del file da cui recuperare i dati
	 * @throws FileSyntaxException --> errori nel file
	 */
	public FactoryOggetti(String pathName) throws FileSyntaxException {
		
		String path= FileSystems.getDefault().getPath(pathName).toString();
		this.fileReader=new FileReader(path);
		
		this.numeroMaxGiocatori=fileReader.getNumMaxPlayer();
		this.costiAggiuntiviPerAzioni=fileReader.getCostoAggiuntivo();
		this.costoStrade=fileReader.getCostoLink();
		
		List<Citta> cittas= fileReader.getCity();
		this.cittaMappa=fileReader.getCityLink(cittas);
		
		this.re=fileReader.getKing(cittaMappa);

		
		this.mappaPartita=null;
		
		this.giocatori=new ArrayList<>(this.numeroMaxGiocatori);
		
		this.percorsoNobilta=fileReader.getNobilityPath();
		this.percorsoRicchezza=fileReader.getRichPath();
		this.percorsoVittoria=fileReader.getVictoryPath();
		
		this.aiutanti=fileReader.getHelpersNum();
		
		this.coloriPolitici=fileReader.getColoreConsiglieri();
		this.coloriGiocatori=fileReader.readColor();
		
		this.numeroConsiglieri=fileReader.getNumeroConsiglieri();
		
		this.consiglieri=new ArrayList<>();
		
	
		
		this.mazzoPolitico=fileReader.getCartePolitica();
		this.mazzoPolitico.mescolaMazzo();
	
		
		this.regioni=new HashSet<>(fileReader.getRegioni(cittas));
		

		
		this.tessereBonusRegione=new ArrayList<>();
		this.tessereBonusRegione.addAll(fileReader.getBonusRegione(this.regioni));
		this.tessereBonusCitta=new ArrayList<>();
		this.tessereBonusCitta.addAll(fileReader.getBonusCitta(cittas));
		
		this.gruppoBonusRe=new GruppoBonusRe(new ArrayDeque<TesseraBonusRe>(fileReader.getBonusRe()));
		

		this.coloriCitta=fileReader.getColoriCitta();
		
		
		this.numeroEmpori=fileReader.getNumeroEmpori();
		
		this.gettoni= fileReader.getBonus();
		
	}

	/**
	 * Restituisce il lettore del file XML
	 * @return the fileReader
	 */
	public FileReader getFileReader() {
		return fileReader;
	}

	/**
	 * restituisce il numero massimo di giocatori per quella configurazione
	 * @return the numeroMaxGiocatori
	 */
	public int getNumeroMaxGiocatori() {
		return numeroMaxGiocatori;
	}

	/**
	 * Restituisce il costo da pagare per effettuare uno spostamento col re
	 * @return the costoStrade
	 */
	public int getCostoStrade() {
		return costoStrade;
	}

	/**
	 * Restituisce i costi aggiuntivi per corrompere un consiglio
	 * @return the costiAggiuntiviPerAzioni
	 */
	public List<Integer> getCostiAggiuntiviPerAzioni() {
		return costiAggiuntiviPerAzioni;
	}

	/**
	 * Restituisce il numero di consiglieri
	 * @return the numeroConsiglieri
	 */
	public int getNumeroConsiglieri() {
		return numeroConsiglieri;
	}

	/**
	 * Restituisce il numero di empori
	 * @return the numeroEmpori
	 */
	public int getNumeroEmpori() {
		return numeroEmpori;
	}

	/**
	 * Restituisce il Re
	 * @return the re
	 */
	public King getRe() {
		return re;
	}

	/**
	 * Restituisce la mappa relativa alla partita
	 * @return the mappaPartita
	 */
	public Mappa getMappaPartita() {
		return mappaPartita;
	}

	/**
	 * Restituisce il grafo delle città presenti nella mappa
	 * @return the cittaMappa
	 */
	public List<NodoCitta> getCittaMappa() {
		return cittaMappa;
	}

	/**
	 * Restituisce la lista di giocatori
	 * @return the giocatori
	 */
	public List<Giocatore> getGiocatori() {
		return giocatori;
	}

	/**
	 * Restituisce le tessere bonus della regione
	 * @return the tessereBonus
	 */
	public List<TesseraBonusRegione> getTessereBonusRegione() {
		return tessereBonusRegione;
	}

	/**
	 * Restituisce il percorsoNobilta
	 * @return the percorsoNobilta
	 */
	public PercorsoNobilta getPercorsoNobilta() {
		return percorsoNobilta;
	}

	/**
	 * Restituisce il PercorsoRicchezza
	 * @return the percorsoRicchezza
	 */
	public PercorsoRicchezza getPercorsoRicchezza() {
		return percorsoRicchezza;
	}

	/**
	 * Restituisce il percorsoVittoria
	 * @return the percorsoVittoria
	 */
	public PercorsoVittoria getPercorsoVittoria() {
		return percorsoVittoria;
	}

	/**
	 * Restituisce la lista di consiglieri
	 * @return the consiglieri
	 */
	public List<Consigliere> getConsiglieri() {
		return consiglieri;
	}

	/**
	 * Restituisce il numero di aiutanti della partita
	 * @return the aiutanti
	 */
	public List<Aiutante> getAiutanti() {
		return aiutanti;
	}

	/**
	 * Restituisce il mazzo politico da impiegare nella partita
	 * @return the mazzoPolitico
	 */
	public MazzoPolitico getMazzoPolitico() {
		return mazzoPolitico;
	}

	/**
	 * Restituisce i BonusRe presenti nella partita
	 * @return the gruppoBonusRe
	 */
	public GruppoBonusRe getGruppoBonusRe() {
		return gruppoBonusRe;
	}


	/**
	 * Restituisce il set di reghioni che compongono la mappa
	 * @return the regioni
	 */
	public Set<Regione> getRegioni() {
		return regioni;
	}


	/**
	 * Restituisce la lista di giocatori che devono compiere un' azione
	 * @return the giocatoriMustDoAction
	 */
	public List<Giocatore> getGiocatoriMustDoAction() {
		return giocatoriMustDoAction;
	}

	/**
	 * restituisce il giocatore corrente 
	 * @return the giocatoreCorrente
	 */
	public Giocatore getGiocatoreCorrente() {
		return giocatoreCorrente;
	}

	/**
	 * Ritorna i colori politici presenti nella partita
	 * @return the coloriPolitici
	 */
	public List<Colore> getColoriPolitici() {
		return coloriPolitici;
	}

	/**
	 * Restituisce i colori dei giocatori presenti nella partita
	 * @return the coloriGiocatori
	 */
	public List<Colore> getColoriGiocatori() {
		return coloriGiocatori;
	}
	
	/**
	 * Restituisce le tessere bonus legate alle città
	 * @return tessereBonusCitta
	 */
	public List<TesseraBonusGruppoCitta> getTessereBonusCitta() {
		return tessereBonusCitta;
	}
	
	/**
	 * Restituisce i colori delle città presenti sulla mappa
	 * @return coloriCitta
	 */
	public List<Colore> getColoriCitta() {
		return coloriCitta;
	}
	
	/**
	 * Restituisce i bonus/ricompense che verranno assegnate alle città
	 * @return gettoni
	 */
	public List<Set<Ricompensa>> getGettoni() {
		return gettoni;
	}
}


