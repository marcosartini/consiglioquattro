package it.polimi.ingsw.cg13.eccezioni;

public class RegioneNotFoundException extends CdQException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8237580586526995238L;

	public RegioneNotFoundException() {
		super();
	}

	public RegioneNotFoundException(String errore) {
		super(errore);
	}
	

}
