package it.polimi.ingsw.cg13.eccezioni;

public class PermessoNotFoundException extends CdQException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8838126532539781652L;

	public PermessoNotFoundException() {
		super();
	}
	
	public PermessoNotFoundException(String errore) {
		super(errore);
	}
	


}
