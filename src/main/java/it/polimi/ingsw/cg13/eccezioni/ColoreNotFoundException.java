package it.polimi.ingsw.cg13.eccezioni;

public class ColoreNotFoundException extends CdQException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8302387268855341752L;

	public ColoreNotFoundException() {
		super();
	}
	
	public ColoreNotFoundException(String errore) {
		super(errore);
	}

}