package it.polimi.ingsw.cg13.eccezioni;

public class ErrorUpdateServerException extends CdQException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5847014667921554572L;
	private static final String tipoErrore="Errore sul server: ";
			
	public ErrorUpdateServerException() {
		super();
	}
	
	public ErrorUpdateServerException(String errore) {
		super(tipoErrore+errore);
	}

	public ErrorUpdateServerException(CdQException exception){
		super(exception);
	}
}
