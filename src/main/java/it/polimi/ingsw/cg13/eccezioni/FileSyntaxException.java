package it.polimi.ingsw.cg13.eccezioni;

public class FileSyntaxException extends CdQException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3385429760142839778L;

	public FileSyntaxException() {
		super();
	}
	
	public FileSyntaxException(String messaggio){
		super(messaggio);
	}
	
	public FileSyntaxException(CdQException exception){
		super(exception);
	}
}
