package it.polimi.ingsw.cg13.eccezioni;

public class ConsigliereNotFoundException extends CdQException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7941691486414846931L;

	public ConsigliereNotFoundException() {
		super();
	}
	
	public ConsigliereNotFoundException(String errore) {
		super(errore);
	}

}
