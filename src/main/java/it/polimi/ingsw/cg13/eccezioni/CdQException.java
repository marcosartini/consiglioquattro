package it.polimi.ingsw.cg13.eccezioni;

public abstract class CdQException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3500013541291521761L;
	
	public CdQException() {
		super();
	}
	
	public CdQException(String message){
		super(message);
	}
	
	public CdQException(CdQException exception){
		super(exception);
	}
}
