package it.polimi.ingsw.cg13.eccezioni;

public class AiutantiFinitiException extends CdQException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1074522934304506979L;

	public AiutantiFinitiException() {
		super();
	}

	public AiutantiFinitiException(String message) {
		super(message);
	}

	public AiutantiFinitiException(CdQException exception) {
		super(exception);
	}

}
