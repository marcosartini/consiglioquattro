package it.polimi.ingsw.cg13.eccezioni;

public class IllegalPercorsoException extends AzioneNotPossibleException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2373344403598627223L;

	public IllegalPercorsoException() {
		super();
	}
	
	public IllegalPercorsoException(String errore) {
		super(errore);
	}

	@Override
	public String toString() {
		return this.getMessage();
	}

}
