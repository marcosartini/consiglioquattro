package it.polimi.ingsw.cg13.eccezioni;

public class CartePoliticheNotFoundException extends CdQException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1124136588360599907L;

	public CartePoliticheNotFoundException() {
		super();
	}
	
	public CartePoliticheNotFoundException(String errore) {
		super(errore);
	}


}
