package it.polimi.ingsw.cg13.eccezioni;

public class CittaNotFoundException extends CdQException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3183521399774671483L;

	public CittaNotFoundException() {
		super();
	}
	
	public CittaNotFoundException(String errore) {
		super(errore);
	}


}
