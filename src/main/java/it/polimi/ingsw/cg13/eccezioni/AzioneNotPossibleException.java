package it.polimi.ingsw.cg13.eccezioni;

public class AzioneNotPossibleException extends CdQException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8583144524532219920L;

	public AzioneNotPossibleException(){
		super();
	}
	
	public AzioneNotPossibleException(String errore){
		super(errore);
	}
	
	public AzioneNotPossibleException (CdQException exception){
		super(exception);
	}
	
}
