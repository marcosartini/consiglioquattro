package it.polimi.ingsw.cg13.observable;


public interface ObserverC <C>{

	public void update(C change);

	public default void update(Exception errore){
		//Non fa un niente, e' per evitare di implementarla dove non serve
	}
}
