package it.polimi.ingsw.cg13.observable;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg13.comandi.ChatMessage;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.comandi.Query;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.server.View;


public abstract class ObservableMVC<C> {
	private List<ObserverMVC<C>> observers;

	public ObservableMVC() {
		this.observers=new ArrayList<>();
	}
	
	public void registerObserver(ObserverMVC<C> o){
		this.observers.add(o);
	}
	
	public void unregisterObserve(ObserverMVC<C> o){
		observers.remove(o);
	}
	
	
	public void notifyObservers(C change) throws RemoteException, CdQException {
		for(ObserverMVC<C> o: this.observers){
			o.update(change);
		}
	}
	
	public void notifyObservers (Exception e){
		for(ObserverMVC<C> o: this.observers){
			o.update(e);
		}
	}
	
	public void notifyObservers (Login log) throws RemoteException, CdQException {
		for(ObserverMVC<C> o: this.observers){
			o.update(log);
		}
	}
	
	public void notifyObservers(Query query) throws RemoteException, CdQException {
		for(ObserverMVC<C> o: this.observers){
			o.update(query);
		}
	}
	
	public void notifyObservers(View view) throws RemoteException, CdQException {
		for(ObserverMVC<C> o: this.observers){
			o.update(view);
		}
	}
	
	public void notifyObservers(ChatMessage chat) {
		for(ObserverMVC<C> o: this.observers){
			o.update(chat);
		}
	}
}

