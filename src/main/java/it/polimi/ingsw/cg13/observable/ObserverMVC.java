package it.polimi.ingsw.cg13.observable;

import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.comandi.ChatMessage;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.comandi.Query;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.server.View;

public interface ObserverMVC<C> {
	
	
	public void update (Exception e);
	
	public  default void  update(C change) throws CdQException, RemoteException{
		System.out.println("I am the"+this.getClass().getSimpleName()+
				"\nI have been notified with the"+change.getClass().getSimpleName());// Rappresenta il cambiamento per ogni istanza osbserver 
	}
	
	public default void update(Query query) throws RemoteException, CdQException{
		System.out.println("I am the"+this.getClass().getSimpleName()+
				"\nI have been notified with the"+query.getClass().getSimpleName());
	}
	
	public default void update(Login login) throws CdQException,RemoteException{
		System.out.println("Il giocatore "+login.getNomeGiocatore()+" sta cercando di connettersi alla partita ");
	}
	
	public default void update(ChatMessage chatMessage){
		System.out.println("Registrazione messaggio chat in corso");
	}

	public default void update(View view){
		System.out.println("Unregister view in corso...");
	}
	

		
}
