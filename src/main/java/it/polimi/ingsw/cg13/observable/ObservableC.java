package it.polimi.ingsw.cg13.observable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;



public abstract class ObservableC<C> {
	private List<ObserverC<C>> observers;

	public ObservableC() {
		this.observers=new ArrayList<>();
	}
	
	public void registerObserver(ObserverC<C> o){
		this.observers.add(o);
	}
	
	public void registerObserver(Collection<ObserverC<C>> obs){
		this.observers.addAll(obs);
	}
	
	public void unregisterObserver(ObserverC<C> o){
		this.observers.remove(o);
	}
	
	
	public void notifyObservers(C change){
		for(ObserverC<C> o: this.observers){
			o.update(change);
		}
	}
	
	public void notifyObservers(Exception errore){
		for(ObserverC<C> o: this.observers){
			o.update(errore);
		}
	}
	
	
}
