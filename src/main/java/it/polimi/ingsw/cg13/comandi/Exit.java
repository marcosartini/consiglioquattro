package it.polimi.ingsw.cg13.comandi;

import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.mutatio.LineGiocatoreMutatio;
import it.polimi.ingsw.cg13.mutatio.StateMutatio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class Exit extends Query {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -4610964210323046308L;
	private static final Logger logger = LoggerFactory.getLogger(Exit.class);
	
	/**
	 * Crea una query Exit con giocatore, il giocatore passato come parametro
	 * @param giocatore che sta eseguendo la query Exit
	 * @throw IllegalArgumentException: se si passa un giocatore nullo
	 */
	public Exit(Giocatore giocatore) {
		super(giocatore);
	}

	/**
	 * Ritorna una stringa rappresentante il risultato della query
	 * Torna la stringa giocatore loggato se il logout funziona correttamente
	 * Setta il giocatore offline e lo sposta nei giocatoriOffline di partita
	 * Notifica l'avvenuto logout e nel caso sia il giocatore corrente, fa passare
	 * la partita al turno successivo altrimenti lo rimuove tra i giocatori prossimi all'azione
	 * @param partita su cui deve lavorare
	 * @return una stringa che rappresente il risultato della query
	 * @throws AzioneNotPossibleException: se la query che si sta cercando di fare non è possibile perche il giocatore non esiste
	 */
	@Override
	public String perform(Partita partita) throws AzioneNotPossibleException {
		Giocatore giocatoreUscente=this.setGiocatoreOffline(partita);
		this.updateStatoPartita(partita, giocatoreUscente);
		try {
			partita.notifyObservers(new LineGiocatoreMutatio(giocatoreUscente));
			partita.notifyObservers(new StateMutatio(partita.getStatoPartita()));
		} catch (RemoteException | CdQException e) {
			logger.error(e.getMessage(),e);
		}
		
		return "Giocatore: "+giocatoreUscente.getNome()+" logout\n";	
	}

	
	/**
	 * Setta l'attributo online del giocatore a false
	 * e rimuove il giocatore dai giocatori della partita e lo 
	 * metto nei giocatori offline
	 * @param partita: modello su cui applicare la Exit
	 * @return il giocatore nel modello che sta effettuando il logout
	 * @throws AzioneNotPossibleException: se il giocatore che compie la exit non viene trovato nel modello
	 */
	public Giocatore setGiocatoreOffline(Partita partita) throws AzioneNotPossibleException{
		Giocatore giocatoreUscente=partita.getGiocatori().stream().
				filter(giocatore -> giocatore.equals(this.giocatore)).findFirst().
				orElseThrow(()-> new AzioneNotPossibleException("Giocatore non trovato nella partita\n"));
		giocatoreUscente.setIsOnline(false);
		partita.getGiocatoriOffline().add(giocatoreUscente);
		partita.getGiocatori().remove(giocatoreUscente);
		return giocatoreUscente;
	}
	
	/**
	 * Prende lo stato Corrente della partita,
	 * se il giocatore corrente e' il giocatore che sta effettuando il logout passa al turno successivo
	 * altrimenti lo rimuove dai giocatori prossimi a fare il loro turno
	 * @param partita: modello su cui si sta effettuando l'azione
	 * @param giocatoreUscente che sta effettuando il logout
	 */
	public void updateStatoPartita(Partita partita,Giocatore giocatoreUscente){
		State stato=partita.getStatoPartita();
		if(giocatoreUscente.equals(partita.getGiocatoreCorrente())){
			stato=stato.setEndState();
			partita.setStatoPartita(stato);
		}
		else{
			stato.getGiocatoriMustDoAction().remove(giocatoreUscente);
		}
	}
}
