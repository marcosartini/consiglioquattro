package it.polimi.ingsw.cg13.comandi;

public class Login implements Comando {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 530425398213111460L;
	/**
	 * nome del giocatore che vuole loggarsi
	 */
	private String nomeGiocatore;
	
	/**
	 * Costruttore di una classe Login contenente il nome 
	 * del giocatore che vuole loggarsi
	 * @param nome del giocatore che si sta loggando
	 * @throws IllegalArgumentException: non puoi passare un nome nullo o vuoto
	 */
	public Login(String nome){
		if(nome==null)
			throw new IllegalArgumentException("Non puoi passare un nome nullo");
		if(nome.trim().equals(""))
			throw new IllegalArgumentException("Non puoi passare una stringa vuota");
		this.setNomeGiocatore(nome);
	}

	/**
	 * Ritorna il nome del giocatore che si e' loggato
	 * @return il nome del giocatore
	 */
	public String getNomeGiocatore() {
		return nomeGiocatore;
	}

	/**
	 * Assegna il nome inserito come parametro al nome della login
	 * @param nomeGiocatore del giocatore che si vuole assegnare alla Login
	 * @throws IllegalArgumentException: non puoi passare un nome nullo o vuoto
	 */
	public void setNomeGiocatore(String nomeGiocatore) {
		if(nomeGiocatore==null)
			throw new IllegalArgumentException("Non puoi passare un nome nullo");
		if(nomeGiocatore.trim().equals(""))
			throw new IllegalArgumentException("Non puoi passare una stringa vuota");
		this.nomeGiocatore = nomeGiocatore;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nomeGiocatore == null) ? 0 : nomeGiocatore.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Login other = (Login) obj;
		if (nomeGiocatore == null) {
			if (other.nomeGiocatore != null)
				return false;
		} else if (!nomeGiocatore.equals(other.nomeGiocatore))
			return false;
		return true;
	}
}
