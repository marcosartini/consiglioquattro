package it.polimi.ingsw.cg13.comandi;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class VisualizzaMarket extends Query{

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -5560985669599332890L;

	/**
	 * Crea una query VisualizzaMarket con giocatore, il giocatore passato come parametro
	 * @param giocatore che sta eseguendo la query
	 * @throw IllegalArgumentException: se si passa un giocatore nullo
	 */
	public VisualizzaMarket(Giocatore giocatore) {
		super(giocatore);
	}
	
	/**
	 * Ritorna una stringa rappresentante il risultato della query
	 * In questo caso torna la stampa del market
	 * @param partita su cui deve lavorare
	 * @return una stringa che rappresente il risultato della query visualizzMarket
	 * @throws AzioneNotPossibleException: se la query che si sta cercando di fare non è possibile
	 */
	@Override
	public String perform(Partita partita) throws AzioneNotPossibleException {
		return partita.getStatoPartita().getMarket().toString();
	}

	
}
