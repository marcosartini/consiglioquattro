package it.polimi.ingsw.cg13.comandi;

import java.io.Serializable;

public interface Comando extends Serializable {
	// Interfaccia che indica tutti i comandi del gioco, dalle azioni all'help
}
