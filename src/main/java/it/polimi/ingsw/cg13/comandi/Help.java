package it.polimi.ingsw.cg13.comandi;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class Help extends Query {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 5640934874036198033L;

	/**
	 * Crea una query Help con giocatore il giocatore passato come parametro
	 * @param giocatore che sta eseguendo la query Help
	 * @throw IllegalArgumentException: se si passa un giocatore nullo
	 */
	public Help(Giocatore giocatore) {
		super(giocatore);
	}

	/**
	 * Ritorna una stringa rappresentante il risultato della query
	 * In questo caso torna l'help per i comandi del giocatore
	 * @param partita su cui deve lavorare
	 * @return una stringa che rappresente il risultato della query
	 * @throws AzioneNotPossibleException: se la query che si sta cercando di fare non è possibile
	 */
	@Override
	public String perform(Partita partita) throws AzioneNotPossibleException {
		String help="";
		help+="\nSintassi mosse:    --> nomeMossa parametro1 parametroMultiplo2 parametro3 ...";
		help+="ParametroMultiplo: --> valore1-valore2-... \n\n";
		help+="Elenco delle mosse giocatore:\n";
		help+="AcquistoPermesso regione numeroPermesso coloreCartaPolitica1-coloreCartaPolitica2-..\n";
		help+="CambioConsiglioVeloce regione coloreConsigliereDaInserire\n";
		help+="CambioConsiglio regione coloreConsigliereDaInserire\n";
		help+="CostruisciConRe citta1-...-cittaDoveCostruire coloreCartaPolitica1-cartaPolitica2-..\n";
		help+="CostruisciConRe cittaDelRe coloreCartaPolitica1-cartaPolitica2-..\n";
		help+="CostruisciConPermesso  numeroPermesso citta\n";
		help+="CambioPermessi regione\n";
		help+="AcquistoAiutante\n";
		help+="AcquistoAzionePrincipale\n";
		help+="\nElenco mosse di market:\n";
		help+="Vendo tipoVendibile vendibile prezzo\n";
		help+="Acquisto idInserzione\n";
		help+="PrintMarket\n";
		help+="\nAltri comandi:\n";
		help+="Exit   		 			--> esci dalla partita\n";
		help+="Help   		 			--> stampa l'help\n";
		help+="Print  		 			--> stampa stato della Partita\n";
		help+="FinisciTurno  			--> finisce il tuo turno corrente\n";
		help+="BonusInput parametro  	--> Inserisci il bonus da riusare\n"
				+ "Dove il parametro può essere un permesso o una citta a seconda della richiesta\n";
		return help;
	}

}
