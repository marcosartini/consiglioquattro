package it.polimi.ingsw.cg13.comandi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class Print extends Query {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 5636568018287933826L;
	/**
	 * logger per stampare eventuali errori
	 */
	private static final Logger logger = LoggerFactory.getLogger(Print.class);
	
	/**
	 * Crea una query Print con giocatore il giocatore passato come parametro
	 * richiama la super di Query
	 * @param giocatore che sta eseguendo la query print
	 * @throw IllegalArgumentException: se si passa un giocatore nullo
	 */
	public Print(Giocatore giocatore) {
		super(giocatore);
	}
	
	/**
	 * Ritorna una stringa rappresentante il risultato della query
	 * In questo caso ritorna il modello sotto forma di stringa
	 * @param partita su cui deve lavorare
	 * @return una stringa che rappresente il risultato della query
	 * @throws AzioneNotPossibleException: se la query che si sta cercando di fare non è possibile
	 */
	@Override
	public String perform(Partita modello) throws AzioneNotPossibleException {
		
		String stringModel="";
		try{
		stringModel+="\n::: STATO DEL MODELLO :::";
		stringModel+=modello.toString();
		stringModel+="\n### FINE STATO MODELLO ###\n";
		
		stringModel+="StatoGiocatore:\n "+this.giocatore.toString()+"\n";
		}catch(NullPointerException e){
			stringModel="Non e' possibile stampare il modello\n";
			logger.error(e.getMessage(), e);
		}
		return stringModel;
	}

}
