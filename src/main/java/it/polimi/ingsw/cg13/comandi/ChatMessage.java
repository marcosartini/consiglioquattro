package it.polimi.ingsw.cg13.comandi;

import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class ChatMessage implements Comando {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8586169848641448603L;
	private Giocatore giocatore;
	private String testo;
	
	/**
	 * Costruttore, crea un messaggio di chat con giocatore che invia e testo
	 * @param giocatore: giocatore che sta inviando il messaggio
	 * @param testo: testo che sta inviando 
	 */
	public ChatMessage(Giocatore giocatore,String testo) {
		this.giocatore=giocatore;
		this.testo=testo;
	}

	/**
	 * Ritorna il giocatore che sta inviando il messaggio
	 * @return giocatore
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}

	/**
	 * Ritorna il testo del messaggio
	 * @return testo del messaggio
	 */
	public String getTesto() {
		return testo;
	}

	/**
	 * {@inheritDoc}
	 * Stringa composta dal giocatore e dal messaggio scritto
	 */
	@Override
	public String toString() {
		return "Giocatore " + giocatore.getNome() + " says ->  " + testo ;
	}

}
