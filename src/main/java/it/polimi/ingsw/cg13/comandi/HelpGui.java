package it.polimi.ingsw.cg13.comandi;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class HelpGui extends Query {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2126896129489345722L;

	/**
	 * Costruttore della classe--> crea una query per aiutare il giocatore a eseguire le azioni nella GUI
	 * @param giocatore
	 * @throw IllegalArgumentException: se si passa un giocatore nullo
	 */
	public HelpGui(Giocatore giocatore) {
		super(giocatore);
	}

	
	@Override
	/**
	 * Restituisce la stinga di help per le azioni della GUI
	 * @param partita
	 * @return String
	 */
	public String perform(Partita partita) throws AzioneNotPossibleException {
		String str="";
		str+="Durante il proprio turno ogni giocatore può effettuare un' Azione Principale e un' Azione Veloce\n";
		str+=this.helpAcquistoAiutante();
		str+=this.helpAcquistoAzionePrincipale();
		str+=this.helpAcquistoPermesso();
		str+=this.helpCambioConsiglio();
		str+=this.helpCambioConsiglioVeloce();
		str+=this.helpCambioPermessi();
		str+=this.helpCostruzioneConPermesso();
		str+=this.helpCostruzioneConKing();
		return str;
		
	}
	
	/**
	 * Restituisce la stringa di help per l'azione di acquisto aiutante della GUI
	 * @return str
	 */
	public String helpAcquistoAiutante(){
		String str="";		
		str+="-AcquistoAiutante: Azione Veloce\npermette di aggiungere un nuovo aiutante a quelli che si possiedono al costo di 3 punti ricchezza\n";		
		return str;
		
	}

	/**
	 * Restituisce la stringa di help per l'azione di acquisto azione principale della GUI
	 * @return str
	 */
	public String helpAcquistoAzionePrincipale(){
		String str="";		
		str+="-AcquistoAzionePrincipale: Azione Veloce\nPermette di comprare una nuova azione principale da eseguire  al costo di 3 aiutanti\n";
		str+="Selezionare gli aiutanti da quelli in proprio possesso per eseguire l'azione\n";
		return str;
		
	}
	
	/**
	 * Restituisce la stringa di help per l'azione di acquisto Permesso di costruzione della GUI
	 * @return str
	 */
	public String helpAcquistoPermesso(){
		String str="";		
		str+="-AcquistoPermesso: Azione Principale\nPermette di comprare un permesso di costruzione corrompendo il consiglio della Regione a cui appartiene\n";
		str+="Selezionare il consiglio da corrompere e le carte politiche da utilizzare\n";
		return str;
		
	}
	
	/**
	 * Restituisce la stringa di help per l'azione di Cambio Consiglio della GUI
	 * @return str
	 */
	public String helpCambioConsiglio(){
		String str="";		
		str+="-CambioConsiglio: Azione Principale\nPermette di cambiare un consigliere di un consiglio sulla mappa e ricevere 4 punti ricchezza \n";
		str+="Selezionare il consiglio da cambiare e il relativo consigliere che si vuole inserire dal banco\n";
		return str;
		
	}
	
	/**
	 * Restituisce la stringa di help per l'azione di Cambio Consiglio Veloce della GUI
	 * @return str
	 */
	public String helpCambioConsiglioVeloce(){
		String str="";		
		str+="-CambioConsiglioVeloce: Azione Veloce\nPermette di cambiare un consigliere di un consiglio sulla mappa al costo di 1 aiutante \n";
		str+="Selezionare il consiglio da cambiare, il relativo consigliere che si vuole inserire dal banco e i consiglieri da utilizzare\n";
		return str;
		
	}
	
	/**
	 * Restituisce la stringa di help per l'azione di Cambio Permessi della GUI
	 * @return str
	 */
	public String helpCambioPermessi(){
		String str="";		
		str+="-CambioPermessi: Azione Veloce\nPermette di cambiare i permessi di costruzione scoperti di una regione al costo di 1 aiutante \n";
		str+="Selezionare i consiglieri da utilizzare e il mazzo da cui prendere le carte\n";
		return str;
		
	}
	
	/**
	 * Restituisce la stringa di help per l'azione di Costruzione con permesso della GUI
	 * @return str
	 */
	public String helpCostruzioneConPermesso(){
		String str="";		
		str+="-CostruzioneConPermesso: Azione Principale\nPermette di costruire un emporio utilizzando un permesso di cosrtuzione \n";
		str+="Selezionare la città in cui si vuole costruire e il permesso che si vuole utilizzare\n";
		return str;
		
	}
	
	/**
	 * Restituisce la stringa di help per l'azione di Costruzione con King della GUI
	 * @return str
	 */
	public String helpCostruzioneConKing(){
		String str="";		
		str+="-CostruzioneConKing: Azione Principale\nPermette di costruire un emporio corrompendo il consiglio del re e spostando il King sulla città prescelta\n";
		str+="Selezionare le carte politiche da utilizzare per la corruzione, il consiglio del re e spostare il re sulla città desiderata\n";
		return str;
		
	}
}
