package it.polimi.ingsw.cg13.comandi;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public abstract class Query implements Comando{

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -8953924434262074024L;
	/**
	 * Giocatore che sta eseguendo la query
	 */
	protected Giocatore giocatore;
	
	/**
	 * Crea una query con giocatore il giocatore passato come parametro
	 * @param giocatore che sta eseguendo la query
	 * @throw IllegalArgumentException: se si passa un giocatore nullo
	 */
	public Query(Giocatore giocatore){
		if(giocatore==null)
			throw new IllegalArgumentException("Non puoi passare un giocatore nullo");
		this.giocatore=giocatore;
	}
	
	/**
	 * Ritorna una stringa rappresentante il risultato della query
	 * @param partita su cui deve lavorare
	 * @return una stringa che rappresente il risultato della query
	 * @throws AzioneNotPossibleException: se la query che si sta cercando di fare non è possibile
	 */
	public abstract String perform(Partita partita) throws AzioneNotPossibleException;

	/**
	 * Ritorna il giocatore che vuole eseguire la query
	 * @return il giocatore della query
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}
	

}
