package it.polimi.ingsw.cg13.chat;

import java.io.Serializable;
import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.comandi.Comando;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mutatio.ChatMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableMVC;
import it.polimi.ingsw.cg13.observable.ObserverC;

public class ChatText extends ObservableMVC<Mutatio> implements ObserverC<Comando>,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4816450531745079828L;
	private String text;
	
	/*
	 * Costruttore che crea una chat con contenuto vuoto
	 */
	public ChatText() {
		text="";
	}

	/**
	 * 
	 * @return il testo della chat
	 */
	public String getText() {
		return text;
	}

	/**
	 * Aggiunge il testo alla stringa della chat
	 * @param stringa da appendere
	 */
	public void append(String stringa){
		text+=stringa+"\n";
	}


	/**
	 * {@inheritDoc}
	 * Notifica il cambiamento del contenuto della chat
	 */
	@Override
	public void update(Comando sendChatMessage) {
		this.append(sendChatMessage.toString());
		try {
			this.notifyObservers(new ChatMutatio(text));
		} catch (RemoteException | CdQException e) {
			this.notifyObservers(e);
		}
	}

}
