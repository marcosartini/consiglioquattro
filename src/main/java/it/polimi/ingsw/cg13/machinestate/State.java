package it.polimi.ingsw.cg13.machinestate;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mutatio.ClassificaMutatio;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.punteggio.CalcolatorePunteggio;
import it.polimi.ingsw.cg13.visitor.ElementToVisit;

public abstract class State implements ElementToVisit {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5537460644604922750L;
	/**
	 * lista dei giocatori che devono fare ancora il proprio turno
	 */
	protected List<Giocatore> giocatoriMustDoAction;
	/**
	 * giocatore corrente
	 */
	protected Giocatore giocatoreCorrente;
	
	/**
	 * Costruttore di un geenrico stato della partita
	 * @param giocatoriOnline: lista giocatori che possono fare le mosse
	 * @param giocatore: giocatore corrente che deve fare il proprio turno
	 */
	public State(List<Giocatore> giocatoriOnline,Giocatore giocatore) {
		this.giocatoriMustDoAction=new ArrayList<>();
		this.giocatoriMustDoAction.addAll(giocatoriOnline);
		this.giocatoreCorrente=giocatore;
		
	}
	
	/**
	 * Ritorna se il turno della partita e' finito oppure no
	 * @return true solo per i turni di Fine, negli torna false
	 */
	public boolean isEnd(){
		return false;
	}

	/**
	 * Ritorna la lista dei giocatori che deovno giocare ancora
	 * @return lista dei giocatori in attesa
	 */
	public List<Giocatore> getGiocatoriMustDoAction() {
		return giocatoriMustDoAction;
	}

	/**
	 * Ritorna il giocatore che sta giocando
	 * @return giocatore che deve eseguire le azioni
	 */
	public Giocatore getGiocatoreCorrente() {
		return giocatoreCorrente;
	}
	
	
	
	/**
	 * Aggiunge una ricompensa speciale allo stato corrente
	 * @param ricompensa : ricompensa aggiunta
	 * @throws AzioneNotPossibleException se non siamo in una fase di gioco
	 */
	public abstract void addRicompensa(RicompensaSpecial ricompensa) throws AzioneNotPossibleException;
	
	/**
	 * Torna il market della fase della partita
	 * @return market contenente le inserzioni dei giocatori
	 * @throws AzioneNotPossibleException se non siamo in una fase di market
	 */
	public abstract Market getMarket() throws AzioneNotPossibleException;
	
	/**
	 * Ritorna lo stato successivo della partita 
	 * @param partita: modello con cui interagire
	 * @return lo stato prossimo della partita
	 * @throws RemoteException: probelma con chiamata in remoto
	 * @throws CdQException: se si verifica un errore dovuto ad un problema di aggiornamento o di azione non possibile
	 */
	public abstract State nextState(Partita partita) throws RemoteException, CdQException;
	
	/**
	 * Porta ad uno degli stati di fine fase(FineGioco,FineMarketVendita,FineMarketAcquisto)
	 * @return stato finale corrispondente alla fase in corso
	 */
	public abstract State setEndState();

	@Override
	public String toString() {
		return "StatoPartita -> ERRORE DEL GIOCO, STATO GENERICO";
	}

	/**
	 * Funzione che calcola il punteggio di fine partita
	 * @param stato: stato in cui si trova la partita
	 * @param partita modello su cui interagire
	 * @return stato di fine partita
	 * @throws RemoteException: probelma con chiamata in remoto
	 * @throws CdQException: se si verifica un errore dovuto ad un problema di aggiornamento o di azione non possibile
	 */
	protected State calcolaPunteggioFinale(State stato,Partita partita) throws RemoteException, CdQException {
		CalcolatorePunteggio punteggio=new CalcolatorePunteggio();
		punteggio.calcola(partita);
		List<Giocatore> giocatori=new ArrayList<>();
		giocatori.addAll(partita.getGiocatori());
		giocatori.addAll(partita.getGiocatoriOffline());
		stato=new StateFinePartita(giocatori,giocatori.get(0));
		partita.notifyObservers(new ClassificaMutatio(punteggio));
		partita.setEnded(true);
		return stato;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((giocatoreCorrente == null) ? 0 : giocatoreCorrente.hashCode());
		result = prime * result + ((giocatoriMustDoAction == null) ? 0 : giocatoriMustDoAction.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (giocatoreCorrente == null) {
			if (other.giocatoreCorrente != null)
				return false;
		} else if (!giocatoreCorrente.equals(other.giocatoreCorrente))
			return false;
		if (giocatoriMustDoAction == null) {
			if (other.giocatoriMustDoAction != null)
				return false;
		} else if (!giocatoriMustDoAction.equals(other.giocatoriMustDoAction))
			return false;
		return true;
	}

	
}
