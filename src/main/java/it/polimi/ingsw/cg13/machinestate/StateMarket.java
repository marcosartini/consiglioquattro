package it.polimi.ingsw.cg13.machinestate;

import java.util.List;

import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public abstract class StateMarket extends State {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3939553992822686583L;
	private Market market;

	public StateMarket(List<Giocatore> giocatoriOnline, Giocatore giocatore,Market market) {
		super(giocatoriOnline, giocatore);
		this.setMarket(market);
	}

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}
	
	
	@Override
	public void addRicompensa(RicompensaSpecial ricompensa) throws AzioneNotPossibleException{
		throw new AzioneNotPossibleException("Errore: non ci possono essere ricompense nella fase di Market");
	}
	
	
	@Override
	public State accept(RicompensaSpecial ricompensa, Partita partita) throws AzioneNotPossibleException {
		throw new AzioneNotPossibleException("Errore: non ci possono essere ricompense nella fase di Market");
	}

	@Override
	public String toString() {
		return "StateMarket";
	}


}
