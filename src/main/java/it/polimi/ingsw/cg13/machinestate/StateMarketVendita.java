package it.polimi.ingsw.cg13.machinestate;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mutatio.StateMutatio;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public abstract class StateMarketVendita extends StateMarket {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1098818901796916704L;


	public StateMarketVendita(List<Giocatore> giocatoriOnline, Giocatore giocatore, Market market) {
		super(giocatoriOnline, giocatore, market);
	}

	
	@Override
	public State setEndState(){
		return new StateFineVendita(this.giocatoriMustDoAction,this.giocatoreCorrente,this.getMarket());
	}

	@Override
	public State nextState(Partita partita) throws RemoteException, CdQException {
		State stato=this;
		if(this.isEnd()){
			if(!this.giocatoriMustDoAction.isEmpty()){
				Giocatore giocatore=this.giocatoriMustDoAction.remove(0);
				stato= new StateInizioVendita(giocatoriMustDoAction,giocatore,this.getMarket());
			}
			else if(partita.getGiocatori().isEmpty()){
				// Partita Finita, sono usciti tutti i giocatori
				stato=this.calcolaPunteggioFinale(stato,partita);
			}
			else{
				List<Giocatore> giocatoriMustDoAction=new ArrayList<>();
				giocatoriMustDoAction.addAll(partita.getGiocatori());
				Collections.shuffle(giocatoriMustDoAction);
				Giocatore giocatore=giocatoriMustDoAction.remove(0);
				stato=new StateAcquistoInizio(giocatoriMustDoAction,giocatore,this.getMarket());
			}
			partita.setStatoPartita(stato);
			partita.notifyObservers(new StateMutatio(stato));
		}
		return stato;
	}

}
