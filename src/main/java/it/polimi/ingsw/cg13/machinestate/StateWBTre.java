package it.polimi.ingsw.cg13.machinestate;

import java.util.List;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class StateWBTre extends StateGioco {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2841732143479957708L;


	public StateWBTre(List<Giocatore> giocatoriOnline, Giocatore giocatore, List<RicompensaSpecial> ricompense) {
		super(giocatoriOnline, giocatore, ricompense);
	}

	@Override
	public State accept(Azione azione,Partita partita) throws CdQException {
		return azione.visit(this,partita);
	}

	@Override
	public State accept(RicompensaSpecial ricompensa, Partita partita) throws CdQException {
		return ricompensa.visit(this, partita);
	}


	@Override
	public String toString() {
		return "StatoPartita\ngiocatoreCorrente: "+this.giocatoreCorrente.getNome()+ " -> In attesa di un input bonus "+this.getRicompense().get(0);
	}
	
}
