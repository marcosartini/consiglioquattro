package it.polimi.ingsw.cg13.machinestate;

import java.util.List;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class StateInizioVendita extends StateMarketVendita {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5840156352143459517L;

	public StateInizioVendita(List<Giocatore> giocatoriOnline, Giocatore giocatore,
			Market market) {
		super(giocatoriOnline, giocatore, market);
	}

	@Override
	public State accept(Azione azione, Partita partita) throws CdQException {
		return azione.visit(this, partita);
	}

	@Override
	public String toString() {
		return super.toString()+"InizioVendita: "+this.getGiocatoreCorrente().getNome()+"\n";
	}

}
