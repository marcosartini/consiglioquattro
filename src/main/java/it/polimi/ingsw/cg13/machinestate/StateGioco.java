package it.polimi.ingsw.cg13.machinestate;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mutatio.BonusLosedMutatio;
import it.polimi.ingsw.cg13.mutatio.InizioFaseMarketMutatio;
import it.polimi.ingsw.cg13.mutatio.StateMutatio;
import it.polimi.ingsw.cg13.mutatio.UltimoTurnoMutatio;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;


public abstract class StateGioco extends State {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1334991290801886419L;
	protected List<RicompensaSpecial> ricompense;

	public StateGioco(List<Giocatore> giocatoriOnline, Giocatore giocatore, List<RicompensaSpecial> ricompense) {
		super(giocatoriOnline, giocatore);
		this.ricompense=ricompense;
	}


	@Override
	public State setEndState(){
		return new StateFine(this.giocatoriMustDoAction,this.giocatoreCorrente,this.getRicompense());
	}
	
	
	public List<RicompensaSpecial> getRicompense() {
		return ricompense;
	}
	
	@Override
	public void addRicompensa(RicompensaSpecial ricompensa){
		this.ricompense.add(ricompensa);
	}

	@Override
	public State nextState(Partita partita) throws RemoteException, CdQException{
		State stato=this;
		if(!this.ricompense.isEmpty()){
			stato=this.assegnaRicompensaSpecial(stato,partita);
		}
		else if(this.isLastTurnBegin(partita)){
			stato=this.startLastTurn(partita);
			partita.setStatoPartita(stato);
			partita.notifyObservers(new StateMutatio(stato));
		}
		else if(this.isEnd()){
				if(!this.giocatoriMustDoAction.isEmpty()){
					// Turno Giocatore Successivo
					stato=this.passaGiocatoreSuccessivo(partita);
				}else if(partita.getGiocatori().isEmpty()){
					// Partita Finita, sono usciti tutti i giocatori
					stato=this.calcolaPunteggioFinale(stato,partita);
				}
				else if(partita.getGiocatori().stream().anyMatch(gioc->gioc.getEmpori().isEmpty())){
					// Fine partita
					stato=this.calcolaPunteggioFinale(stato,partita);
				}else{
					// Turno Market di Vendita
					stato=this.passaStatoVenditaMarket(partita);
				}
				partita.setStatoPartita(stato);
				partita.notifyObservers(new StateMutatio(stato));
		}
		return stato;
		
	}
	
	
	private State passaGiocatoreSuccessivo(Partita partita) {
		Giocatore giocatore=this.giocatoriMustDoAction.remove(0);
		giocatore.addCartaPolitica(partita.pescaCarta());
		return new StateInizio(giocatoriMustDoAction,giocatore,this.getRicompense());
	}


	private State passaStatoVenditaMarket(Partita partita) throws RemoteException, CdQException {
		List<Giocatore> giocatoriMustDoAction=new ArrayList<>();
		giocatoriMustDoAction.addAll(partita.getGiocatori());
		Giocatore giocatore=giocatoriMustDoAction.remove(0);
		partita.notifyObservers(new InizioFaseMarketMutatio());
		return new StateInizioVendita(giocatoriMustDoAction,giocatore,new Market());
	}


	private State assegnaRicompensaSpecial(State stato,Partita partita) throws RemoteException, CdQException {
		RicompensaSpecial ricompensa=this.getRicompense().get(0);
		if(ricompensa.checkBonus(getGiocatoreCorrente(), partita)){
			stato=this.accept(ricompensa, partita);
			partita.setStatoPartita(stato);
			partita.notifyObservers(new StateMutatio(stato));
		}else{
			this.getRicompense().remove(0);
			//Notifica bonus non utilizzabile
			partita.notifyObservers(new BonusLosedMutatio());
			stato=this.nextState(partita);
		}
		return stato;
		
	}

	@Override
	public Market getMarket() throws AzioneNotPossibleException{
		throw new AzioneNotPossibleException("Siamo nella fase di Gioco, non nella fase di market");
	}
	

	
	
	public boolean isLastTurnBegin(Partita partita){
		return this.giocatoreCorrente.getEmpori().isEmpty() 
				&&  !partita.getGiocatori().stream()
				.anyMatch(gioc->gioc.getEmpori().isEmpty() && !gioc.equals(this.giocatoreCorrente));
	}
	
	public State startLastTurn(Partita partita) throws RemoteException, CdQException{
		partita.notifyObservers(new UltimoTurnoMutatio(this.giocatoreCorrente));
		List<Giocatore> giocatoriMancanti=new ArrayList<>();
		giocatoriMancanti.addAll(this.giocatoriMustDoAction);
		List<Giocatore> giocatoriPartita=new ArrayList<>();
		giocatoriPartita.addAll(partita.getGiocatori());
		giocatoriPartita.remove(this.giocatoreCorrente);
		giocatoriPartita.removeAll(giocatoriMancanti);
		giocatoriMancanti.addAll(giocatoriPartita);
		Giocatore giocatore=giocatoriMancanti.remove(0);
		return new StateInizio(giocatoriMancanti,giocatore,new ArrayList<RicompensaSpecial>());
		
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ricompense == null) ? 0 : ricompense.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateGioco other = (StateGioco) obj;
		if (ricompense == null) {
			if (other.ricompense != null)
				return false;
		} else if (!ricompense.equals(other.ricompense))
			return false;
		return true;
	}

}
