package it.polimi.ingsw.cg13.machinestate;

import java.util.List;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class StateFineVendita extends StateMarketVendita {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5999716471126753845L;

	public StateFineVendita(List<Giocatore> giocatoriOnline, Giocatore giocatore,Market market) {
		super(giocatoriOnline, giocatore, market);
	}

	@Override
	public State accept(Azione azione, Partita partita) throws CdQException {
		return azione.visit(this, partita);
	}
	
	@Override
	public boolean isEnd(){
		return true;
	}

	@Override
	public String toString() {
		return super.toString()+"FineVendita: "+this.getGiocatoreCorrente().getNome()+"\n";
	}

}
