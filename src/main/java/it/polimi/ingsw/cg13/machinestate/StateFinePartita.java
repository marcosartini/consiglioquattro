package it.polimi.ingsw.cg13.machinestate;

import java.rmi.RemoteException;
import java.util.List;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class StateFinePartita extends State {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3700597293563002520L;
	private static final String FINE_PARTITA = "E' finita la partita, non puoi fare nessun'altra azione";
	public StateFinePartita(List<Giocatore> giocatoriOnline, Giocatore giocatore) {
		super(giocatoriOnline, giocatore);
	}

	@Override
	public State accept(Azione azione, Partita partita) throws CdQException {
		throw new AzioneNotPossibleException(FINE_PARTITA);
	}

	@Override
	public State accept(RicompensaSpecial ricompensa, Partita partita) throws CdQException {
		throw new AzioneNotPossibleException(FINE_PARTITA);
	}


	@Override
	public State nextState(Partita partita) throws RemoteException, CdQException {
		return this;
	}

	@Override
	public void addRicompensa(RicompensaSpecial ricompensa) throws AzioneNotPossibleException {
		throw new AzioneNotPossibleException(FINE_PARTITA);
	}

	@Override
	public Market getMarket() throws AzioneNotPossibleException {
		throw new AzioneNotPossibleException(FINE_PARTITA);
	}

	@Override
	public State setEndState() {
		return this;
	}

	@Override
	public String toString() {
		return "Fine Partita";
	}


}
