package it.polimi.ingsw.cg13.machinestate;

import java.util.List;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class StateQuattro extends StateGioco {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4273481814587250027L;

	public StateQuattro(List<Giocatore> giocatoriOnline, Giocatore giocatore, List<RicompensaSpecial> ricompense) {
		super(giocatoriOnline, giocatore, ricompense);
	}

	
	@Override
	public State accept(Azione azione,Partita partita) throws CdQException {
		return azione.visit(this,partita);
	}
	
	@Override
	public State accept(RicompensaSpecial ricompensa, Partita partita) throws CdQException {
		return ricompensa.visit(this, partita);
	}

	
	@Override
	public boolean isEnd(){
		return this.ricompense.isEmpty();
	}

	@Override
	public String toString() {
		return "StatoPartita\ngiocatoreCorrente: "+this.giocatoreCorrente.getNome()+ " -> Assegnamento Bonus in corso";
	}
	
}
