package it.polimi.ingsw.cg13.controller;


import java.rmi.RemoteException;


import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.comandi.ChatMessage;
import it.polimi.ingsw.cg13.comandi.Comando;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.comandi.Query;
import it.polimi.ingsw.cg13.cronometro.Cronometro;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.mutatio.GiocatoreNuovoMutatio;
import it.polimi.ingsw.cg13.mutatio.QueryMutatio;
import it.polimi.ingsw.cg13.mutatio.StateMutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.observable.ObserverMVC;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.server.View;


public class Controller extends ObservableC<Comando> implements ObserverMVC<Azione>{
	private final Partita partita;
	public Controller (Partita partita){
		this.partita=partita;
	}
	
	@Override
	public synchronized void update(Query query) throws CdQException, RemoteException {
			String result=query.perform(partita);
			this.partita.notifyObservers(new QueryMutatio(result,query.getGiocatore()));
			partita.getStatoPartita().nextState(partita);
	}

	@Override
	public synchronized void update(Azione azione) throws CdQException, RemoteException{
		//try{
			//Scrivere equals tra giocatori
			if(!azione.getGiocatore().equals(partita.getGiocatoreCorrente())){
				//Non è il suo turno 
				throw new AzioneNotPossibleException("Non e' il tuo turno, non puoi fare questa mossa");
			}
			else{
				Cronometro.getIstanza(partita).setTempo(partita.getParametri().getDurataMossa());
				azione.updateParametri(partita);
				State newStato=partita.getStatoPartita();
				newStato=newStato.accept(azione, partita);
				partita.setStatoPartita(newStato);
				partita.notifyObservers(new StateMutatio(newStato));
				newStato=newStato.nextState(partita);	
			}
	}
	
	@Override
	public synchronized void update(View view){
		partita.unregisterObserve(view);
		view.unregisterObserve(this);
	}

	@Override
	public synchronized void update(Exception e) {
		throw new UnsupportedOperationException("Metodo non supportato");
		
	}
	
	@Override 
	public synchronized void update(ChatMessage chatMessage){
		this.notifyObservers(chatMessage);
	}

	@Override
	public synchronized void update(Login login) throws RemoteException, CdQException {
		Giocatore nuovoGiocatore=this.partita.addGiocatore(login);
		partita.notifyObservers(new GiocatoreNuovoMutatio(nuovoGiocatore));
		
	}
	
	
	
	
}
