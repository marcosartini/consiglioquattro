package it.polimi.ingsw.cg13.visitor;

import java.io.Serializable;
import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoInizio;
import it.polimi.ingsw.cg13.machinestate.StateCinque;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateFine;
import it.polimi.ingsw.cg13.machinestate.StateInizio;
import it.polimi.ingsw.cg13.machinestate.StateInizioVendita;
import it.polimi.ingsw.cg13.machinestate.StateOtto;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateSei;
import it.polimi.ingsw.cg13.machinestate.StateSette;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.machinestate.StateWBDue;
import it.polimi.ingsw.cg13.machinestate.StateWBQuattro;
import it.polimi.ingsw.cg13.machinestate.StateWBTre;
import it.polimi.ingsw.cg13.machinestate.StateWBUno;
import it.polimi.ingsw.cg13.partita.Partita;

public interface Visitor extends Serializable {

	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo
	 * @param stato: stato generico della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(State stato,Partita partita) throws CdQException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateUno
	 * @param stato: stato di tipo StateUno della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateUno stato,Partita partita) throws CdQException, RemoteException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateDue
	 * @param stato: stato di tipo StateDue della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateDue stato,Partita partita) throws CdQException, RemoteException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateTre
	 * @param stato: stato di tipo StateTre della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateTre stato,Partita partita) throws CdQException, RemoteException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateQuattro
	 * @param stato: stato di tipo StateQuattro della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateQuattro stato,Partita partita) throws CdQException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateCinque
	 * @param stato: stato di tipo StateCinque della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateCinque stato,Partita partita) throws CdQException, RemoteException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateSei
	 * @param stato: stato di tipo StateSei della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateSei stato,Partita partita) throws CdQException, RemoteException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateSette
	 * @param stato: stato di tipo StateSette della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateSette stato,Partita partita) throws CdQException, RemoteException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateOtto
	 * @param stato: stato di tipo StateOtto della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateOtto stato,Partita partita) throws CdQException, RemoteException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateInizio
	 * @param stato: stato di tipo StateInizio della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateInizio stato,Partita partita) throws CdQException, RemoteException{
		throw new AzioneNotPossibleException(" Mossa non consentita, e' nello stato di \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateFine
	 * @param stato: stato di tipo StateFine della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateFine stato,Partita partita) throws CdQException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateWBUno
	 * @param stato: stato di tipo StateWBUno della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateWBUno stato,Partita partita) throws CdQException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateWBDUE
	 * @param stato: stato di tipo StateWBDUE della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateWBDue stato,Partita partita) throws CdQException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateWBTre
	 * @param stato: stato di tipo StateWBTre della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateWBTre stato,Partita partita) throws CdQException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
	
	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateWBQuattro
	 * @param stato: stato di tipo StateWBQuattro della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateWBQuattro stato,Partita partita) throws CdQException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}

	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateInizioVendita
	 * @param stato: stato di tipo StateInizioVendita della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateInizioVendita stato, Partita partita) throws CdQException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}

	/**
	 * Metodo che permette ad un visitor di visitare uno stato della partita e se possibile,
	 * ti rimanda lo stato successivo; in questo caso visita uno stateInizioAcquisto
	 * @param stato: stato di tipo StateInizioAcquisto della partita che sta venendo visitato 
	 * @param partita: modello di riferimento
	 * @return lo stato successivo a quello passato come parametro
	 * @throws CdQException se il visitor non puo' compiere un'azione in quello stato 
	 */
	public default State visit(StateAcquistoInizio stato, Partita partita) throws CdQException{
		throw new AzioneNotPossibleException("Mossa non consentita, e' nello  \n"+
				stato.toString());
	}
		
}
