package it.polimi.ingsw.cg13.visitor;

import java.io.Serializable;
import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.partita.Partita;

public interface ElementToVisit extends Serializable {

	/**
	 * Elemento che puo essere visitato da un visitor, in questo caso un'azione
	 * Quando questo elemento viene visitato, chiama la visit dell'elemento di quest'ultimo 
	 * portando tutto allo stato successivo
	 * @param azione: visitor con il compito di visitare l'elemento
	 * @param partita: modello di riferimento
	 * @return stato: lo stato successivo a quello dell'elemento
	 * @throws CdQException se un elemento non puo' essere visitato da una certa azione lancia l'eccezione
	 * @throws RemoteException: errore di chiamata in remoto
	 */
	public State accept(Azione azione,Partita partita) throws  CdQException, RemoteException;
	/**
	 * Elemento che puo essere visitato da un visitor, in questo una ricompensaSpeciale
	 * Quando questo elemento viene visitato, chiama la visit dell'elemento di quest'ultimo 
	 * portando tutto allo stato successivo
	 * @param azione: visitor con il compito di visitare l'elemento
	 * @param partita: modello di riferimento
	 * @return stato: lo stato successivo a quello dell'elemento
	 * @throws CdQException se un elemento non puo' essere visitato da una certa azione lancia l'eccezione
	 * @throws RemoteException: errore di chiamata in remoto
	 */
	public State accept(RicompensaSpecial ricompensa, Partita partita) throws CdQException, RemoteException;
}
