package it.polimi.ingsw.cg13.punteggio;

import java.io.Serializable;

import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class Punteggio implements Serializable{

	/**
	 *  id della classe per la serializzazione
	 */
	private static final long serialVersionUID = -8607031030829405248L;
	
	/**
	 * giocatore di cui voglio conoscere il punteggio
	 */
	private Giocatore giocatore;
	
	/**
	 * punteggio calcolato del giocatore
	 */
	private int punteggio;
	
	/**
	 * Costruttore della classe Punteggio
	 * Dato un giocatore, ne instanzia la classe Punteggio calcolando i punti
	 * in base ai puntiVittoria,alle tessereBonus e se non ha più empori
	 * @param gioc: giocatore di cui si vuole calcolare il punteggio
	 * @throws IllegalArgumentException se il giocatore passato è nullo
	 */
	public Punteggio(Giocatore gioc){
		if(gioc==null)
			throw new IllegalArgumentException("Non puoi passare un giocatore nullo per il calcolo del punteggio");
		this.giocatore=gioc;
		setPunteggio(gioc.getVittoriaGiocatore());
		this.assegnaPunti();
	}
	
	/**
	 * Assegna al punteggio i puntiVittoria delle tessereBonus  del giocatore
	 * e aggiunge tre punti se il giocatore ha usato tutti gli empori
	 */
	public void assegnaPunti(){
		this.giocatore.getTessereBonus().forEach(tessera->this.addPunti(tessera.getPunteggioCaselleVittoria()));
		if(giocatore.getEmpori().isEmpty())
			this.addPunti(3);
	}
	
	/**
	 * Aggiunge i punti all'attributo punteggio
	 * @param punti: punti da aggiungere al punteggio
	 * @throws IllegalArgumentException: se si cerca di aggiungere dei punti negativi
	 */
	public void addPunti(int punti){
		if(punti<0)
			throw new IllegalArgumentException("Non puoi aggiungere dei punti negativi");
		this.setPunteggio(this.getPunteggio() + punti);
	}

	/**
	 *	Ritorna il punteggio calcolato del Giocatore
	 * @return il punteggio del giocatore
	 */
	public int getPunteggio() {
		return punteggio;
	}

	/**
	 * Assegna i punti al giocatore
	 * @param punteggio: punti che vogliamo assegnare al giocatore
	 * @throws IllegalArgumentException: non puoi assegnare un punteggio negativo
	 */
	public void setPunteggio(int punteggio) {
		if(punteggio<0)
			throw new IllegalArgumentException("Non puoi assegnare un punteggio negativo");
		this.punteggio = punteggio;
	}
	
	/**
	 * Ritorna il giocatore della classe
	 * @return giocatore di questa classe
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}
	
	/**
	 * Calcola la somma del numero degli aiutanti e il numero della cartepolitiche
	 * in posseso al giocatore alla fine della partita
	 * @return numeroAiutanti+numeroCartePolitiche
	 */
	public int aiutantiECarte(){
		return this.giocatore.getAiutanti().size()+this.giocatore.getCartePolitiche().size();
	}

	/**
	 * trasforma la classe in stringa
	 * @return: stringa della classe Punteggio
	 */
	@Override
	public String toString() {
		return "Giocatore: " + giocatore.getNome() + ", punteggio: " + punteggio;
	}

	/**
	 * calcola la hashCode di questa istanza di punteggio
	 * @return hashCode della classe
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((giocatore == null) ? 0 : giocatore.hashCode());
		result = prime * result + punteggio;
		return result;
	}

	/**
	 * Effettua la equals dell'istanza di questa classe con l'Object obj
	 * return true se obj e' uguale all'istanza di questa classe, altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punteggio other = (Punteggio) obj;
		if (giocatore == null) {
			if (other.giocatore != null)
				return false;
		} else if (!giocatore.equals(other.giocatore))
			return false;
		if (punteggio != other.punteggio)
			return false;
		return true;
	}

}
