package it.polimi.ingsw.cg13.punteggio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CalcolatorePunteggio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -463519195481881665L;
	private List<Punteggio> punteggi;
	private Giocatore vincitore;
	
	public CalcolatorePunteggio() {
		punteggi=new ArrayList<>();
	}
	
	public void calcola(Partita partita){
		
		List<Giocatore> giocatori=new ArrayList<>();
		giocatori.addAll(partita.getGiocatori());
		giocatori.addAll(partita.getGiocatoriOffline());
		this.calcolaPunteggioBase(giocatori);
		
		Iterator<CasellaNobilta> iterator=partita.getPercorsoNobilta().getPercorso().stream()
		.filter(casella->!casella.getGiocatoriPresenti().isEmpty())
		.sorted((c1,c2)-> -Integer.compare(c1.getPuntiCasella(),c2.getPuntiCasella())).iterator();
		
		if(iterator.hasNext()){
			CasellaNobilta casella=iterator.next();
			casella.getGiocatoriPresenti().forEach(gioc-> 
				punteggi.stream().filter(punt->punt.getGiocatore().equals(gioc)).
				forEach(punt->punt.addPunti(5)));
			if(casella.getGiocatoriPresenti().size()<2 && iterator.hasNext()){
				casella=iterator.next();
				casella.getGiocatoriPresenti().forEach(gioc-> 
				punteggi.stream().filter(punt->punt.getGiocatore().equals(gioc)).
				forEach(punt->punt.addPunti(2)));
			}
		}
		
		giocatori.stream().max((g1,g2)->
			Integer.compare(g1.getCartePermessoUsate().size()+g1.getPermessiCostruzione().size(),
					g2.getCartePermessoUsate().size()+g2.getPermessiCostruzione().size())).
						ifPresent(giocatore->punteggi.stream().filter(punt->punt.getGiocatore().equals(giocatore))
								.forEach(punt->punt.addPunti(3)));
		
		Iterator<Punteggio> iteratorPunti=punteggi.stream().sorted((p1,p2)-> -Integer.compare(p1.getPunteggio(), p2.getPunteggio())).iterator();
		List<Punteggio> classifica=new ArrayList<>();
		while(iteratorPunti.hasNext())
			classifica.add(iteratorPunti.next());
		Iterator<Punteggio> iterClassifica=classifica.iterator();
		
		if(iterClassifica.hasNext()){
			Punteggio max=iterClassifica.next();
			while(iterClassifica.hasNext()){
				Punteggio punti=iterClassifica.next();
				if(max.getPunteggio()<=punti.getPunteggio())
					if(max.aiutantiECarte()<punti.aiutantiECarte())
						max=punti;
			}
			vincitore=max.getGiocatore();
			classifica.remove(max);
			classifica.add(0, max);
			punteggi=classifica;
		}
		
		
	}

	public void calcolaPunteggioBase(List<Giocatore> giocatori){		
		giocatori.forEach(gioc-> punteggi.add(new Punteggio(gioc)));
	}
	
	@Override
	public String toString() {
		String punteggio= "Calcolo Punteggio in corso...\n"+
				"Vincitore: "+vincitore.getNome()+"\n"+
				"Classifica:\n";
		int i=1;
		for(Punteggio punti:this.punteggi)
			punteggio+=i+". "+punti.toString()+"\n";
		return punteggio;
	}

	public List<Punteggio> getPunteggi() {
		return punteggi;
	}

	public Giocatore getVincitore() {
		return vincitore;
	}

}
