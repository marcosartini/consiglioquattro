/**
 * 
 */
package it.polimi.ingsw.cg13.mappa;

import java.io.Serializable;

import java.util.Set;


import it.polimi.ingsw.cg13.bonus.OggettoBonusInput;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.mutatio.CittaMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class Citta extends ObservableC<Mutatio> implements Serializable,OggettoBonusInput{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6659386329139327899L;
	private final Colore colore;
	private final String nome;
	private Set<Ricompensa> ricompense;
	private Set<Emporio> empori;	
	
	/**
	 * @param colore
	 * @param nome
	 * @param ricompense
	 * @param empori
	 * 
	 */
	public Citta(Colore colore, String nome, Set<Ricompensa> ricompense, Set<Emporio> empori) {
		super();
		this.colore = colore;
		this.nome = nome;
		this.ricompense = ricompense;
		this.empori=empori;

	}
	
	/**
	 * Restituisce il nome della città
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Citta other = (Citta) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	/**
	 * Getter di empori
	 * @return HashSet di empori : insieme degli empori nella citta
	 */
	public Set<Emporio> getEmpori() {
		return empori;
	}

	/**
	 * Aggiunge un emporio nella citta
	 * @param emporioNuovo
	 * @throws IllegalArgumentException--> emporio passato nullo
	 */

	public void costruisciEmporio(Emporio emporioNuovo) {
		if(emporioNuovo!=null){
			this.empori.add(emporioNuovo);
			emporioNuovo.setCittaPosizionato(this);
			this.notifyObservers(new CittaMutatio(this,emporioNuovo));
		}
		else throw new IllegalArgumentException("Ho bisogno un nuovo emporio da costruire!");
	}
	
	/**
	 * 
	 * @param giocatore
	 * @return true:se il giocatore ha un emporio in questa citta , false: se non ne ha o ha passato un valore nullo
	 */
	public boolean hasEmporio(Giocatore giocatore){	
		if(giocatore!=null)
			return this.empori.stream().filter(e -> e.getGiocatoreAppartenente().equals(giocatore)).findAny().isPresent();
		else
			return false;

	}
	
	/**
	 * Getter di colore
	 * @return il colore della citta'
	 */
	public Colore getColor() {
		return this.colore;
	}

	@Override
	public String toString() {
		return "Citta nome="+nome+", " + colore +
				", ricompense=" + ricompense + ",\nempori=" + empori;
	}
	
	/**
	 * restituisce il set di ricompense(Gettone) assegnato alla città
	 * @return ricompense--> SET
	 */
	public Set<Ricompensa> getRicompense() {
		return ricompense;
	}
	
	/**
	 * Assegnaa alla citta' il gettone contenente le ricompense
	 * @param ricompense
	 * @throws IllegalArgumentException--> passato valore NULL
	 */
	public void setRicompense(Set<Ricompensa> ricompense) {
		if(ricompense!=null)
			this.ricompense = ricompense;
		else throw new IllegalArgumentException("Le ricompense passate non possono essere NULL!");
	}


	@Override
	public void assegnaBonusInput(Giocatore giocatore, Partita partita) {
		this.ricompense.forEach(ricompensa -> ricompensa.assegnaRicompensa(giocatore, partita));
		
	}

	/*
	 * Non serve, la update del oggetto basta per verificare se l'oggetto passat va bene come parametro
	@Override
	public boolean acceptParametroCorretto(RicompensaSpecial ricompensa,Giocatore giocatore,Partita partita) throws CdQException {
		return ricompensa.visitOggettoBonusInput(this,giocatore,partita);
	}*/
	
}
