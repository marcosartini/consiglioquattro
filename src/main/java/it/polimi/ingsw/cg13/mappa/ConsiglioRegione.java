/**
 * 
 */
package it.polimi.ingsw.cg13.mappa;

import java.util.Queue;

import it.polimi.ingsw.cg13.mutatio.ConsiglioMutatio;

import it.polimi.ingsw.cg13.personaggi.Consigliere;

/**
 * @author lorenzo
 *
 */
public class ConsiglioRegione extends Consiglio{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8783774233097218794L;

	/**
	 * Costruttore del Consiglio
	 * @param consiglieri
	 * @param prop
	 */
	public ConsiglioRegione(Queue<Consigliere> consiglieri,ProprietarioConsiglio prop){
		super(consiglieri,prop);
	}

	@Override
	public String toString() {
		return "ConsiglioRegione={" + super.toString() + "}";
	}
	
	@Override
	/**
	 * sostituisce il consigliere e restituisce quello sostituito--> notifica l' osservatore del cambiamento
	 * @param consigliere--> nuovo consigliere da sostituire
	 * @return the sost 
	 * @throws IllegalArgumentException--> valore passato nullo
	 */	
	public Consigliere sostituzioneConsigliere(Consigliere consigliere) {
		Consigliere sost=super.sostituzioneConsigliere(consigliere);
		this.notifyObservers(new ConsiglioMutatio(this,consigliere));
		return sost;
	}
	
}
