/**
 * 
 */
package it.polimi.ingsw.cg13.mappa;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.file.FactoryOggetti;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.observable.ObserverC;


/**
 * @author Marco
 *
 */

public class Mappa extends ObservableC<Mutatio> implements Serializable, ObserverC<Mutatio> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1462667450303788437L;
	private final List<NodoCitta> grafo;
	private final Set<Regione> regioni;
	

	/**
	 * Costruttore
	 * @param grafo {@link List} di NodiCitta
	 * @param regioni {@link Set} di Regioni
	 */
	public Mappa(List<NodoCitta> grafo, Set<Regione> regioni) {
		this.grafo = grafo;
		this.regioni=regioni;
		this.registraCitta();
	}
	
	/**
	 * Costruttore con oggetto Factory
	 * @param factoryOggetti
	 */
	public Mappa(FactoryOggetti factoryOggetti) {
		this.grafo=factoryOggetti.getCittaMappa();
		this.regioni=factoryOggetti.getRegioni();
		this.registraCitta();
	}
	
	/**
	 * Costruttore che genera una copia distinta dell'oggetto ricevuto
	 * @param mappa not null
	 */
	public Mappa (Mappa mappa){
		this.grafo=mappa.grafo;
		this.regioni=mappa.regioni;
		this.registraCitta();
	}

	/**
	 * Restituisce la lista di nodi citta contenuti nella mappa--> GRAFO
	 * @return the grafo
	 */
	public List<NodoCitta> getGrafo() {
		return grafo;
	}

	/**
	 * Restituisce il set di regioni contenute nella mappa
	 * @return the regioni
	 */
	public Set<Regione> getRegioni() {
		return regioni;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grafo == null) ? 0 : grafo.hashCode());
		result = prime * result + ((regioni == null) ? 0 : regioni.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mappa other = (Mappa) obj;
		if (grafo == null) {
			if (other.grafo != null)
				return false;
		} else if (!grafo.equals(other.grafo))
			return false;
		if (regioni == null) {
			if (other.regioni != null)
				return false;
		} else if (!regioni.equals(other.regioni))
			return false;
		return true;
	}
	
	/**
	 * Restituisce il nodoCitta cercato passando come parametro la Città corrispondente
	 * @param citta
	 * @return NodoCitta cercata
	 * @throws CittaNotFoundException
	 */
	public NodoCitta getNodoCitta(Citta citta) throws CittaNotFoundException{
		return this.grafo.stream().filter(city -> city.getCittaNodo().equals(citta)).
				findFirst().orElseThrow(() -> new CittaNotFoundException("Non esiste nessun nodo che contenga la citta' ricevuta"));
	}
	
	/**
	 * Restituisce il nodoCitta cercato passando come parametro il nome della citta corrispondente
	 * @param nomeCitta
	 * @return NodoCitta cercata
	 * @throws CittaNotFoundException
	 */
	public NodoCitta getNodoCitta(String nomeCitta) throws CittaNotFoundException {
		return this.grafo.stream().
				filter(n -> n.getCittaNodo().getNome().equals(nomeCitta)).
				findFirst().orElseThrow(() -> new CittaNotFoundException("La citta' "+ nomeCitta +" non esiste"));
		
	}
	
	/**
	 * Restituisce la Regione cercata passando come parametro il nome della regione 
	 * @param nomeRegione
	 * @return Regione cercata
	 * @throws RegioneNotFoundException
	 */
	public Regione getRegione(String nomeRegione) throws RegioneNotFoundException{
		return this.regioni.stream().filter(r -> r.getNome().equals(nomeRegione)).
				findFirst().orElseThrow(() -> new RegioneNotFoundException("La regione "+ nomeRegione +"non esiste"));
	}
	
	/**
	 * Restituisce la Regione cercata passando come parametro la Regione 
	 * @param regione
	 * @return Regione cercata
	 * @throws RegioneNotFoundException
	 */
	public Regione getRegione(Regione regione) throws RegioneNotFoundException{
		return this.regioni.stream().filter(r -> r.equals(regione)).
				findFirst().orElseThrow(() -> new RegioneNotFoundException("La regione "+ regione.getNome() +"non esiste"));
	}
	
	@Override
	public String toString() {
		String stringa = "";
		
		stringa+="\n\nRegioni:\n\n";
		for(Regione regione:this.regioni){
			stringa+=regione.toString()+"\n\n";
		}
		stringa+="\nMappa con collegamenti tra citta:\n\n";
		for(NodoCitta citta:this.grafo){
			stringa+=citta.toString()+"\n\n";
		}
		
		return stringa;
	}


	@Override
	public void update(Mutatio change) {
		this.notifyObservers(change);
	}
		
	/**
	 * Permette di registrare gli osservatori di tutte le citta del grafo contenuto nella mappa
	 */
	public void registraCitta(){
		this.grafo.forEach(nodo->nodo.getCittaNodo().registerObserver(this));
	}
	
}
