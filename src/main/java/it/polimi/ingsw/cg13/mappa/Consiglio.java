package it.polimi.ingsw.cg13.mappa;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Queue;

import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.personaggi.Consigliere;


public class Consiglio extends ObservableC<Mutatio> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6568193290159519523L;
	private Queue<Consigliere> consiglieri;
	private ProprietarioConsiglio proprietario;
	
	/**
	 * Costruttore del Consiglio
	 * @param consiglieri
	 * @param prop
	 */
	public Consiglio(Queue<Consigliere> consiglieri,ProprietarioConsiglio prop){
		
		this.consiglieri=consiglieri;
		this.proprietario=prop;
	}
	
	/**
	 * sostituisce il consigliere e restituisce quello sostituito
	 * @param consigliere--> nuovo consigliere da sostituire
	 * @return the sost
	 * @throws IllegalArgumentException--> valore passato nullo
	 */	
	public Consigliere sostituzioneConsigliere(Consigliere consigliere){
		if(consigliere==null)
			throw new IllegalArgumentException("Il consigliere da sosittuire non può essere nullo!");
		Consigliere sost=null;
		if(consiglieri.size()==4)// perche' questo controllo?
		{
			sost=consiglieri.poll();
			consiglieri.add(consigliere);
		}
		return sost;
	}
	
	/**
	 * Restituisce la coda di consiglieri presenti nel consiglio
	 * @return consiglieri
	 */
	public Queue<Consigliere> getBalcone(){
		return this.consiglieri;
	}
	
	/**
	 * Fa una copia del consiglio e la restituisce
	 * @return new Consiglio
	 */
	public Consiglio copiaConsiglio(){
		Queue<Consigliere> consiglio=new ArrayDeque<>();
		for(Consigliere cons:this.consiglieri){
			consiglio.add(cons);
		}
		return new Consiglio(consiglio,this.proprietario);
	}

	@Override
	public String toString() {
		return "(" + consiglieri + ")";
	}

	
	
	/**
	 * Restituisce il proprietario del consiglio
	 * @return proprietario
	 */
	public ProprietarioConsiglio getProprietario() {
		return proprietario;
	}

	/**
	 * Setta il proprietario del consiglio
	 */
	public void setProprietario(ProprietarioConsiglio proprietario) {
		this.proprietario = proprietario;
	}

	/**
	 * Setta i consiglieri del consiglio
	 * @param consiglieri
	 * @throws IllegalArgumentException--> passato valore nullo
	 */
	public void setConsiglieri(Queue<Consigliere> consiglieri) {
		if(consiglieri!=null)
			this.consiglieri = consiglieri;
		else throw new IllegalArgumentException("Il valore passato non può essere nullo!");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((consiglieri == null) ? 0 : consiglieri.hashCode());
		result = prime * result + ((proprietario == null) ? 0 : proprietario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Consiglio other = (Consiglio) obj;
		if (consiglieri == null) {
			if (other.consiglieri != null)
				return false;
		} else if (!consiglieri.equals(other.consiglieri))
			return false;
		if (proprietario == null) {
			if (other.proprietario != null)
				return false;
		} else if (!proprietario.equals(other.proprietario))
			return false;
		return true;
	}



}
