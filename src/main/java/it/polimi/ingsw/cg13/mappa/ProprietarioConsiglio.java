package it.polimi.ingsw.cg13.mappa;

import java.io.Serializable;

public interface ProprietarioConsiglio extends Serializable{

	public Consiglio getConsiglio();
	public String getNome();
}
