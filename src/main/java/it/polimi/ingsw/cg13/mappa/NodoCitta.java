package it.polimi.ingsw.cg13.mappa;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;


import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.eccezioni.IllegalPercorsoException;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class NodoCitta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4796732590836986634L;
	private Citta cittaNodo;
	private Set<NodoCitta> cittaVicine;
	
	
	/**
	 * Costruttore con solo la citta del nodo corrente
	 * @param citta : citta del nodo Corrente
	 */
	public NodoCitta(Citta citta) {
		this.cittaNodo=citta;
		this.cittaVicine=new HashSet<>();
	}
	
	
	/**
	 * Costruttore con citta corrente e suoi vicini
	 * @param citta del nodo corrente
	 * @param vicini : insieme dei vicini della citta crrente
	 */
	public NodoCitta(Citta citta,Set<NodoCitta> vicini){
		this.cittaNodo=citta;
		this.cittaVicine=vicini;
	}

	
	/**
	 * Aggiunge nuovoVicini all'insieme delle cittaVicine
	 * @param nuovoVicino 
	 * @throws IllegalArgumentException--> valore nuovoVicino nullo
	 */
	public void addVicino(NodoCitta nuovoVicino){
		if(nuovoVicino!=null)
			cittaVicine.add(nuovoVicino);
		else throw new IllegalArgumentException("il nodo passato non puo essere nullo!");
	}
	
	
	/**
	 * Restituisce la città a cui quel nodo fa riferimento
	 * @return la citta corrente del nodo
	 */
	public Citta getCittaNodo() {
		return cittaNodo;
	}

	
	/**
	 * Setter della citta corrente del nodo
	 * @param cittaAttuale
	 * @throws IllegalArgumentException--> valore cittaAttuale nullo
	 */
	public void setCittaNodo(Citta cittaAttuale) {
		if(cittaAttuale!=null)
			this.cittaNodo = cittaAttuale;
		else throw new IllegalArgumentException("la cittaAttuale non puo essere nulla!");
	}
	
	
	/**
	 * 
	 * @param vicino
	 * @return true se il vicino appartiene all'insieme delle cittaVicine,false se non lo è o è stato passato NULL
	 */
	public boolean isCittaVicina(NodoCitta vicino){
		if(vicino==null)
			return false;
		else
			return this.cittaVicine.contains(vicino);
	}
	
	
	/**
	 * Valuta se un percorso di citta e' corretto con partenza la citta di questo oggetto
	 * @param cittaDaAttraversare : le citta da attraversare compreso l'arrivo
	 * @return true se il percorso di citta e' corretto
	 */
	public boolean percorsoCorretto(List<NodoCitta> cittaDaAttraversare){
		if(cittaDaAttraversare==null){
			return false;
		}
		else{
			NodoCitta partenza=this;
			for(NodoCitta vicino:cittaDaAttraversare){
				if(!partenza.isCittaVicina(vicino)){
					return false;
				}
				else{
					partenza=vicino;
				}
			}
		return true;
		}
	}
	
	
	/**
	 * 
	 * @param costoStrada : costo per passare da una citta all'altra
	 * @param cittaDaAttraversare : percorso delle citta da attraversare
	 * @return costo totale del percorso attraversato
	 * @throws IllegalPercorsoException 
	 */
	public int costoPercorso(int costoStrada,List<NodoCitta> cittaDaAttraversare) throws IllegalPercorsoException{
		if(percorsoCorretto(cittaDaAttraversare)){
			return costoStrada*cittaDaAttraversare.size();
		}
		else{
			throw new IllegalPercorsoException("Percorso selezionato Errato, non e' possibile calcolare il costo");
		}
	}


	@Override
	public String toString() {
		String nodo= "" + cittaNodo + ",\n";
		nodo+="CittaVicine=";
		for(NodoCitta citta:this.cittaVicine){

			nodo+=citta.cittaNodo.getNome()+", ";

		}
		return nodo;
	}


	/**
	 * Restituisce le citta vicine rispetto a questa
	 * @return
	 */
	public Set<NodoCitta> getCittaVicine() {
		return cittaVicine;
	}
	
	/**
	 * Restituisce le ricompense da assegnare al giocatore passato quando questo ha un emporio sulla cittào quelle vicine
	 * @param giocatore
	 * @return ricompense
	 */
	public  List<Ricompensa> bfsRicompense(Giocatore giocatore){
		//visita in ampiezza del grafo
		List<Ricompensa> ricompense= new LinkedList<>();
		Queue<NodoCitta> q=new LinkedList<>();
		Set<NodoCitta> nodiVisitati= new HashSet<>();
		q.add(this);
		nodiVisitati.add(this);
		while(!q.isEmpty()){
			NodoCitta n = q.remove();
			nodiVisitati.add(n);
			if(n.getCittaNodo().hasEmporio(giocatore)){
				ricompense.addAll(n.getCittaNodo().getRicompense());
				for(NodoCitta vicino : n.getCittaVicine()){
					if(!nodiVisitati.contains(vicino))
						q.add(vicino);
				}
			}
		}
		return ricompense;
	}
	
	//@requires giaVisitate!=null && giaVisitate.get(0).equals(this) && giaVisitate.size()==1 && ricompense!=null
	public List<Ricompensa> bfsRicompensa(Giocatore giocatore, List<NodoCitta> giaVisitate){
		Iterator<NodoCitta> cittaAssegnabili=this.getCittaVicine().stream().
				filter(citta -> citta.getCittaNodo().hasEmporio(giocatore) && !giaVisitate.contains(citta)).iterator();
		NodoCitta nodo=null;
		List<Ricompensa> ricompense=new ArrayList<>();
		while(cittaAssegnabili.hasNext()){
			nodo=cittaAssegnabili.next();
			giaVisitate.add(nodo);	
			ricompense.addAll(nodo.getCittaNodo().getRicompense());
			ricompense.addAll(this.bfsRicompensa(giocatore, giaVisitate));	
		}
		return ricompense;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cittaNodo == null) ? 0 : cittaNodo.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodoCitta other = (NodoCitta) obj;
		if (cittaNodo == null) {
			if (other.cittaNodo != null)
				return false;
		} else if (!cittaNodo.equals(other.cittaNodo))
			return false;
		return true;
	}
	


}
