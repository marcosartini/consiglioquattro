package it.polimi.ingsw.cg13.mappa;


import java.io.Serializable;
import java.util.List;
import java.util.Set;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;

public class Regione implements Serializable, ProprietarioConsiglio{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8926184432600718803L;
	private final String nomeRegione;
	private Set<Citta> cittaAppartenenti;
	private ConsiglioRegione consiglio;
	private MazzoPermessi mazzoPermesso;
	
	public Regione(String nome, Set<Citta> cittaApp, ConsiglioRegione consiglio, MazzoPermessi mazzoPermesso){
		this.nomeRegione=nome;
		this.cittaAppartenenti=cittaApp;
		this.setConsiglio(consiglio);
		this.setMazzoPermesso(mazzoPermesso);
		
	}
	
	/*
	 * per tenere traccia delle città completate e lanciare il giusto evento, si potrebbe
	 * anche creare un hashmap con i giorcaori, con un contatore di città in cui costruire
	 * ogni città con emporio lo decrementa, e se a 0 si lancia assegnamento bonus
	 * 
	 * invece di creare l'hashmap con le città e il set di giocatori, che bisogna verificare 
	 * se i giocatori sono presenti in tutte le righe
	 */
	
		
	/**
	 * Genera una copia distinta dell'oggetto ricevuto
	 * @param regione
	 */
	public Regione(Regione regione) {
		this.nomeRegione=regione.nomeRegione;
		this.cittaAppartenenti=regione.cittaAppartenenti;
		this.consiglio=regione.consiglio;
		this.mazzoPermesso=regione.mazzoPermesso;
	}
	/**
	 * Restituisce il set di città presenti nella regione
	 * @return cittaAppartenenti
	 */
	public Set<Citta> getCitta(){
		return cittaAppartenenti;
	}

	@Override
	/**
	 * Restituisce il nome della regione
	 * @return nomeRegione
	 */
	public String getNome() {
		return nomeRegione;
	}

	@Override
	/**
	 * Restituisce il consiglio della regione
	 * @return consiglio
	 */
	public ConsiglioRegione getConsiglio() {
		return consiglio;
	}

	/**
	 * Permette di settare il consiglio della regione passandogli il parametro
	 * @param consiglio
	 */
	public void setConsiglio(ConsiglioRegione consiglio) {
		this.consiglio = consiglio;
	}
	
	/**
	 * Restituisce il mazzoPermesso della regione
	 * @return mazzoPermesso
	 */
	public MazzoPermessi getMazzoPermesso() {
		return mazzoPermesso;
	}

	/**
	 * Permette di settare il mazzoPermesso della regione passandogli il parametro
	 * @param mazzoPermesso
	 */
	public void setMazzoPermesso(MazzoPermessi mazzoPermesso) {
		this.mazzoPermesso = mazzoPermesso;
	}
	
	/**
	 * Restituisce la stringa di permessi disponibili della regione
	 * @param numeroPermessi da visualizzare e che possono essere scelti
	 * @return stringa dei permessi disponibili
	 */
	public String getStringPermessiDisponibili(int numeroPermessi){
		String stringaPermessi="";
		List<PermessoCostruzione> permessiInCima=this.mazzoPermesso.getCarteInCima(numeroPermessi);
		for(int i=0;i<numeroPermessi;i++){
			stringaPermessi+=i+": "+permessiInCima.get(i).toString()+"\n";
		}
		return stringaPermessi;
	}

	@Override
	public String toString() {
		String regione="Regione nome: " + nomeRegione +"\n"+consiglio + "\n" + mazzoPermesso +"\n";
		regione+="CittaAppartenenti:\n";
		for(Citta citta:this.cittaAppartenenti){
			regione+=citta.getNome()+", ";
		}
		return regione;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cittaAppartenenti == null) ? 0 : cittaAppartenenti.hashCode());
		result = prime * result + ((nomeRegione == null) ? 0 : nomeRegione.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Regione other = (Regione) obj;
		if (cittaAppartenenti == null) {
			if (other.cittaAppartenenti != null)
				return false;
		} else if (!cittaAppartenenti.equals(other.cittaAppartenenti))
			return false;
		if (nomeRegione == null) {
			if (other.nomeRegione != null)
				return false;
		} else if (!nomeRegione.equals(other.nomeRegione))
			return false;
		return true;
	}

	
	

}
