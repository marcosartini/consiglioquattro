package it.polimi.ingsw.cg13.gui;
import java.net.URL;
import java.util.ResourceBundle;

import it.polimi.ingsw.cg13.client.InviaParametriPartita;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


public class ParametriController{
	

	private Stage parametriStage;
	private ClientGUIController guiController;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ImageView anteprimaCollina;

    @FXML
    private ImageView anteprimaMare;

    @FXML
    private ImageView anteprimaMontagna;

    @FXML
    private ToggleGroup collinaGroup;

    @FXML
    private ToggleGroup mareGroup;

    @FXML
    private ToggleGroup montagnaGroup;

    @FXML
    private RadioButton collina1;

    @FXML
    private RadioButton collina2;

    @FXML
    private RadioButton mare1;

    @FXML
    private RadioButton mare2;


    @FXML
    private RadioButton montagna1;

    @FXML
    private RadioButton montagna2;

    @FXML
    private TextField numGiocatori;

    @FXML
    private TextField timeout;

    @FXML
    void settaAnteprimaCollina() {
    	this.anteprimaCollina.setImage(new Image(getClass().getResource("/gui/image/collina"+(String)collinaGroup.getSelectedToggle().getUserData()+".png").toString()));
    }

    @FXML
    void settaAnteprimaMare() {
    	this.anteprimaMare.setImage(new Image(getClass().getResource("/gui/image/mare"+(String)mareGroup.getSelectedToggle().getUserData()+".png").toString()));
    }

    @FXML
    void settaAnteprimaMontagna() {
    	this.anteprimaMontagna.setImage(new Image(getClass().getResource("/gui/image/montagna"+(String)montagnaGroup.getSelectedToggle().getUserData()+".png").toString()));
    }

    @FXML
    void initialize() {
        assert anteprimaCollina != null : "fx:id=\"anteprimaCollina\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert anteprimaMare != null : "fx:id=\"anteprimaMare\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert anteprimaMontagna != null : "fx:id=\"anteprimaMontagna\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert collinaGroup != null : "fx:id=\"collinaGroup\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert mareGroup != null : "fx:id=\"mareGroup\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert montagnaGroup != null : "fx:id=\"montagnaGroup\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert collina1 != null : "fx:id=\"collina1\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert collina2 != null : "fx:id=\"collina2\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert mare1 != null : "fx:id=\"mare1\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert mare2 != null : "fx:id=\"mare2\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert montagna1 != null : "fx:id=\"montagna1\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        assert montagna2 != null : "fx:id=\"montagna2\" was not injected: check your FXML file 'selezioneParametriLayout.fxml'.";
        
        mare1.setUserData("0");
        mare2.setUserData("1");
        collina1.setUserData("0");
        collina2.setUserData("1");
        montagna1.setUserData("0");
        montagna2.setUserData("1");
        


    }
    
	public void setParametriStage(Stage parametriStage) {
		this.parametriStage = parametriStage;
	}
	
	public void setGuiController(ClientGUIController guiController) {
		this.guiController = guiController;
	}

	
	private String getMappaId() {
		String binario = (String)this.mareGroup.getSelectedToggle().getUserData() + (String)this.collinaGroup.getSelectedToggle().getUserData() +(String)this.montagnaGroup.getSelectedToggle().getUserData();
		return String.valueOf(Integer.parseInt(binario, 2));
	}
	
	private int getNumeroGiocatori(){
		return Integer.parseInt(this.numGiocatori.getText());
	}
	
	private int getTimeoutSecondi(){
		return Integer.parseInt(this.timeout.getText());
	}
	
	@FXML
	public void inviaParametri(){
        this.guiController.notifyObservers(new InviaParametriPartita(getNumeroGiocatori(), getTimeoutSecondi(), getMappaId()));
        this.parametriStage.close();
	}
}
