package it.polimi.ingsw.cg13.gui;

import it.polimi.ingsw.cg13.personaggi.Consigliere;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;

public class ConsigliereToggleButton extends ToggleButton implements Parametro{

	CDQToggleButtonSkin skin = new CDQToggleButtonSkin(this);
	private Consigliere consigliere;
	
	public ConsigliereToggleButton(int quantitaResidua, Consigliere consigliere) {
		super(String.valueOf(quantitaResidua));
		ImageView sfondo = new ImageView(ConsigliereToggleButton.class.getResource("/gui/image/consiglieri/consigliere_" + consigliere.getColor().getNome() + ".png").toString());
		sfondo.setScaleX(0.5);
		sfondo.setScaleY(0.5);
		this.setGraphic(sfondo);
		this.setGraphicTextGap(3);
		this.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		this.setMinSize(0,0);
		this.consigliere = consigliere;
		this.setContentDisplay(ContentDisplay.BOTTOM);
	}
	
	public Consigliere getConsigliere() {
		return consigliere;
	}
}
