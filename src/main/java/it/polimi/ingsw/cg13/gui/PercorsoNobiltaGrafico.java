package it.polimi.ingsw.cg13.gui;

import java.util.LinkedList;
import java.util.List;

import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.PercorsoNobilta;

import javafx.geometry.Pos;
import javafx.scene.layout.HBox;

public class PercorsoNobiltaGrafico extends HBox {

	private PercorsoNobilta percorsoV;
	private List<CasellaGrafica<CasellaNobilta>> caselle;
	
	public PercorsoNobiltaGrafico(PercorsoNobilta percorso) {
		super(0);
		this.percorsoV = percorso;
		this.caselle = new LinkedList<>();
		this.setAlignment(Pos.TOP_LEFT);
		percorso.getPercorso().forEach(c -> caselle.add(new CasellaNobiltaGrafica(c)));
		
		this.getChildren().addAll(caselle);
	}
	
	public PercorsoNobilta getPercorsoV() {
		return percorsoV;
	}
	
	public List<CasellaGrafica<CasellaNobilta>> getCaselle() {
		return caselle;
	}
}
