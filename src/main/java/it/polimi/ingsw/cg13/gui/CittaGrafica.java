package it.polimi.ingsw.cg13.gui;

import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import javafx.fxml.FXML;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;



public class CittaGrafica extends ToggleButton implements Parametro{
	
	double dimensioneX; //dimensione
	double dimensioneY;
	
	private NodoCitta citta;

    @FXML
    private HBox empori;

    @FXML
    private GettoneGrafico gettone;
    
    @FXML
    private AnchorPane baseNodo;
    
    CDQToggleButtonSkin skin = new CDQToggleButtonSkin(this);
    
	public CittaGrafica(NodoCitta citta, double dimensioneX, double dimensioneY) {
		super();
		this.baseNodo = new AnchorPane();

		this.dimensioneX=dimensioneX;
		this.dimensioneY=dimensioneY;
		
		this.citta=citta;
		this.baseNodo.setPrefWidth(dimensioneX);
		this.baseNodo.setPrefHeight(dimensioneY);		
	
		empori = new HBox();
		AnchorPane.setBottomAnchor(empori, 0.0);
		empori.setLayoutX(10);
		gettone=new GettoneGrafico(citta.getCittaNodo().getRicompense(), dimensioneX*0.5);
		gettone.setLayoutX(0);
		gettone.setLayoutY(0);
		
        this.baseNodo.getChildren().addAll(gettone, empori);

       this.setGraphic(baseNodo);
	}
    
	public CittaGrafica( int layoutX, int layoutY, double dimensioneX, double dimensioneY, NodoCitta citta) {
		super();
		this.baseNodo = new AnchorPane();
		this.dimensioneX=dimensioneX;
		this.dimensioneY=dimensioneY;
		this.citta=citta;
		this.baseNodo.setPrefWidth(dimensioneX);
		this.baseNodo.setPrefHeight(dimensioneY);		
	
		empori = new HBox();
		AnchorPane.setBottomAnchor(empori, 0.0);
		empori.setLayoutX(dimensioneX*0.1);
		gettone=new GettoneGrafico(citta.getCittaNodo().getRicompense(), dimensioneX*0.5);

		gettone.setLayoutX(0);
		gettone.setLayoutY(0);
		
        this.baseNodo.getChildren().addAll(gettone, empori);

        this.setLayoutX(layoutX);
        this.setLayoutY(layoutY);
        
        this.setGraphic(baseNodo);
		this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
	}
	

	
	public void addEmporio(Emporio emporio){
		ImageView emporioG;
		try {
			emporioG = new ImageView(getClass().getResource("/gui/image/empori/emporio_"+emporio.getColore().getNome()+".png").toString());
		} catch (ColoreNotFoundException e) {
			throw new IllegalArgumentException(e);
		}
		emporioG.setFitHeight(dimensioneY*0.20);
		emporioG.setFitWidth(dimensioneX*0.30);
		this.empori.getChildren().add(emporioG);
	}
	
	public NodoCitta getCitta(){
		return this.citta;
	}
	
	public void addKing(KingGrafico king){
		this.baseNodo.getChildren().add(king);
		this.baseNodo.getChildren().get(this.baseNodo.getChildren().size()-1).setLayoutX(30);
		this.baseNodo.getChildren().get(this.baseNodo.getChildren().size()-1).setLayoutY(20);
	}
	
	public void setLayouts(int layoutX, int layoutY){
        this.setLayoutX(layoutX);
        this.setLayoutY(layoutY);
	}
	

	public double getDimX(){
		return this.dimensioneX;
	}
	
	public double getDimY(){
		return this.dimensioneY;
	}
	
	public void resizeX(double percentuale){
		double temp;
		this.dimensioneX=dimensioneX*percentuale;
		
		for(int i=0;i<this.baseNodo.getChildren().size();i++){
			temp=this.baseNodo.getChildren().get(i).getScaleX();
			this.baseNodo.getChildren().get(i).setScaleX(temp*percentuale);			
		}		
	}
	
	public void resizeY(double percentuale){
		double temp;
		this.dimensioneY=dimensioneY*percentuale;
		
		for(int i=0;i<this.baseNodo.getChildren().size();i++){
			temp=this.baseNodo.getChildren().get(i).getScaleY();
			this.baseNodo.getChildren().get(i).setScaleY(temp*percentuale);			
		}		
	}
}
