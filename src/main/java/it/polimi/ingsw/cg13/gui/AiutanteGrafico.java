package it.polimi.ingsw.cg13.gui;

import java.util.List;

import it.polimi.ingsw.cg13.personaggi.Aiutante;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;

public class AiutanteGrafico extends ToggleButton {

	CDQToggleButtonSkin skin = new CDQToggleButtonSkin(this);
	
	private List<Aiutante> aiutanti;

	public AiutanteGrafico(List<Aiutante> aiutanti) {
		super(Integer.toString(aiutanti.size()));
		this.aiutanti = aiutanti;
		ImageView sfondo = new ImageView(getClass().getResource("/gui/image/aiutante.png").toString());
		sfondo.setFitHeight(60);
		sfondo.setFitWidth(30);
		this.setContentDisplay(ContentDisplay.CENTER);
		this.setGraphic(sfondo);
	}
	
	
}