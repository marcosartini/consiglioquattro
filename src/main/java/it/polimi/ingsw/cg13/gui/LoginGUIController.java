package it.polimi.ingsw.cg13.gui;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;

import it.polimi.ingsw.cg13.client.ClientConnection;
import it.polimi.ingsw.cg13.client.ClientRMI;
import it.polimi.ingsw.cg13.client.ClientSocket;
import it.polimi.ingsw.cg13.comandi.Login;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

public class LoginGUIController {

	@FXML
	private Login login;
	
	@FXML
	private TextField nome;
	
	@FXML
	private TextField host;
	@FXML
	private TextField port;
	@FXML
	private Button submit;
	@FXML
	private ProgressIndicator progress;
	
	@FXML
	private RadioButton radioSocket;
	@FXML
	private RadioButton radioRmi;
	@FXML
	private ToggleGroup radioGroup;
	
	
	private MainGUI mainGui;
	private Stage loginStage;

	
	@FXML
	public void initialize(){
		radioGroup = new ToggleGroup();
		radioSocket.setToggleGroup(radioGroup);
		radioRmi.setToggleGroup(radioGroup);
		radioSocket.setSelected(true);
	}
	
	public void setMainGui(MainGUI mainGui) {
		this.mainGui = mainGui;
	}
	@FXML
	public void startConnection(){
		this.progress.setVisible(true);
		this.nome.setEditable(false);
		this.host.setEditable(false);
		this.port.setEditable(false);
		this.radioSocket.setDisable(true);
		this.radioRmi.setDisable(true);
		this.submit.setDisable(true);
		this.login = new Login(nome.getText());
		ClientConnection connessione;
		
		try {
			if(this.radioGroup.getSelectedToggle().equals(radioSocket)){
				connessione=new ClientSocket( host.getText(), port.getText(), login);

				connessione.startConnection(this.mainGui.getController());
				this.mainGui.getController().setLoginPlayer(login);
			}
		else if(this.radioGroup.getSelectedToggle().equals(radioRmi)){
				connessione=new ClientRMI(port.getText(), host.getText(), login);

				connessione.startConnection(this.mainGui.getController());
				this.mainGui.getController().setLoginPlayer(login);
			}
		} catch (IOException | NotBoundException | AlreadyBoundException e) {
			loginStage.close();
			this.mainGui.visualizzaErrore(e);
		}
		loginStage.close();
	}
	
	public void setLoginStage(Stage loginStage) {
		this.loginStage = loginStage;
	}
	
	
	public Login getLogin() {
		return login;
	}
}
