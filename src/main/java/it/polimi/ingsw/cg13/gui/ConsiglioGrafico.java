package it.polimi.ingsw.cg13.gui;



import java.util.ArrayList;
import java.util.List;


import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import javafx.animation.PathTransition;
import javafx.fxml.FXML;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.util.Duration;

public class ConsiglioGrafico extends ToggleButton implements Parametro{

	List<ImageView> imgCons;
	Consiglio consiglio;
	CDQToggleButtonSkin skin = new CDQToggleButtonSkin(this);

    @FXML
    private HBox consiglieri;
    
    @FXML
    private ImageView balcone;
    
    private AnchorPane baseNodo;
    private double width;
    
    public ConsiglioGrafico(Consiglio consiglio, double width, double layoutX, double layoutY) {
    	super();
    	this.baseNodo = new AnchorPane();
    	this.consiglio = consiglio;
    	imgCons = new ArrayList<>();
    	consiglio.getBalcone().forEach(c -> this.imgCons.add(0,
        		new ImageView(getClass().getResource("/gui/image/consiglieri/consigliere_"+c.getColor().getNome()+".png").toString())));
    	imgCons.forEach(img -> img.setFitHeight(width*0.5));
    	imgCons.forEach(img -> img.setFitWidth(width*0.2));
        consiglieri = new HBox();
        consiglieri.setSpacing(1);
        consiglieri.getChildren().addAll(imgCons);
      
        this.balcone=new ImageView(getClass().getResource("/gui/image/balcone3D.png").toString());
        this.balcone.setFitHeight(width*0.35);
        this.balcone.setFitWidth(width);
        AnchorPane.setLeftAnchor(consiglieri, width*0.05);
        AnchorPane.setBottomAnchor(balcone, 0.0);
        AnchorPane.setLeftAnchor(balcone, 0.0);
        AnchorPane.setRightAnchor(balcone, 0.0);
        
        this.baseNodo.getChildren().addAll(consiglieri,balcone);
        this.width = width;
        this.setLayoutX(layoutX);
        this.setLayoutY(layoutY);
   
        this.setGraphic(baseNodo);
		this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
	}
    
    public void sostituzioneConsigliere(Consigliere consigliere){
    	//aggiungere transizione carina
    	
    	Path path = new Path();
    	path.getElements().add(new MoveTo(20,20));
    	path.getElements().add(new CubicCurveTo(50, 30, 30, 70, 100, 220));

    	PathTransition pathTransition = new PathTransition();
    	pathTransition.setDuration(Duration.millis(2000));
    	pathTransition.setPath(path);
    	pathTransition.setNode(this.consiglieri.getChildren().get(this.consiglieri.getChildren().size()-1));
    	pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
    	pathTransition.setCycleCount(1);
    	pathTransition.setAutoReverse(false);
    	pathTransition.play();
    	pathTransition.setOnFinished(finishHim -> {
    		this.consiglieri.getChildren().remove(this.consiglieri.getChildren().size()-1);
          });
    	ImageView img = new ImageView(getClass().getResource("/gui/image/consiglieri/consigliere_"+consigliere.getColor().getNome()+".png").toString());
    	img.setFitHeight(width*0.5);
    	img.setFitWidth(width*0.2);
    	this.consiglieri.getChildren().add(0,img);
    }
    
    public Consiglio getConsiglio() {
		return consiglio;
	}   
   
	
	public void resizeX(double percentuale){
		double temp;		
		
		for(int i=0;i<this.baseNodo.getChildren().size();i++){
			temp=this.baseNodo.getChildren().get(i).getScaleX();
			temp=temp*percentuale;
			if(temp<1.2 && temp>0.8)
				this.baseNodo.getChildren().get(i).setScaleX(temp);			
		}		
	}
	
	public void resizeY(double percentuale){
		double temp;		
		
		for(int i=0;i<this.baseNodo.getChildren().size();i++){
			temp=this.baseNodo.getChildren().get(i).getScaleY();
			temp=temp*percentuale;
			if(temp<1.2 && temp>0.8)
				this.baseNodo.getChildren().get(i).setScaleY(temp);			
		}		
	}
    
}
