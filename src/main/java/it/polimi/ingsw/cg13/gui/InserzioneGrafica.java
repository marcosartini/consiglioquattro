package it.polimi.ingsw.cg13.gui;



import it.polimi.ingsw.cg13.oggetti.Inserzione;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;


public class InserzioneGrafica extends ToggleButton {
	
	CDQToggleButtonSkin consigliereSkin = new CDQToggleButtonSkin(this);
	private Inserzione inserzione;
	private ToggleButton vendibileButton;

	public InserzioneGrafica(Inserzione ins, ToggleButton vendibileButton) {
		super();
		Tooltip tip=new Tooltip("Prezzo oggetto:\n"+Integer.toString(ins.getPrezzo().getPuntiCasella())+ " monete");
		this.setTextFill(Color.WHITE);
		this.setText(Integer.toString(ins.getPrezzo().getPuntiCasella()));
		this.tooltipProperty().setValue(tip);
		this.inserzione=ins;
		this.vendibileButton=vendibileButton;
		this.setGraphic(vendibileButton.getGraphic());
		//this.getGraphic().setScaleX(0.5);
		//this.getGraphic().setScaleY(0.5);
		this.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		this.setMinSize(0,0);
		this.setPrefHeight(100);
		this.setPrefWidth(80);
	}

	public Inserzione getInserzione() {
		return inserzione;
	}

	public ToggleButton getVendibileButton() {
		return vendibileButton;
	}

}
