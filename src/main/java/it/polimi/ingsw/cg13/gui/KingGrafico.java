package it.polimi.ingsw.cg13.gui;

import it.polimi.ingsw.cg13.personaggi.King;
import javafx.animation.PathTransition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.util.Duration;

public class KingGrafico extends ImageView{

	private King king;
	
	public KingGrafico(King king, double height) {
		this.king = king;
		this.setImage(new Image(getClass().getResource("/gui/image/king.png").toString()));
		this.setFitWidth(height*0.5);
		this.setFitHeight(height);
	}
	
	public void spostaKing(CittaGrafica nuovaDestinazione){
		Path path = new Path();
    	path.getElements().add(new MoveTo(nuovaDestinazione.getLayoutX(),nuovaDestinazione.getLayoutY()));
    	//path.getElements().add(new CubicCurveTo(50, 30, 30, 70, 100, 220));

    	PathTransition pathTransition = new PathTransition();
    	pathTransition.setDuration(Duration.millis(2000));
    	pathTransition.setPath(path);
    	pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
    	pathTransition.setCycleCount(1);
    	pathTransition.setAutoReverse(false);
    	pathTransition.play();
    	/*this.setTranslateX(nuovaDestinazione.getLayoutX()-15);
    	this.setTranslateY(nuovaDestinazione.getLayoutY()+15);*/
    	/*pathTransition.setOnFinished(finishHim -> {
    		
          });*/
    	nuovaDestinazione.addKing(this);
	}
	
	public King getKing() {
		return king;
	}

	public void setKing(King king) {
		this.king = king;
	}
}
