package it.polimi.ingsw.cg13.gui;

import it.polimi.ingsw.cg13.bonus.Ricompensa;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class RicompensaGrafica extends StackPane {

	private Ricompensa ricompensa;
	private Text quantita;
	
	public RicompensaGrafica(Ricompensa ricompensa, double width, double height) {
		super();
		this.ricompensa = ricompensa;
		
		this.setStyle("-fx-background-image: url("+getClass().getResource("/gui/image/ricompense/"+this.ricompensa.getClass().getSimpleName()+".png").toString()+"); -fx-background-repeat: stretch; -fx-background-size: 100% 100%;");
		this.quantita = new Text(String.valueOf(this.ricompensa.getQuantita()));
		this.quantita.setFill(Color.WHITE);
		this.setAlignment(Pos.CENTER);
		this.getChildren().add(quantita);
		this.setPrefWidth(width);
		this.setPrefHeight(height);
	}

	public Ricompensa getRicompensa() {
		return ricompensa;
	}


	



}
