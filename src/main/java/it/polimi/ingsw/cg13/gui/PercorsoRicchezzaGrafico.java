package it.polimi.ingsw.cg13.gui;

import java.util.LinkedList;
import java.util.List;

import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;

public class PercorsoRicchezzaGrafico extends HBox {

	private PercorsoRicchezza percorsoV;
	private List<CasellaGrafica<CasellaRicchezza>> caselle;
	
	public PercorsoRicchezzaGrafico(PercorsoRicchezza percorso) {
		super(0);
		this.percorsoV = percorso;
		this.caselle = new LinkedList<>();
		this.setAlignment(Pos.TOP_LEFT);
		percorso.getPercorso().forEach(c -> caselle.add(new CasellaGrafica<CasellaRicchezza>(c, "Ricchezza")));
		
		this.getChildren().addAll(caselle);
	}
	
	public PercorsoRicchezza getPercorsoV() {
		return percorsoV;
	}
	
	public List<CasellaGrafica<CasellaRicchezza>> getCaselle() {
		return caselle;
	}
}
