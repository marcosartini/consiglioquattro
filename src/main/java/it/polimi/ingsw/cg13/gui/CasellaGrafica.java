package it.polimi.ingsw.cg13.gui;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import it.polimi.ingsw.cg13.percorsi.Casella;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import javafx.geometry.Pos;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class CasellaGrafica<C extends Casella> extends AnchorPane{

	protected static final Font FONT = Font.loadFont(CasellaGrafica.class.getResourceAsStream("/gui/font/Enchanted Land.otf"), 24);
	protected C casella;
	protected VBox baseNodo;
	protected HBox ricompense;
	protected List<PedinaGrafica> pedine;
	protected HBox pedineHBox;
	
	public CasellaGrafica(C casella, String sfondo) {
		super();
		this.casella = casella;
		this.baseNodo = new VBox(18);
		this.baseNodo.setStyle("-fx-background-image: url("+getClass().getResource("/gui/image/caselle/"+sfondo+".png").toString()+"); -fx-background-repeat: stretch; -fx-background-size: 100% 100%;");
		this.baseNodo.setAlignment(Pos.CENTER);
//		this.ricompense = new HBox(1);
//		this.ricompense.setAlignment(Pos.CENTER);

		Text punti = new Text(String.valueOf(casella.getPuntiCasella()));
		punti.setFont(FONT);
		punti.setFill(Color.FLORALWHITE);
		this.baseNodo.getChildren().add(punti);
		
	/*	ArrayList<RicompensaGrafica> gRicompense = new ArrayList<>();
		this.casella.getRicompense().forEach(r -> gRicompense.add(new RicompensaGrafica(r)));
		this.ricompense.getChildren().addAll(gRicompense);
		*/
//		this.baseNodo.getChildren().add(ricompense);

		AnchorPane.setBottomAnchor(baseNodo, 0.0);
		AnchorPane.setLeftAnchor(baseNodo, 0.0);
		AnchorPane.setRightAnchor(baseNodo, 0.0);
		AnchorPane.setTopAnchor(baseNodo, 0.0);
		
		this.baseNodo.setPrefHeight(57);
		this.baseNodo.setPrefWidth(63);
		
		this.pedine = new ArrayList<>();
		this.casella.getGiocatoriPresenti().forEach(g -> pedine.add(new PedinaGrafica(g)));
		this.pedineHBox = new HBox(2);
		this.pedineHBox.getChildren().addAll(pedine);
		
		AnchorPane.setBottomAnchor(pedineHBox, 0.0);
		AnchorPane.setLeftAnchor(pedineHBox, 0.0);
		AnchorPane.setRightAnchor(pedineHBox, 0.0);
		AnchorPane.setTopAnchor(pedineHBox, 0.0);
	
		this.getChildren().addAll(baseNodo, pedineHBox);

		
	}

	public void aggiungiGiocatore(Giocatore giocatore) {
		PedinaGrafica pedina = new PedinaGrafica(giocatore);
		this.pedine.add(pedina);
		this.pedineHBox.getChildren().add(pedina);
	}
	public void rimuoviGiocatore(Giocatore giocatore){
		PedinaGrafica pedina = this.pedine.stream().filter(p -> p.getGiocatore().getNome().equals(giocatore.getNome())).findFirst().orElse(null);
		if(pedina!=null)
			this.pedineHBox.getChildren().remove(pedina);
	}
	
	public Casella getCasella() {
		return casella;
	}

	
}
