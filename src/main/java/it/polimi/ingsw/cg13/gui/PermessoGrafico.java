package it.polimi.ingsw.cg13.gui;

import java.util.ArrayList;

import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.mappa.Regione;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class PermessoGrafico extends ToggleButton implements Parametro{

	private static final Font FONT = Font.loadFont(PermessoGrafico.class.getResourceAsStream("/gui/font/Enchanted Land.otf"), 16);
	private PermessoCostruzione permessoCostruzione;
	private AnchorPane baseNodo;
	private HBox citta;
	private HBox ricompense;
	private Regione regione;
	CDQToggleButtonSkin buttonSkin = new CDQToggleButtonSkin(this);
	
	public PermessoGrafico(PermessoCostruzione permessoCostruzione, Regione regione, double width) {
		super();
		this.permessoCostruzione = permessoCostruzione;
		this.baseNodo = new AnchorPane();
		this.baseNodo.setStyle("-fx-background-image: url("+getClass().getResource("/gui/image/permessi/permesso_base.png").toString()+"); -fx-background-repeat: stretch; -fx-background-size: 100% 100%;");
		this.citta = new HBox(3);
		this.citta.setAlignment(Pos.CENTER);
		this.regione = regione;
		this.ricompense = new HBox(3);
		this.ricompense.setAlignment(Pos.CENTER);

		ArrayList<Text> lettere = new ArrayList<>();
		this.permessoCostruzione.getListaCitta().forEach(c -> lettere.add(new Text(c.getNome().substring(0, 1))));
		lettere.forEach(t -> t.setFont(FONT));
		this.citta.getChildren().addAll(lettere);
		
		ArrayList<RicompensaGrafica> gRicompense = new ArrayList<>();
		this.permessoCostruzione.getRicompense().forEach(r -> gRicompense.add(new RicompensaGrafica(r, width*0.4, width*0.4)));
		this.ricompense.getChildren().addAll(gRicompense);
		
		this.baseNodo.getChildren().addAll(citta, ricompense);
		AnchorPane.setTopAnchor(citta, width*0.08);
		AnchorPane.setBottomAnchor(ricompense, width*0.08);
		AnchorPane.setLeftAnchor(citta, 0.0);
		AnchorPane.setLeftAnchor(ricompense, 0.0);
		AnchorPane.setRightAnchor(citta, 0.0);
		AnchorPane.setRightAnchor(ricompense, 0.0);
		
		this.baseNodo.setPrefHeight(width*1.2);
		this.baseNodo.setPrefWidth(width);
		
//		this.setLayoutX(30);
//		this.setLayoutY(30);
		
//		this.setOnMouseEntered(new EventHandler<MouseEvent>() {
//            @Override public void handle(MouseEvent e) {
//            	ColorAdjust adjust = new ColorAdjust();
//            	adjust.setBrightness(+0.5);
//            	setEffect(adjust);
//            }
//        });
//        
//        this.setOnMouseExited(new EventHandler<MouseEvent>() {
//            @Override public void handle(MouseEvent e) {
//            	ColorAdjust adjust = new ColorAdjust();
//            	adjust.setBrightness(0.0);
//            	setEffect(adjust);
//            }
//        });
//        
//        this.setOnMouseClicked(selectionHandler);
		this.setGraphic(baseNodo);
		this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		
	}
	
	public void capovolgi(){
		//mette il retro o lo toglie
	}
	
	public PermessoCostruzione getPermessoCostruzione() {
		return permessoCostruzione;
	}
	
	public Regione getRegione() {
		return regione;
	}
	
}
