package it.polimi.ingsw.cg13.gui;





import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import it.polimi.ingsw.cg13.azioni.AcquistoAiutante;
import it.polimi.ingsw.cg13.azioni.AcquistoPermesso;
import it.polimi.ingsw.cg13.azioni.BonusInput;
import it.polimi.ingsw.cg13.azioni.CambioConsiglio;
import it.polimi.ingsw.cg13.azioni.CambioConsiglioVeloce;
import it.polimi.ingsw.cg13.azioni.CambioPermessi;
import it.polimi.ingsw.cg13.azioni.CompraAzionePrincipale;
import it.polimi.ingsw.cg13.azioni.CostruzioneConKing;
import it.polimi.ingsw.cg13.azioni.CostruzioneConPermesso;
import it.polimi.ingsw.cg13.azioni.FinisciTurno;
import it.polimi.ingsw.cg13.azionimarket.AcquistaInserzione;
import it.polimi.ingsw.cg13.azionimarket.VendiInserzione;
import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.client.ClientInterfaccia;
import it.polimi.ingsw.cg13.comandi.ChatMessage;
import it.polimi.ingsw.cg13.comandi.Exit;
import it.polimi.ingsw.cg13.comandi.HelpGui;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.comandi.Query;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.mutatio.BonusLosedMutatio;
import it.polimi.ingsw.cg13.mutatio.CambioPermessiMutatio;
import it.polimi.ingsw.cg13.mutatio.CasellaMutatioAddGiocatore;
import it.polimi.ingsw.cg13.mutatio.CasellaMutatioRemoveGiocatore;
import it.polimi.ingsw.cg13.mutatio.ChatMutatio;
import it.polimi.ingsw.cg13.mutatio.CittaMutatio;
import it.polimi.ingsw.cg13.mutatio.ClassificaMutatio;
import it.polimi.ingsw.cg13.mutatio.ConsiglieriBancoMutatio;
import it.polimi.ingsw.cg13.mutatio.ConsiglioMutatio;
import it.polimi.ingsw.cg13.mutatio.FineFaseMarketMutatio;
import it.polimi.ingsw.cg13.mutatio.InizioFaseMarketMutatio;
import it.polimi.ingsw.cg13.mutatio.InserzioneAcquistataMutatio;
import it.polimi.ingsw.cg13.mutatio.InserzioneInVenditaMutatio;
import it.polimi.ingsw.cg13.mutatio.KingMutatio;
import it.polimi.ingsw.cg13.mutatio.LineGiocatoreMutatio;
import it.polimi.ingsw.cg13.mutatio.MescolatoMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.mutatio.NomeNonDisponibileMutatio;
import it.polimi.ingsw.cg13.mutatio.PartitaMutatio;
import it.polimi.ingsw.cg13.mutatio.PermessoToltoMutatio;
import it.polimi.ingsw.cg13.mutatio.PrimoGiocatoreMutatio;
import it.polimi.ingsw.cg13.mutatio.QueryMutatio;
import it.polimi.ingsw.cg13.mutatio.StartConnessioneMutatio;
import it.polimi.ingsw.cg13.mutatio.StateMutatio;
import it.polimi.ingsw.cg13.mutatio.TempoScadutoMutatio;
import it.polimi.ingsw.cg13.mutatio.TesseraBonusMutatio;
import it.polimi.ingsw.cg13.mutatio.UltimoTurnoMutatio;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.oggetti.Vendibile;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.visitorforgui.VisitMutatio;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.*;
import javafx.scene.*;

public class ClientGUIController extends ClientInterfaccia implements VisitMutatio{
	
    @FXML
    private VBox azioniBase;
    @FXML
    private VBox azioniMarket;
    
	@FXML
	protected HBox carte;
	@FXML
	protected HBox permessi;
	@FXML
	protected HBox tBonus;
	@FXML
	protected HBox banco;
	@FXML
	protected HBox mappa;
	@FXML
	protected HBox oggetti;
	@FXML
	protected ImageView planciaRe;
	@FXML
	protected HBox permessiUsati;

	@FXML
	protected TabPane statoGiocatore;
    @FXML
    protected SplitPane horizSplitPane;
    @FXML
    protected GridPane grigliaMappa;	
    @FXML
    protected AnchorPane cellaCollina;
    @FXML
    protected AnchorPane cellaMare;
    @FXML
    protected AnchorPane cellaMontagna;

    @FXML
    protected AnchorPane cellaPlanciaCollina;

    @FXML
    protected AnchorPane cellaPlanciaMare;

    @FXML
    protected AnchorPane cellaPlanciaMontagna;
    
	@FXML
	protected ToggleGroup consiglieriBanco;
	protected ToggleGroup consigli;
	@FXML
	protected AnchorPane anchorMappa;

    @FXML
    protected Button bAcquistoAiutante;

    @FXML
    protected Button bAcquistoAzionePrincipale;

    @FXML
    protected Button bAcquistoPermesso;

    @FXML
    protected Button bCambioConsiglio;

    @FXML
    protected Button bCambioConsiglioVeloce;

    @FXML
    protected Button bCambioPermessi;

    @FXML
    protected Button bCostruisciConPermesso;

    @FXML
    protected Button bCostruisciConRe;
    @FXML
    protected Button bVendiOggetto;
    @FXML
    protected Button bConfermaBonus;
    
    @FXML
    protected AnchorPane destra;
    
    @FXML
    protected ProgressIndicator progress;
    
    @FXML
    protected AnchorPane finestraGioco;

    @FXML
    private StackPane stackAzioniFase;
    @FXML
    private StackPane stackFase;
    @FXML
    private Group gruppoAzioniFase;
    @FXML
    protected VBox percorsiBox;
    @FXML
    private TextArea messaggi;
    @FXML
    private TextField inputChat;
    @FXML
    private Group gruppoFase;
    @FXML
    private AnchorPane vistaMappa;
    @FXML
    private FlowPane listaInserzioni;

    @FXML
    private AnchorPane market;
    
    @FXML
    private TextArea elencoMutatio;
    @FXML
    private Label statoGioco;
    @FXML
    private ToggleButton selezionaVista;
    

    @FXML
    private ImageView planciaCollinaImg;

    @FXML
    private ImageView planciaMareImg;

    @FXML
    private ImageView planciaMontagnaImg;
    @FXML
    private ImageView montagnaImg;

    @FXML
    private ImageView mareImg;

    @FXML
    private ImageView collinaImg;

    @FXML
    private Group mappaGroup;
    @FXML
    private AnchorPane anchorImageMappa;
    
    @FXML
    private Slider zoomer;
    
    private AiutanteGrafico aiutanteGiocatore;
    
    protected List<ConsigliereToggleButton> consiglieriB;
    protected ToggleGroup permessiEsposti;
	protected List<CittaGrafica> cittaPresenti;
	protected List<ConsiglioGrafico> consigliPresenti;
	protected List<MazzoPermessiGrafico> mazziPresenti;
	protected Label nAiutanti;
	protected Label nEmpori;
	protected List<CartaPoliticaGrafica> carteGiocatore;
	protected List<PermessoGrafico> permessiGiocatore;
	protected List<PermessoGrafico> permessiUsatiGiocatore;
	protected List<TesseraBonusGrafica> tessereBonusGiocatore;
	protected List<TesseraBonusGrafica> tessereBonusBanco;
	protected ToggleGroup permessiGiocatoriToggleGroup;
	protected ToggleGroup permessiUsatiGiocatoreToggleGroup;
	protected PercorsoVittoriaGrafico percorsoVittoria;
	protected PercorsoRicchezzaGrafico percorsoRicchezza;
	protected PercorsoNobiltaGrafico percorsoNobilta;
	KingGrafico king;
	protected Map<String, ArrayList<Integer>> coordinateCitta;
	protected MainGUI mainGui;
	private ToggleGroup inserzioni;
	private ConsiglioGrafico cons1;
	private ConsiglioGrafico cons2;
	private ConsiglioGrafico cons3;
	private ConsiglioGrafico consRe;

	private List<CittaGrafica> cittaSelezionateInOrdine;
	private List<InserzioneGrafica> inserzioniGrafiche;
	
	
	protected Partita modello;
	 
	protected Giocatore giocatore;
	protected Login loginPlayer;

	VBox dialogVbox ;
	
	static final String FX_BACKGROUND = "-fx-background-image: url(";
	static final String FX_BG_SIZE = "-fx-background-repeat: stretch; -fx-background-size: 100% 100%;";
	
	@FXML
	private void initialize(){
		//si prepara, binding tra FXML e attributi
		horizSplitPane.setDisable(true);
		this.consiglieriB = new LinkedList<>();
		this.consiglieriBanco = new ToggleGroup();
		this.consigli = new ToggleGroup();
		this.permessiEsposti = new ToggleGroup();
		this.cittaPresenti = new ArrayList<>();
		this.permessiGiocatoriToggleGroup = new ToggleGroup();
		this.coordinateCitta = new HashMap<>();
		this.settaCoordinate();
		this.consigliPresenti = new ArrayList<>();
		this.mazziPresenti = new ArrayList<>();
		this.carteGiocatore = new ArrayList<>();
		this.permessiGiocatore = new ArrayList<>();
		this.permessiUsatiGiocatore = new ArrayList<>();
		this.tessereBonusGiocatore = new ArrayList<>();
		this.tessereBonusBanco = new ArrayList<>();
		this.permessiUsatiGiocatoreToggleGroup = new ToggleGroup();
		this.inserzioni=new ToggleGroup();
		this.cittaSelezionateInOrdine = new ArrayList<>();
		this.inserzioniGrafiche=new ArrayList<>();
		
		

	}

	public void setMainGui(MainGUI mainGui) {
		this.mainGui = mainGui;
	}
	

	public void inizioPartita(){

		 market.setStyle("-fx-background-image: url("+getClass().getResource("/gui/image/bancarella.png").toString()+"); -fx-background-repeat: stretch; -fx-background-size: 100% 100%;");

		this.updateGiocatore();

		this.statoGiocatore.setBackground(new Background(new BackgroundFill(Color.rgb(this.giocatore.getColore().getRed(), this.giocatore.getColore().getGreen(), this.giocatore.getColore().getBlue(), 0.2), CornerRadii.EMPTY, Insets.EMPTY)));
		
		mettiSfondoAzioni();
		mettiEventiAzioni();
		
		this.percorsoVittoria = new PercorsoVittoriaGrafico(this.modello.getPercorsoVittoria());
		this.percorsoRicchezza = new PercorsoRicchezzaGrafico(this.modello.getPercorsoRicchezza());
		this.percorsoNobilta = new PercorsoNobiltaGrafico(this.modello.getPercorsoNobilta());
		
		this.tBonus.setDisable(true); //disabilita il contenuto
		

		this.azioniBase.toFront();
		this.azioniBase.setVisible(true);
		this.azioniMarket.setVisible(false);
		
		String configurazioneMappa = Integer.toBinaryString(this.modello.getParametri().getConfigurazioneMappa() + 0b1000).substring(1);	

		this.mareImg.setImage(new Image(getClass().getResource("/gui/image/mare"+configurazioneMappa.charAt(0)+".png").toString()));
		this.collinaImg.setImage(new Image(getClass().getResource("/gui/image/collina"+configurazioneMappa.charAt(0)+".png").toString()));
		this.montagnaImg.setImage(new Image(getClass().getResource("/gui/image/montagna"+configurazioneMappa.charAt(0)+".png").toString()));
		this.planciaMareImg.setImage(new Image(getClass().getResource("/gui/image/plancia_mare.png").toString()));
		this.planciaCollinaImg.setImage(new Image(getClass().getResource("/gui/image/plancia_collina.png").toString()));
		this.planciaMontagnaImg.setImage(new Image(getClass().getResource("/gui/image/plancia_montagna.png").toString()));
		
		initStatoGiocatore();
		posizionaCitta();
		
		Iterator<Regione> iter = this.modello.getMappaPartita().getRegioni().iterator();
		cons1 = new ConsiglioGrafico(iter.next().getConsiglio(), Math.floor(220*anchorImageMappa.getWidth()/1686.0), Math.floor(214*anchorImageMappa.getWidth()/1686.0), Math.floor(1009*anchorImageMappa.getHeight()/1172.0));
		cons1.setToggleGroup(consigli);
		cons2 = new ConsiglioGrafico(iter.next().getConsiglio(), Math.floor(220*anchorImageMappa.getWidth()/1686.0), Math.floor(725*anchorImageMappa.getWidth()/1686.0), Math.floor(1009*anchorImageMappa.getHeight()/1172.0));
		cons2.setToggleGroup(consigli);
		cons3 = new ConsiglioGrafico(iter.next().getConsiglio(), Math.floor(220*anchorImageMappa.getWidth()/1686.0), Math.floor(1287*anchorImageMappa.getWidth()/1686.0), Math.floor(1009*anchorImageMappa.getHeight()/1172.0));
		cons3.setToggleGroup(consigli);
		consRe = new ConsiglioGrafico(this.modello.getKing().getConsiglio(), Math.floor(220*anchorImageMappa.getWidth()/1686.0),Math.floor(1052*anchorImageMappa.getWidth()/1686.0), Math.floor(1058*anchorImageMappa.getHeight()/1172.0));
		consRe.setToggleGroup(consigli);

		this.anchorImageMappa.getChildren().addAll(cons1, cons2, cons3, consRe);
		consigliPresenti.add(cons1);
		consigliPresenti.add(cons2);
		consigliPresenti.add(cons3);
		consigliPresenti.add(consRe);
		
		try {
			MazzoPermessiGrafico mazzo = new MazzoPermessiGrafico(this.modello.getRegione("mare"), permessiEsposti, Math.floor(110*anchorImageMappa.getWidth()/1686.0),Math.floor(109*anchorImageMappa.getWidth()/1686.0), Math.floor(870*anchorImageMappa.getHeight()/1172.0));

			this.anchorImageMappa.getChildren().add(mazzo);
			this.mazziPresenti.add(mazzo);
		} catch (RegioneNotFoundException e1) {
			showError("Errore regione 1", "Regione 1 non trovata", e1);
		}
		try {
			MazzoPermessiGrafico mazzo = new MazzoPermessiGrafico(this.modello.getRegione("collina"), permessiEsposti, Math.floor(110*anchorImageMappa.getWidth()/1686.0),Math.floor(619*anchorImageMappa.getWidth()/1686.0), Math.floor(870*anchorImageMappa.getHeight()/1172.0));

			this.anchorImageMappa.getChildren().add(mazzo);
			this.mazziPresenti.add(mazzo);
		} catch (RegioneNotFoundException e1) {
			showError("Errore regione 2", "Regione 2 non trovata", e1);
		}
		try {
			MazzoPermessiGrafico mazzo = new MazzoPermessiGrafico(this.modello.getRegione("montagna"), permessiEsposti, Math.floor(110*anchorImageMappa.getWidth()/1686.0),Math.floor(1181*anchorImageMappa.getWidth()/1686.0), Math.floor(870*anchorImageMappa.getHeight()/1172.0));

			this.anchorImageMappa.getChildren().add(mazzo);
			this.mazziPresenti.add(mazzo);
		} catch (RegioneNotFoundException e1) {
			showError("Errore regione 3", "Regione 3 non trovata", e1);
		}
		
		king = new KingGrafico(this.modello.getKing(), Math.floor(150*anchorImageMappa.getWidth()/1686.0)*0.5);
		
		
		try {
			(this.cittaPresenti.stream().filter(nodo -> nodo.getCitta().equals(king.getKing().getPosizione())).findFirst().orElseThrow(()-> new CittaNotFoundException("Il Re non puo' essere posizionato su nessuna citta'"))).addKing(king);
		} catch (CittaNotFoundException e1) {
			showError("Errore Re", "Il Re non ha una citta' sulla mappa", e1);
		}
		
		
		this.percorsiBox.getChildren().add(percorsoVittoria);
		this.percorsiBox.getChildren().add(percorsoNobilta);
		this.percorsiBox.getChildren().add(percorsoRicchezza);


		this.market.setVisible(false);
		finestraGioco.getChildren().remove(progress);
		horizSplitPane.setDisable(false);
		
		zoomer.valueProperty().addListener(new ChangeListener<Number>() {
	        public void changed(ObservableValue<? extends Number> ov, Number oldVal, Number newVal) {
	                anchorImageMappa.setScaleX(newVal.doubleValue()*0.5);
	                anchorImageMappa.setScaleY(newVal.doubleValue()*0.5);
	                
	        }
	    });
		
		
	}

	private void showError(String title, String header, Exception exception){
		Alert alert = new Alert(AlertType.ERROR);
		Platform.runLater(() -> mainGui.setAlert(alert,title,header,exception.getMessage()));
	}

	private void showInfo(String title,String header,String message){
		Alert alert = new Alert(AlertType.INFORMATION);
        Platform.runLater(() -> mainGui.setAlert(alert,title,header,message));
	}


	/* (non-Javadoc)
	 * @see it.polimi.ingsw.cg13.observable.ObserverMVC#update(java.lang.Object)
	 */
	@Override
	public void update(Mutatio change) {
		try {
			modello=change.changeModel(modello);
		} catch (IllegalArgumentException | CdQException e) {
			this.showError("Errore nell'update", "Questa Mutatio ha un problema", e);
			//richiedi modello aggiornato 
		}
		finally{		
			this.updateGiocatore();
			Platform.runLater(()->this.elencoMutatio.appendText("\n"+change.toString()));
			Platform.runLater(() -> change.accept(this));
		}
	}
	public void updateGiocatore(){
		if(modello!=null){
			if(this.giocatore==null){
				this.giocatore=this.modello.getGiocatori().stream().
						filter(g->g.getNome().equals(this.loginPlayer.getNomeGiocatore()))
						.findFirst().orElse(null);
			}
			else{
				Giocatore giocatoreAggiornato=this.modello.getGiocatori().stream().
						filter(g -> g.equals(this.giocatore)).findFirst().orElse(null);
				if(giocatoreAggiornato!=null){
					this.giocatore=giocatoreAggiornato;
				}else{
					giocatoreAggiornato=this.modello.getGiocatoriOffline().stream().
							filter(g -> g.equals(this.giocatore)).findFirst().orElse(null);
					if(giocatoreAggiornato!=null){
						this.giocatore=giocatoreAggiornato;
					}
				}
			}
		}
	}

	@Override
	public void update(Exception e) {
		Platform.runLater(() -> showError("Errore", "Errore da controller", e));
	}
	
	private void erroreAzione(Exception e){
		showError("Errore azione", "La tua Azione ha un problema", e);
	}
	
	private void checkCambioConsiglio(ConsiglioGrafico consiglioSelezionato, ConsigliereToggleButton consigliereSelezionato) throws AzioneNotPossibleException{
		if(consiglioSelezionato == null){
			throw new AzioneNotPossibleException("Non hai selezionato nessun consiglio");
		}
		if(consigliereSelezionato == null){
			throw new AzioneNotPossibleException("Non hai selezionato nessun consigliere del banco");
		}
	}
	
	private void posizionaCitta(){
		//posizionamenti e dimensioni NON DINAMICHE
			Iterator<Regione> iterator = this.modello.getMappaPartita().getRegioni().iterator();
			
			while(iterator.hasNext()){
				Regione regione = iterator.next();
				List<CittaGrafica> cittaRegione = new LinkedList<>();
				this.modello.getMappaPartita().getGrafo().stream().filter(n -> regione.getCitta().contains(n.getCittaNodo())).forEach(nodo -> cittaRegione.add(new CittaGrafica(nodo, Math.floor(150*anchorImageMappa.getWidth()/1686.0), Math.floor(193*anchorImageMappa.getHeight()/1172.0))));
				this.cittaPresenti.addAll(cittaRegione);

			}
			this.anchorImageMappa.getChildren().addAll(cittaPresenti);
			this.cittaPresenti.forEach(this::posizionaCitta);
	}
	
	private void settaCoordinate(){
		
		//regione MARE
		this.coordinateCitta.put("Arkon", new ArrayList<>(Arrays.asList(116,91)));
		this.coordinateCitta.put("Burgen", new ArrayList<>(Arrays.asList(102,340)));
		this.coordinateCitta.put("Castrum", new ArrayList<>(Arrays.asList(362,164)));
		this.coordinateCitta.put("Dorful", new ArrayList<>(Arrays.asList(328,402)));
		this.coordinateCitta.put("Esti", new ArrayList<>(Arrays.asList(187,585)));
		
		//regione COLLINA
		this.coordinateCitta.put("Framek", new ArrayList<>(Arrays.asList(650,118)));
		this.coordinateCitta.put("Graden", new ArrayList<>(Arrays.asList(661,350)));
		this.coordinateCitta.put("Indur", new ArrayList<>(Arrays.asList(924,118)));
		this.coordinateCitta.put("Juvelar", new ArrayList<>(Arrays.asList(924,420)));
		this.coordinateCitta.put("Hellar", new ArrayList<>(Arrays.asList(691,556)));
		
		//regione MONTAGNA
		this.coordinateCitta.put("Kultos", new ArrayList<>(Arrays.asList(1201,96)));
		this.coordinateCitta.put("Lyram", new ArrayList<>(Arrays.asList(1156,361)));
		this.coordinateCitta.put("Naris", new ArrayList<>(Arrays.asList(1419,239)));
		this.coordinateCitta.put("Osium", new ArrayList<>(Arrays.asList(1429,506)));
		this.coordinateCitta.put("Merkatim", new ArrayList<>(Arrays.asList(1137,615)));

	}
	
	private void posizionaCitta(CittaGrafica cittaGrafica){
		ArrayList<Integer> coordinate = this.coordinateCitta.get(cittaGrafica.getCitta().getCittaNodo().getNome());
		cittaGrafica.setLayouts((int)Math.floor(coordinate.get(0)*anchorImageMappa.getWidth()/1686.0), (int)Math.floor(coordinate.get(1)*anchorImageMappa.getHeight()/1172.0));
		cittaGrafica.setOnAction(evt -> cittaSelezionata(evt.getSource()));
	}
	
	private void cittaSelezionata(Object object) {
		if(object instanceof CittaGrafica){
			CittaGrafica cittaGrafica = (CittaGrafica) object;
			if(cittaGrafica.isSelected()){
				if(king.getKing().getPosizione().equals(cittaGrafica.getCitta())){
					cittaSelezionateInOrdine.forEach(citta -> citta.setSelected(false));
					cittaSelezionateInOrdine.clear();
				}
				cittaSelezionateInOrdine.add(cittaGrafica);
			}
			else{
				cittaSelezionateInOrdine.remove(cittaGrafica);
			}
		}
	}

	private void mettiSfondoAzioni(){
		this.bAcquistoAiutante.setStyle(FX_BACKGROUND+getClass().getResource("/gui/image/azioni/acquistoAiutante.png").toString()+");"+FX_BG_SIZE);
		this.bAcquistoAzionePrincipale.setStyle(FX_BACKGROUND+getClass().getResource("/gui/image/azioni/acquistoAzionePrincipale.png").toString()+");"+FX_BG_SIZE);
		this.bAcquistoPermesso.setStyle(FX_BACKGROUND+getClass().getResource("/gui/image/azioni/acquistoPermesso.png").toString()+");"+FX_BG_SIZE);
		this.bCambioConsiglio.setStyle(FX_BACKGROUND+getClass().getResource("/gui/image/azioni/cambioConsiglio.png").toString()+");"+FX_BG_SIZE);
		this.bCambioConsiglioVeloce.setStyle(FX_BACKGROUND+getClass().getResource("/gui/image/azioni/cambioConsiglioVeloce.png").toString()+");"+FX_BG_SIZE);
		this.bCambioPermessi.setStyle(FX_BACKGROUND+getClass().getResource("/gui/image/azioni/cambioPermessi.png").toString()+");"+FX_BG_SIZE);
		this.bCostruisciConPermesso.setStyle(FX_BACKGROUND+getClass().getResource("/gui/image/azioni/costruisciConPermesso.png").toString()+");"+FX_BG_SIZE);
		this.bCostruisciConRe.setStyle(FX_BACKGROUND+getClass().getResource("/gui/image/azioni/costruisciConRe.png").toString()+");"+FX_BG_SIZE);
	}
	
	private void mettiEventiAzioni(){
		this.bAcquistoAiutante.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				notifyObservers(new AcquistoAiutante(giocatore));

			}
		});
		this.bAcquistoAzionePrincipale.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				notifyObservers(new CompraAzionePrincipale(giocatore));

			}
		});
		this.bAcquistoPermesso.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					List<CartaPolitica> carteSelezionate = new ArrayList<>();
					carte.getChildrenUnmodifiable().stream().filter(c -> ((CartaPoliticaGrafica)c).isSelected()).forEach(cp -> carteSelezionate.add(((CartaPoliticaGrafica)cp).getCarta()));
					if(carteSelezionate.isEmpty()){
						throw new AzioneNotPossibleException("Non hai selezionato nessuna carta");
					}
					carteSelezionate.sort((carta1, carta2) -> carta1.getCosto()>=carta2.getCosto() ? 1 : -1);
		
					PermessoGrafico permessoSelezionato = (PermessoGrafico)permessiEsposti.getSelectedToggle();
					if(permessoSelezionato == null){
						throw new AzioneNotPossibleException("Non hai selezionato nessun permesso di costruzione");
					}
					notifyObservers(new AcquistoPermesso(giocatore, carteSelezionate,permessoSelezionato.getRegione(),permessoSelezionato.getPermessoCostruzione()));
				} catch (CdQException e) {
					erroreAzione(e);
				}
			}
		});
		this.bCambioConsiglio.setOnAction(new EventHandler<ActionEvent>(
				) {

			@Override
			public void handle(ActionEvent event) {
				try {
					ConsiglioGrafico consiglioSelezionato = (ConsiglioGrafico)consigli.getSelectedToggle();
					ConsigliereToggleButton consigliereSelezionato = (ConsigliereToggleButton)consiglieriBanco.getSelectedToggle();
					checkCambioConsiglio(consiglioSelezionato, consigliereSelezionato);
					notifyObservers(new CambioConsiglio(giocatore, consiglioSelezionato.getConsiglio(), consigliereSelezionato.getConsigliere()));
				} catch (CdQException e) {
					erroreAzione(e);
				}
			}
		});
		this.bCambioConsiglioVeloce.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					ConsiglioGrafico consiglioSelezionato = (ConsiglioGrafico)consigli.getSelectedToggle();
					ConsigliereToggleButton consigliereSelezionato = (ConsigliereToggleButton)consiglieriBanco.getSelectedToggle();
					checkCambioConsiglio(consiglioSelezionato, consigliereSelezionato);
					notifyObservers(new CambioConsiglioVeloce(giocatore, consiglioSelezionato.getConsiglio(),consigliereSelezionato.getConsigliere()));
				} catch (CdQException e) {
					erroreAzione(e);
				}
			}
		});
		this.bCambioPermessi.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					PermessoGrafico permessoSelezionato = (PermessoGrafico)permessiEsposti.getSelectedToggle();
					if(permessoSelezionato == null){
						throw new AzioneNotPossibleException("Non hai selezionato nessun mazzo (uno dei permessi)");
					}
					notifyObservers(new CambioPermessi(permessoSelezionato.getRegione().getMazzoPermesso(), giocatore));
				} catch (CdQException e) {
					erroreAzione(e);
				}
			}
		});
		this.bCostruisciConPermesso.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					PermessoGrafico permessoSelezionato = (PermessoGrafico)permessiGiocatoriToggleGroup.getSelectedToggle();
					if(permessoSelezionato == null){
						throw new AzioneNotPossibleException("Non hai selezionato nessun permesso di costruzione");
					}
					notifyObservers(new CostruzioneConPermesso(giocatore, cittaPresenti.stream().filter(c -> c.isSelected()).findFirst().orElseThrow(() -> new CittaNotFoundException("Citta non selezionata")).getCitta(), permessoSelezionato.getPermessoCostruzione()));
				} catch (CdQException e) {
					erroreAzione(e);
				}
			}
		});
		this.bCostruisciConRe.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					ConsiglioGrafico consiglioSelezionato = (ConsiglioGrafico)consigli.getSelectedToggle();
					if(consiglioSelezionato == null || !modello.getKing().equals(consiglioSelezionato.getConsiglio().getProprietario())){
						throw new AzioneNotPossibleException("Non hai selezionato il consiglio del Re");
					}
					List<CartaPolitica> carteSelezionate = new ArrayList<>();
					carte.getChildrenUnmodifiable().stream().filter(c -> ((CartaPoliticaGrafica)c).isSelected()).forEach(cp -> carteSelezionate.add(((CartaPoliticaGrafica)cp).getCarta()));
					if(carteSelezionate.isEmpty()){
						throw new AzioneNotPossibleException("Non hai selezionato nessuna carta");
					}
					carteSelezionate.sort((carta1, carta2) -> carta1.getCosto()>=carta2.getCosto() ? 1 : -1);
					List<NodoCitta> cittaSelezionate = new LinkedList<>();
					if(cittaSelezionateInOrdine.size()>1){
						cittaSelezionateInOrdine.remove(0);
					}
					cittaSelezionateInOrdine.forEach(cg -> cittaSelezionate.add(cg.getCitta()));
					if(cittaSelezionate.isEmpty()){
						throw new AzioneNotPossibleException("Non hai selezionato nessuna citta");
					}
					notifyObservers(new CostruzioneConKing(giocatore, cittaSelezionate, carteSelezionate));
				} catch (CdQException e) {
					erroreAzione(e);
				}
				finally{
					cittaSelezionateInOrdine.forEach(citta -> citta.setSelected(false));
					cittaSelezionateInOrdine.clear();
				}
			}
		});
		
		this.bVendiOggetto.setOnAction(evt -> venditaOggetto());
		
		this.bConfermaBonus.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

					PermessoGrafico permessoSelezionato = (PermessoGrafico)permessiUsatiGiocatoreToggleGroup.getSelectedToggle();
					if(permessoSelezionato != null){
						notifyObservers(new BonusInput(giocatore, permessoSelezionato.getPermessoCostruzione()));
						return;
					}
					permessoSelezionato = (PermessoGrafico)permessiEsposti.getSelectedToggle();
					if(permessoSelezionato != null){
						notifyObservers(new BonusInput(giocatore, permessoSelezionato.getPermessoCostruzione()));
						return;
					}
					List<NodoCitta> cittaSelezionate = new LinkedList<>();
					cittaPresenti.stream().filter(c -> c.isSelected()).forEach(nc -> cittaSelezionate.add(nc.getCitta()));
					if(cittaSelezionate.size()==1){
						notifyObservers(new BonusInput(giocatore, cittaSelezionate.get(0).getCittaNodo()));
						return;
					}
					erroreAzione(new AzioneNotPossibleException("Non hai selezionato alcun oggetto con Bonus"));
			}
		});
		
		this.inputChat.setOnKeyPressed(event -> { if(event.getCode() == KeyCode.ENTER) { inviaMessaggio(); } });

	}
	
	@FXML
	private void compraInserzione(){
		InserzioneGrafica ins=(InserzioneGrafica) this.inserzioni.getSelectedToggle();
		try {
			if(ins==null)
				throw new AzioneNotPossibleException("Non hai selezionato nessun oggetto del market");
			this.notifyObservers(new AcquistaInserzione(this.giocatore,ins.getInserzione()));	
		} catch (AzioneNotPossibleException e) {
			this.erroreAzione(e);	
		}
	}
	
	
	private void venditaOggetto(){
		try {
			Vendibile vendibile;
			List<Vendibile> carteSelezionate = new ArrayList<>();
			carte.getChildrenUnmodifiable().stream().filter(c -> ((CartaPoliticaGrafica)c).isSelected()).forEach(cp -> carteSelezionate.add(((CartaPoliticaGrafica)cp).getCarta()));
			if(!carteSelezionate.isEmpty()){
				if(carteSelezionate.size()==1){
					vendibile = carteSelezionate.get(0);
					creaInserzione(vendibile);
					return;
				}
				else throw new AzioneNotPossibleException("Puoi vendere solo una carta alla volta");
			}
			PermessoGrafico permessoSelezionato = (PermessoGrafico)permessiGiocatoriToggleGroup.getSelectedToggle();
			if(permessoSelezionato != null){
				vendibile = permessoSelezionato.getPermessoCostruzione();
				creaInserzione(vendibile);
				return;
			}

			if(this.aiutanteGiocatore.isSelected() && this.giocatore.numeroAiutanti()>0){
			 vendibile = new Aiutante(); //aiutanti da prendere invece del permesso

				creaInserzione(vendibile);
				return;
			}
				throw new AzioneNotPossibleException("Non hai selezionato alcun oggetto vendibile");

		} catch (Exception e) {
			erroreAzione(e);
		}
		
	}
	
	private void creaInserzione(Vendibile oggettoInVendita) throws AzioneNotPossibleException{
		TextInputDialog dialog = new TextInputDialog("");
		dialog.setTitle("Crea nuova inserzione");
		dialog.setHeaderText("A quanti gettoni vendi il tuo oggetto?");
		dialog.setContentText("Numero gettoni:");

		try {
			int result = Integer.parseUnsignedInt(dialog.showAndWait().orElseThrow(() -> new AzioneNotPossibleException("Devi inserire un prezzo per la tua inserzione")));
			notifyObservers(new VendiInserzione(this.giocatore, new Inserzione(new CasellaRicchezza(result), this.giocatore, oggettoInVendita)));
		} catch (NumberFormatException e) {
			throw new AzioneNotPossibleException("Il prezzo non e' un numero positivo valido");
		}

	}
	
	
	private void initStatoGiocatore(){
		this.giocatore.getCartePolitiche().forEach(c -> carteGiocatore.add(new CartaPoliticaGrafica(c)));
		this.carte.getChildren().addAll(carteGiocatore);
		this.giocatore.getPermessiCostruzione().forEach(p -> permessiGiocatore.add(new PermessoGrafico(p, null, Math.floor(110*anchorImageMappa.getWidth()/1686.0)))); //da rivedere il costruttore regione qui non la so... :(
		permessiGiocatore.forEach(p -> p.setToggleGroup(permessiGiocatoriToggleGroup));
		this.permessi.getChildren().addAll(permessiGiocatore);

		
		this.aiutanteGiocatore = new AiutanteGrafico(this.giocatore.getAiutanti());
		
		nEmpori = new Label(String.valueOf(this.giocatore.getEmpori().size()));
		nEmpori.setStyle("-fx-background-image: url("+getClass().getResource("/gui/image/empori/emporio_"+this.giocatore.getColore()+".png").toString()+"); -fx-background-repeat: stretch; -fx-background-size: 100% 100%;");

		nEmpori.setTextFill(Color.WHITE);
		nEmpori.setFont(Font.font(50));
		this.oggetti.getChildren().add(aiutanteGiocatore);
		this.oggetti.getChildren().addAll(/*nAiutanti,*/ nEmpori);
		this.oggetti.getChildren().addAll(this.tessereBonusGiocatore);
		this.modello.getBanco().getConsiglieri().entrySet().forEach(e -> consiglieriB.add(new ConsigliereToggleButton(e.getValue().size(), new Consigliere(e.getKey()))));
		this.consiglieriB.forEach(tb -> tb.setToggleGroup(consiglieriBanco));
		this.banco.getChildren().addAll(consiglieriB);
		
		this.modello.getBanco().getTessereBonus().forEach(tb -> this.tessereBonusBanco.add(new TesseraBonusGrafica(tb)));
		this.modello.getBanco().getGruppoBonusRe().getGruppo().forEach(tbr -> this.tessereBonusBanco.add(new TesseraBonusGrafica(tbr)));
		
		this.tBonus.getChildren().addAll(tessereBonusBanco);
	}

	public void setLoginPlayer(Login loginPlayer) {
		this.loginPlayer = loginPlayer;
	}
	
	@FXML
	public void inviaMessaggio(){
		String testo = inputChat.getText();
		if(!"".equals(testo.trim())){
			this.notifyObservers(new ChatMessage(giocatore, testo));
			inputChat.clear();
		}
	}
	
	
	@FXML
	public void finisciTurnoCommand(){
		notifyObservers(new FinisciTurno(this.giocatore));
	}
	
	@FXML
	public void exitCommand(){
		if(giocatore!=null)
			try{
				notifyObservers(new Exit(this.giocatore));
			}catch (Exception e) {
				mainGui.visualizzaErrore(e);
			}
	}

	@FXML
	public void helpCommand(){
		Query localQuery= new HelpGui(this.giocatore);
		try {
			String help=localQuery.perform(modello);
			final Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(mainGui.getPrimaryStage());
            
            dialogVbox = new VBox(20);
            dialogVbox.getChildren().add(new Text(help));

            Scene dialogScene = new Scene(dialogVbox, 800, 600);
            dialog.setScene(dialogScene);
            dialog.show();
		} catch (AzioneNotPossibleException e) {			
			showError("Errore Help", "Help ha un problema", e);
		}
	}
	
	@Override
	public void visit(BonusLosedMutatio change) {
		this.showInfo("Info","Bonus non utilizzabile" ,change.toString());
	}


	@Override
	public void visit(CasellaMutatioAddGiocatore change) {
		if(this.percorsoVittoria.getCaselle().stream().anyMatch(c->c.getCasella().equals(change.getCasella()))){
			this.percorsoVittoria.getCaselle().get(change.getCasella().getPuntiCasella()).aggiungiGiocatore(change.getGiocatore());
		}
		if(this.percorsoRicchezza.getCaselle().stream().anyMatch(c->c.getCasella().equals(change.getCasella()))){
			this.percorsoRicchezza.getCaselle().get(change.getCasella().getPuntiCasella()).aggiungiGiocatore(change.getGiocatore());
		}
		if(this.percorsoNobilta.getCaselle().stream().anyMatch(c->c.getCasella().equals(change.getCasella()))){
			this.percorsoNobilta.getCaselle().get(change.getCasella().getPuntiCasella()).aggiungiGiocatore(change.getGiocatore());
		}
//		
	}

	@Override
	public void visit(CasellaMutatioRemoveGiocatore change) {
		if(this.percorsoVittoria.getCaselle().stream().anyMatch(c->c.getCasella().equals(change.getCasella()))){
			this.percorsoVittoria.getCaselle().get(change.getCasella().getPuntiCasella()).rimuoviGiocatore(change.getGiocatore());
		}
		if(this.percorsoRicchezza.getCaselle().stream().anyMatch(c->c.getCasella().equals(change.getCasella()))){
			this.percorsoRicchezza.getCaselle().get(change.getCasella().getPuntiCasella()).rimuoviGiocatore(change.getGiocatore());
		}
		if(this.percorsoNobilta.getCaselle().stream().anyMatch(c->c.getCasella().equals(change.getCasella()))){
			this.percorsoNobilta.getCaselle().get(change.getCasella().getPuntiCasella()).rimuoviGiocatore(change.getGiocatore());
		}
	}

	@Override
	public void visit(ClassificaMutatio change) {
		 // Load person overview.
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainGUI.class.getResource("/gui/layout/classificaLayout.fxml"));
        try {
			AnchorPane classifica = (AnchorPane) loader.load();
		

     // Give the controller access to the main app.
        ClassificaController claCont = loader.getController();
        
        claCont.riempiElenco(change.getCalcolo());
		mainGui.showClassifica(classifica);
		
        } catch (IOException e) {
			showError("Errore lettura layout", "Errore nel caricamento della classifica", e);
		}
	}

	@Override
	public void visit(ConsiglieriBancoMutatio change) {
		try {
			ConsigliereToggleButton consigliereTolto=this.consiglieriB.stream().filter(toggle->
				toggle.getConsigliere().equals(change.getConsigliereTolto())).findFirst()
					.orElseThrow(()-> new ErrorUpdateServerException("Errore aggiornamento dei consiglieri del banco"));
			consigliereTolto.setText(Integer.toString(Integer.parseInt(consigliereTolto.getText())-1));
			
			ConsigliereToggleButton consigliereAggiunto=this.consiglieriB.stream().filter(toggle->
				toggle.getConsigliere().equals(change.getConsigliereAggiunto())).findFirst()
					.orElseThrow(()-> new ErrorUpdateServerException("Errore aggiornamento dei consiglieri del banco"));
			consigliereAggiunto.setText(Integer.toString(Integer.parseInt(consigliereAggiunto.getText())+1));
		} catch (ErrorUpdateServerException e) {
			this.showError("Errore", "Aggiornamento modello", e);
		}
	}

	@Override
	public void visit(ConsiglioMutatio change) {
		try {
			this.consigliPresenti.stream().filter(consiglio->
			consiglio.getConsiglio().getProprietario().equals(change.getConsiglio().getProprietario())).findFirst().
					orElseThrow(()-> new ErrorUpdateServerException()).
					sostituzioneConsigliere(change.getConsigliereAggiunto());
		} catch (ErrorUpdateServerException e) {
			this.showError("Errore Aggiornamento dal server", "Consiglio non coincidente", e);
		}
	}

	@Override
	public void visit(CittaMutatio change) {
		//Da sistemare assolutamente
		this.cittaPresenti.stream().filter(citta->
			citta.getCitta().getCittaNodo().equals(change.getCitta()))
				.forEach(c->c.addEmporio(change.getNuovoEmporio()));	
	}

	@Override
	public void visit(InserzioneAcquistataMutatio change) {
		try {
			InserzioneGrafica ins=this.inserzioniGrafiche.stream().
					filter(i->i.getInserzione().getIdInserzione()==change.getInserzione().getIdInserzione()).
						findFirst().orElseThrow(()->new ErrorUpdateServerException("Errore corrispondenza inserzioni"));
			this.inserzioniGrafiche.remove(ins);
			this.listaInserzioni.getChildren().remove(ins);
		} catch (ErrorUpdateServerException e) {
			this.showError("errore", "Aggiornamento Market", e);
		}
		
	}

	@Override
	public void visit(InserzioneInVenditaMutatio change) {
		Inserzione inserzione=change.getInserzione();
		ToggleButton vendibileGrafico=null;
		if(inserzione.getOggettoInVendita() instanceof Aiutante)
			vendibileGrafico=this.creaAiutanteVendibile(inserzione);
		else if(inserzione.getOggettoInVendita() instanceof PermessoCostruzione)
			vendibileGrafico=this.creaPermessoVendibile(inserzione);
		else if(inserzione.getOggettoInVendita() instanceof CartaPolitica)
			vendibileGrafico=this.creaCartaVendibile(inserzione);
		InserzioneGrafica gInserzione;
		if(vendibileGrafico!=null){
			gInserzione=new InserzioneGrafica(inserzione,vendibileGrafico);
			gInserzione.setToggleGroup(inserzioni);
			listaInserzioni.getChildren().add(gInserzione);
			this.inserzioniGrafiche.add(gInserzione);
		}
			
	}
	
	private ToggleButton creaAiutanteVendibile(Inserzione ins) {
		List<Aiutante> lista=new ArrayList<>();
		lista.add((Aiutante)ins.getOggettoInVendita());
		return new AiutanteGrafico(lista);
	}

	private ToggleButton creaPermessoVendibile(Inserzione ins) {
		return new PermessoGrafico((PermessoCostruzione)ins.getOggettoInVendita(),null, Math.floor(110*anchorImageMappa.getWidth()/1686.0));
	}

	private ToggleButton creaCartaVendibile(Inserzione ins) {
		return new CartaPoliticaGrafica((CartaPolitica)ins.getOggettoInVendita());

	}


	@Override
	public void visit(KingMutatio change) {
		this.king.setKing(change.getKing());
		this.consRe.getConsiglio().setProprietario(change.getKing());
		for(NodoCitta cittaPercorso:change.getPercorsoRe()){
			Optional<CittaGrafica> citta = this.cittaPresenti.stream().filter(citta1 -> 
				citta1.getCitta().equals(cittaPercorso)).findFirst();
			citta.ifPresent(cit -> king.spostaKing(cit));
		}
		
	}

	@Override
	public void visit(LineGiocatoreMutatio change) {
		this.showInfo("Info","Giocatore OFFLINE" ,change.toString());
		
	}

	
	@Override
	public void visit(Mutatio change) {

	}

	@Override
	public void visit(PartitaMutatio change) {
		this.inizioPartita();
	}


	@Override
	public void visit(QueryMutatio change) {
		this.showInfo("Info","Notifica risultato" ,change.toString());
		
	}

	@Override
	public void visit(StartConnessioneMutatio change) {
		//this.showInfo("Info","Connesione Creata" ,change.toString())
		//Noiosa, tolta
	}

	public void aggiornaGUIGiocatore(){
		//Aggiorno cartepolitiche
		this.carteGiocatore.clear();	
		this.carte.getChildren().clear();
		for(CartaPolitica carta:giocatore.getCartePolitiche()){
			//Non serve il toggle group ??
			CartaPoliticaGrafica cartaGrafica=new CartaPoliticaGrafica(carta);
			this.carteGiocatore.add(cartaGrafica);
		}
		this.carte.getChildren().addAll(carteGiocatore);
		
		//Aggiornamento num Aiutanti

		this.aiutanteGiocatore.setText(Integer.toString(giocatore.numeroAiutanti()));

		//Aggiornamento Empori
		this.nEmpori.setText(Integer.toString(giocatore.getNumeroEmpori()));
		
		//Aggiornamento permessiCostruzione nuovi
		for(PermessoCostruzione permesso:giocatore.getPermessiCostruzione()){
			if(!this.permessiGiocatore.stream().anyMatch(p->p.getPermessoCostruzione().equals(permesso))){
				PermessoGrafico grafPermesso=new PermessoGrafico(permesso, null, Math.floor(110*anchorImageMappa.getWidth()/1686.0));
				grafPermesso.setToggleGroup(this.permessiGiocatoriToggleGroup);
				this.permessiGiocatore.add(grafPermesso);
				this.permessi.getChildren().add(grafPermesso);
			}
		}
		
		//Elimino PermessiCostruzione non piu' presenti
		Iterator<PermessoGrafico> iteratorGPermessi=this.permessiGiocatore.stream().filter(p->
			!giocatore.getPermessiCostruzione().contains(p.getPermessoCostruzione())).
				iterator();
		while(iteratorGPermessi.hasNext())
			this.removePermessoGrafico(iteratorGPermessi.next(),this.permessiGiocatore,this.permessi);
		
		//Aggiornamento Permessi Usati
		for(PermessoCostruzione permesso:giocatore.getCartePermessoUsate()){
			if(!this.permessiUsatiGiocatore.stream().anyMatch(p->p.getPermessoCostruzione().equals(permesso))){
				PermessoGrafico grafPermesso=new PermessoGrafico(permesso, null, Math.floor(110*anchorImageMappa.getWidth()/1686.0));

				//Mettere il toggle di quelli usati
				grafPermesso.setToggleGroup(this.permessiGiocatoriToggleGroup);
				this.permessiUsatiGiocatore.add(grafPermesso);
				this.permessiUsati.getChildren().add(grafPermesso);
			}
		}
		
		//Elimino cartePermessoUsate non piu presenti
		iteratorGPermessi=this.permessiUsatiGiocatore.stream().filter(p->
			!giocatore.getCartePermessoUsate().contains(p.getPermessoCostruzione())).iterator();
		while(iteratorGPermessi.hasNext())
			this.removePermessoGrafico(iteratorGPermessi.next(), permessiUsatiGiocatore, permessiUsati);
		
		// Aggiornamento Percorsi
		// Dovrebbe essere gestita dalle mutatio senza problemi
		
	}
	
	@Override
	public void visit(StateMutatio change) {
		this.statoGioco.setText(change.getStato().toString());
		this.aggiornaGUIGiocatore();
	
	}

	private void removePermessoGrafico(PermessoGrafico gp, List<PermessoGrafico> listaPermessi, HBox boxPermessi) {
		listaPermessi.remove(gp);
		boxPermessi.getChildren().remove(gp);
	}

	@Override
	public void visit(TesseraBonusMutatio change) {
		TesseraBonusGrafica tessera;
		try {
			tessera = this.tessereBonusBanco.stream().filter(tes->
				tes.getTesseraBonus().equals(change.getTesseraBonus())).findFirst()
					.orElseThrow(()-> new ErrorUpdateServerException());
		this.tessereBonusBanco.remove(tessera);
		this.tBonus.getChildren().remove(tessera);
		if(this.giocatore.equals(modello.getStatoPartita().getGiocatoreCorrente())){
			this.tessereBonusGiocatore.add(tessera);
			this.oggetti.getChildren().add(tessera);
		}
		} catch (ErrorUpdateServerException e) {
			this.showError("Errore aggiornamento", "Tessera Bonus non coincidenti col server", e);
		}
		
	}

	@Override
	public void visit(UltimoTurnoMutatio change) {
		this.showInfo("Info","Ultimo Turno Partita" ,change.toString());
	}

	@Override
	public void visit(CambioPermessiMutatio change) {
		//Chiama la cambia permessi
		try {
			MazzoPermessiGrafico mazzoGrafico=this.mazziPresenti.stream().filter(mazzo-> 
				mazzo.getRegione().equals(change.getRegione())).findFirst().
					orElseThrow(()-> new ErrorUpdateServerException());
			mazzoGrafico.getRegione().setMazzoPermesso(change.getMazzo());
			mazzoGrafico.sostituisciPermessi();
		} catch (ErrorUpdateServerException e) {
			this.showError("Errore Aggiornamento dal server", "Mazzo non trovato", e);
		}
		
	}

	@Override
	public void visit(PermessoToltoMutatio change) {
		try {
			MazzoPermessiGrafico mazzoGrafico=this.mazziPresenti.stream().filter(mazzo-> 
				mazzo.getRegione().equals(change.getRegione())).findFirst().
					orElseThrow(()-> new ErrorUpdateServerException());
			mazzoGrafico.rimuoviPermesso(change.getPermesso());
		} catch (ErrorUpdateServerException e) {
			this.showError("Errore Aggiornamento dal server", "Mazzo non trovato", e);
		}

	}

	@Override
	public void visit(ChatMutatio change) {
		messaggi.setText(change.toString());
		messaggi.setScrollTop(Double.MAX_VALUE);
	}

	@Override
	public void visit(TempoScadutoMutatio change) {
		if(this.giocatore.equals(modello.getStatoPartita().getGiocatoreCorrente()))
			this.showInfo("Info","TimeOut" ,change.toString());	
	}

	@Override
	public void visit(InizioFaseMarketMutatio change) {
		this.inserzioniGrafiche.clear();
		this.stackFase.getChildren().clear();
		this.stackAzioniFase.getChildren().clear();
		this.stackAzioniFase.getChildren().add(azioniMarket);
		azioniMarket.setVisible(true);
		azioniBase.setVisible(false);
		this.stackFase.getChildren().add(market);
		market.setVisible(true);
		vistaMappa.setVisible(false);
		 
	}

	@Override
	public void visit(FineFaseMarketMutatio change) {
		this.inserzioniGrafiche.clear();
		this.stackFase.getChildren().clear();
		this.stackAzioniFase.getChildren().clear();
		this.stackAzioniFase.getChildren().add(azioniBase);
		azioniMarket.setVisible(false);
		azioniBase.setVisible(true);
		this.stackFase.getChildren().add(vistaMappa);
		market.setVisible(false);
		vistaMappa.setVisible(true);
		this.listaInserzioni.getChildren().clear();
		this.inserzioniGrafiche.clear();

	}
	
	@Override
	public void visit(PrimoGiocatoreMutatio mutatio){
			Platform.runLater(()-> this.mainGui.showParametri());
	}
	
	@Override
	public void visit(NomeNonDisponibileMutatio mutatio){
		TextInputDialog dialog = new TextInputDialog("Pluto");
		dialog.setTitle("Nome non disponibile");
		dialog.setHeaderText("Il nome che hai scelto e' gia' utilizzato. Scegline un altro");
		dialog.setContentText("Nuovo nome:");
		
		Optional<String> nome = dialog.showAndWait();
		nome.ifPresent(n -> this.loginPlayer = new Login(n));
		notifyObservers(loginPlayer);
		this.mainGui.getPrimaryStage().setTitle(this.mainGui.getPrimaryStage().getTitle() + " - " + this.loginPlayer.getNomeGiocatore());
	}
	
	@FXML
	public void cambiaVistaMappa(){
		stackFase.getChildren().clear();
		if(selezionaVista.isSelected()){
			stackFase.getChildren().add(vistaMappa);
			market.setVisible(false);
			vistaMappa.setVisible(true);
		}
		else
		{
			stackFase.getChildren().add(market);
			market.setVisible(true);
			vistaMappa.setVisible(false);
		}
	}
	
	
	/**
	 * Modifica La larghezza di tutti gli elementi della mappa in base alla percentuale passata
	 * @param percentuale
	 */
	public void widthResize(double percentuale){
		double width=this.anchorMappa.getWidth();
		this.anchorMappa.setPrefWidth(width*percentuale);
		
		width=this.king.getFitWidth();
		this.king.setFitWidth(width*percentuale);
				
		for(CittaGrafica c:this.cittaPresenti)
		{
			c.resizeX(percentuale);
		}
				
		for(ConsiglioGrafico cg:this.consigliPresenti){			
			cg.resizeX(percentuale);;			
		}
		
		for(MazzoPermessiGrafico m:this.mazziPresenti){			
			m.resizeX(percentuale);
		}			
		
		mappaGroup.setScaleX(mappaGroup.getScaleX()*percentuale);
	}
	
	/**
	 * Modifica La larghezza di tutti gli elementi della mappa in base alla percentuale passata
	 * @param percentuale
	 */
	public void heightResize(double percentuale){
		double height=this.anchorMappa.getHeight();
		this.anchorMappa.setPrefHeight(height*percentuale);
		height=this.king.getFitHeight();
		this.king.setFitHeight(height*percentuale);
		
		for(CittaGrafica c:this.cittaPresenti)
		{			
			c.resizeY(percentuale);
		}		
		
		for(ConsiglioGrafico cg:this.consigliPresenti){			
			cg.resizeY(percentuale);;			
		}
		
		for(MazzoPermessiGrafico m:this.mazziPresenti){		
			m.resizeY(percentuale);
		}
		mappaGroup.setScaleY(mappaGroup.getScaleY()*percentuale);
		
	}
	
	@FXML
	public void schermoIntero(){
		mainGui.getPrimaryStage().setFullScreen(true);
	}

	@Override
	public void visit(MescolatoMutatio change) {
		//Chiama la cambia permessi
				try {
					MazzoPermessiGrafico mazzoGrafico=this.mazziPresenti.stream().filter(mazzo-> 
						mazzo.getRegione().equals(change.getRegione())).findFirst().
							orElseThrow(()-> new ErrorUpdateServerException());
					mazzoGrafico.getRegione().setMazzoPermesso(change.getMazzo());
					mazzoGrafico.sostituisciPermessi();
				} catch (ErrorUpdateServerException e) {
					this.showError("Errore Aggiornamento dal server", "Mazzo non trovato", e);
				}
		
	}
	
	

}
