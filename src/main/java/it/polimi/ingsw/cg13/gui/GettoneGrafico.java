package it.polimi.ingsw.cg13.gui;

import java.util.Collection;

import it.polimi.ingsw.cg13.bonus.Ricompensa;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;

public class GettoneGrafico extends HBox {

	public GettoneGrafico(Collection<Ricompensa> ricompense, double diametro) {
		super();
		this.setStyle("-fx-background-image: url("+getClass().getResource("/gui/image/bonus/gettone_base.png").toString()+"); -fx-background-repeat: stretch; -fx-background-size: 100% 100%;");
		ricompense.forEach(r -> this.getChildren().add(new RicompensaGrafica(r, diametro/2, diametro/2)));
		
//		this.getChildren().forEach(c -> c.setScaleX(0.7));
//		this.getChildren().forEach(c -> c.setScaleY(0.7));
		this.setAlignment(Pos.CENTER);
		this.setPrefSize(diametro, diametro);
	}
}
