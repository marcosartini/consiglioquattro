package it.polimi.ingsw.cg13.gui;

import java.util.ArrayList;
import java.util.Optional;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;


public class CasellaNobiltaGrafica extends CasellaGrafica<CasellaNobilta>{

	
	
	private HBox ricompense;

	
	public CasellaNobiltaGrafica(CasellaNobilta casella) {
		super(casella, "Nobilta");

		this.ricompense = new HBox(3);
		this.ricompense.setAlignment(Pos.CENTER);

		ArrayList<RicompensaGrafica> gRicompense = new ArrayList<>();
		this.casella.getRicompense().forEach(r -> gRicompense.add(new RicompensaGrafica(r, 20, 20)));
		gRicompense.forEach(gr -> gr.setScaleX(2));
		gRicompense.forEach(gr -> gr.setScaleY(2));
		this.ricompense.getChildren().addAll(gRicompense);
		

		this.baseNodo.setAlignment(Pos.CENTER);
		this.baseNodo.getChildren().add(0,ricompense);
		
		this.baseNodo.setPrefHeight(126);
		this.baseNodo.setPrefWidth(63);


	}
	
	public void aggiungiGiocatore(Giocatore giocatore) {
		PedinaGrafica pedina = new PedinaGrafica(giocatore);
		this.pedine.add(pedina);
		this.pedineHBox.getChildren().add(pedina);
	}
	public void rimuoviGiocatore(Giocatore giocatore){
		Optional<PedinaGrafica> pedina = this.pedine.stream().filter(p -> p.getGiocatore().equals(giocatore)).findFirst();
		this.pedineHBox.getChildren().remove(pedina);
	}

	
	public CasellaNobilta getCasella() {
		return casella;
	}

}
