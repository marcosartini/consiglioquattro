package it.polimi.ingsw.cg13.gui;

import com.sun.javafx.scene.control.skin.ToggleButtonSkin;

import javafx.scene.control.ToggleButton;



@SuppressWarnings("restriction")
public class CDQToggleButtonSkin extends ToggleButtonSkin {

	String css = this.getClass().getResource("/gui/css/customToggle.css").toExternalForm();
	ToggleButton toggleButton;
	public CDQToggleButtonSkin(ToggleButton toggleButton) {
		super(toggleButton);
		this.toggleButton = toggleButton;
		
		this.toggleButton.getStylesheets().clear();
		this.toggleButton.getStylesheets().add(css);
	}
	
	

}
