package it.polimi.ingsw.cg13.gui;

import it.polimi.ingsw.cg13.bonus.TesseraBonus;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class TesseraBonusGrafica extends ImageView {
	
	TesseraBonus tesseraBonus;

	public TesseraBonusGrafica(TesseraBonus tesseraBonus) {
		super(new Image(TesseraBonusGrafica.class.getResource("/gui/image/bonus/"+tesseraBonus.toString()+".png").toString()));
		this.tesseraBonus = tesseraBonus;
		this.setFitWidth(60);
		this.setFitHeight(30);
		
	}
	
	public TesseraBonus getTesseraBonus() {
		return tesseraBonus;
	}
}
