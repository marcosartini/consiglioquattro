package it.polimi.ingsw.cg13.gui;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.mappa.Regione;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

public class MazzoPermessiGrafico extends AnchorPane{

	@FXML
	private HBox portaCarte;
	private ToggleGroup permessiEsposti;
	private Regione regione;
	private ArrayList<PermessoGrafico> permessi;
	private List<PermessoCostruzione> permessiCostruzioneVisibili;
	private double widthPermesso;
	public MazzoPermessiGrafico(Regione regione, ToggleGroup permessiEsposti, double widthPermesso, double layoutX, double layoutY) {
		super();
		this.portaCarte=new HBox(widthPermesso*0.1);
		this.portaCarte.setLayoutX(widthPermesso*1.2 + widthPermesso*0.1);
		this.permessiEsposti = permessiEsposti;
		ImageView retro = new ImageView(getClass().getResource("/gui/image/permessi/retro_"+regione.getNome()+".png").toString());
		retro.setFitHeight(widthPermesso*1.2);
		retro.setFitWidth(widthPermesso);
		this.regione = regione;
		this.getChildren().add(retro);
		this.permessi = new ArrayList<>();
		this.permessiCostruzioneVisibili = new ArrayList<>();
		this.widthPermesso = widthPermesso;
		settaPermessi(this.regione.getMazzoPermesso().getCarteInCima(2));
		this.getChildren().add(portaCarte);
		
		this.setLayoutX(layoutX);
		this.setLayoutY(layoutY);
		
	}
	
	public void rimuoviPermesso(PermessoCostruzione permesso){
		settaPermessi(this.regione.getMazzoPermesso().getCarteInCima(2));
		
		
	}
	public void sostituisciPermessi(){
		this.permessiCostruzioneVisibili = this.regione.getMazzoPermesso().getCarteInCima(2);

		settaPermessi(this.permessiCostruzioneVisibili);
	}
	
	private void settaPermessi(List<PermessoCostruzione> visibili){
		if(!permessi.isEmpty()){
			permessi.clear();
		}
		this.portaCarte.getChildren().clear();
		visibili.forEach(p -> this.permessi.add(new PermessoGrafico(p, regione, widthPermesso)));
		permessi.forEach(p -> p.setToggleGroup(permessiEsposti));
		
		this.portaCarte.getChildren().addAll(this.permessi);

	}
	
	public PermessoCostruzione getPermessoSelezionato(){
		return (PermessoCostruzione)this.permessiEsposti.getSelectedToggle();
	}
	
	public Regione getRegione() {
		return regione;
	}
	
	public void resizeX(double percentuale){
		double temp;		
		
		temp=this.getScaleX();
		this.setScaleX(temp*percentuale);
		
		for(int i=0;i<this.getChildren().size();i++){
			temp=this.getChildren().get(i).getScaleX();
			temp=temp*percentuale;
			if(temp<1.2 && temp>0.8)
				this.getChildren().get(i).setScaleX(temp);	
		}
	}
	
	public void resizeY(double percentuale){
		double temp;		
		
		temp=this.getScaleY();
		this.setScaleY(temp*percentuale);
		
		for(int i=0;i<this.getChildren().size();i++){
			temp=this.getChildren().get(i).getScaleY();
			temp=temp*percentuale;
			if(temp<1.2 && temp>0.8)
				this.getChildren().get(i).setScaleY(temp);	
		}
			
	}
}
