package it.polimi.ingsw.cg13.gui;

import java.util.LinkedList;
import java.util.List;

import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;

public class PercorsoVittoriaGrafico extends HBox {

	private PercorsoVittoria percorsoV;
	private List<CasellaGrafica<CasellaVittoria>> caselle;
	
	public PercorsoVittoriaGrafico(PercorsoVittoria percorso) {
		super(0);
		this.percorsoV = percorso;
		this.caselle = new LinkedList<>();
		this.setAlignment(Pos.TOP_LEFT);
		percorso.getPercorso().forEach(c -> caselle.add(new CasellaGrafica<CasellaVittoria>(c, "Vittoria")));
		
		this.getChildren().addAll(caselle);
	}
	
	public PercorsoVittoria getPercorsoV() {
		return percorsoV;
	}
	
	public List<CasellaGrafica<CasellaVittoria>> getCaselle() {
		return caselle;
	}
}
