package it.polimi.ingsw.cg13.gui;

import java.awt.Toolkit;
import java.io.IOException;

import it.polimi.ingsw.cg13.comandi.Login;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MainGUI extends Application{

    private Stage primaryStage;
    private Stage dialogStage;
    
    private BorderPane rootLayout;

   	private Login loginPlayer;
   	
    ClientGUIController controller;
    
    public MainGUI() {
    	super();

	}

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Consiglio dei Quattro");
  
   //   this.primaryStage.setOnCloseRequest(value); per chiedergli se vuole davvero uscire, da mettere la window
      this.primaryStage.getIcons().add(new Image(MainGUI.class.getResource("/gui/image/icona.png").toString()));

        
        
        initRootLayout();

        showPartita();
        
        

    }

    /**
     * Initializes the root layout.
     */
    
	public void initRootLayout() {
            rootLayout = new BorderPane();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);    

            primaryStage.setScene(scene);
            primaryStage.show();
            
    }

    /**
     * Shows the person overview inside the root layout.
     */
    public void showLogin() {
    	
        try {
            // Load login overview
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/gui/layout/loginLayout.fxml"));
            AnchorPane loginForm = (AnchorPane) loader.load();

            // Create the dialog Stage.
            dialogStage = new Stage();
            dialogStage.setTitle("Login");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(loginForm);
            dialogStage.setScene(scene);

         // Give the controller access to the main app.
            LoginGUIController loginGui = loader.getController();
            loginGui.setMainGui(this);
   
            loginGui.setLoginStage(dialogStage);
            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            

            this.loginPlayer=loginGui.getLogin();
            primaryStage.setTitle(primaryStage.getTitle() + " - " + this.loginPlayer.getNomeGiocatore());
            dialogStage.close();
 
            
        } catch (IOException e) {
            visualizzaErrore(e);
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    private void showPartita(){
    	try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainGUI.class.getResource("/gui/layout/basicLayout.fxml"));
            AnchorPane gioco = (AnchorPane) loader.load();

         // Give the controller access to the main app.
            controller = loader.getController();
          controller.setMainGui(this);
          showLogin();
            
            rootLayout.setCenter(gioco);
          
            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				
				@Override
				public void handle(WindowEvent event) {
					controller.exitCommand();
					termina();
				}
			});
        } catch (IOException e) {
           	visualizzaErrore(e);
        }
    	return;
    }

	
	
	protected void termina() {
			Platform.exit();
	}

	public void visualizzaErrore(Exception errore) {
		Alert alert = new Alert(AlertType.ERROR);
        alert.initOwner(primaryStage);
        alert.setTitle("Problema");
        alert.setHeaderText(errore.getMessage());
        alert.showAndWait();
		
	}


	
	 public static void main(String[] args) {
	        launch(args);
	    }

	
	public ClientGUIController getController() {
		return controller;
	}
	

	
	public void showParametri() {
		
		 try {
	            // Load login overview
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(MainGUI.class.getResource("/gui/layout/selezioneParametriLayout.fxml"));
	            AnchorPane parametriForm = (AnchorPane) loader.load();

	            // Create the dialog Stage.
	            dialogStage = new Stage();
	            dialogStage.setTitle("Selezione parametri della partita");
	            dialogStage.initModality(Modality.WINDOW_MODAL);
	            dialogStage.initOwner(primaryStage);
	            Scene scene = new Scene(parametriForm);
	            dialogStage.setScene(scene);

	         // Give the controller access to the main app.
	           	ParametriController paramGui = loader.getController();
	           	paramGui.setGuiController(this.controller);
	   
	           	paramGui.setParametriStage(dialogStage);
	            // Show the dialog and wait until the user closes it
	            dialogStage.showAndWait();

	            dialogStage.close();

	            
	        } catch (IOException e) {
	            visualizzaErrore(e);
	        }

		 
	}
	
	public void showClassifica(AnchorPane classifica){
		rootLayout.setCenter(classifica);
	}
	
	
	public void setAlert(Alert alert, String title, String header, String message) {
		alert.initOwner(primaryStage);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        Toolkit.getDefaultToolkit().beep();
        alert.showAndWait();
	}
	
}
