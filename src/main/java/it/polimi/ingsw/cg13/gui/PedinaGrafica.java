package it.polimi.ingsw.cg13.gui;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class PedinaGrafica extends Rectangle {
	private Giocatore giocatore;
	
	public PedinaGrafica(Giocatore giocatore) {
		super();
		this.giocatore = giocatore;
		Colore colore = giocatore.getColore();
		this.setFill(Color.rgb(colore.getRed(), colore.getGreen(), colore.getBlue()));

		this.setWidth(10);
		this.setHeight(15);
		this.setLayoutX(10);
		this.setLayoutY(10);
	}
	
	public Giocatore getGiocatore() {
		return giocatore;
	}
}
