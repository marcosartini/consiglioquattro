package it.polimi.ingsw.cg13.gui;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


import it.polimi.ingsw.cg13.punteggio.CalcolatorePunteggio;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ClassificaController {


	    @FXML
	    private ResourceBundle resources;

	    @FXML
	    private URL location;

	    @FXML
	    private TableView<PunteggioGiocatore> tabella;

	    @FXML
	    private TableColumn<PunteggioGiocatore, String> giocatore;

	    @FXML
	    private TableColumn<PunteggioGiocatore, Integer> posizione;

	    @FXML
	    private TableColumn<PunteggioGiocatore, Integer> punti;

	    ObservableList<PunteggioGiocatore> elenco;

	    @FXML
	    void initialize() {
	    	//niente da fare perche' aspetta riempimento da controller gui
	    }
	    
	    public void riempiElenco(CalcolatorePunteggio calcolatorePunteggio){

	    	ArrayList<PunteggioGiocatore> elencoPerTabella = new ArrayList<>();
	    	calcolatorePunteggio.getPunteggi().forEach(p -> elencoPerTabella.add(new PunteggioGiocatore(calcolatorePunteggio.getPunteggi().indexOf(p)+1, p.getGiocatore().getNome(), p.getPunteggio())));
	    	elenco = FXCollections.observableArrayList(elencoPerTabella);
	    	posizione.setCellValueFactory(new PropertyValueFactory<>("posizione"));
	    	giocatore.setCellValueFactory(new PropertyValueFactory<>("nome"));
	    	punti.setCellValueFactory(new PropertyValueFactory<>("punti"));

	    	tabella.setItems(elenco);
	    }
	    
//	    public void evidenziaGiocatore(Giocatore giocatore){
//	    	Optional<Label> ris = elenco.getItems().stream().filter(label -> label.getText().startsWith(giocatore.getNome())).findFirst();
//	    	ris.ifPresent(l -> l.setFont(Font.font(null, FontWeight.BOLD, 16)));
//	    }

	    
	    public class PunteggioGiocatore {
	    	private final IntegerProperty posizione;
	        private final SimpleStringProperty nome;
	        private final IntegerProperty punti;
	     
	        private PunteggioGiocatore(Integer posizione, String nome, Integer punti) {
	            this.posizione = new SimpleIntegerProperty(posizione);
	            this.nome = new SimpleStringProperty(nome);
	            this.punti = new SimpleIntegerProperty(punti);
	        }
	     
	        public Integer getPosizione() {
	            return posizione.get();
	        }
	   
	            
	        public String getNome() {
	            return nome.get();
	        }

	        
	        public Integer getPunti() {
	            return punti.get();
	        }
	
	            
	    }
}

