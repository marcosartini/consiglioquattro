package it.polimi.ingsw.cg13.gui;

import it.polimi.ingsw.cg13.carte.CartaPolitica;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;

public class CartaPoliticaGrafica extends ToggleButton {

	CDQToggleButtonSkin consigliereSkin = new CDQToggleButtonSkin(this);
	
	CartaPolitica carta;
	
	public CartaPoliticaGrafica(CartaPolitica cartaPolitica) {
		super();
		this.carta = cartaPolitica;
		this.setGraphic(new ImageView(CartaPoliticaGrafica.class.getResource("/gui/image/carte/politica_" + carta.getColor().getNome() + ".png").toString()));
		this.getGraphic().setScaleX(0.5);
		this.getGraphic().setScaleY(0.5);
		this.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		this.setMinSize(0,0);
		this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
	}
	
	public CartaPolitica getCarta() {
		return carta;
	}
}
