package it.polimi.ingsw.cg13.azionimarket;

import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateInizioVendita;
import it.polimi.ingsw.cg13.mutatio.InserzioneInVenditaMutatio;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class VendiInserzione extends AzioneMarket{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3186150704237928548L;
	private static final Logger logger = LoggerFactory.getLogger(VendiInserzione.class);

	/**
	 * Costruttore dell'azione di vendita di un inserzione
	 * @param giocatore: giocatore che sta vendendo l'inserzione
	 * @param inserzione: inserzione da vendere
	 */
	public VendiInserzione(Giocatore giocatore,Inserzione inserzione) {
		super(giocatore,inserzione);
	}
	
	/**
	 * {@inheritDoc}
	 * mette in vendita nel market l'inserzione con oggetto e prezzo 
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws AzioneNotPossibleException {
		this.giocatore.mettiVendita(partita.getStatoPartita().getMarket(),this.inserizione);
		try {
			partita.notifyObservers(new InserzioneInVenditaMutatio(this.inserizione));
		} catch (RemoteException | CdQException e) {
			logger.error(e.getMessage(),e);
			//Questa viene loggata sul server
		}
		return true;
	}


	/**
	 * {@inheritDoc}
	 * Aggiorna l'inserzione inviata dal client con quella presente sul server
	 */
	@Override
	public void updateParametri(Partita partita) throws CdQException {
		super.updateGiocatore(partita);
		this.inserizione.setProprietario(this.giocatore);
		this.inserizione.getOggettoInVendita().updateFromServer(this.giocatore, this.inserizione);
	}
	
	/**
	 * {@inheritDoc}
	 * Ritorna nello stato di vendita finche giocatore non fa finisce turno
	 */
	@Override
	public State visit(StateInizioVendita stato,Partita partita) throws AzioneNotPossibleException{
		if(this.eseguiAzione(partita))
			return stato;
		else throw new AzioneNotPossibleException("ERROR: Azione non conclusa");
	}

}
