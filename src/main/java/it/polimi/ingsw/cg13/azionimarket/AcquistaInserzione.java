package it.polimi.ingsw.cg13.azionimarket;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateAcquistoInizio;
import it.polimi.ingsw.cg13.mutatio.InserzioneAcquistataMutatio;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.oggetti.Market;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class AcquistaInserzione extends AzioneMarket {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9130113021810153548L;
	
	private static final Logger logger = LoggerFactory.getLogger(AcquistaInserzione.class);
	
	/**
	 * Costruttore di un'azione di acquisto 
	 * @param giocatore che vuole acquistare l'inserzione
	 * @param inserzione: inserzione da acquistare
	 */
	public AcquistaInserzione(Giocatore giocatore,Inserzione inserzione) {
		super(giocatore,inserzione);
		
	}

	/**
	 * {@inheritDoc}
	 * Preleva l'inserzione scelta dal market
	 */
	@Override
	public boolean eseguiAzione(Partita partita) throws AzioneNotPossibleException {
		Market market=partita.getStatoPartita().getMarket();
		if(this.giocatoreCanbuy(market)){
			try {
				market.compraInserzione(this.giocatore,this.inserizione,partita.getPercorsoRicchezza());
				partita.notifyObservers(new InserzioneAcquistataMutatio(this.inserizione,this.giocatore));
			} catch (RemoteException | CdQException e) {
				logger.error(e.getMessage(),e);
			}
			return true;
		}
		else throw new AzioneNotPossibleException("Non hai abbastanza punti ricchezza per acquistare l'articolo scelto");
	
	}


	/**
	 * {@inheritDoc}
	 * Aggiorna l'inserzione inviata dal client con quella presente sul server
	 */
	@Override
	public void updateParametri(Partita partita) throws ErrorUpdateServerException {
		super.updateGiocatore(partita);
		List<Inserzione> inserzioniMarket=new ArrayList<>();
		try {
			inserzioniMarket = partita.getStatoPartita().getMarket().getBanchetto();
		} catch (AzioneNotPossibleException e) {
			partita.notifyObservers(new ErrorUpdateServerException(e));
		}
		if(inserzioniMarket.isEmpty())
			throw new ErrorUpdateServerException("Non ci sono inserzioni da comprare");
		else{
			this.inserizione=inserzioniMarket.
					stream().filter(ins->ins.getIdInserzione()==this.inserizione.getIdInserzione()).
					findFirst().orElseThrow(()->new ErrorUpdateServerException("L'inserzione con questo id non esiste"));
		}
		
	}
	
	/**
	 * Ritorna true se il giocatore puo' comrare l'inserzione altrimenti false
	 * @param market: market con il banchetto delle inserzioni
	 * @return true se il giocatore ha abbastanza monete 
	 */
	private boolean giocatoreCanbuy(Market market){			
		if(this.giocatore.getRicchezzaGiocatore()>=this.inserizione.getPrezzo().getPuntiCasella()
				&& !market.getBanchetto().isEmpty()){
			return true;
		}
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * Ritorna nello stato di acquisto finche giocatore non fa finisce turno
	 */
	@Override
	public State visit(StateAcquistoInizio stato,Partita partita) throws AzioneNotPossibleException{
		if(this.eseguiAzione(partita))
			return stato;
		else throw new AzioneNotPossibleException("ERROR: Azione non conclusa");
	}

}
