package it.polimi.ingsw.cg13.azionimarket;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public abstract class AzioneMarket extends Azione{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4325385464264050399L;
	/**
	 * inserzione dell'azione
	 */
	Inserzione inserizione;
	
	/**
	 * Costruttore di un'azione market generica
	 * @param giocatore che sta vendendo o comprando l'inserzione
	 * @param ins: inserzione venduta o comprata dal giocatore
	 */
	public AzioneMarket(Giocatore giocatore,Inserzione ins) {
		super(giocatore);
		this.inserizione=ins;
	}

	/**
	 * 
	 * @return inserzione scelta dal giocatore
	 */
	public Inserzione getInserizione() {
		return inserizione;
	}



}
