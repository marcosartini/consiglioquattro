package it.polimi.ingsw.cg13.colore;

import java.awt.Color;

public class Colore extends Color {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1463371881765207536L;
	String nome;
	/**
	 * Costruttore di un colore
	 * @param nome del colore
	 * @param r: intero corrispondente al rosso
	 * @param g:intero corrispondente al verde
	 * @param b; intero corrispondente al blu
	 * @throws IllegalArgumentException--> valori nulli
	 */
	public Colore(String nome,int r, int g, int b) {
		
		super(r,g,b);
		if(nome!=null && !nome.isEmpty())
			this.nome=nome;
		else
			throw new IllegalArgumentException("Nome del colore non valido!");
	}
	/**
	 * Setta il colore passato come parametro
	 * @param color
	 */
	public Colore(int color){
		super(color);
	}
	
	/**
	 * to string classe--> restituisce RGB
	 * @return RGB del colore
	 */
	public String toStringRGB() {
		return "Colore=["+this.getRed()+"-"+this.getGreen()+"-"+this.getBlue()+"]";
	}
	
	/**
	 * Permette di settare il nome del colore
	 * @param nome
	 * @throws IllegalArgumentException--> valore nullo
	 */
	public void setNome(String nome)
	{
		if(nome!=null && !nome.isEmpty())
			this.nome=nome;
		else throw new IllegalArgumentException("Il nome da settare non può essere nullo!");
	}
	
	/**
	 * Restituisce il nome del colore
	 * @return nome
	 */
	public String getNome()
	{
		return this.nome;
	}
	
	
	@Override
	/**
	 * to string classe--> restituisce Nome
	 * @return nome del colore
	 */
	public String toString() {
		return nome;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Colore other = (Colore) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
	
	
}
