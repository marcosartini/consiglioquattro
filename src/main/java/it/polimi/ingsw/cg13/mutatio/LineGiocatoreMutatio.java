package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class LineGiocatoreMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8327977410584494709L;
	
	private final Giocatore giocatore;
	
	public LineGiocatoreMutatio(Giocatore giocatore) {
		this.giocatore=giocatore;
	}
	
	@Override
	public String toString() {
		return super.toString()+"LineGiocatore_"+this.giocatore.getNome()+
				": [ online="+this.giocatore.getIsOnline()+" ]";
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws ErrorUpdateServerException {
		partita.updateGiocatore(giocatore);
		//Rimuovo da giocatri online e aggiungo alla lista dei giocatori offline
		partita.getGiocatoriOffline().add(giocatore);
		partita.getGiocatori().remove(giocatore);
		return partita;
	}
	
	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}


}
