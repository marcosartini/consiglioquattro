package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;


public class BonusLosedMutatio extends Mutatio {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 3308582603626561800L;

	@Override
	/**
	 * @param partita
	 * @return Partita
	 */
	public Partita changeModel(Partita partita) throws CdQException {
		return partita;
	}

	@Override
	/**
	 * ToString della classe
	 */
	public String toString() {
		return "BonusLosedMutatio: Non hai citta o permessi da riutilizzare";
	}

	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

}
