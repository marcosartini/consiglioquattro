package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.punteggio.CalcolatorePunteggio;

public class ClassificaMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3901209466201657155L;
	CalcolatorePunteggio calcolo;
	
	public ClassificaMutatio(CalcolatorePunteggio calc) {
		this.calcolo=calc;
	}

	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.setEnded(true);
		return partita;
	}

	@Override
	public String toString() {
		return calcolo.toString();
	}

	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

	public CalcolatorePunteggio getCalcolo() {
		return calcolo;
	}
}
