package it.polimi.ingsw.cg13.mutatio;

import java.util.List;

import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.King;

public class KingMutatio extends Mutatio {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6837644518323777761L;
	
	private final King king;
	private List<NodoCitta> percorsoRe;
	
	public KingMutatio(King king, List<NodoCitta> kingPath) {
		this.king=king;
		this.percorsoRe=kingPath;
	}
	
	@Override
	public String toString() {
		return super.toString()+"King: [citta="+this.king.getPosizione().getCittaNodo().getNome()+"]";
	}

	
	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) {
		partita.setKing(king);
		return partita;
	}
	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

	/**
	 * Restituisce il re
	 * @return king
	 */
	public King getKing() {
		return king;
	}

	/**
	 * Restituisce il percorso compiuto dal re
	 * @return percorsoRe
	 */
	public List<NodoCitta> getPercorsoRe() {
		return percorsoRe;
	}
}
