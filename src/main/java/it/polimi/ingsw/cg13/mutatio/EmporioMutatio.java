package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class EmporioMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7965994404438970264L;
	
	private final Emporio emporio;
	
	public EmporioMutatio(Emporio emporio) {
		this.emporio=emporio;
	}
	
	/**
	 * Restituisce il proprietario dell' emporio
	 * @return GiocatoreAppartenente
	 */
	public Giocatore getGiocatore(){
		return emporio.getGiocatoreAppartenente();
	}
	
	@Override
	public String toString() {
		return super.toString()+"Emporio: [giocatore="+this.emporio.getGiocatoreAppartenente().getNome()+
				" citta="+this.emporio.getCittaPosizionato().getNome()+"]";
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.updateGiocatore(emporio.getGiocatoreAppartenente());
		partita.getMappaPartita().getNodoCitta(emporio.getCittaPosizionato())
				.setCittaNodo(emporio.getCittaPosizionato());
		return partita;
	}

	/**
	 * Restituisce l' emporio
	 * @return emporio
	 */
	public Emporio getEmporio() {
		return emporio;
	}

	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

	
}
