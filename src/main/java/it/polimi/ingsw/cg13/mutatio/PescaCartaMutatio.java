package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.carte.Carta;
import it.polimi.ingsw.cg13.carte.Mazzo;
import it.polimi.ingsw.cg13.partita.Partita;

public class PescaCartaMutatio<C extends Carta> extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 574519778367379794L;
	
	private final Mazzo<C> mazzo;
	
	public PescaCartaMutatio(Mazzo<C> mazzo) {
		this.mazzo=mazzo;
	}
	
	@Override
	public String toString() {
		return super.toString()+"Mazzo"+mazzo.getClass().getSimpleName()+": ["+
				"carte="+this.mazzo.getMazzoCarte().size()+"]";
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) {
		partita.getBanco().getMazzoPolitico().pescaCarta();
		return partita;
	}

}
