package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class QueryMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2829676792992590159L;
	private String result;
	private Giocatore giocatore;
	
	public QueryMutatio(String result,Giocatore giocatore){
		this.result=result;
		this.giocatore=giocatore;
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws ErrorUpdateServerException {
		partita.updateGiocatore(giocatore);
		return partita;

	}

	@Override
	public String toString() {
		return  result;
	}

	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

}
