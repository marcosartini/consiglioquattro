package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;


public class PartitaMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -976009772468813956L;
	Partita modello;
	String tipoPartita;
	
	public PartitaMutatio(Partita gioco,String tipo) {
		this.modello=gioco;
		this.tipoPartita=tipo;
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		return modello;
	}

	@Override
	public String toString() {
		return super.toString() + tipoPartita;
	}

	/**
	 * Restituisce il modello della partita
	 * @return modello
	 */
	public Partita getModello() {
		return modello;
	}
	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

}
