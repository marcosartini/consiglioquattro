package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.partita.Partita;


public class CittaMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1935459311389964590L;
	
	private final Citta citta;
	private final Emporio nuovoEmporio;
	
	public CittaMutatio(Citta citta,Emporio emporio) {
		this.citta=citta;
		this.nuovoEmporio=emporio;
	}
	
	@Override
	public String toString() {
		String stringa=super.toString()+" Citta: "+citta.getNome()+"\nEmporiPresenti: (";
		for(Emporio emporio:citta.getEmpori()){
			stringa+=emporio.getGiocatoreAppartenente().getNome()+", ";			
		}
		stringa+=")";
		return stringa;
	}

	@Override
	public Partita changeModel(Partita partita) throws CdQException {		
		partita.getMappaPartita().getNodoCitta(citta).setCittaNodo(citta);
		return partita;
	}

	/**
	 * Restituisce la città
	 * @return citta
	 */
	public Citta getCitta() {
		return citta;
	}
	
	/**
	 * restituicse il nuovoEmporio
	 * @return nuovoEmporio
	 */ 
	public Emporio getNuovoEmporio() {
		return nuovoEmporio;
	}

	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

	

}
