package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;

public class InizioFaseMarketMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4147274786634019577L;


	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		return partita;
	}


	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}


	/**
	 * toString della classe
	 */
	@Override
	public String toString() {
		return "Inizio fase di Market";
	}

}
