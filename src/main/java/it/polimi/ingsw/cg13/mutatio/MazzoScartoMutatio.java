package it.polimi.ingsw.cg13.mutatio;

import java.util.List;

import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;

public class MazzoScartoMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5532418488145379486L;
	List<CartaPolitica> carteScarto;
	
	public MazzoScartoMutatio(List<CartaPolitica> carte) {
		carteScarto=carte;
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.getBanco().getMazzoScarto().getMazzoCarte().addAll(carteScarto);
		return partita;
	
	}

	@Override
	public String toString() {
		return "MazzoScartoMutatio carte scartate: " +carteScarto ;
	}

}
