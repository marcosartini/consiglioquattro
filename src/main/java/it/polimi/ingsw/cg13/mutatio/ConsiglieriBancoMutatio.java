package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

public class ConsiglieriBancoMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8086650753744914748L;
	private Consigliere consigliereTolto;
	private Consigliere consigliereAggiunto;
	
	public ConsiglieriBancoMutatio(Consigliere consTolto,Consigliere consAgg){
		this.consigliereTolto=consTolto;
		this.consigliereAggiunto=consAgg;
	}
	
	/**
	 * Cambia il modello della partita modificando i consiglier--> aggiunge/toglie
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.getBanco().accumulaConsigliere(consigliereAggiunto);
		partita.getBanco().getConsigliere(consigliereTolto.getColor());
		return partita;
	}


	/**
	 * toString della classe
	 */
	@Override
	public String toString() {
		return "ConsiglieriBancoMutatio consigliere tolto: " + consigliereTolto.getColor() + ", consigliere aggiunto: "
				+ consigliereAggiunto.getColor();
	}

	/**
	 * restituisce il consigliere tolto
	 * @return consigliereTolto
	 */
	public Consigliere getConsigliereTolto() {
		return consigliereTolto;
	}


	/**
	 * restituisce il consigliere aggiunto
	 * @return consigliereAggiunto
	 */
	public Consigliere getConsigliereAggiunto() {
		return consigliereAggiunto;
	}
	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}


}
