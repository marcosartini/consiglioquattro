package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;

public class StartConnessioneMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 67100395397009933L;
	String tipoConnessione;
	
	public StartConnessioneMutatio(String con) {
		tipoConnessione=con;
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		return partita;
	}

	@Override
	public String toString() {
		return "Creata connessione "+tipoConnessione+" con il server\n"
				+"Attendi l'inizio della partita";
	}

	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

	
}
