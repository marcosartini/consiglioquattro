package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.bonus.TesseraBonus;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;

public class TesseraBonusMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4766931495175224407L;
	private TesseraBonus tesseraBonus;
	
	public TesseraBonusMutatio(TesseraBonus tesseraBonus) {
		this.tesseraBonus=tesseraBonus;
	}
	
	public TesseraBonus getTesseraBonus() {
		return tesseraBonus;
	}
	
	@Override
	public String toString() {
		return super.toString()+ this.tesseraBonus.getClass().getSimpleName()+"_"+
				this.tesseraBonus.getPunteggioCaselleVittoria()+": [giocatore="+this.tesseraBonus.getGiocatore().getNome()+"]";
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws ErrorUpdateServerException {
		partita.updateGiocatore(tesseraBonus.getGiocatore());
		partita.getBanco().getTessereBonus().remove(tesseraBonus);
		partita.getBanco().getGruppoBonusRe().getGruppo().remove(tesseraBonus);
		return partita;
	}
	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

}
