package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class InserzioneAcquistataMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6380594221355038146L;
	Inserzione inserzione;
	Giocatore giocatore;
	
	public InserzioneAcquistataMutatio(Inserzione ins,Giocatore gioc) {
		this.inserzione=ins;
		this.giocatore=gioc;
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.updateGiocatore(giocatore);
		partita.updateGiocatore(inserzione.getProprietario());
		partita.getStatoPartita().getMarket().getBanchetto().remove(inserzione);
		return partita;
	}
	
	/**
	 * toString della classe
	 */
	@Override
	public String toString() {
		return "InserzioneAcquistataMutatio@ " + inserzione+
				"\nE' stata acquistata da "+giocatore.getNome();
	}

	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

	/**
	 * Restituisce l' inserzione interessata
	 * @return inserzione
	 */
	public Inserzione getInserzione() {
		return inserzione;
	}

	/**
	 * Restituisce il giocatore interessata
	 * @return giocatore
	 */ 
	public Giocatore getGiocatore() {
		return giocatore;
	}


}
