package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.partita.Partita;

public class MescolatoMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7613695962974992568L;
	private MazzoPermessi mazzo;
	private Regione regione;
	
	public MescolatoMutatio(MazzoPermessi mazzoPermesso, Regione regione) {
		this.mazzo=mazzoPermesso;
		this.regione=regione;
	}

	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.getRegione(regione).setMazzoPermesso(mazzo);
		return partita;
	}
	
	@Override
	public void accept(ClientGUIController gui){
		gui.visit(this);
	}

	public MazzoPermessi getMazzo() {
		return mazzo;
	}

	public Regione getRegione() {
		return regione;
	}

}
