package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.partita.Partita;


public class CambioPermessiMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5612880101729323338L;
	private MazzoPermessi mazzo;
	private Regione regione;

	public CambioPermessiMutatio(MazzoPermessi mazzo,Regione reg) {
		this.mazzo=mazzo;
		this.regione=reg;
	}

	/**
	 * toString della classe
	 */
	@Override
	public String toString() {
		return super.toString()+"MazzoPermessi: ["+
				"carte="+this.mazzo.getMazzoCarte().size()+"]";
	}
	
	/**
	 * Cambia il modello della partita e lo restituisce assegnando alla regione il mazzo passato
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.getMappaPartita().getRegione(this.regione).setMazzoPermesso(mazzo);
		return partita;
	}

	/**
	 * Restituisce il mazzo da modificare
	 * @return mazzo
	 */
	public MazzoPermessi getMazzo() {
		return mazzo;
	}

	/**
	 * Restituisce la regione
	 * @return regione
	 */
	public Regione getRegione() {
		return regione;
	}
	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}


}
