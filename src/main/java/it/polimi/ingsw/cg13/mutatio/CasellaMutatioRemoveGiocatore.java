package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.Casella;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CasellaMutatioRemoveGiocatore extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -382104864670715071L;
	private final Casella casella;
	private Giocatore giocatore;
	
	public CasellaMutatioRemoveGiocatore(Casella casella,Giocatore giocatore) {
		this.casella=casella;
		this.giocatore=giocatore;
	}
	
	@Override
	public String toString() {
		return "CasellaMutatio: "+this.giocatore.getNome()+" rimosso dalla "+
				casella.getClass().getSimpleName()+" "+ casella.getPuntiCasella();
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.updateGiocatore(giocatore);
		// Si dovrebbe usare la remove ma per il primo caso non ha intenzione di funzionare
		if(partita.getPercorsoRicchezza().getPercorso().contains(casella)){
				partita.getPercorsoRicchezza().getCasella(casella.getPuntiCasella())
				.setGiocatoriPresenti(casella.getGiocatoriPresenti());
		}
		else if(partita.getPercorsoNobilta().getPercorso().contains(casella)){
			partita.getPercorsoNobilta().getCasella(casella.getPuntiCasella())
			.setGiocatoriPresenti(casella.getGiocatoriPresenti());
		}
		else if(partita.getPercorsoVittoria().getPercorso().contains(casella)){
			partita.getPercorsoVittoria().getCasella(casella.getPuntiCasella())
			.setGiocatoriPresenti(casella.getGiocatoriPresenti());
		}	
		else throw new ErrorUpdateServerException("Errore aggiornamento del modello locale");
		return partita;
		
	}

	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

	public Casella getCasella() {
		return casella;
	}
	
	public Giocatore getGiocatore() {
		return giocatore;
	}

}
