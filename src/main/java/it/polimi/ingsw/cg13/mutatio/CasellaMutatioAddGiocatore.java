package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.Casella;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class CasellaMutatioAddGiocatore extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3628458596867431252L;
	
	private final Casella casella;
	private Giocatore giocatore;
	
	public CasellaMutatioAddGiocatore(Casella casella,Giocatore giocatore) {
		this.casella=casella;
		this.giocatore=giocatore;
	}
	
	@Override
	public String toString() {
		return "CasellaMutatio: "+this.giocatore.getNome()+" aggiunto alla "+
				casella.getClass().getSimpleName()+" "+ casella.getPuntiCasella();
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.updateGiocatore(giocatore);
		if(partita.getPercorsoRicchezza().getPercorso().contains(casella))
			partita.getPercorsoRicchezza().getPercorso().
				get(casella.getPuntiCasella()).addGiocatore(this.giocatore);
		if(partita.getPercorsoVittoria().getPercorso().contains(casella))
			partita.getPercorsoVittoria().getPercorso().
				get(casella.getPuntiCasella()).addGiocatore(this.giocatore);
		if(partita.getPercorsoNobilta().getPercorso().contains(casella))
			partita.getPercorsoNobilta().getPercorso().
				get(casella.getPuntiCasella()).addGiocatore(this.giocatore);
		return partita;
		
	}

	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}
	/**
	 * Restituisce la casella
	 * @return casella
	 */
	public Casella getCasella() {
		return casella;
	}
	
	/**
	 * Restituisce il giocatore
	 * @return giocatore
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}

}
