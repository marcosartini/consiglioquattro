package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;

public class PrimoGiocatoreMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -693044077197437386L;

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		return partita;
	}
	
	
	@Override
	public String toString() {
		return "Sei il primo giocatore collegato !! Inizio creazione nuova partita\n";
	}
	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

	
	

}
