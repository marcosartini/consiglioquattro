package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;

public class FineFaseMarketMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4601927563012468762L;

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		return partita;
	}


	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}


	/**
	 * toString metodo
	 */
	@Override
	public String toString() {
		return "Fine fase di Market";
	}
}
