package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class TempoScadutoMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6412817390742920996L;
	private Giocatore giocatore;
	
	public TempoScadutoMutatio(Giocatore giocatore){
		this.giocatore=giocatore;
	}
	
	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.updateGiocatore(giocatore);
		//Rimuovo da giocatri online e aggiungo alla lista dei giocatori offline
		partita.getGiocatoriOffline().add(giocatore);
		partita.getGiocatori().remove(giocatore);
		return partita;
	}

	@Override
	public String toString() {
		return "TempoScadutoMutatio: il tempo  per fare l'azione e' finito !!\n"
				+ " Passaggio stato successivo in corso...";
	}
	
	@Override
	public void accept(ClientGUIController gui){
		gui.visit(this);
	}

}
