package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.partita.Partita;

public class PermessoToltoMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2814011665695438099L;
	private Regione regione;
	private PermessoCostruzione permesso;

	public PermessoToltoMutatio(Regione reg,PermessoCostruzione per) {
		this.regione=reg;
		this.permesso=per;
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.getMappaPartita().getRegione(regione).getMazzoPermesso()
			.getMazzoCarte().remove(permesso);
		return partita;
	}

	/**
	 * Restituisce la regione
	 * @return regione
	 */
	public Regione getRegione() {
		return regione;
	}

	/**
	 * Restituisce il permesso di costruzione
	 * @return permesso
	 */
	public PermessoCostruzione getPermesso() {
		return permesso;
	}

	@Override
	public String toString() {
		return "PermessoToltoMutatio: tolto dal mazzo "+permesso.toString();
	}

	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

	
}
