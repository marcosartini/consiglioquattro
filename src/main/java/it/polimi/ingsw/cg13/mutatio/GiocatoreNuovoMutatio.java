package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class GiocatoreNuovoMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4924028943246042869L;
	private Giocatore giocatore;
	
	public GiocatoreNuovoMutatio(Giocatore gioc){
		this.giocatore=gioc;
	}
	
	/**
	 * toString metodo
	 */
	@Override
	public String toString() {
		return super.toString()+"Giocatore loggato: "+giocatore.getNome();
	}

	/**
	 * Restituisce il nuovo giocatore
	 * @return giocatore
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws ErrorUpdateServerException {
		return partita;
	}
}
