package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.partita.Partita;


public  class StateMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1761135313856842457L;
	State stato;
	
	public StateMutatio(State stato) {
		this.stato=stato;
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws ErrorUpdateServerException {
		partita.updateGiocatore(stato.getGiocatoreCorrente());
		partita.setStatoPartita(stato);
		return partita;
	}

	@Override
	public String toString() {
		return "StateMutatio@ " + stato ;
	}

	/**
	 * Restituisce lo stato
	 * @return stato
	 */
	public State getStato() {
		return stato;
	}

	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

}
