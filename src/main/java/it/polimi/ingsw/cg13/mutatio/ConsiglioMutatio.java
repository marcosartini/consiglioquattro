package it.polimi.ingsw.cg13.mutatio;

import java.util.Iterator;

import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

public class ConsiglioMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 643488404253650810L;
	
	private Consiglio consiglio;
	private Consigliere consigliereAggiunto;
	
		public ConsiglioMutatio(Consiglio consiglio,Consigliere cons) {
			this.consiglio=consiglio;
			this.consigliereAggiunto=cons;
		}
		
		
		/**
		 * @return the consiglio
		 */
		public Consiglio getConsiglio() {
			return consiglio;
		}
		
		
		@Override
		public Partita changeModel(Partita partita) throws ErrorUpdateServerException {
			partita.getConsiglio(consiglio).sostituzioneConsigliere(consigliereAggiunto);
			return partita;
		}
		
		@Override
		public String toString() {		
			String stringa="Consiglio_"+this.getConsiglio().getProprietario().getNome()+": [";
			
			int i=0;
			Iterator<Consigliere> iterator = this.getConsiglio().getBalcone().iterator();

			while(iterator.hasNext()){	
				stringa+=" consigliere"+ i++ +"="+iterator.next().getColor().toString();
			}
			return super.toString() + stringa + "]";
		}


		public Consigliere getConsigliereAggiunto() {
			return consigliereAggiunto;
		}
		
		@Override
		public void accept(ClientGUIController gui) {
			gui.visit(this);
		}

}
