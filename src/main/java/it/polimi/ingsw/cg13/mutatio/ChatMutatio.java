package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;

public class ChatMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1122582235344605569L;
	private String text;
	
	public ChatMutatio(String testo) {
		text=testo;
	}

	@Override
	/**
	 * cambia il modello della partita e la restituisce
	 * @param partita
	 * @return partita
	 */
	public Partita changeModel(Partita partita) throws CdQException {
		return partita;
	}

	/**
	 * restituisce il testo contenuto nella Mutatio
	 * @return text
	 */
	public String getText() {
		return text;
	}

	@Override
	public void accept(ClientGUIController gui){
		gui.visit(this);
	}

	@Override
	/**
	 * ToString classe
	 */
	public String toString() {
		return  text;
	}

}
