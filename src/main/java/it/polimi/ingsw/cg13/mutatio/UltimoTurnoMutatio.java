package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class UltimoTurnoMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6322497624557312478L;
	Giocatore giocatore; 
	
	public UltimoTurnoMutatio(Giocatore giocatore) {
		this.giocatore=giocatore;
	}

	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.updateGiocatore(giocatore);
		return partita;
	}

	@Override
	public String toString() {
		return "UltimoTurnoMutatio: "+giocatore.getNome()+ " ha costruito l'ultimo emporio\n"
				+ "Inizio ultimo turno per i giocatore rimanenti";
	}

	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}
	

}
