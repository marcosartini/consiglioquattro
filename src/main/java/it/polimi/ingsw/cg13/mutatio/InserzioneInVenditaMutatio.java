package it.polimi.ingsw.cg13.mutatio;


import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.partita.Partita;


public class InserzioneInVenditaMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 641306524807408271L;
	Inserzione inserzione;
	
	public InserzioneInVenditaMutatio(Inserzione ins){
		this.inserzione=ins;
	}
	
	/**
	 * Cambia il modello della partita passata e la restituisce
	 * @param partita
	 * @return partita
	 */
	@Override
	public Partita changeModel(Partita partita) throws CdQException {
		partita.updateGiocatore(inserzione.getProprietario());
		partita.getStatoPartita().getMarket().getBanchetto().add(inserzione);
		return partita;
	}
	
	/**
	 * @return the inserzione
	 */
	public Inserzione getInserzione() {
		return inserzione;
	}
	
	@Override
	public String toString() {
		return "InserzioneInVenditaMutatio@ " + inserzione;
	}
	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}


}
