package it.polimi.ingsw.cg13.mutatio;



import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.gui.ClientGUIController;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.visitorforgui.ElementToVisitByGui;
;

public abstract class Mutatio implements ElementToVisitByGui{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8525390956004208422L;

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Mutatio@";
	}
	
	public abstract Partita changeModel(Partita partita) throws CdQException ;
	
	@Override
	public void accept(ClientGUIController gui) {
		gui.visit(this);
	}

}
