package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;


public class AiutantiBancoMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8413330258920567398L;
	private int aiutantiRimossi;
	private int aiutantiAggiunti;
	
	public AiutantiBancoMutatio(int aiutRimossi,int aiutAggiunti) {
		this.aiutantiAggiunti=aiutAggiunti;
		this.aiutantiRimossi=aiutRimossi;
	}

	@Override
	/**
	 * Cambia il modello della partita rimuovendo dal banco gli aiutanti e aggiungendone il numero indicato
	 * @param partita
	 */
	public Partita changeModel(Partita partita) throws CdQException {
		partita.getBanco().getAiutante(aiutantiRimossi);
		for(int i=0;i<aiutantiAggiunti;i++)
			partita.getBanco().accumulaAiutante();
		return partita;

	}

	@Override
	/**
	 * ToString classe
	 */
	public String toString() {
		return "AiutantiBancoMutatio aiutantiRimossi: " + aiutantiRimossi + ","
				+ " aiutantiAggiunti: " + aiutantiAggiunti;
	}

	/**
	 * Restituisce il numero di aiutanti rimossi
	 * @return aiutantiRimossi
	 */
	public int getAiutantiRimossi() {
		return aiutantiRimossi;
	}

	/**
	 * Restituisce il numero di aiutanti aggiunti
	 * @return aiutantiAggiunti
	 */
	public int getAiutantiAggiunti() {
		return aiutantiAggiunti;
	}

}
