package it.polimi.ingsw.cg13.mutatio;

import it.polimi.ingsw.cg13.carte.MazzoPolitico;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.partita.Partita;

public class CambioMazziPoliticiMutatio extends Mutatio {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6064952348105399510L;
	private MazzoPolitico mazzoPrincipale;
	private MazzoPolitico mazzoScarto;

	public CambioMazziPoliticiMutatio(MazzoPolitico princ,MazzoPolitico scarto) {
		this.mazzoPrincipale=princ;
		this.mazzoScarto=scarto;
	}

	@Override
	/**
	 * Cambia il modello della partita e lo restituisce assegnando al banco princ come mazzo politico e scarto come mazzo scarto
	 * @param partita
	 * @return partita
	 */
	public Partita changeModel(Partita partita) throws CdQException {
		partita.getBanco().setMazzoPolitico(mazzoPrincipale);
		partita.getBanco().setMazzoScarto(mazzoScarto);
		return partita;
	}

	@Override
	/**
	 * ToSrting della classe
	 */
	public String toString() {
		return "CambioMazziPoliticiMutatio: mazzo terminato, mescolamento mazzo scarti in corso";
	}

}
