/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;

import java.io.Serializable;

import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public abstract class Ricompensa implements Serializable{

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 4334124067968785568L;

	/**
	 * Assegna la ricompensa al giocatore beneficiario in base al modello pasato
	 * @param giocatore beneficiario del bonus
	 * @param modello con cui interagire
	 * @throws IllegalArgumentException: se si passa partita o giocatore null
	 */
	public void assegnaRicompensa(Giocatore giocatore, Partita partita){
		if(giocatore==null || partita==null)
			throw new IllegalArgumentException("Non puoi passare giocatore o partita nulli");
	}
	
	/**
	 * Ritorna true se la ricompensa da nobilta, false altrimenti
	 * @return false
	 */
	public boolean checkGiveNobilta(){
		return false;
	}
	
	/**
	 * Ritorna la quantita che la ricompensa assegna
	 * @return la quantita della ricompensa da assegnare
	 */
	public abstract int getQuantita();
	
	/**
	 * Controlla se due ricompense sono uguali, ovviamente per esere uguali 
	 * devono essere entrambe della stessa classe e assegnare la stessa ricompensa
	 * @return true se le ricompense sono uguali, altrimenti false
	 */
	@Override
	public abstract boolean equals(Object obj);
	
	/**
	 * Calcola l'hashCode della ricompensa
	 * @return hashCode della ricompensa
	 */
	@Override
	public abstract int hashCode();
	
		
}
