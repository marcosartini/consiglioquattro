/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;

import java.io.Serializable;


import it.polimi.ingsw.cg13.mutatio.EmporioMutatio;

import it.polimi.ingsw.cg13.mutatio.TesseraBonusMutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.observable.ObserverC;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author lorenzo
 *
 */
public abstract class TesseraBonus extends ObservableC<TesseraBonusMutatio> implements ObserverC<EmporioMutatio>,Serializable{
	
	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 5452578174160938789L;
	/**
	 * punteggio che assegna la tessera 
	 */
	private final int punteggioCaselleVittoria;
	/**
	 * indica se il bonus e' stato assegnato oppure no
	 */
	private boolean assegnato; //true se assegnato ad un giocatore
	/**
	 * giocatore a cui viene assegnata la tessera 
	 */
	private Giocatore giocatore;
	
	/**
	 * Costruttore di una tesseraBonus dato come parametro il
	 * punteggio che la tessera deve assegnare
	 * Setta assegnato a false e giocatore a null in quanto
	 * non appartiene ancora a nessun giocatore
	 * @param punteggioCaselleVittoria: punteggio che la tessera assegna
	 * @throws IllegalArgumentException: se dai un punteggio negativo o pari a 0
	 */
	public TesseraBonus(int punteggioCaselleVittoria){
		if(punteggioCaselleVittoria<=0)
			throw new IllegalArgumentException("Non puoi assegnare punteggio negativo o zero alla tessera");
		this.punteggioCaselleVittoria=punteggioCaselleVittoria;
		this.assegnato=false;
		this.giocatore=null;
	}
	
	/**
	 * Arrivata la notifica di costruzione dell'emporio
	 * set assegnato a true,prende il giocatore dal cambiamento
	 * e assegna la tessera al giocatore
	 * @param change: cambiamento che indica la costruzione di una citta
	 */
	public void assegnaBonus(EmporioMutatio change) {
			this.setAssegnato(true);
			this.giocatore=change.getGiocatore();
			change.getGiocatore().addBonus(this);	
		}
	
	/**
	 * Ritorna se la tessera e' stata assegnata oppure no
	 * @return assegnato
	 */
	protected boolean isAssegnato() {
		return assegnato;
	}
	
	
	
	/**
	 * Setta il valore assegnato alla variabile della tessera
	 * @param assegnato: valore da attribuire alla tessera
	 */
	protected void setAssegnato(boolean assegnato) {
		this.assegnato = assegnato;
	}

	/**
	 * Controlla se la tessera e' assegnabile 
	 * @param change: notifica di costruzione di un emporio
	 * @return true se assegnabile oppure false
	 */
	protected abstract boolean isAssegnabile(EmporioMutatio change);

	
	/**
	 * {@inheritDoc}
	 * Ricevuta la mutatio di emporio costruito
	 * controlla se non e' gia' stato assegnato e se assegnabile
	 * e se e' possibile lo assegna chiamando assegnaBonus
	 */
	@Override
	public void update(EmporioMutatio change) {
		if(!isAssegnato() && isAssegnabile(change)){
				this.assegnaBonus(change);
				this.notifyObservers(new TesseraBonusMutatio(this));
			}
		
	}

	
	/**
	 * Ritorna il giocatore in posseso della tessera
	 * @return giocatore della tessera
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}

	
	/**
	 * Setta il giocatore passato come parametro come 
	 * giocatore della tessera 
	 * @param giocatore : il giocatore da settare
	 */
	public void setGiocatore(Giocatore giocatore) {
		this.giocatore = giocatore;
	}
	
	/**
	 * {@inheritDoc}
	 * Torna la stringa del punteggio della tessera
	 * puntiVittoria=num )
	 */
	@Override
	public String toString() {
		return "puntiVittoria=" + punteggioCaselleVittoria +  ")";
	}
	
	/**
	 * Ritorna il punteggio della casella
	 * @return punteggio della casella
	 */
	public int getPunteggioCaselleVittoria() {
		return punteggioCaselleVittoria;
	}

	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + punteggioCaselleVittoria;
		return result;
	}

	
	/**
	 * {@inheritDoc}
	 * Una tesseraBonus e' uguale in base al punteggio e alla classe
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TesseraBonus other = (TesseraBonus) obj;
		if (punteggioCaselleVittoria != other.punteggioCaselleVittoria)
			return false;
		return true;
	}
	

	
}
