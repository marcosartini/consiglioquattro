package it.polimi.ingsw.cg13.bonus;

import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.mutatio.PermessoToltoMutatio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class OttieniPermesso extends BonusNeedInput {



	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -8918660426871683103L;
	private static final Logger logger = LoggerFactory.getLogger(OttieniPermesso.class);
	
	/**
	 * Ritorna la stringa della ricompensa: OttieniPermessoCostruzione
	 * return stringa della ricompensa
	 */
	@Override
	public String toString() {
		return "OttieniPermessoCostruzione";
	}

	/**
	 * Controlla se due ricompense sono uguali,  per esere uguali 
	 * devono essere entrambe della stessa classe
	 * @return true se le ricompense sono uguali, altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	/**
	 * Calcola l'hashCode=31 perche' tutte le ottieniPermesso 
	 * sono uguali tra loro, basta che appartengono alla stessa classe
	 * @return 31
	 */
	@Override
	public int hashCode() {
		return 31;
	}

	/*
	 * (non-Javadoc)
	 * @see it.polimi.ingsw.cg13.bonus.RicompensaSpecial#assegnaBonusScelto(it.polimi.ingsw.cg13.bonus.OggettoBonusInput, it.polimi.ingsw.cg13.personaggi.Giocatore, it.polimi.ingsw.cg13.partita.Partita)
	 */
	@Override
	public void assegnaBonusScelto(OggettoBonusInput oggetto, Giocatore giocatore, Partita partita) throws CdQException {
		PermessoCostruzione permesso=this.cercaByBonusInput(partita, oggetto);
		//Rimuovo il permesso dalla regione
		for(Regione regione:partita.getMappaPartita().getRegioni()){
			MazzoPermessi mazzo=regione.getMazzoPermesso();
			if(mazzo.getMazzoCarte().contains(permesso)){
				mazzo.getMazzoCarte().remove(permesso);
				try {
					partita.notifyObservers(new PermessoToltoMutatio(regione,permesso));
				} catch (RemoteException e) {
					logger.error(e.getMessage(),e);
				}
			}
		}
		//Assegno il permesso al giocatore
		giocatore.accumulaPermesso(permesso);
		//Assegno i bonus
		oggetto.assegnaBonusInput(giocatore, partita);
	}

	
	/**
	 * Cerca la carta Permesso corrispondente all'oggetto passato e
	 * torna quindi il PermessoCostruzione trovato
	 */
	@Override
	public OggettoBonusInput updateFromServer(Giocatore giocatore, Partita partita, OggettoBonusInput oggetto)throws ErrorUpdateServerException {
		return cercaByBonusInput(partita,oggetto);
	}
	
	
	/**
	 * Cerca l'oggetto tra le cartePermesso dei mazzi delle regioni
	 * Se lo trova, torna l'oggetto altrimenti lancia l'eccezione
	 * @param partita modello con cui interagire
	 * @param oggetto oggetto da cercare
	 * @return oggetto trovato
	 * @throws ErrorUpdateServerException: se non trova l'ggetto
	 */
	public PermessoCostruzione cercaByBonusInput(Partita partita,OggettoBonusInput oggetto)throws ErrorUpdateServerException{
		PermessoCostruzione permesso=null;
		for(Regione regione:partita.getMappaPartita().getRegioni()){
			MazzoPermessi mazzo=regione.getMazzoPermesso();
			permesso=mazzo.getCarteInCima(regione.getMazzoPermesso().getPermessiVisibili()).stream()
				.filter(carta->carta.equals(oggetto)).findFirst()
				.orElse(null);
			if(permesso!=null){
				return permesso;
			}
				
		}
		throw new ErrorUpdateServerException("Permesso scelto non trovato tra le carte delle regioni");
	}

	/**
	 * Controlla se il bonus e' assegnabile
	 * Inquesto caso se esiste almeno un mazzoPermesso con una carta disponibile
	 * @param giocatore: beneficiario del bonus
	 * @param partita: modello su cui agire
	 * @return true se il bonus e' assegnabile, false altrimenti 
	 */
	@Override
	public boolean checkBonus(Giocatore giocatore, Partita partita) {
		return partita.getMappaPartita().getRegioni().stream()
				.anyMatch(regione->!regione.getMazzoPermesso().getMazzoCarte().isEmpty());
	}

	/*
	 * Controlla se il permesso passato dal client non e' presente tra le carte permesso usate e 
	 * e quindi e' un permesso della regione
	 * Non serve perche' l'update cerca tra i permessi delle regioni e se non lo trova manda un eccezione
	 * 
	@Override
	public boolean visitOggettoBonusInput(PermessoCostruzione permesso, Giocatore giocatore, Partita partita)
			throws CdQException {
		if(!giocatore.getCartePermessoUsate().contains(permesso))
			return true;
		else throw new AzioneNotPossibleException("Hai scelto un permesso usato dei tuoi e non uno libero");
	}
	*/
	
	
}
