package it.polimi.ingsw.cg13.bonus;


import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class RiutilizzaBonusCitta extends BonusNeedInput {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 7821345759185695321L;

	/**
	 * Ritorna la stringa della ricompensa: RiutilizzoBonusCitta
	 * return stringa della ricompensa
	 */
	@Override
	public String toString() {
		return "RiutilizzaBonusCitta";
	}

	/**
	 * Controlla se due ricompense sono uguali,  per esere uguali 
	 * devono essere entrambe della stessa classe
	 * @return true se le ricompense sono uguali, altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	/**
	 * Calcola l'hashCode=32 perche' tutte le RiutilizzoBonusCitta 
	 * sono uguali tra loro, basta che appartengono alla stessa classe
	 * @return 32
	 */
	@Override
	public int hashCode() {
		return 32;
	}

	/**
	 * Assegna il bonus input scelto
	 */
	@Override
	public void assegnaBonusScelto(OggettoBonusInput oggetto, Giocatore giocatore, Partita partita) {
		oggetto.assegnaBonusInput(giocatore, partita);	
	}
	
	/**
	 * Aggiorna l'oggetto con l'istanza presente sul server
	 * Cerca l'oggetto tra le citta della mappa , se la trovo torna l'oggetto
	 * Se non lo trova, lancia un'eccezione
	 */
	@Override
	public OggettoBonusInput updateFromServer(Giocatore giocatore, Partita partita,OggettoBonusInput oggetto) throws CdQException {
		Citta citta=partita.getMappaPartita().getGrafo().stream().filter(nodo->nodo.getCittaNodo().equals(oggetto))
				.findFirst().orElseThrow(()->new ErrorUpdateServerException("Citta non trovata sul server")).getCittaNodo();
		if(!citta.hasEmporio(giocatore))
			throw new AzioneNotPossibleException("La citta selezionata non ha empori di "+giocatore.getNome()+ "selezionane un'altra");
		else if(citta.getRicompense().stream().anyMatch(ricompensa -> ricompensa.checkGiveNobilta())){
			throw new AzioneNotPossibleException("La citta selezionata ha ricompense nobilta', selezionane un'altra ");
		}
		return citta;
	}


	/**
	 * Controlla se il bonus e' assegnabile
	 * In questo caso e' assegnabile se esiste una citta co l'emporio del giocatore 
	 * passato come parametro e questa citta non abbia ricompense che assegnano nobilta
	 * @param giocatore: beneficiario del bonus
	 * @param partita: modello su cui agire
	 * @return true se il bonus e' assegnabile, false altrimenti 
	 */
	@Override
	public boolean checkBonus(Giocatore giocatore, Partita partita) {
		return partita.getMappaPartita().getGrafo().stream()
				.anyMatch(nodo->nodo.getCittaNodo().hasEmporio(giocatore)
				&& !nodo.getCittaNodo().getRicompense().stream()
				.anyMatch(ricompensa -> ricompensa.checkGiveNobilta()));
	}


	/*
	 * Viene richiamato solo se gli viene passata una citta e quindi l'oggetto scelto e' corretto
	 * Altrimenti lancia un eccezione
	 * Non serve perchè nell'update controlla che l'oggetto sia tra tutte le citta
	 * Se viene passato qualcosa che non e' un citta, non lo trova e lancia l'eccezione
	 * 
	@Override
	public boolean visitOggettoBonusInput(Citta citta, Giocatore giocatore, Partita partita) {
		return true;
	}
	 */
	

	
}
