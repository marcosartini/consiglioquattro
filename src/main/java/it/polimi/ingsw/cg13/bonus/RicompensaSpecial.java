package it.polimi.ingsw.cg13.bonus;

import java.io.Serializable;


import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;


import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.visitor.Visitor;

public interface RicompensaSpecial extends Visitor,Serializable {
	
	/**
	 * Controlla se il bonus e' assegnabile
	 * @param giocatore: beneficiario del bonus
	 * @param partita: modello su cui agire
	 * @return true se il bonus e' assegnabile, false altrimenti 
	 */
	public boolean checkBonus(Giocatore giocatore, Partita partita);
	
	
	/**
	 * Assegna al giocatore passato come parametro l'oggetto bonus input scelto
	 * Nel caso che il bonus input scelto non c'entri con la ricompensa che il giocatore deve ricevere 
	 * allora lancia un'eccezione di CdQException
	 * @param oggetto: bonus input scelto dal giocatore
	 * @param giocatore: beneficiario della ricompensa
	 * @param partita: modello con cui interagire
	 * @throws CdQException : se l'oggetto non c'entra con la ricompensa da ricevere
	 */
	public default void assegnaBonusScelto(OggettoBonusInput oggetto,Giocatore giocatore, Partita partita) 
			throws CdQException{
		throw new AzioneNotPossibleException("Errore assegnamento ricompensa -> Correzione in corso");
	}
	
	/**
	 * Aggiorna l'oggetto ricevuto dal client con l'istanza del server
	 * @param giocatore: giocatore beneficiario della ricompensa
	 * @param partita: modello con cui interagire
	 * @param oggetto: oggetto scelto e proveniente dal client
	 * @return oggettoBonusInput: istanza corrispondete del server dell'oggetto passato
	 * @throws CdQException nel caso in cui l'oggetto passato non venga trovato sul server o non c'entri col bonus da scegliere
	 */
	public default OggettoBonusInput updateFromServer(Giocatore giocatore,Partita partita,OggettoBonusInput oggetto) 
			throws  CdQException{
		throw new ErrorUpdateServerException("Errore aggiornamento ricompensa -> Correzione in corso");
	}
	
	
	
	/*
	 * Visitor per ogni bonus input per verificare se sta scegliendo quello giusto
	 * e non sta cercando di inserire un parametro che non riguarda quel bonus
	 * 
	 * Per ora non serve, perche' andando a cercare una citta o un permesso specifico a seconda del bonus
	 * se gli arriva qualcosa di una classe diversa o un oggetto che non c'entra col parametro da cercare torna 
	 * torna gia un errore 
	 * 
	 * 
	public default boolean visitOggettoBonusInput(OggettoBonusInput oggetto,Giocatore giocatore,Partita partita)
			throws CdQException{
		throw new AzioneNotPossibleException("Stai passando un parametro che non c'entra con il bonus da assegnare");
	}
	
	
	
	
	public default boolean visitOggettoBonusInput(Citta citta,Giocatore giocatore,Partita partita) 
			throws CdQException{
		throw new AzioneNotPossibleException("Stai passando un parametro che non c'entra con il bonus da assegnare");
	}
	
	
	public default boolean visitOggettoBonusInput(PermessoCostruzione permesso,Giocatore giocatore,Partita partita)
			throws CdQException{
		throw new AzioneNotPossibleException("Stai passando un parametro che non c'entra con il bonus da assegnare");
	}
	*/
	
	
}
