/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;


import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mutatio.AiutantiBancoMutatio;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class AiutanteAggiuntivo extends Ricompensa {

	/**
	 * id per la serialiazzazione
	 */
	private static final long serialVersionUID = 1962030194336449566L;
	private static final Logger logger = LoggerFactory.getLogger(AiutanteAggiuntivo.class);
	
	/**
	 * numero di aiutanti da dare come ricompensa
	 */
	private int num;
	
	/**
	 * Costruttore che setta gli aiutanti di ricompensa a 1
	 */
	public AiutanteAggiuntivo() {		
		num=1;
	}
	
	/**
	 * Costruttore che setta num come numero di aiutanti da assegnare
	 * @param num: numero di aiutanti da assegnare come ricompensa
	 * @throws IllegalArgumentException: se assegno un numero negativo o pari a zero alla ricompensa
	 */
	public AiutanteAggiuntivo(int num) {	
		if(num<=0)
			throw new IllegalArgumentException("Non puoi assegnare un bonus con ricompensa negativa o pari a zero");
		this.num=num;
	}
	
	/**
	 * Assegna la ricompensa al giocatore beneficiario in base al modello pasato
	 * Assegna il num di aiutanti della ricompensa al giocatore
	 * @param giocatore beneficiario del bonus
	 * @param modello con cui interagire
	 * @throws IllegalArgumentException: se si passa partita o giocatore null
	 */
	@Override
	public void assegnaRicompensa(Giocatore giocatore, Partita partita){
		super.assegnaRicompensa(giocatore, partita);
		List<Aiutante> aiutanti=partita.getAiutanti(num);
		giocatore.addAiutanti(aiutanti.size()); 
		try {
			partita.notifyObservers(new AiutantiBancoMutatio(aiutanti.size(),0));
		} catch (RemoteException | CdQException e) {
			logger.error(e.getMessage(),e);
		}
	}

	
	/**
	 * Stringa del bonus: AiutanteAggiuntivo num
	 * @return stringa corrispondente al bonus
	 */
	@Override
	public String toString() {
		return "AiutanteAggiuntivo ("+num+")";
	}

	/**
	 * Calcola l'hashCode della ricompensa
	 * @return hashCode della ricompensa
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + num;
		return result;
	}

	/**
	 * Controlla se due ricompense sono uguali, ovviamente per esere uguali 
	 * devono essere entrambe della stessa classe e assegnare la stessa ricompensa
	 * @return true se le ricompense sono uguali, altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AiutanteAggiuntivo other = (AiutanteAggiuntivo) obj;
		if (num != other.num)
			return false;
		return true;
	}

	
	/**
	 * Ritorna il numero di aiutanti che la ricompensa assegna
	 * @return la quantita della ricompensa da assegnare
	 */
	@Override
	public int getQuantita() {
		return this.num;
	}

	

}
