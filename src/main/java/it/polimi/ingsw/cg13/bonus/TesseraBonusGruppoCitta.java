/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;

import java.util.Set;


import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mutatio.EmporioMutatio;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author lorenzo
 *
 */
public class TesseraBonusGruppoCitta extends TesseraBonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 631146341136394895L;
	/**
	 * Colore corrispodente al gruppo di citta a cui assegnare il bonus
	 */
	private final Colore colore; 
	/**
	 * Il set delle citta da completare per farsi che il bonus venga assegnato 
	 */
	private final Set<Citta> cittaColorate;
	
	/**
	 * Costruttore di una tesseraBonus del gruppo citta
	 * @param punteggioCaselleVittoria: punteggio della tessera
	 * @param colore: colore delle citta che devono essere completate
	 * @param cittaColorate: insieme delle citta di quel colore
	 * @throws IllegalArgumentException: se si passa un punteggio negativo o pari a zero oppure il gruppo citta o il colore sono null
	 */
	public TesseraBonusGruppoCitta(int punteggioCaselleVittoria,Colore colore,Set<Citta>cittaColorate){
		super(punteggioCaselleVittoria);
		if(cittaColorate==null || colore==null)
			throw new IllegalArgumentException("Non vanno le citta o il colore null");
		this.cittaColorate=cittaColorate;
		this.colore=colore;
	}

	
	/**
	 * {@inheritDoc}
	 * Nel caso della tesseraBonusGruppoCitta e' assegnabile quando
	 * tutte le citta del colore della tessera hanno un emporio di un giocatore
	 * Se una sola citta di quel colore non ha un emporio del giocatore torna false
	 * 
	 */
	@Override
	protected boolean isAssegnabile(EmporioMutatio change) {
		
			Giocatore giocatore=change.getGiocatore();	
			for(Citta citta: this.cittaColorate){
				if(!citta.hasEmporio(giocatore))
					return false;
			}
			return true;			
		}
	
	/**
	 * {@inheritDoc}
	 * Ritorna la stringa
	 * TesseraBonusGruppoCitta (colore=col, super della tessera bonus
	 */
	@Override
	public String toString() {
		return "TesseraBonusGruppoCitta (" + colore + ", "+super.toString();
	}
	
	
	/**
	 * Ritorna il colore per il quale la tessera viene assegnata
	 * @return il colore della tessera
	 */
	public Colore getColore() {
		return colore;
	}

	/*
	 * (non-Javadoc)
	 * @see it.polimi.ingsw.cg13.bonus.TesseraBonus#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((colore == null) ? 0 : colore.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * Sono uguali quando sono tessereBonus uguali e 
	 * hanno lo stesso colore di assegnamento
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TesseraBonusGruppoCitta other = (TesseraBonusGruppoCitta) obj;
		if (colore == null) {
			if (other.colore != null)
				return false;
		} else if (!colore.equals(other.colore))
			return false;
		return true;
	}
	
}
