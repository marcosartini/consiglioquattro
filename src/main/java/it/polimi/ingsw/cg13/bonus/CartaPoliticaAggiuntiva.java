/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;

import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class CartaPoliticaAggiuntiva extends Ricompensa {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -1357086843210746540L;
	
	/**
	 * numero di carte politiche da assegnare come bonus 
	 */
	private int num;
	
	/**
	 * Costruttore che setta le carte di ricompensa a 1
	 */
	public CartaPoliticaAggiuntiva() {
		num=1;
	}
	
	/**
	 * Costruttore che setta num come numero di carte da assegnare
	 * @param num: numero di carte da assegnare come ricompensa
	 * @throws IllegalArgumentException: se assegno un numero negativo o pari a zero alla ricompensa
	 */
	public CartaPoliticaAggiuntiva(int num) {
		if(num<=0)
			throw new IllegalArgumentException("Non puoi assegnare un bonus con ricompensa negativa o pari a zero");
		this.num=num;
	}
	
	/**
	 * Assegna la ricompensa al giocatore beneficiario in base al modello pasato
	 * Assegna il num di carte della ricompensa al giocatore
	 * @param giocatore beneficiario del bonus
	 * @param modello con cui interagire
	 * @throws IllegalArgumentException: se si passa partita o giocatore null
	 */
	@Override
	public void assegnaRicompensa(Giocatore giocatore, Partita partita){
		super.assegnaRicompensa(giocatore, partita);
		for(int i=0;i<num;i++)
			giocatore.addCartaPolitica(partita.pescaCarta());
	}

	/**
	 * Stringa del bonus: AiutanteAggiuntivo num
	 * @return stringa corrispondente al bonus
	 */
	@Override
	public String toString() {
		return "CartaPoliticaAggiuntiva ("+num+")";
	}

	/**
	 * Calcola l'hashCode della ricompensa
	 * @return hashCode della ricompensa
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + num;
		return result;
	}

	/**
	 * Controlla se due ricompense sono uguali, ovviamente per esere uguali 
	 * devono essere entrambe della stessa classe e assegnare la stessa ricompensa
	 * @return true se le ricompense sono uguali, altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartaPoliticaAggiuntiva other = (CartaPoliticaAggiuntiva) obj;
		if (num != other.num)
			return false;
		return true;
	}

	/**
	 * Ritorna il numero di carte che la ricompensa assegna
	 * @return la quantita della ricompensa da assegnare
	 */
	@Override
	public int getQuantita() {
		return this.num;
	}

}
