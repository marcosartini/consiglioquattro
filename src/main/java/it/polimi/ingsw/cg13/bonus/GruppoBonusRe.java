package it.polimi.ingsw.cg13.bonus;

import java.io.Serializable;
import java.util.Queue;

import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.mutatio.TesseraBonusMutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.observable.ObserverC;

public class GruppoBonusRe extends ObservableC<Mutatio> implements ObserverC<TesseraBonusMutatio>,Serializable {
	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 7873095514062750756L;
	/**
	 * Coda delle tessereBonus del Re
	 */
	private final Queue<TesseraBonusRe> gruppo;
	
	

	/**
	 * Costruttore del gruppo di tessere del Re
	 * @param gruppo: insieme delle tessere del re
	 */
	public GruppoBonusRe(Queue<TesseraBonusRe> gruppo) {
		this.gruppo = gruppo;
	}
	
	/**
	 * Assegna il gruppo passato al gruppo della tessera
	 * @param gruppoBonusRe
	 */
	public GruppoBonusRe(GruppoBonusRe gruppoBonusRe) {
		this.gruppo=gruppoBonusRe.gruppo;
	}

	/**
	 * Aggiunge al gruppo una tesseraBonus del re
	 * @param tbr: tessera bonus del re da aggiungere
	 * @throws IlllegalArgumentException: se passi una tessera null
	 */
	protected void setTesseraBonusRe(TesseraBonusRe tbr){
		if(tbr==null)
			throw new IllegalArgumentException("Non si puo' passare una tessera null");
		this.gruppo.add(tbr);
	}

	/**
	 * Ritorna il gruppo delle tessere del re
	 * @return the gruppo
	 */
	public Queue<TesseraBonusRe> getGruppo() {
		return gruppo;
	}

	/**
	 * {@inheritDoc}
	 * Arriva una notifica di tessera assegnata, la rinotifica a partita
	 * Poi se c'e' ancora una tessera da assegnare del gruppo del re
	 * la assegna a quel giocatore e la notifica a partita
	 * Altrimenti se le tessere sono finite non fa nulla
	 */
	@Override
	public void update(TesseraBonusMutatio change) {	
		this.notifyObservers(change);
		if(!gruppo.isEmpty()){
			TesseraBonusRe tbr = gruppo.poll();
			tbr.setAssegnato(true);
			tbr.setGiocatore(change.getTesseraBonus().getGiocatore());
			change.getTesseraBonus().getGiocatore().addBonus(tbr);
			this.notifyObservers(new TesseraBonusMutatio(tbr));
		}
		
	}

	/**
	 * {@inheritDoc}
	 * Ritorna la stringa
	 * GruppoBonusRe {lista del gruppo}
	 */
	@Override
	public String toString() {
		return "GruppoBonusRe {" + gruppo + "}";
	}
	
	
	
}
