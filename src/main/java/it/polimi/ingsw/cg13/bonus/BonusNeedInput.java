package it.polimi.ingsw.cg13.bonus;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.machinestate.StateWBDue;
import it.polimi.ingsw.cg13.machinestate.StateWBQuattro;
import it.polimi.ingsw.cg13.machinestate.StateWBTre;
import it.polimi.ingsw.cg13.machinestate.StateWBUno;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public abstract class BonusNeedInput extends Ricompensa implements RicompensaSpecial {

	
	/**
	 * id per la serializzazione 
	 */
	private static final long serialVersionUID = 4487876394378585022L;
	
	/**
	 * Ritorna se il bonus è assegnabile
	 * Di norma false , poi ogni bonus overrida il metodo e torna il suo check
	 * @return false
	 */
	@Override
	public boolean checkBonus(Giocatore giocatore, Partita partita) {
		return false;
	}


	/**
	 * ritorna la quantita' del bonus che per i bonusInput e' sempre uno
	 * @return quantia' del Bonus
	 */
	@Override //da controllare se sempre uno
	public int getQuantita() {
		return 1;
	}

	
	/**
	 * Assegna la ricompensa al giocatore beneficiario in base al modello passato
	 * Assegna il bonus con input della ricompensa allo stato del giocatore
	 * in modo che nello stato successivo possa tenerne conto per eseguire la scelta del bonus
	 * Nel caso questa ricompensa venga effettuata in uno stato non corretto della partita,
	 * lancia un eccezione che viene catchata e non assegna la ricompensa
	 * @param giocatore beneficiario del bonus
	 * @param modello con cui interagire
	 * @throws IllegalArgumentException: se si passa partita o giocatore null
	 */
	@Override
	public void assegnaRicompensa(Giocatore giocatore, Partita partita) {
		try {
			partita.getStatoPartita().addRicompensa(this);
		} catch (AzioneNotPossibleException e) {
			partita.notifyObservers(e);
		}
	}
	
	
	/**
	 * Dato uno stato beneficiario di un bonus con input, 
	 * ritorna il nuovo stato in base allo stato precendente arrivato come parametro
	 * Questo caso: da StateUno a StateWB1
	 * @param stato: stato in cui si verifica assegnamento di bonus con input
	 * @param partita: modello con cui interagire
	 * @return lo stato successivo
	 */
	@Override 
	public State visit(StateUno stato,Partita partita){
		return new StateWBUno(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());
	}
	
	/**
	 * Dato uno stato beneficiario di un bonus con input, 
	 * ritorna il nuovo stato in base allo stato precendente arrivato come parametro
	 * Questo caso: da StateDue a StateWB2
	 * @param stato: stato in cui si verifica assegnamento di bonus con input
	 * @param partita: modello con cui interagire
	 * @return lo stato successivo
	 */
	@Override 
	public State visit(StateDue stato,Partita partita){
		return new StateWBDue(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());

	}
	
	/**
	 * Dato uno stato beneficiario di un bonus con input, 
	 * ritorna il nuovo stato in base allo stato precendente arrivato come parametro
	 * Questo caso: da StateTre a StateWB3
	 * @param stato: stato in cui si verifica assegnamento di bonus con input
	 * @param partita: modello con cui interagire
	 * @return lo stato successivo
	 */
	@Override 
	public State visit(StateTre stato,Partita partita){
		return new StateWBTre(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());
	}
	
	/**
	 * Dato uno stato beneficiario di un bonus con input, 
	 * ritorna il nuovo stato in base allo stato precendente arrivato come parametro
	 * Questo caso: da StateQuattro a StateWB4
	 * @param stato: stato in cui si verifica assegnamento di bonus con input
	 * @param partita: modello con cui interagire
	 * @return lo stato successivo
	 */
	@Override 
	public State visit(StateQuattro stato,Partita partita){
		return new StateWBQuattro(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());

	}
}
