/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;


import it.polimi.ingsw.cg13.personaggi.Giocatore;

import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.mutatio.EmporioMutatio;

/**
 * @author lorenzo
 *
 */

/*
 * osserva la costruzione degli empori, e verifica se è stata completata qualche regione
 * il bonus viene assegnato quando tutte le città di una regione contengono un emporio di un certo giocatore.
 *  Però solo la prima volta
 */
public class TesseraBonusRegione extends TesseraBonus { //implements Observer extends Observable

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 7714904713357890157L;
	/**
	 * regione per cui viene assegnato il bonus
	 */
	private final Regione regione;	
	
	/**
	 * Costruttore di una tesseraBonusRegione
	 * @param punteggioCaselleVittoria: punteggio che assegna la tessera
	 * @param regione: regione per cui viene assegnato il punteggio
	 * @throws IllegalArgumentException se i punti sono negativo o pari a zero oppure se la regione e' null
	 */
	public TesseraBonusRegione(int punteggioCaselleVittoria, Regione regione){
		super(punteggioCaselleVittoria);
		if(regione==null)
			throw new IllegalArgumentException("Non puoi passare una regione nulla");
		this.regione=regione;
	}
	

	/**
	 * {@inheritDoc}
	 * Nel caso della tesseraBonusRegione e' assegnabile quando
	 * tutte le citta della regione della tessera hanno un emporio di un giocatore
	 * Se una sola citta di quela regione non ha un emporio del giocatore torna false
	 */
	@Override
	protected boolean isAssegnabile(EmporioMutatio change){
		
			Giocatore giocatore=change.getGiocatore();
			for(Citta citta: this.regione.getCitta()){
				if(!citta.hasEmporio(giocatore))
					return false;
				}
			return true;			
	}

	/**
	 * {@inheritDoc}
	 * Ritorna la stringa della tessera
	 * TesseraBonusRegione (regione=regione, super di tesseraBonus
	 */
	@Override
	public String toString() {
		return "TesseraBonusRegione (regione=" + regione.getNome() + ", "+super.toString();
	}

	/**
	 * Ritorna la regione per cui viene assegnata la tessera
	 * @return regione
	 */
	public Regione getRegione() {
		return regione;
	}

	/*
	 * (non-Javadoc)
	 * @see it.polimi.ingsw.cg13.bonus.TesseraBonus#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((regione == null) ? 0 : regione.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * Sono uguali quando sono tessereBonus uguali e 
	 * hanno la stessa regione di assegnamento
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TesseraBonusRegione other = (TesseraBonusRegione) obj;
		if (regione == null) {
			if (other.regione != null)
				return false;
		} else if (!regione.equals(other.regione))
			return false;
		return true;
	}	
	

}
