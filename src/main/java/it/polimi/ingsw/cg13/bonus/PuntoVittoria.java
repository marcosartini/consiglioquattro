/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;

import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class PuntoVittoria extends Ricompensa {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 4587316024510815315L;
	/**
	 * punti vittoria da assegnare come ricompensa
	 */
	int puntiVittoria;
	/**
	 * Costruttore che setta punti come numero di punti da assegnare
	 * @param num: numero di punti da assegnare come ricompensa
	 * @throws IllegalArgumentException: se assegno un numero negativo o pari a zero alla ricompensa
	 */
	public PuntoVittoria(int punti) {
		if(punti<=0)
			throw new IllegalArgumentException("Non puoi assegnare un bonus con ricompensa negativa o pari a zero");
		this.puntiVittoria=punti;
	}
	
	/**
	 * Assegna la ricompensa al giocatore beneficiario in base al modello pasato
	 * Assegna i punti della ricompensa al giocatore
	 * @param giocatore beneficiario del bonus
	 * @param modello con cui interagire
	 * @throws IllegalArgumentException: se si passa partita o giocatore null
	 */
	@Override
	public void assegnaRicompensa(Giocatore giocatore, Partita partita){
		super.assegnaRicompensa(giocatore, partita);
		giocatore.riceviVittoria(puntiVittoria, partita.getPercorsoVittoria());
	}
	
	/**
	 * Stringa del bonus: PuntoVittoria (num)
	 * @return stringa corrispondente al bonus
	 */
	@Override
	public String toString() {
		return "PuntoVittoria (" + puntiVittoria + ")";
	}
	
	/**
	 * Calcola l'hashCode della ricompensa
	 * @return hashCode della ricompensa
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + puntiVittoria;
		return result;
	}
	
	/**
	 * Controlla se due ricompense sono uguali, ovviamente per esere uguali 
	 * devono essere entrambe della stessa classe e assegnare la stessa ricompensa
	 * @return true se le ricompense sono uguali, altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PuntoVittoria other = (PuntoVittoria) obj;
		if (puntiVittoria != other.puntiVittoria)
			return false;
		return true;
	}
	
	/**
	 * Ritorna i punti che la ricompensa assegna
	 * @return la quantita della ricompensa da assegnare
	 */
	@Override
	public int getQuantita() {
		return this.puntiVittoria;
	}

}
