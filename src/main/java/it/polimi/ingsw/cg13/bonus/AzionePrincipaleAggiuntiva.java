/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;

import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateCinque;
import it.polimi.ingsw.cg13.machinestate.StateDue;
import it.polimi.ingsw.cg13.machinestate.StateQuattro;
import it.polimi.ingsw.cg13.machinestate.StateSette;
import it.polimi.ingsw.cg13.machinestate.StateTre;
import it.polimi.ingsw.cg13.machinestate.StateUno;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class AzionePrincipaleAggiuntiva extends Ricompensa implements RicompensaSpecial {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 4116085730948383536L;
	
	/**
	 * numero azioni principali da assegnare come ricompensa
	 */
	private int num;
	
	/**
	 * Costruttore che setta a 1 le azioniPrinc da assegnare
	 */
	public AzionePrincipaleAggiuntiva() {
		num=1;
	}
	
	/**
	 * Costruttore che setta num come numero di azioniPrincipali da assegnare
	 * @param num: numero di azioniPrinc da assegnare come ricompensa
	 * @throws IllegalArgumentException: se assegno un numero negativo o pari a zero alla ricompensa
	 */
	public AzionePrincipaleAggiuntiva(int num) {
		if(num<=0)
			throw new IllegalArgumentException("Non puoi assegnare un bonus con ricompensa negativa o pari a zero");
		this.num=num;
	}

	/**
	 * Assegna la ricompensa al giocatore beneficiario in base al modello passato
	 * Assegna il num di azioniPrincipali della ricompensa allo stato del giocatore
	 * in modo che nello stato successivo possa tenerne conto per eseguire le principali in piu'
	 * Nel caso questa ricompensa venga effettuata in uno stato non corretto della partita,
	 * lancia un eccezione che viene catchata e non assegna la ricompensa
	 * @param giocatore beneficiario del bonus
	 * @param modello con cui interagire
	 * @throws IllegalArgumentException: se si passa partita o giocatore null
	 */
	@Override
	public void assegnaRicompensa(Giocatore giocatore, Partita partita) {
		super.assegnaRicompensa(giocatore, partita);
		int numPrinc=num;
		while(numPrinc>0){
			try {
				partita.getStatoPartita().addRicompensa(this);
			} catch (AzioneNotPossibleException e) {
				partita.notifyObservers(e);
			}
			numPrinc--;
		}
	}
	


	/**
	 * Calcola l'hashCode della ricompensa
	 * @return hashCode della ricompensa
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + num;
		return result;
	}

	/**
	 * Controlla se due ricompense sono uguali, ovviamente per esere uguali 
	 * devono essere entrambe della stessa classe e assegnare la stessa ricompensa
	 * @return true se le ricompense sono uguali, altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AzionePrincipaleAggiuntiva other = (AzionePrincipaleAggiuntiva) obj;
		if (num != other.num)
			return false;
		return true;
	}
	
	/**
	 * Dato uno stato beneficiario di un azione principale, rimuove l'azione bonus
	 * e ritorna il nuovo stato in base allo stato precendente arrivato come parametro
	 * Questo caso: da StateUno a StateDue
	 * @param stato: stato in cui si verifica assegnamento di azionePrincipale
	 * @param partita: modello con cui interagire
	 * @return lo stato successivo
	 */
	@Override 
	public State visit(StateUno stato,Partita partita){
		stato.getRicompense().remove(0);
		StateDue newStato=new StateDue(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());
		return newStato;
	}
	
	/**
	 * Dato uno stato beneficiario di un azione principale, rimuove l'azione bonus
	 * e ritorna il nuovo stato in base allo stato precendente arrivato come parametro
	 * Questo caso: da StateTre a StateSette
	 * @param stato: stato in cui si verifica assegnamento di azionePrincipale
	 * @param partita: modello con cui interagire
	 * @return lo stato successivo
	 */
	@Override 
	public State visit(StateTre stato,Partita partita){
		stato.getRicompense().remove(0);
		StateSette newStato=new StateSette(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());
		return newStato;
	}
	
	/**
	 * Dato uno stato beneficiario di un azione principale, rimuove l'azione bonus
	 * e ritorna il nuovo stato in base allo stato precendente arrivato come parametro
	 * Questo caso: da StateQuattro a StateCinque
	 * @param stato: stato in cui si verifica assegnamento di azionePrincipale
	 * @param partita: modello con cui interagire
	 * @return lo stato successivo
	 */
	@Override 
	public State visit(StateQuattro stato,Partita partita){
		stato.getRicompense().remove(0);
		StateCinque newStato=new StateCinque(stato.getGiocatoriMustDoAction(),stato.getGiocatoreCorrente(), stato.getRicompense());
		return newStato;
	}

	
	/**
	 * Stringa del bonus: AzionePrincipaleAggiuntiva (num)
	 * @return stringa corrispondente al bonus
	 */
	@Override
	public String toString() {
		return "AzionePrincipaleAggiuntiva ("+num+")";
	}

	/**
	 * Nel caso la ricompensa sia una ricompensa special,
	 * torna se la ricompensa può essere assegnata 
	 * @return true se il bonus e' assegnabile
	 */
	@Override
	public boolean checkBonus(Giocatore giocatore, Partita partita) {
		return true;
	}

	/**
	 * Ritorna il numero di azioni che la ricompensa assegna
	 * @return la quantita della ricompensa da assegnare
	 */
	@Override
	public int getQuantita() {
		return this.num;
	}

}
