/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;

import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class PuntoNobilta extends Ricompensa {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -7990504449702787094L;
	/**
	 * punti nobilta da assegnare al giocatore
	 */
	int puntiNobilta;
	
	/**
	 * Costruttore che setta punti come numero di punti da assegnare
	 * @param num: numero di punti da assegnare come ricompensa
	 * @throws IllegalArgumentException: se assegno un numero negativo o pari a zero alla ricompensa
	 */
	public PuntoNobilta(int punti) {
		if(punti<=0)
			throw new IllegalArgumentException("Non puoi assegnare un bonus con ricompensa negativa o pari a zero");
		this.puntiNobilta=punti;
	}
	
	/**
	 * Assegna la ricompensa al giocatore beneficiario in base al modello pasato
	 * Assegna i punti della ricompensa al giocatore
	 * @param giocatore beneficiario del bonus
	 * @param modello con cui interagire
	 * @throws IllegalArgumentException: se si passa partita o giocatore null
	 */
	@Override
	public void assegnaRicompensa(Giocatore giocatore, Partita partita){
		super.assegnaRicompensa(giocatore, partita);
		giocatore.riceviNobilta(puntiNobilta, partita);
	}
	
	/**
	 * Ritorna true se la ricompensa da nobilta, false altrimenti
	 * @return true
	 */
	@Override
	public boolean checkGiveNobilta(){
		return true;
	}

	
	/**
	 * Stringa del bonus: PuntoNobilta (num)
	 * @return stringa corrispondente al bonus
	 */
	@Override
	public String toString() {
		return "PuntoNobilta (" + puntiNobilta + ")";
	}
	
	/**
	 * Calcola l'hashCode della ricompensa
	 * @return hashCode della ricompensa
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + puntiNobilta;
		return result;
	}
	
	/**
	 * Controlla se due ricompense sono uguali, ovviamente per esere uguali 
	 * devono essere entrambe della stessa classe e assegnare la stessa ricompensa
	 * @return true se le ricompense sono uguali, altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PuntoNobilta other = (PuntoNobilta) obj;
		if (puntiNobilta != other.puntiNobilta)
			return false;
		return true;
	}
	
	/**
	 * Ritorna i punti che la ricompensa assegna
	 * @return la quantita della ricompensa da assegnare
	 */
	@Override
	public int getQuantita() {
		return this.puntiNobilta;
	}
	
	

}
