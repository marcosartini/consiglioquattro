/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;


import it.polimi.ingsw.cg13.mutatio.EmporioMutatio;

/**
 * @author lorenzo
 *
 */
public class TesseraBonusRe extends TesseraBonus { 
	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = 8936880475465283004L;
	/**
	 * ordine con cui le tessere bonus vanno assegnate
	 */
	private final int ordine;
	
	/**
	 * Costruttore di una tesseraBonus del re
	 * @param punteggioCaselleVittoria: punteggio che assegna la tessera
	 * @param ordine: ordine in cui verra' assegnata rispetto alle altre tessere
	 * @throws IllegalArgumentException: se punti negativi o pari a zero e se ordine negativo
	 */
	public TesseraBonusRe(int punteggioCaselleVittoria,int ordine){
		super(punteggioCaselleVittoria);
		if(ordine<0)
			throw new IllegalArgumentException("Non puoi assegnare un ordine negativo");
		this.ordine=ordine;
	}
	
	/**
	 * {@inheritDoc}
	 * Metodo non supportato per la tesseraDelRe
	 * Si assegna non alla costruzione di un emporio ma all'assegnamento
	 * di un tesseraBonus gruppocitta o regione
	 * @throws UnsopporttedOperationException
	 */
	@Override
	public void update(EmporioMutatio change) {

		throw new UnsupportedOperationException("TesseraBonusRe: Metodo non supportato");
	}

	/**
	 * {@inheritDoc}
	 * Viene asssegnata come conseguenza di un'altra tessera e non per 
	 * la costruzione di un emporio
	 * @return false 
	 */
	@Override
	protected boolean isAssegnabile(EmporioMutatio change) {
		return false;
	}

	/**
	 * {@inheritDoc}
	 * Ritorna la stringa:
	 * TesseraBonusRe (ordine=num, super della tessera bonus
	 */
	@Override
	public String toString() {
		return "TesseraBonusRe (ordine=" + ordine +", "+super.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see it.polimi.ingsw.cg13.bonus.TesseraBonus#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ordine;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see it.polimi.ingsw.cg13.bonus.TesseraBonus#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TesseraBonusRe other = (TesseraBonusRe) obj;
		if (ordine != other.ordine)
			return false;
		return true;
	}


}
