/**
 * 
 */
package it.polimi.ingsw.cg13.bonus;


import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class RiutilizzaBonusPermesso extends BonusNeedInput {

	/**
	 * id per la serializzazione
	 */
	private static final long serialVersionUID = -3729840915471988249L;

	/**
	 * 
	 */

	/**
	 * Ritorna la stringa della ricompensa: RiutilizzoBonusPermesso
	 * return stringa della ricompensa
	 */
	@Override
	public String toString() {
		return "RiutilizzaBonusPermesso";
	}

	/**
	 * Controlla se due ricompense sono uguali,  per esere uguali 
	 * devono essere entrambe della stessa classe
	 * @return true se le ricompense sono uguali, altrimenti false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	/**
	 * Calcola l'hashCode=33 perche' tutte le RiutilizzoBonusPermesso
	 * sono uguali tra loro, basta che appartengono alla stessa classe
	 * @return 33
	 */
	@Override
	public int hashCode() {
		return 33;
	}

	@Override
	public void assegnaBonusScelto(OggettoBonusInput oggetto, Giocatore giocatore, Partita partita) {
		oggetto.assegnaBonusInput(giocatore, partita);
	}

	/**
	 * Aggiorna l'oggetto di istanza sul server cercandolo tra le carte permesso usate del giocatore
	 * Se non lo trova lancia un'eccezione
	 */
	@Override
	public OggettoBonusInput updateFromServer(Giocatore giocatore, Partita partita, OggettoBonusInput oggetto)throws ErrorUpdateServerException {
		List<PermessoCostruzione> permessi=new ArrayList<>();
		permessi.addAll(giocatore.getPermessiCostruzione());
		permessi.addAll(giocatore.getCartePermessoUsate());
		return permessi.stream().filter(carta->carta.equals(oggetto))
		.findFirst().orElseThrow(()->new ErrorUpdateServerException(""
				+ "La carta permesso scelta non e' presente tra le carte gia' usate"));
	}

	/**
	 * Controlla se il bonus e' assegnabile
	 * In questo caso e' assegnabile se il giocatore ha almeno una carta permesso 
	 * passato come parametro e questa citta non abbia ricompense che assegnano nobilta
	 * @param giocatore: beneficiario del bonus
	 * @param partita: modello su cui agire
	 * @return true se il bonus e' assegnabile, false altrimenti 
	 */
	@Override
	public boolean checkBonus(Giocatore giocatore, Partita partita) {
		return !giocatore.getCartePermessoUsate().isEmpty() ||
				!giocatore.getPermessiCostruzione().isEmpty();
	}

	/*
	 * Non serve controllare perche' l'update cerca gia' tra le carte del giocatore
	 * e se non la trova lancia un'eccezione
	 * 
	@Override
	public boolean visitOggettoBonusInput(PermessoCostruzione permesso,Giocatore giocatore,Partita partita) throws CdQException {
		if(giocatore.getCartePermessoUsate().contains(permesso)){
			return true;
		}
		else{
			throw new AzioneNotPossibleException("La carta permesso scelta non e' presente tra le carte gia' usate");
		}
	}
	*/
	
	

}
