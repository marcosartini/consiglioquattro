package it.polimi.ingsw.cg13.bonus;

import java.io.Serializable;

import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public interface OggettoBonusInput extends Serializable{

	/**
	 * Assegna il bonus con input al giocatore
	 * @param giocatore beneficiario del bonus
	 * @param partita modello con cui interagire
	 */
	public void assegnaBonusInput(Giocatore giocatore,Partita partita);
	
	/*
	 * Funzione che accetta se un parametro di bonusInput e' corretto o se
	 * viene passato qualcosa che non c'entra lancia un eccezione
	 * Funziona anche senza
	 * 
	public boolean acceptParametroCorretto(RicompensaSpecial ricompensa,Giocatore giocatore,Partita partita) throws CdQException;
	*/

}

