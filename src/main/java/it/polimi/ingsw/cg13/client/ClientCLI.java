package it.polimi.ingsw.cg13.client;


import java.io.IOException;
import java.io.PrintStream;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.azioni.AcquistoAiutante;
import it.polimi.ingsw.cg13.azioni.AcquistoPermesso;
import it.polimi.ingsw.cg13.azioni.BonusInput;
import it.polimi.ingsw.cg13.azioni.CambioConsiglio;
import it.polimi.ingsw.cg13.azioni.CambioConsiglioVeloce;
import it.polimi.ingsw.cg13.azioni.CambioPermessi;
import it.polimi.ingsw.cg13.azioni.CompraAzionePrincipale;
import it.polimi.ingsw.cg13.azioni.CostruzioneConKing;
import it.polimi.ingsw.cg13.azioni.CostruzioneConPermesso;
import it.polimi.ingsw.cg13.azioni.FinisciTurno;
import it.polimi.ingsw.cg13.azionimarket.AcquistaInserzione;
import it.polimi.ingsw.cg13.azionimarket.VendiInserzione;
import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.CartaPoliticaJolly;
import it.polimi.ingsw.cg13.carte.CartaPoliticaSemplice;
import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.comandi.ChatMessage;
import it.polimi.ingsw.cg13.comandi.Comando;
import it.polimi.ingsw.cg13.comandi.Exit;
import it.polimi.ingsw.cg13.comandi.Help;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.comandi.Print;
import it.polimi.ingsw.cg13.comandi.Query;
import it.polimi.ingsw.cg13.comandi.VisualizzaMarket;
import it.polimi.ingsw.cg13.eccezioni.AzioneNotPossibleException;
import it.polimi.ingsw.cg13.eccezioni.CartePoliticheNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.PermessoNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.mutatio.NomeNonDisponibileMutatio;
import it.polimi.ingsw.cg13.mutatio.PartitaMutatio;
import it.polimi.ingsw.cg13.mutatio.PrimoGiocatoreMutatio;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.oggetti.Vendibile;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

public class ClientCLI extends ClientInterfaccia implements Runnable {
	
	private Scanner scanner;
	private Partita modello;
	private Giocatore giocatore;
	private Login loginPlayer;
	private PrintStream printer = new PrintStream(System.out);
	private boolean partitaIniziata=false;
	

	private static final Logger logger = LoggerFactory.getLogger(ClientCLI.class);

	private static final String NUMERO_ERRATO_PARAMETRI = "Hai inserito un numero sbagliato di parametri";
	
	public ClientCLI(Scanner scan){
		this.scanner=scan;
		this.startConnection();
	}
	
	public boolean parametriInseriti(String[] parametri,int numParametri){
		if(parametri.length-1!=numParametri){
			throw new IllegalArgumentException(NUMERO_ERRATO_PARAMETRI +"\n"
					+ "Azione_"+parametri[0]+" richiede "+numParametri+" parametri\n");
		}
		else return true;
	}
	
	/**
	 * Parser che data una stringa in input on certe caratteristiche
	 * la converte in un comando del gioco
	 * @param comando: stringa corrispondente ad un azione del gioco
	 * @return comando: comando/azione che vuole compiere il giocatore
	 */
	private Comando parserComandi(String comando) {
		String[] parametriAzione=comando.split(" ");
		int indice=0;
		Comando azione=null;
		Query localQuery=null;
		String tipoAzione=parametriAzione[indice];
		try{
			switch (tipoAzione){
			case "AcquistoPermesso":
					if(parametriInseriti(parametriAzione,3))
						azione=generaAcquistaPermesso(parametriAzione);
				break;
			case "CambioConsiglioVeloce":
					if(parametriInseriti(parametriAzione,2))
						azione=generaCambioConsiglioVeloce(parametriAzione);
				break;
			case "CambioConsiglio":
					if(parametriInseriti(parametriAzione,2))
						azione=generaCambioConsiglioPrinc(parametriAzione);
				break;
			case "CostruisciConRe":
					if(parametriInseriti(parametriAzione,2))
						azione=generaCostruisciConRe(parametriAzione);
				break;	
			case "AcquistoAiutante":
					if(parametriInseriti(parametriAzione,0))
						azione=new AcquistoAiutante(this.giocatore);
				break;
			case "CambioPermessi":
					if(parametriInseriti(parametriAzione,1))
						azione=generaCambioPermessi(parametriAzione);
				break;
			case "AcquistoAzionePrincipale":
					if(parametriInseriti(parametriAzione,0))
						azione=new CompraAzionePrincipale(this.giocatore);
				break;
			case "CostruisciConPermesso":
					if(parametriInseriti(parametriAzione,2))
						azione=generaCostruisciConPermesso(parametriAzione);
				break;
			case "Help":
					localQuery=new Help(this.giocatore);
					printer.print(localQuery.perform(modello));
				break;
			case "Print":
					localQuery=new Print(this.giocatore);
					printer.print(localQuery.perform(modello));
				break;
			case "Exit":
					azione=new Exit(this.giocatore);
				break;
			case "BonusInput":
					if(parametriAzione.length>2)
						azione=generaBonusInput(parametriAzione);
					else throw new IllegalArgumentException("Hai inserito un numero sbagliato di parametri\n");
				break;
			case "Vendo":
					if(parametriAzione.length>2)
						azione=generaVendiInserzione(parametriAzione);
					else throw new IllegalArgumentException("Hai inserito un numero sbagliato di parametri\n");
				break;
			case "Acquisto":
					if(parametriInseriti(parametriAzione,1))
						azione=generaAcquistoInserzione(parametriAzione);
				break;
			case "FinisciTurno":
					azione=new FinisciTurno(this.giocatore);
				break;
			case "PrintMarket":
					localQuery=new VisualizzaMarket(this.giocatore);
					printer.print(localQuery.perform(modello));
				break;
			case "chat":
					azione=generaChatMessage(parametriAzione);
				break;
			default:
				printer.println("Comando inserito non valido !!\nUsa Help per vedere i comandi :)");
				break;
			}
		}catch(IllegalArgumentException | CdQException errore){
			logger.error(errore.getMessage(), errore);
		}
		return azione;
		
	}
	
	
	/**
	 * 
	 * @param parametriAzione: vettore di stringa del messaggio
	 * @return messaggioChat: messaggio scritto dal giocatore
	 */
	private Comando generaChatMessage(String[] parametriAzione) {
		String testo="";
		for(int i=1;i<parametriAzione.length;i++){
			testo+=parametriAzione[i]+" ";
		}
		return new ChatMessage(this.giocatore,testo);
	}

	/**
	 * Ritorna un azione di vendita di un inserzione
	 * @param parametriAzione
	 * @return vendiInserzione, azione di vendita
	 * @throws CdQException
	 */
	private VendiInserzione generaVendiInserzione(String[] parametriAzione) throws CdQException{
		//Vendi Oggetti
		int indice=0;
		indice++;
		//Oggetto in vendita scelto
		String tipoScelto=parametriAzione[indice];
		indice++;
		CasellaRicchezza prezzo;
		Vendibile oggetto=null;
		switch(tipoScelto){
			case "aiutante":
				if(this.giocatore.numeroAiutanti()==0 || !this.parametriInseriti(parametriAzione, 2))
					throw new IllegalArgumentException("Non hai aiutanti da vendere");
				else{
					prezzo=new CasellaRicchezza(Integer.parseInt(parametriAzione[indice]));
					oggetto=new Aiutante();
					return new VendiInserzione(giocatore,new Inserzione(prezzo,giocatore,oggetto));
				}
			case "permesso":
				int numeroPermesso=Integer.parseInt(parametriAzione[indice]);
				List<PermessoCostruzione> permessi=new ArrayList<>();
				permessi.addAll(giocatore.getPermessiCostruzione());
				permessi.addAll(giocatore.getCartePermessoUsate());
				indice++;
				if(numeroPermesso>=permessi.size() || !this.parametriInseriti(parametriAzione, 3))
					throw new IllegalArgumentException("Non hai un permesso corrispondente a quel numero");
				else{
					oggetto=permessi.get(numeroPermesso);
					prezzo=new CasellaRicchezza(Integer.parseInt(parametriAzione[indice]));
					return new VendiInserzione(giocatore,new Inserzione(prezzo,giocatore,oggetto));
				}
			case "cartapolitica":
				Colore coloreCarta;
				List<CartaPolitica> carte=new ArrayList<>();
				if("jolly".equals(parametriAzione[indice])){
					carte.add(new CartaPoliticaJolly());
				}
				else{
					coloreCarta=modello.getColorePolitico(parametriAzione[indice]);
					carte.add(new CartaPoliticaSemplice(coloreCarta));
				}
				indice++;
				if(!giocatore.hasCartePolitiche(carte) || !this.parametriInseriti(parametriAzione, 3))
					throw new IllegalArgumentException("Non hai questa cartaPolitica");
				else{
					oggetto=carte.get(0);
					prezzo=new CasellaRicchezza(Integer.parseInt(parametriAzione[indice]));
					return new VendiInserzione(giocatore,new Inserzione(prezzo,giocatore,oggetto));
				}
			default:
				throw new IllegalArgumentException("Oggetto in vendita inserito errato");
		}
		
	}
	
	private AcquistaInserzione generaAcquistoInserzione(String[] parametriAzione) 
			throws AzioneNotPossibleException{
		//Vendi Oggetti
		int indice=0;
		indice++;
		//Oggetto in vendita scelto
		int numInserzione=Integer.parseInt(parametriAzione[indice]);
		List<Inserzione> inserzioniMarket=modello.getStatoPartita().getMarket().getBanchetto();
		if(inserzioniMarket.isEmpty())
			throw new IllegalArgumentException("Non ci sono inserzioni da comprare");
		else return new AcquistaInserzione(giocatore,inserzioniMarket.
					stream().filter(ins->ins.getIdInserzione()==numInserzione).
					findFirst().orElseThrow(()
							->new IllegalArgumentException("L'inserzione con questo id non esiste")));
		
	}
	
	private BonusInput generaBonusInput(String[] parametriAzione) throws CittaNotFoundException, RegioneNotFoundException{
		//Inserimento input Bonus
			int indice=0;
			indice++;
			String tipoBonus=parametriAzione[indice];
			indice++;
			PermessoCostruzione permesso=null;
			int numeroPermesso;
			switch(tipoBonus){
			case "riusaPermesso":
				numeroPermesso=Integer.parseInt(parametriAzione[indice]);
				List<PermessoCostruzione> permessi=new ArrayList<>();
				permessi.addAll(giocatore.getPermessiCostruzione());
				permessi.addAll(giocatore.getCartePermessoUsate());
				if(numeroPermesso<permessi.size() && 
						this.parametriInseriti(parametriAzione, 2)){
					permesso=permessi.get(numeroPermesso);
					return new BonusInput(this.giocatore,permesso);
				}
				else throw new IllegalArgumentException("Non hai un permesso corrispondente a quel numero");
			case "riusaCitta":
				if(this.parametriInseriti(parametriAzione, 2)){
					String nomeCitta=parametriAzione[indice];
					Citta citta=modello.getMappaPartita().getNodoCitta(nomeCitta).getCittaNodo();
					return new BonusInput(this.giocatore,citta);
				}
			case "ottieniPermesso":	
				if(this.parametriInseriti(parametriAzione, 3)){
					String nomeRegione=parametriAzione[indice];
					Regione regione=modello.getRegione(nomeRegione);
					indice++;
					numeroPermesso=Integer.parseInt(parametriAzione[indice]);
					MazzoPermessi mazzo=regione.getMazzoPermesso();
					if(numeroPermesso<mazzo.getPermessiVisibili()){
						permesso=mazzo.getCarta(numeroPermesso, mazzo.getPermessiVisibili());
						return new BonusInput(this.giocatore,permesso);
					}
					else throw new IllegalArgumentException("Non esiste un permesso corrispondente a quel numero");
				}
			default:	
				throw new IllegalArgumentException("Bonus inserito errato");
			}
		// PermessoScelto
	}
	
	private CostruzioneConPermesso generaCostruisciConPermesso(String[] parametriAzione) throws PermessoNotFoundException, CittaNotFoundException {
		//Costruzione con permesso
		int indice=0;
		indice++;
		// PermessoScelto
		int numeroPermesso=Integer.parseInt(parametriAzione[indice]);
		List<PermessoCostruzione> permessiGiocatore=this.giocatore.getPermessiCostruzione();
		if(!permessiGiocatore.isEmpty()){
			if(permessiGiocatore.size()<numeroPermesso){
				throw new PermessoNotFoundException("Il numero di permesso inserito e' sbagliato");
			}
			PermessoCostruzione permesso=permessiGiocatore.get(numeroPermesso);
			
			indice++;
			// Citta scelta
			String nomeCitta=parametriAzione[indice];
			Citta cittaScelta=permesso.getCitta(nomeCitta); 

			NodoCitta nodoScelto = modello.getMappaPartita().getNodoCitta(cittaScelta);
			return new CostruzioneConPermesso(modello.getGiocatoreCorrente(),nodoScelto,permesso);
		}
		throw new PermessoNotFoundException("Non ci sono permessi da utilizzare");
	}
	
	private CambioPermessi generaCambioPermessi(String[] parametriAzione) throws RegioneNotFoundException {
		//CambioPermessi
		int indice=0;
		indice++;
		//Acquisci Regione scelta
		String nomeRegione=parametriAzione[indice];
		Regione regione=modello.getRegione(nomeRegione);
		return new CambioPermessi(regione.getMazzoPermesso(),this.giocatore);
	}
	
	private CostruzioneConKing generaCostruisciConRe(String[] parametriAzione) throws CittaNotFoundException, CartePoliticheNotFoundException, ColoreNotFoundException{
		
		int indice=0;
		indice++;
		//Citta del Percorso
		String[] cittaScelte;
		cittaScelte=parametriAzione[indice].split("-");
		List<NodoCitta> pathRe=new ArrayList<>();
		for(int j=0;j<cittaScelte.length;j++){
			//Ricerca nodo Citta e lo aggiunge
			pathRe.add(modello.getNodoCitta(cittaScelte[j]));
		}
		indice++;
		List<CartaPolitica> carteScelte = carteScelte(parametriAzione[indice]);
		if(!this.giocatore.hasCartePolitiche(carteScelte)){
			throw new CartePoliticheNotFoundException("Carte Politiche inserite errate");
		}
		return new CostruzioneConKing(this.giocatore,pathRe,carteScelte);
	}
	
	private CambioConsiglioVeloce generaCambioConsiglioVeloce(String[] parametriAzione) throws RegioneNotFoundException, ColoreNotFoundException, AzioneNotPossibleException {
		//CambioConsiglioVeloce
		int indice=0;
		indice++;
		Consiglio consiglio=getConsiglioScelto(parametriAzione[indice]);	
		indice++;
		//Consigliere scelto dall'utente
		Consigliere consigliereScelto;
		try{
			consigliereScelto = getConsigliereScelto(parametriAzione[indice]);
			return new CambioConsiglioVeloce(this.giocatore,consiglio,consigliereScelto);
		}catch(NoSuchElementException e){
			logger.error(e.getMessage(),e);
			throw new AzioneNotPossibleException("Non ci sono consiglieri "+parametriAzione[indice]+" nel banco");
		}
		
	
		
	}
	
	private CambioConsiglio generaCambioConsiglioPrinc(String[] parametriAzione) throws RegioneNotFoundException, ColoreNotFoundException, AzioneNotPossibleException{
		//CambioConsiglio
		int indice=0;
		indice++;
		Consiglio consiglio=getConsiglioScelto(parametriAzione[indice]);	
		indice++;
		//Consigliere scelto dall'utente
		Consigliere consigliereScelto;
		try{
			consigliereScelto = getConsigliereScelto(parametriAzione[indice]);
			return new CambioConsiglio(this.giocatore,consiglio,consigliereScelto);
		}catch(NoSuchElementException e){
			logger.error(e.getMessage(),e);
			throw new AzioneNotPossibleException("Non ci sono consiglieri "+parametriAzione[indice]+" nel banco");
		}
		
	}

	
	
	private AcquistoPermesso generaAcquistaPermesso(String[] parametriAzione) throws RegioneNotFoundException, PermessoNotFoundException, CartePoliticheNotFoundException, ColoreNotFoundException{
		
		// AcquistaPermesso
		int indice=0;
		indice++;
		// RegioneScelta		
		String nomeRegione=parametriAzione[indice];
		Regione regione=modello.getRegione(nomeRegione);
		
		indice++;
		// PermessoScelto
		int numeroPermesso=Integer.parseInt(parametriAzione[indice]);
		PermessoCostruzione permesso;
		if(numeroPermesso<regione.getMazzoPermesso().getPermessiVisibili()){
			permesso=regione.getMazzoPermesso().getCarta(numeroPermesso,regione.getMazzoPermesso().getPermessiVisibili());
		}
		else{
			throw new PermessoNotFoundException("Il numero di permesso inserito e' sbagliato");
		}
		//CartePoliticheScelte
		indice++;
		List<CartaPolitica> carteScelte= carteScelte(parametriAzione[indice]);
		if(this.giocatore.hasCartePolitiche(carteScelte)){
				return new AcquistoPermesso(this.giocatore,carteScelte,regione,permesso);
		}                                 
		else{
			throw new CartePoliticheNotFoundException("Carte Politiche inserite errate");
		}	
	}
	
	

	public Comando getInput() {
		String input=scanner.nextLine();
		return parserComandi(input);
	}
	
	
	private List<CartaPolitica> carteScelte(String colori) throws ColoreNotFoundException{
		String[] coloriCarte;
		coloriCarte=colori.split("-");
		List<CartaPolitica> carteScelte=new ArrayList<>();
		List<CartaPolitica> carteJolly=new ArrayList<>();
		Colore coloreCarta;
		for(int i=0;i<coloriCarte.length;i++){
			if("jolly".equals(coloriCarte[i])){
				carteJolly.add(new CartaPoliticaJolly());
			}
			else{
				coloreCarta=modello.getColorePolitico(coloriCarte[i]);
				carteScelte.add(new CartaPoliticaSemplice(coloreCarta));
				}
		}
		carteScelte.addAll(carteJolly);
		return carteScelte;
	}
	
	private Consiglio getConsiglioScelto(String zonaConsiglio) throws RegioneNotFoundException{
		Consiglio consiglio;
		//Acquisci Regione del Consiglio scelta
		if("#re".equals(zonaConsiglio)){
		consiglio=modello.getKing().getConsiglio();
		}
		else{
			Regione regione=modello.getRegione(zonaConsiglio);
			consiglio=regione.getConsiglio();
		}	
		return consiglio;
	}
	
	private Consigliere getConsigliereScelto(String colore) throws ColoreNotFoundException{
		Colore coloreConsigliere=modello.getColorePolitico(colore);
		if(!modello.getBanco().getConsiglieri().containsKey(coloreConsigliere))
			throw new ColoreNotFoundException("Colore non presente");
		return modello.getBanco().getConsiglieri().get(coloreConsigliere).peek();
	}

	public Giocatore getGiocatore() {
		return giocatore;
	}

	public void setGiocatore(Giocatore giocatore) {
		if(giocatore.getNome().equals(loginPlayer.getNomeGiocatore())){
			this.giocatore = giocatore;
		}
	}

	public void info(String message) {
		printer.println(message);
	}
	
	public void updateGiocatore(){
		if(modello!=null){
			if(this.giocatore==null){
				this.giocatore=this.modello.getGiocatori().stream().
						filter(g->g.getNome().equals(this.loginPlayer.getNomeGiocatore()))
						.findFirst().orElse(null);
			}
			else{
				Giocatore giocatoreAggiornato=this.modello.getGiocatori().stream().
						filter(g -> g.equals(this.giocatore)).findFirst().orElse(null);
				if(giocatoreAggiornato!=null){
					this.giocatore=giocatoreAggiornato;
				}else{
					giocatoreAggiornato=this.modello.getGiocatoriOffline().stream().
							filter(g -> g.equals(this.giocatore)).findFirst().orElse(null);
					if(giocatoreAggiornato!=null){
						this.giocatore=giocatoreAggiornato;
					}
				}
			}
		}
	}
	
	private void selectConnection(){
		String tipoConnessione;
		String ipAdress;
		String port;
		do{
		printer.print("Scegli il tipo di connessione: Socket(S) o RMI(R)-> ");
		tipoConnessione=scanner.nextLine();
		}while(!"S".equalsIgnoreCase(tipoConnessione) && !"R".equalsIgnoreCase(tipoConnessione));
		printer.print("Inserisci indirizzo IP-> ");
		ipAdress=scanner.nextLine();
		printer.print("Inserisci PORT-> ");
		port=scanner.nextLine();
		ClientConnection connessione;
		try {	
			if("S".equalsIgnoreCase(tipoConnessione)){
				connessione=new ClientSocket(ipAdress,port,loginPlayer);
				connessione.startConnection(this);
			}
			else if("R".equalsIgnoreCase(tipoConnessione)){
				connessione=new ClientRMI(port, ipAdress, loginPlayer);
				connessione.startConnection(this);
			}
			
		} catch (IOException | NotBoundException | AlreadyBoundException  e) {
				logger.error(e.getMessage(), e);
				printer.println("Impossibile effettuare la connessione");
		}
	}
	
	private Login selectLogin(){
		printer.print("Inserisci il nome utente -> ");
		return new Login(scanner.nextLine());
	}
	

	public void visualizzaErrore(Exception errore){
		printer.println(errore.getMessage());
	}



	@Override
	public  void update(Mutatio change) {
		try {
			modello=change.changeModel(modello);
		} catch (IllegalArgumentException | CdQException e) {
			logger.error(e.getMessage(), e);
			this.visualizzaErrore(e);
			//richiedi modello aggiornato 
		}
		printer.println(change.toString());
		this.updateGiocatore();
		if(change instanceof PrimoGiocatoreMutatio){
			inserisciParametriNuovaPartita();
		}
		if(change instanceof NomeNonDisponibileMutatio){
			loginPlayer=this.selectLogin();
			this.notifyObservers(loginPlayer);
		}
		if(change instanceof PartitaMutatio){
			synchronized(this){
				partitaIniziata=true;
			}	
		}
			
	}
	
	private void inserisciParametriNuovaPartita() {
		String maxGiocatori;
		String mappa;
		String tempoAzione;
		do{
			printer.print("Inserisci Numero Massimo(>1) di Giocatori: ");
			maxGiocatori=this.scanner.nextLine();
		}while(Integer.parseInt(maxGiocatori)<2);
		do{
		printer.print("Inserisci numero di una delle mappe disponibili ( 0 - 7 ): ");
		mappa=this.scanner.nextLine();
		}while(Integer.parseInt(mappa)<0 || Integer.parseInt(mappa)>8);
		do{
		printer.println("Inserisci il tempo massimo( secondi, >60 ) per le azioni di un giocatore: ");
		tempoAzione=this.scanner.nextLine();
		}while(Integer.parseInt(tempoAzione)<60);
		this.notifyObservers(new InviaParametriPartita(
				Integer.parseInt(maxGiocatori),Integer.parseInt(tempoAzione),mappa));
		printer.println("Caricamento dati in corso... \nAttendi il login di almeno un altro giocatore");
	}

	@Override
	public void update(Exception errore) {
		this.visualizzaErrore(errore);
	}

	public void startConnection() {
		loginPlayer=this.selectLogin();
		this.selectConnection();
	}

	@Override
	public void run() {
		Comando comando;
		boolean exit=false;
		while(!this.partitaIsIniziata());
		
		while(!exit){
			comando=this.getInput();
			if(comando instanceof Exit)
				exit=true;
			this.notifyObservers(comando);		
			
		}
	}

	private synchronized boolean partitaIsIniziata() {
		return this.partitaIniziata;
	}

}
