package it.polimi.ingsw.cg13.client;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.polimi.ingsw.cg13.comandi.Comando;
import it.polimi.ingsw.cg13.comandi.Exit;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.observable.ObserverC;
import it.polimi.ingsw.cg13.server.RMIGameViewRemote;
import it.polimi.ingsw.cg13.server.RMIRegistrationViewRemote;

public class ClientRMI extends ObservableC<Mutatio> implements ClientConnection, Runnable,ObserverC<Comando>, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2802304390330409422L;
	private final int rmiPort;
	private final String host;
	private Login loginPlayer;
	private String name = "ConsiglioDeiQuattro";
	private transient RMIGameViewRemote server;
	private ClientRMIView rmiView;
	private boolean exit;
	

	public ClientRMI(String rmiPort, String host,  Login login) {
		this.rmiPort=Integer.parseInt(rmiPort);
		this.host=host;
		this.loginPlayer=login;
		exit=false;
	}

	@Override
	public void startConnection(ClientInterfaccia interfaccia) throws IOException, NotBoundException, AlreadyBoundException {
		Registry registry = LocateRegistry.getRegistry(host, rmiPort); 
		RMIRegistrationViewRemote register = (RMIRegistrationViewRemote)registry.lookup(name);
		this.registerObserver(interfaccia);
		interfaccia.registerObserver(this);
		rmiView= new ClientRMIView(this);
		server=register.registerClient(rmiView, loginPlayer);
		server.checkClientRegistration(loginPlayer);
		ExecutorService executor = Executors.newCachedThreadPool();
		executor.submit(this);
	}

	@Override
	public void run() {

		while(!exit){	
			}
		
		//chiusura connessione RMI
		try {
			this.server.unregisterClient(this.rmiView);
		} catch (RemoteException e) {
			this.notifyObservers(e);
		}
		
		
		
	}
	
	
	@Override
	public void update(Comando comando) {
		try{
			if(comando!=null){
				server.eseguiComando(comando);
				if(comando instanceof Exit)
					this.exit=true;
			}
		} catch (Exception e) {
			this.notifyObservers(e);
		}
		
	}
	
	
	
}
