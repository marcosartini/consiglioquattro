package it.polimi.ingsw.cg13.client;

import it.polimi.ingsw.cg13.comandi.Comando;


public class InviaParametriPartita implements Comando {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6128643348861531311L;
	private int maxNumGiocatori;
	private int tempoAzione;
	private String nomeMappa;
	
	public InviaParametriPartita(int num,int tempo,String nomeMappa) {
		this.maxNumGiocatori=num;
		this.tempoAzione=tempo;
		this.nomeMappa=nomeMappa;
	}

	public int getMaxNumGiocatori() {
		return maxNumGiocatori;
	}

	public int getTempoAzione() {
		return tempoAzione;
	}

	public String getNomeMappa() {
		return nomeMappa;
	}


}
