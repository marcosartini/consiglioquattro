package it.polimi.ingsw.cg13.client;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;

@FunctionalInterface
public interface ClientConnection {



	public void startConnection(ClientInterfaccia interfaccia) throws IOException, NotBoundException, AlreadyBoundException;
}
