package it.polimi.ingsw.cg13.client;

import java.io.PrintStream;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.gui.MainGUI;
import javafx.application.Application;



public class Client {

	private static final Logger logger = LoggerFactory.getLogger(Client.class);

	
	public static void main(String[] args) {

		
		Scanner scanner=new Scanner(System.in);
		PrintStream printer = new PrintStream(System.out);
		
		String interfaccia;
		ExecutorService executor;
		do{
		printer.print("Scegli il tipo di interfaccia: CLI(C) o GUI(G)-> ");
		interfaccia=scanner.nextLine();
		}while(!"G".equalsIgnoreCase(interfaccia) && !"C".equalsIgnoreCase(interfaccia));
		
		try{
			if("G".equalsIgnoreCase(interfaccia)){
				Application.launch(MainGUI.class, args);	
			}
			else if ("C".equalsIgnoreCase(interfaccia)){
				executor = Executors.newCachedThreadPool();
				executor.submit(new ClientCLI(scanner));
			}
		}catch (Exception e) {
			logger.error(e.getMessage(),e);
		}	
		
	}

}
