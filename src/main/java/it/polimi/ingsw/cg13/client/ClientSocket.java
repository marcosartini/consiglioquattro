package it.polimi.ingsw.cg13.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.polimi.ingsw.cg13.comandi.Login;

public class ClientSocket implements ClientConnection{
	
	private final int port;
	private final String ip;
	private Login loginPlayer;
	
	public ClientSocket(String ip,String port,Login loginPlayer){
		this.port=Integer.parseInt(port);
		this.ip=ip;
		this.loginPlayer=loginPlayer;
	}
	@Override
	public void startConnection(ClientInterfaccia interfaccia) throws IOException {

		Socket socket = new Socket(ip, port);
		ExecutorService executor = Executors.newCachedThreadPool();

		executor.submit(new ClientOutHandler(
				new ObjectOutputStream(socket.getOutputStream()),interfaccia,this.loginPlayer,socket));
		
		
		executor.submit(new ClientInHandler(
				new ObjectInputStream(socket.getInputStream()),interfaccia,socket));
	}
	
	
}
