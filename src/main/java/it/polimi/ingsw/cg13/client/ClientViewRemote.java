package it.polimi.ingsw.cg13.client;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.mutatio.GiocatoreNuovoMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;

public interface ClientViewRemote extends Remote {

	public void updateClient(Mutatio mutatio) throws RemoteException;
	public void updateClient(GiocatoreNuovoMutatio giocatore) throws RemoteException;
	public void updateClient(Exception e) throws RemoteException;
}
