package it.polimi.ingsw.cg13.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;


public class ClientInHandler extends ObservableC<Mutatio> implements Runnable {

	private ObjectInputStream socketIn;
	private Socket socket;
	
	public ClientInHandler(ObjectInputStream socketIn,ClientInterfaccia interfaccia,Socket socket) {
		this.socketIn=socketIn;
		this.socket=socket;
		this.registerObserver(interfaccia);
	}
	
	@Override
	public void run() {
		try {
			while(!socket.isClosed()){
				Object object=socketIn.readObject();
				if(object instanceof Mutatio){
					Mutatio change=(Mutatio)object;
					this.notifyObservers(change);	
				}else if(object instanceof Exception){
					Exception errore=(Exception)object;
					this.notifyObservers(errore);
				}
			}
		} catch (ClassNotFoundException | IOException e) {
			this.notifyObservers(e);
		}		
		//Chiusura Socket
		try {
			socket.close();
		} catch (IOException e1) {
			this.notifyObservers(e1);
		}
	}
}
