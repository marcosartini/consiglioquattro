package it.polimi.ingsw.cg13.client;


import it.polimi.ingsw.cg13.comandi.Comando;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.observable.ObserverC;


public abstract class ClientInterfaccia extends ObservableC<Comando> implements ObserverC<Mutatio>{

	
}
