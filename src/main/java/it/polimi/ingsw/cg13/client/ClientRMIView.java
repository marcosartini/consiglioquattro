package it.polimi.ingsw.cg13.client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import it.polimi.ingsw.cg13.mutatio.GiocatoreNuovoMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.mutatio.StartConnessioneMutatio;

public class ClientRMIView extends UnicastRemoteObject implements ClientViewRemote {

	/**
	 * Questa classe riceve le notifiche dal server RMI e le mostra al client attraverso l'interfaccia di visualizzazione scelta
	 */
	private static final long serialVersionUID = 3605237204891764202L;
	private ClientRMI rmi;


	public ClientRMIView(ClientRMI rmi) throws RemoteException {
		this.rmi=rmi;
		this.rmi.notifyObservers(new StartConnessioneMutatio("RMI"));
	}
	

	@Override
	public void updateClient(GiocatoreNuovoMutatio giocatore) throws RemoteException{
		rmi.notifyObservers(giocatore);
	}
	
	@Override
	public void updateClient(Mutatio mutatio) throws RemoteException {
		//instanceof di prova, in attesa di un migliore metodo (forse pub/sub)
		if(mutatio instanceof GiocatoreNuovoMutatio)
			updateClient((GiocatoreNuovoMutatio)mutatio);
		else
			rmi.notifyObservers(mutatio);
	}
	
	@Override
	public void updateClient(Exception e){
		rmi.notifyObservers(e);
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 31;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
	

}
