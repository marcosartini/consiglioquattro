package it.polimi.ingsw.cg13.client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import it.polimi.ingsw.cg13.comandi.Comando;
import it.polimi.ingsw.cg13.comandi.Exit;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.mutatio.StartConnessioneMutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.observable.ObserverC;




public class ClientOutHandler extends ObservableC<Mutatio> implements Runnable,ObserverC<Comando> {

	private ObjectOutputStream socketOut;
	private Login nomePlayer;
	private Socket socket;
	private boolean exit;
	
	public ClientOutHandler(ObjectOutputStream socketOut,ClientInterfaccia interfaccia,Login nome,Socket socket) {
		this.socketOut = socketOut;
		this.nomePlayer=nome;
		this.socket=socket;
		exit=false;
		interfaccia.registerObserver(this);
		this.registerObserver(interfaccia);
		this.notifyObservers(new StartConnessioneMutatio("Socket"));
	}
	
	public void sendNomeGiocatore() throws IOException{
		socketOut.reset();
		socketOut.writeObject(this.nomePlayer);
		socketOut.flush();
	}

	@Override
	public void run() {
		try{
			this.sendNomeGiocatore();
			while (!this.socket.isClosed() && !exit) {
			}
		} 
		catch (IOException errore) {
			this.notifyObservers(errore);
		}
		
		//Chiusura connessione socket
		try{
			socket.close();
		} 
		catch (IOException e){
			this.notifyObservers(e);
		}
		
	}

	@Override
	public void update(Comando comando) {
		try{
			if(comando!=null){
				socketOut.reset();
				socketOut.writeObject(comando);
				socketOut.flush();
				if(comando instanceof Exit)
					this.exit=true;
			}
		} catch (IOException e) {
			this.notifyObservers(e);
		}
		
	}

}
