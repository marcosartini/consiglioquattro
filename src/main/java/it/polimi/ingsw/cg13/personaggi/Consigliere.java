/**
 * 
 */
package it.polimi.ingsw.cg13.personaggi;

import java.io.Serializable;

import it.polimi.ingsw.cg13.colore.Colore;


/**
 * @author Marco
 *
 */
public class Consigliere implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -573494469957266770L;
	private final Colore colore;
	/**
	 * @throws IllegalArgumentException --> il colore inserito è nullo
	 * @param colore
	 * 
	 */
	public Consigliere(Colore colore) {
		if(colore!=null)
			this.colore=colore;
		else
			throw new IllegalArgumentException("Colore inserito nullo!");
	}
	/**
	 * @return the colore
	 */
	public Colore getColor() {
		return colore;
	}
	
	public boolean equals(Consigliere cons){
		if(cons!=null){
		return this.getColor().equals(cons.getColor());
		}
		throw new IllegalArgumentException("Consigliere nullo");
	}
	@Override
	/**
	 * toString del metodo
	 */
	public String toString() {
		return "Consigliere " + colore + "";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colore == null) ? 0 : colore.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Consigliere other = (Consigliere) obj;
		if (colore == null) {
			if (other.colore != null)
				return false;
		} else if (!colore.equals(other.colore))
			return false;
		return true;
	}
	

}
