/**
 * 
 */
package it.polimi.ingsw.cg13.personaggi;


import java.io.Serializable;
import java.util.List;

import it.polimi.ingsw.cg13.mappa.ConsiglioRe;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.ProprietarioConsiglio;
import it.polimi.ingsw.cg13.mutatio.KingMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;

/**
 * @author Marco
 *
 */
public class King extends ObservableC<Mutatio> implements Serializable, ProprietarioConsiglio{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7244361505553374111L;
	private NodoCitta posizione;
	private ConsiglioRe consiglioKing;
	

	/**
	 * Costruttore con parametri
	 * @param partenza: citta di partenza del re
	 * @param consiglio: consiglio del Re 
	 * @throws IllegalArgumentException--> valori nulli di consiglio e partenza
	 */
	public King(NodoCitta partenza,ConsiglioRe consiglio) {
		if(partenza!=null)
			this.setPosizione(partenza);
		else 
			throw new IllegalArgumentException("Citta di partenza non valida!");
		
		this.setConsiglioKing(consiglio);

	}

	/**
	 * Costruttore con King --> viene passato un altro oggetto re
	 * @param re
	 * @throws IllegalArgumentException--> valore passato nullo
	 */
	public King(King re) {
		if(re!=null)
			this.posizione=re.posizione;
		else
			throw new IllegalArgumentException("re non deve essere nullo!");
	}

	/**
	 * Ritorna la citt� in cui � posizionato il re
	 * @return posizione
	 */
	public NodoCitta getPosizione() {
		return posizione;
	}

	/**
	 * Modifica la Posizione del King
	 * @param nuovaPosizione
	 * @throws IllegalArgumentException--> posizione nulla
	 */
	public void setPosizione(NodoCitta nuovaPosizione) {
		if(nuovaPosizione!= null){
			this.posizione = nuovaPosizione;
		}
		else
			throw new IllegalArgumentException("La nuovaPosizione passata al King non è valida!");
	}

	/**
	 * ritorna il consiglio del Re
	 * @return consiglioKing
	 */
	@Override	
	public ConsiglioRe getConsiglio() {
		return consiglioKing;
	}

	/**
	 * modifica il consiglio del Re assegnandogli un valore passato
	 * @param consiglioKing
	 * @throws IllegalArgumentException-->consiglioKing nullo
	 */
	public void setConsiglioKing(ConsiglioRe consiglioKing) {
//		if(consiglioKing!=null)
			this.consiglioKing = consiglioKing;
//		else
//			throw new IllegalArgumentException("Il consiglioKing passato non è valido!");
	}

	/**
	 * ToString della classe
	 */
	@Override
	public String toString() {
		return "King {citta=" + posizione.getCittaNodo().getNome() + ", " + consiglioKing +"}";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((posizione == null) ? 0 : posizione.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		King other = (King) obj;
		if (posizione == null) {
			if (other.posizione != null)
				return false;
		} else if (!posizione.equals(other.posizione))
			return false;
		return true;
	}

	/**
	 * Restituisce il nome della classe
	 * @return "King"
	 */
	@Override
	public String getNome() {
		return "King";
	}

	/**
	 * Modifica la Posizione del King e notifica all'osservatore
	 * @param nuovaPosizione
	 * @param kingPath: percorso del re
	 */
	public void setCambiamentoRe(NodoCitta arrivo, List<NodoCitta> kingPath) {
		this.setPosizione(arrivo);
		this.notifyObservers(new KingMutatio(this,kingPath));
	}
	
	

}
