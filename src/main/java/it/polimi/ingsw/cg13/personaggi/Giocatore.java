/**
 * 
 */
package it.polimi.ingsw.cg13.personaggi;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.PermessoNotFoundException;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.oggetti.Market;

import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.percorsi.CasellaNobilta;
import it.polimi.ingsw.cg13.percorsi.CasellaRicchezza;
import it.polimi.ingsw.cg13.percorsi.CasellaVittoria;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;
import it.polimi.ingsw.cg13.bonus.TesseraBonus;

/**
 * @author Marco
 *
 */
public class Giocatore implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1456037595483898015L;
	private final String nome;
	private final Colore colore;
	private final int numeroGiocatore;
	private final Queue<Emporio> empori;
	private final Set<TesseraBonus> tessereBonus;
	private final List<Aiutante> aiutanti;
	private final List<CartaPolitica> cartePolitiche;
	private final List<PermessoCostruzione> cartePermesso;
	private final List<PermessoCostruzione> cartePermessoUsate;
	private CasellaVittoria vittoria;
	private CasellaNobilta nobilta;
	private CasellaRicchezza ricchezza;
	private boolean isOnline;
	private static int totaleGiocatori=0;
	private final int idGiocatore;
	
	
	/**
	 * @param nome
	 * @param colore
	 * @param numeroGiocatore
	 * @param empori
	 * @param tessereBonus
	 * @param aiutanti
	 * @param cartePolitiche
	 * @param cartePermesso
	 * @param vittoria
	 * @param nobilta
	 * @param ricchezza
	 * @param isOnline
	 * @throws IllegalArgumentException--> valori inseriti nulli o negativi
	 */
	public Giocatore(String nome, int numGiocatore,
			Colore colore, int numeroEmpori, int numeroAiutanti,
			CasellaVittoria vittoria, CasellaNobilta nobilta,
			CasellaRicchezza ricchezza) {
		if(nome!=null&& !nome.isEmpty())
			this.nome = nome;
		else
			throw new IllegalArgumentException("Nome inserito nullo o non valido!");		
		
		if(numGiocatore>=0 && numeroEmpori>=0 && numeroAiutanti>=0){
			this.numeroGiocatore=numGiocatore;
			this.empori=new ArrayDeque<>();
			for(int i=0;i<numeroEmpori;i++){
				empori.add(new Emporio(this));
			}
			this.aiutanti = new ArrayList<>();
			this.addAiutanti(numeroAiutanti);
		}
		else 
			throw new IllegalArgumentException("Hai inserito dei numeri negativi!");
		if(colore!=null){
			this.colore = colore;
		}
		else 
			throw new IllegalArgumentException("Hai inserito un colore non valido!");
		this.idGiocatore = totaleGiocatori++;		
		
		this.tessereBonus = new HashSet<>();
		
		this.cartePolitiche = new ArrayList<>();
		this.cartePermesso = new ArrayList<>();
		this.cartePermessoUsate= new ArrayList<>();
		if(vittoria!=null && nobilta!=null && ricchezza!=null){
			this.vittoria = vittoria;
			this.nobilta = nobilta;	
			this.ricchezza = ricchezza;
		}
		else 
			throw new IllegalArgumentException("Hai inserito delle caselle non valide!");
		this.isOnline = true;
		vittoria.addGiocatore(this);
		nobilta.addGiocatore(this);
		ricchezza.addGiocatore(this);
	}
	
	
	
	/**
	 * @param nome
	 * @param colore
	 * @param numeroGiocatore
	 * @throws IllegalArgumentException--> valori inseriti nulli o negativi
	 */
	public Giocatore(String nome, Colore colore, int numeroGiocatore, int numeroEmpori) {
		if(nome!=null&& !nome.isEmpty())
			this.nome = nome;
		else
			throw new IllegalArgumentException("Nome inserito nullo o non valido!");	
		
		if(colore!=null){
			this.colore = colore;
		}
		else 
			throw new IllegalArgumentException("Hai inserito un colore non valido!");
		
		if(numeroGiocatore>=0 && numeroEmpori>=0){
			this.numeroGiocatore=numeroGiocatore;
			this.empori=new ArrayDeque<>();
			for(int i=0;i<numeroEmpori;i++){
				empori.add(new Emporio(this));
			}			
		}
		else 
			throw new IllegalArgumentException("Hai inserito dei numeri negativi!");
		
		this.idGiocatore = -1;
		
		this.aiutanti = new ArrayList<>();
		this.tessereBonus = new HashSet<>();
		this.cartePolitiche = new ArrayList<>();
		this.cartePermesso = new ArrayList<>();
		this.cartePermessoUsate= new ArrayList<>();
		this.vittoria = new CasellaVittoria(0);
		this.nobilta = new CasellaNobilta(0);	
		this.ricchezza = new CasellaRicchezza(0);
		this.isOnline = false;
	}



	/**
	 * Costruttore che genera una copia distinta dell'oggetto ricevuto
	 * @param giocatoreCorrente
	 */
	public Giocatore(Giocatore giocatoreCorrente) {
		if(giocatoreCorrente!=null){
			this.numeroGiocatore=giocatoreCorrente.numeroGiocatore;
			this.nome = giocatoreCorrente.nome;
			this.colore = giocatoreCorrente.colore;
			this.idGiocatore = giocatoreCorrente.idGiocatore;
			this.empori = giocatoreCorrente.empori;
			this.tessereBonus = giocatoreCorrente.tessereBonus;
			this.aiutanti = giocatoreCorrente.aiutanti;
			this.cartePolitiche = giocatoreCorrente.cartePolitiche;
			this.cartePermesso = giocatoreCorrente.cartePermesso;
			this.cartePermessoUsate= giocatoreCorrente.cartePermessoUsate;
			this.isOnline = giocatoreCorrente.isOnline;
			this.vittoria = giocatoreCorrente.vittoria;
			this.nobilta = giocatoreCorrente.nobilta;
			this.ricchezza = giocatoreCorrente.ricchezza;
		}
		else 
			throw new IllegalArgumentException("Hai inserito un Giocatore nullo!");
	}
	
	/**
	 * Controlla che il giocatore sia in possesso delle CartePolitiche passate
	 * @param carte
	 * @return true--> se le carte politiche cercate sono in possesso del giocatore
	 * false--> se non sono in possesso del giocastore o è stato passato un valore non valido
	 */
	public boolean hasCartePolitiche(List<CartaPolitica> carte){
		int carteRiscontrate=0;
		List<CartaPolitica> cartePoliticheGiocatore=new ArrayList<>();
		
		if(carte==null || carte.contains(null) || carte.isEmpty())
			return false;
		
		//Creo un copia delle cartePolitiche del giocatore in modo da poterle rimuovere in caso di riscontro
		for(CartaPolitica carta:this.cartePolitiche){
			cartePoliticheGiocatore.add(carta);
		}
		//Confronta ognuna delle carte del parametro con le carte del Giocatore
		for(CartaPolitica cartaScelta:carte){
			boolean trovata=false;
			Iterator<CartaPolitica> iteratorCarte=cartePoliticheGiocatore.iterator();
			CartaPolitica cartaGiocatore=null;
			while(!trovata && iteratorCarte.hasNext() ){
				cartaGiocatore=iteratorCarte.next();
				if(cartaScelta.equals(cartaGiocatore)){
					carteRiscontrate++;
					trovata=true;
				}
			}
			if(cartaGiocatore!=null && trovata){
				cartePoliticheGiocatore.remove(cartaGiocatore);
			}
		}
		//Se le carte Riscontrate sono quante le carte passate allora ha le carte giuste !!!
		return carteRiscontrate==carte.size();
	}
	
	/**
	 * Rimuove la lista di CartePolitiche dalla lista del giocatore
	 * @param carte
	 */	
	public void removeCarte(List<CartaPolitica> carte){
		if(carte!=null){
			for(CartaPolitica cartaScelta:carte){
				this.rimuoviCartaPolitica(cartaScelta);
			}
		}
		else throw new IllegalArgumentException("Le Carte passate non sono valide!");
	}
	
	/**
	 * 
	 * @return cartePolitiche del giocatore
	 */
	public List<CartaPolitica> getCartePolitiche() {
		return cartePolitiche;
	}


	/**
	 * Aggiunge numero di aiutanti al Giocatore
	 * NB il numero passato deve essere positivo!
	 * @param numero di aiutanti da aggiungere
	 */
	public void addAiutanti(int numero){
		for(int i=0;i<numero;i++){
			this.aiutanti.add(new Aiutante());
		}
	}
	
	/**
	 * Rimuove numero di aiutanti dal Giocatore
	 * @param numero di aiutanti da usare
	 */
	public void usaAiutanti(int numero){
		if(numero<=this.numeroAiutanti()){
			for(int i=0;i<numero;i++){
				this.aiutanti.remove(0);
			}
		}
	}
	
	/**
	 * 
	 * @return lista degli aiutanti del Giocatore
	 */
	public List<Aiutante> getAiutanti(){
		return aiutanti;
	}
	
	/**
	 * 
	 * @return numero di aiutanti del Giocatore
	 */
	public int numeroAiutanti(){
		return this.aiutanti.size();
	}

	/**
	 * 
	 * @return il valore della ricchezza di un giocatore
	 */
	public int getRicchezzaGiocatore(){
		return this.ricchezza.getPuntiCasella();
	}
	
	/**
	 * Setta il valore della casellaRicchezza del giocatore e aggiorna anche il Percorso
	 * @param numeroCasella : numero della casella ricchezza dove deve essere spostato
	 * @param richPath: percorso della ricchezza
	 * @throws IllegalArgumentException numero casella non presente nel percorso o percorso passato nullo 
	 */
	public void setCasellaRicchezza(int numeroCasella, PercorsoRicchezza richPath){
		if(numeroCasella>=0 && richPath!=null ){
			CasellaRicchezza richCasel=richPath.getCasella(numeroCasella);
			this.ricchezza=richCasel;
			richCasel.addGiocatore(this);
		}
		else throw new IllegalArgumentException("I valori passati a setCasellaRicchezza non sono validi!");
	}
	
	/**
	 * Aumenta le monete di un giocatore 
	 * @param monete da aggiungere al giocatore
	 * @param richPath : percorso Ricchezza Partita
	 * @throws IllegalArgumentException--> monete<0 o richPath nullo
	 */
	public void riceviMonete(int monete, PercorsoRicchezza richPath){
		if(monete>=0 && richPath!=null){
			richPath.getCasella(this.getRicchezzaGiocatore()).removeGiocatore(this);
			int moneteGiocatore=this.getRicchezzaGiocatore()+monete;
			this.setCasellaRicchezza(moneteGiocatore, richPath);
		}
		else throw new IllegalArgumentException("I valori passati a riceviMonete non sono validi!");
	}
	
	/**
	 * Diminuisci le monete di un giocatore
	 * @param monete da togliere al giocatore
	 * @param richPath : percorso Ricchezza Partita
	 * @throws IllegalArgumentException--> monete<0 o richPath nullo
	 */
	public void spendiMonete(int monete, PercorsoRicchezza richPath){
		if(monete>=0 && richPath!=null){
			richPath.getCasella(this.getRicchezzaGiocatore()).removeGiocatore(this);
			int moneteGiocatore=this.getRicchezzaGiocatore()-monete;
			this.setCasellaRicchezza(moneteGiocatore, richPath);
		}
		else throw new IllegalArgumentException("I valori passati a spendiMonete non sono validi!");
	}
	
	/**
	 * Aggiunge il PermessoCostruzione alla mano del giocatore
	 * @param permesso
	 * @throws IllegalArgumentException--> valori passati nulli o listaPermessi vuota
	 */
	public void addPermessoCostruzione(PermessoCostruzione permesso, List<PermessoCostruzione> listaPermessi){
		if(permesso==null || listaPermessi==null)
			throw new IllegalArgumentException("I valori passati non sono validi!");
		else
			listaPermessi.add(permesso);
	}
	
	/**
	 * 
	 * @return le carte Permesso del Giocatore
	 */
	public List<PermessoCostruzione> getPermessiCostruzione(){
		return this.cartePermesso;
	}
	
	/**
	 * 
	 * @return un emporio del giocatore
	 */
	public Emporio usaEmporio(){
		return this.empori.poll();
	}
	
	/**
	 * 
	 * @return il numero di empori del Giocatore
	 */
	public int getNumeroEmpori(){
		return this.empori.size();
	}
	
	/**
	 * aggiunge un bonus al set
	 * @param bonus
	 * @throws IllegalArgumentException--> bonus nullo
	 */
	public void addBonus(TesseraBonus bonus){
		if(bonus!=null)
			this.tessereBonus.add(bonus);
		else
			throw new IllegalArgumentException("Il bonus passato al giocatore è nullo!");
	}
	
	/**
	 * aggiunge una carta passata alla lista del giocatore
	 * @param carta
	 * @throws IllegalArgumentException-->carta nulla
	 */
	public void addCartaPolitica(CartaPolitica carta){
		if(carta!=null)
			this.cartePolitiche.add(carta);
		else
			throw new IllegalArgumentException("La cartaPolitica da aggiungere al giocatore è nulla!");
	}
	
	/**
	 * 
	 * @return il valore dei punti nobilta di un giocatore
	 */
	public int getNobiltaGiocatore(){
		return this.nobilta.getPuntiCasella();
	}
	
	/**
	 * Setta il valore della casellaNobilta del giocatore e aggiorna anche il Percorso
	 * @param numeroCasella : numero della casella nobilta dove deve essere spostato
	 * @param nobPath: percorso della nobilta
	 * @throws IllegalArgumentException--> numero casella <0 o partita nulla
	 */
	public void setCasellaNobilta(int numeroCasella, Partita partita){
		if(numeroCasella>=0 && partita!=null){
			CasellaNobilta nobCasel=partita.getPercorsoNobilta().getCasella(numeroCasella);
			this.nobilta=nobCasel;
			nobCasel.addGiocatore(this, partita);
		}
		else
			throw new IllegalArgumentException("Valori passati a setCasellaNobilta non validi!");
	}
	
	/**
	 * Aumenta i punti nobilta di un giocatore 
	 * @param punti da aggiungere al giocatore
	 * @param nobPath : percorso Nobilta Partita
	 */
	public void riceviNobilta(int punti, Partita partita){
		partita.getPercorsoNobilta().getCasella(this.getNobiltaGiocatore()).removeGiocatore(this);
		int puntiGiocatore=this.getNobiltaGiocatore()+punti;
		this.setCasellaNobilta(puntiGiocatore, partita);
	}
	
	/**
	 * 
	 * @return il valore dei punti vittoria di un giocatore
	 */
	public int getVittoriaGiocatore(){
		return this.vittoria.getPuntiCasella();
	}
	
	/**
	 * Setta il valore della casellaVittoria del giocatore e aggiorna anche il Percorso
	 * @param numeroCasella : numero della casella vittoria dove deve essere spostato
	 * @param nobPath: percorso della vittoria
	 * @throws IllegalArgumentException--> numeroCasella<0 o winPath nullo
	 */
	public void setCasellaVittoria(int numeroCasella, PercorsoVittoria winPath){
		if(numeroCasella>=0&&winPath!=null){
			CasellaVittoria winCasel=winPath.getCasella(numeroCasella);
			this.vittoria=winCasel;
			winCasel.addGiocatore(this);
		}
		else
			throw new IllegalArgumentException("Valori passati a setCasellaVittoria non validi!");
	}
	
	/**
	 * Aumenta i punti vittoria di un giocatore 
	 * @param punti da aggiungere al giocatore
	 * @param nobPath : percorso Vittoria Partita
	 */
	public void riceviVittoria(int punti, PercorsoVittoria winPath){
		winPath.getCasella(this.getVittoriaGiocatore()).removeGiocatore(this);
		int puntiGiocatore=this.getVittoriaGiocatore()+punti;
		this.setCasellaVittoria(puntiGiocatore, winPath);
	}
	
	/**
	 * Metodo per Market -->scelta degli "articoli" da vendere 
	 * @param Market M--> entit� market
	 * @param Vendibile V--> oggetto da vendere
	 * @param CasellaNobiltaGrafica prezzo -->prezzo vendita
	 */	
	public void mettiVendita(Market m,Inserzione i)
	{	
		if(m==null || i==null)
			throw new IllegalArgumentException("Valori passati a mettiVendita non sono validi!");
		else 
			m.inserisciInserzione(i);
	}
	
	/**
	 * rimuove carta politica da lista giocatore
	 * @param CartaPolitica c--> carta da rimuovere
	 * @return risultato rimozione, ritorna false se carta=NULL
	 */	
	public boolean rimuoviCartaPolitica(CartaPolitica cartaScelta){
		if(cartaScelta==null)
			return false;
		
		boolean trovata=false;
		Iterator<CartaPolitica> iteratorCarte=this.getCartePolitiche().iterator();
		CartaPolitica cartaGiocatore=null;
		while(!trovata && iteratorCarte.hasNext() ){
			cartaGiocatore=iteratorCarte.next();
			if(cartaScelta.equals(cartaGiocatore)){
				trovata=true;
			}
		}
		if(cartaGiocatore!=null && trovata){
			this.cartePolitiche.remove(cartaGiocatore);
		}
		return trovata;
	}
	
	/**
	 * rimuove carta permesso da lista giocatore
	 * @param PermessoCostruzione p--> carta da rimuovere
	 * @param listaPermessi da cui rimuovere
	 * @return risultato rimozione
	 */	
	public boolean rimuoviCartaPermesso(PermessoCostruzione p, List<PermessoCostruzione> listaPermessi){
		if(p!=null && listaPermessi!=null && !listaPermessi.isEmpty()){
			return listaPermessi.remove(p);}
		else
			return false;
	}
	
	/**
	 * rimuove aiutante da lista giocatore
	 * @param Aiutante a--> Aiutante da rimuovere
	 * @return risultato rimozione
	 */	
	public boolean rimuoviAiutante(Aiutante a){
		return this.aiutanti.remove(a);
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setIsOnline(boolean stato){
		this.isOnline=stato;
	}
	
	public boolean getIsOnline(){
		return this.isOnline;
	}
	
	public List<PermessoCostruzione> getCartePermessoUsate() {
		return cartePermessoUsate;
	}
	
	@Override
	public String toString() {
		return "{ nome=" + nome + ", " + colore + ", numeroGiocatore=" + numeroGiocatore + 
				", idGiocatore=" + idGiocatore +",\nempori="+ empori.size() + ", aiutanti=" + aiutanti.size() +
				", tessereBonus=" + tessereBonus + ", \ncartePolitiche="+ cartePolitiche + 
				", \ncartePermesso=" + cartePermesso +", \ncartePermessoUsate=" + cartePermessoUsate + ", \nPuntiVittoria=" + vittoria.getPuntiCasella()+ ", PuntiNobilta="+ nobilta.getPuntiCasella()+
				", PuntiRicchezza=" + ricchezza.getPuntiCasella() + "}";
	}

	/**
	 * @return the empori
	 */
	public Queue<Emporio> getEmpori() {
		return empori;
	}
	
	/**
	 * Sposta il permesso dai permesssi disponibili a quelli usati
	 * @param permesso
	 * @throws PermessoNotFoundException se il permesso non e' nell'elenco dei permessi usabili
	 */
	public void usaPermesso(PermessoCostruzione permesso) throws PermessoNotFoundException{
		if(!rimuoviCartaPermesso(permesso, this.cartePermesso)){
			throw new PermessoNotFoundException("Il permesso scelto non e' di questo giocatore");
		}
		permesso.setUsata(true);
		addPermessoCostruzione(permesso,this.cartePermessoUsate);
	}
	
	/**
	 * RImuove un permesso dal giocatore
	 * @param permesso
	 * @throws PermessoNotFoundException se il permesso non e' nell'elenco dei permessi usabili
	 */
	public void perdiPermesso(PermessoCostruzione permesso) throws PermessoNotFoundException{
		if(!rimuoviCartaPermesso(permesso, this.cartePermesso)){
			throw new PermessoNotFoundException("Il permesso scelto non e' di questo giocatore");
		}
	}
	
	/**
	 * Aggiunge il permesso alla lista di permessi usabili
	 * @param permessoCostruzione not null
	 * @throws IllegalArgumentException-->Permesso nullo!
	 */
	public void accumulaPermesso( PermessoCostruzione permessoCostruzione){
		if(permessoCostruzione!=null)
			addPermessoCostruzione(permessoCostruzione, cartePermesso);
		else
			throw new IllegalArgumentException("Permesso nullo!");
	}

	

	public Set<TesseraBonus> getTessereBonus() {
		return tessereBonus;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colore == null) ? 0 : colore.hashCode());
		result = prime * result + idGiocatore;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + numeroGiocatore;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Giocatore other = (Giocatore) obj;
		if (colore == null) {
			if (other.colore != null)
				return false;
		} else if (!colore.equals(other.colore))
			return false;
		if (idGiocatore != other.idGiocatore)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numeroGiocatore != other.numeroGiocatore)
			return false;
		return true;
	}


	public Colore getColore() {
		return colore;
	}



	public int getNumeroGiocatore() {
		return numeroGiocatore;
	}



	public int getIdGiocatore() {
		return idGiocatore;
	}

	


	

	
}
