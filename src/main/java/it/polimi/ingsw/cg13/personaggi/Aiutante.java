/**
 * 
 */
package it.polimi.ingsw.cg13.personaggi;

import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.oggetti.Vendibile;

/**
 * @author Marco
 *
 */
public class Aiutante implements Vendibile {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2084819532129536526L;

	/**
	 * 
	 */
	public Aiutante() {
		
	}

	@Override
	/**
	 * Metodo Acquisto Da market--> aggiunge un aiutante al giocatore
	 * @param g--> giocatore a cui aggiungere
	 */
	public void acquistaDaMarket(Giocatore g) {
		g.addAiutanti(1);
	
	}

	@Override
	/**
	 * Metodo Rimuovi Da giocatore--> rimuove un aiutante dal giocatore
	 * @param g--> giocatore a cui rimuovere
	 */
	public void rimuoviDaGiocatore(Giocatore g) {
		g.usaAiutanti(1);
		
	}

	@Override
	public int hashCode() {
		return 35;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aiutante";
	}

	@Override
	public void updateFromServer(Giocatore g, Inserzione inserzione) throws CdQException {
		g.usaAiutanti(1);
	}

	@Override
	public void riassegnaAlProprietario(Giocatore g) {
		g.addAiutanti(1);
	}

}
