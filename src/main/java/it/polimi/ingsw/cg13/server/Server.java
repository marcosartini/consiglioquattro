package it.polimi.ingsw.cg13.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.comandi.Login;





public class Server {

	private static final int PORT = 29999;
	private static final int RMI_PORT= 44444;
	private static final Logger logger = LoggerFactory.getLogger(Server.class);
	private Registry registry;

	private static final String NAME = "ConsiglioDeiQuattro";

	private GestoreGioco gestoreGioco;
	private ExecutorService executor;


	/**
	 * 
	 * @param durataMossa positiva in millisecondi
	 */
	public Server(){
		this.executor = Executors.newCachedThreadPool();
		try {
			this.registry=LocateRegistry.createRegistry(RMI_PORT);
			logger.info("Registro creato sulla porta "+ RMI_PORT);
		} catch (RemoteException e) {
			logger.error(e.getMessage(), e);
		}
		this.gestoreGioco=GestoreGioco.getIstanza();
		executor.submit(gestoreGioco);
	}

	private void startRMI() throws RemoteException, AlreadyBoundException{
		
		RMIRegistrationViewRemote gestoreGiocoRMI = new RMIRegistrationView(this, gestoreGioco);
		RMIRegistrationViewRemote gestoreGiocoRMIRemoto = (RMIRegistrationViewRemote) UnicastRemoteObject.exportObject(gestoreGiocoRMI,RMI_PORT);
		registry.bind(NAME, gestoreGiocoRMIRemoto);
		
		logger.info("SERVER RMI READY ON PORT " + RMI_PORT);
	}
	
	private void startSocket() throws IOException {

		ServerSocket serverSocket = new ServerSocket(PORT);
		logger.info("SERVER SOCKET READY ON PORT " + PORT);
		
		while (!serverSocket.isClosed()) {
			Socket socket = serverSocket.accept();
			ServerSocketView view = new ServerSocketView(socket, gestoreGioco);
			executor.submit(view);
		}
		
		serverSocket.close();
	}
	
	public synchronized void addRMIClient(View view, Login login) throws RemoteException, AlreadyBoundException{
		this.addClient(view, login);
		RMIRegistrationViewRemote game = new RMIRegistrationView(this, gestoreGioco);
		RMIRegistrationViewRemote gameRemote = 	(RMIRegistrationViewRemote) UnicastRemoteObject.exportObject(game, RMI_PORT);
		logger.info("SERVER: bind game remote view");
		registry.rebind(NAME, gameRemote);
	}
	
	public synchronized void addClient(View view, Login login) {
		gestoreGioco.aggiungiGiocatore(login, view);
	}
		

	public static void main(String[] args) {
	/*	//lettura durata mossa
		Scanner in = new Scanner(System.in);
		System.out.print("Inserisci la durata massima di attesa per una mossa del giocatore [secondi]>");
		int durataMossa=Integer.parseInt(in.nextLine())*1000;
		in.close();
	*/
		Server server = new Server();
		logger.info("START RMI");
		try {
			server.startRMI();
		} catch (RemoteException | AlreadyBoundException e1) {
			logger.error(e1.getMessage(), e1);
		}
		logger.info("START SOCKET");
		try {
			server.startSocket();
		} catch (IOException e) {
			logger.error("Errore Server "+e.getMessage(),e);
		}
	}
}
