package it.polimi.ingsw.cg13.server;

import java.rmi.RemoteException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.chat.ChatText;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.controller.Controller;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.partita.Partita;


public class Gioco implements Runnable {
	
	Partita partita;
	Controller controller;
	ChatText chat;
	private static final Logger logger = LoggerFactory.getLogger(Gioco.class);
	
	
	/**
	 * @param giocatori
	 * @param durataMosse 
	 * @param mappa 
	 * @param viewServer
	 * @throws IllegalArgumentException se non ci sono almeno due giocatori nella lista
	 */
	public Gioco(List<Login> giocatori, List<View> views, String mappa, int durataMosse, int configurazioneMappa) {
		if(giocatori.size()<2){
			throw new IllegalArgumentException("I giocatori sono insufficienti allo svolgimento della partita");
		}
		try {
			this.partita = new Partita(mappa,durataMosse,configurazioneMappa);
		} catch (FileSyntaxException e1) {
			logger.error(e1.getMessage(), e1);
			views.forEach(v -> v.notifyObservers(e1));
		}
		this.controller = new Controller(partita);
		this.chat=new ChatText();
		this.controller.registerObserver(chat);
		views.forEach(v -> this.registraView(v));
		
		for(Login login: giocatori)
			try {
				controller.update(login);
			} catch (CdQException | RemoteException e) {
				logger.error(e.getMessage(), e);
			} //magari la addGiocatore sarà qui in questa classe
		
	}

	private void registraView(View view){
		this.partita.registerObserver(view);
		view.registerObserver(this.controller);
		chat.registerObserver(view);
		//view.setModel(partita);
	}


	@Override
	public void run() {
		
			try {
				this.partita.startTurnoPartita();
			} catch (ColoreNotFoundException e) {
				logger.error(e.getMessage(), e);
				this.partita.notifyObservers(e);
			}
			
			while(!partita.isEnded()); //continua fino alla fine di partita
		
		//calcola punteggi
	//	partita.notifyObservers(); //con classifica
		
		//aspetta eventuali interazioni per statistiche e balle varie
		
		//termina
		
	}
	
	

	
}
