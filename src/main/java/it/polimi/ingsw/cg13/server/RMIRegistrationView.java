package it.polimi.ingsw.cg13.server;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.client.ClientViewRemote;
import it.polimi.ingsw.cg13.comandi.Login;

/**
 * Questa classe si occupa di accogliere una richiesta di connessione da un client RMI
 * e girarla al gestore dei giochi affinche' faccia quel che deve fare con il nuovo utente
 * @author Marco
 *
 */
public class RMIRegistrationView implements RMIRegistrationViewRemote {

	Server server;
	GestoreGioco gestoreGioco;
	
	public RMIRegistrationView(Server server, GestoreGioco gestoreGioco) {
		this.gestoreGioco=gestoreGioco;
		this.server=server;
	}
	
	
	@Override
	public RMIGameViewRemote registerClient(ClientViewRemote client, Login login) throws RemoteException, AlreadyBoundException {
		RMIGameView view = new RMIGameView(client,this.gestoreGioco);
		server.addRMIClient(view, login);
		return view;
	}
	
}
