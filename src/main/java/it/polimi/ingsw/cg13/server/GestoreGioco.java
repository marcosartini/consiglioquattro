/**
 * 
 */
package it.polimi.ingsw.cg13.server;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.client.InviaParametriPartita;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mutatio.PrimoGiocatoreMutatio;
import it.polimi.ingsw.cg13.mutatio.NomeNonDisponibileMutatio;



/**
 * @author Marco
 * La classe segue il Singleton Pattern, perche' puo' essere istanziata solo una volta
 * Gestisce le diverse partite in esecuzione
 */
public final class GestoreGioco implements Runnable{
	private static GestoreGioco istanza=null;
	private static final Logger logger = LoggerFactory.getLogger(GestoreGioco.class);
	
	private int numeroPartite = 0;
	private static final Integer TIMEOUT = 20000; //20 secondi
	private Timer timer;
	private volatile boolean scaduto;
	private List<Login> giocatoriRichiedenti;
	private List<Login> giocatoriConNomiGiaUsati;
	private List<View> viewGiocatori;
	private int maxGiocatori = 4; //da ricevere come parametro dal primo giocatore
	private int durataMosse=90;
	private String mappa="0";
	private int numeroGiocatori = 0;
	ExecutorService executor;
	
	private GestoreGioco() {
		//legge numero giocatori per partita, ma anche no.... dipende
		executor = Executors.newCachedThreadPool();
		giocatoriRichiedenti=new ArrayList<>();
		giocatoriConNomiGiaUsati=new ArrayList<>();
		viewGiocatori = new ArrayList<>();
	}
	
	/**
	 * Se il GestoreGioco e' gia' creato ritorna l'istanza, altrimenti lo crea e ritorna l'istanza
	 * @return un istanza di {@link GestoreGioco}
	 */
	public static GestoreGioco getIstanza() {
		if(istanza==null){
			istanza=new GestoreGioco();
		}
		return istanza;
	}
	
	/**
	 * 
	 * @return il numero di partite istanziate dall'avvio del gestore
	 */
	public synchronized int getNumeroPartite() {
		return numeroPartite;
	}

	/**
	 * In eterno avvia nuove partite: resta in attesa che si connettano due giocatori, poi
	 * quando o è trascorso il timeout o il numero di giocatori iscritti raggiunge il massimo per la partita
	 * avvia un gioco e si rimette a disposizione per avviare una nuova partita
	 */
	@Override
	public synchronized void run() {
		logger.info("GestoreGioco RUNNING");		
		while(true){
			logger.info("GestoreGioco Nuova Partita in avvio");
			timer = new Timer();
			scaduto = false;
			giocatoriRichiedenti.clear();
			giocatoriConNomiGiaUsati.clear();
			viewGiocatori.clear();
			numeroGiocatori=0;

			while(numeroGiocatori<2){
				try {
					logger.info("GestoreGioco aspetto almeno due giocatori");
					wait();
				} catch (InterruptedException e) {
					logger.error(e.getMessage(),e);
					Thread.currentThread().interrupt();
				}
			}
			logger.info("GestoreGioco due giocatori presenti - aspetto "+TIMEOUT/1000+" secondi o altri "+(maxGiocatori-2)+" giocatori");
			timer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					scaduto=true;
					startGioco();
				}
				
			}, TIMEOUT);
						
			while(numeroGiocatori<maxGiocatori && !scaduto){
				try {
					wait();
				} catch (InterruptedException e) {
					logger.error(e.getMessage(),e);
					Thread.currentThread().interrupt();
				}
			}
			
			if(!scaduto)
				startGioco();
		}
	}
	
	/**
	 * Aggiunge la richiesta di login ai giocatori in attesa di giocare
	 * @param login
	 * @throws CdQException 
	 * @throws RemoteException 
	 */
	public synchronized void aggiungiGiocatore(Login login, View view){
		if(!this.giocatoriRichiedenti.contains(login)){
			this.giocatoriRichiedenti.add(login);
			this.viewGiocatori.add(view);
			numeroGiocatori=giocatoriRichiedenti.size();
			logger.info("GestoreGioco aggiunti giocatore e view");
		}else{
			this.giocatoriConNomiGiaUsati.add(login);
			logger.info("Nome gia usato !!!");
		}
		notifyAll();
	}
	
	public synchronized void checkFirstPlayer(View view){
		if(this.giocatoriRichiedenti.size()==1){
			try {
				view.update(new PrimoGiocatoreMutatio());
			} catch (RemoteException | CdQException e) {
				view.update(e);
			}
		}
	}
	
	public synchronized void checkNomeGiaUsato(Login login,View view){
		if(this.giocatoriRichiedenti.contains(login) && this.giocatoriConNomiGiaUsati.contains(login)){
			try {
				view.update(new NomeNonDisponibileMutatio());
			} catch (RemoteException | CdQException e) {
				view.update(e);
			}
			this.giocatoriConNomiGiaUsati.remove(login);
		}
	}
	
	/**
	 * Fa partire il gioco, risveglia i thread in waiting sui suoi lock e distrugge il timer
	 */
	public synchronized void startGioco(){
		List<Login> gioc = new ArrayList<>();
		gioc.addAll(giocatoriRichiedenti);
		executor.submit(new Gioco(gioc, viewGiocatori,mappa,durataMosse, Integer.valueOf(mappa)));
		numeroPartite++;
		notifyAll();
		timer.cancel();
		timer.purge();
	}

	public void setParametriPartita(InviaParametriPartita parametri) {
		this.maxGiocatori=parametri.getMaxNumGiocatori();
		this.mappa=parametri.getNomeMappa();
		this.durataMosse=parametri.getTempoAzione();
		System.out.println("Parametri ricevuti: "+ parametri.getMaxNumGiocatori() +" - "+ parametri.getNomeMappa() + " - "+ parametri.getTempoAzione());
	}

}
