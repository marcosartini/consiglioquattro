package it.polimi.ingsw.cg13.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.client.ClientViewRemote;
import it.polimi.ingsw.cg13.client.InviaParametriPartita;
import it.polimi.ingsw.cg13.comandi.ChatMessage;
import it.polimi.ingsw.cg13.comandi.Comando;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.comandi.Query;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mutatio.Mutatio;


/**
 * Questa classe implementa la view RMI del gioco
 * @author Marco
 *
 */
public class RMIGameView extends View implements RMIGameViewRemote {
	
	private static final Logger logger = LoggerFactory.getLogger(RMIGameView.class);
	
	private ClientViewRemote client;
	private GestoreGioco gestore;
	
	protected RMIGameView(ClientViewRemote client, GestoreGioco gestoreGioco) throws RemoteException {
		UnicastRemoteObject.exportObject(this,44444);
		this.client=client;
		this.gestore=gestoreGioco;
	}

	@Override
	public void update(Exception e) {
		//non fa nulla perche' le eccezioni vengono mandate automaticamente
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.cg13.provamvc.ObserverMVC#update(java.lang.Object)
	 */
	@Override
	public void update(Mutatio mutatio) throws RemoteException {
		logger.info("Mando cambiamento ai clients");
		client.updateClient(mutatio);
	}

	@Override
	public void eseguiComando(Comando comando) throws RemoteException, CdQException {
		if(comando instanceof Azione)
			this.notifyObservers((Azione)comando);
		else if(comando instanceof Query)
			this.notifyObservers((Query)comando);
		else if(comando instanceof InviaParametriPartita){
			InviaParametriPartita parametri=(InviaParametriPartita)comando;
			this.gestore.setParametriPartita(parametri);
		}else if(comando instanceof ChatMessage){;
			ChatMessage messaggio=(ChatMessage)comando;
			this.notifyObservers(messaggio);
		}else if(comando instanceof Login){
			Login log=(Login)comando;
			this.gestore.aggiungiGiocatore(log, this);
			this.checkClientRegistration(log);
		}
	}

	public void checkClientRegistration(Login log) {
		this.gestore.checkFirstPlayer(this);
		this.gestore.checkNomeGiaUsato(log, this);
	}

	@Override
	public void unregisterClient(ClientViewRemote clientStub) throws RemoteException {
		try {
			this.notifyObservers(this);
		} catch (RemoteException | CdQException e1) {
			logger.error(e1.getMessage(),e1);
		}
		UnicastRemoteObject.unexportObject(this, true);
		
	}

	@Override
	public void aggiungiGiocatore(Login login) throws RemoteException, CdQException {
		this.notifyObservers(login);
	}


}
