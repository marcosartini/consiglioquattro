package it.polimi.ingsw.cg13.server;

import java.rmi.Remote;
import java.rmi.RemoteException;


import it.polimi.ingsw.cg13.client.ClientViewRemote;
import it.polimi.ingsw.cg13.comandi.Comando;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.eccezioni.CdQException;

public interface RMIGameViewRemote extends Remote {

	
	public void unregisterClient(ClientViewRemote clientStub) throws RemoteException;
	public void eseguiComando(Comando comando) throws RemoteException, CdQException;
	public void aggiungiGiocatore (Login login)throws RemoteException, CdQException;
	public void checkClientRegistration(Login log) throws RemoteException;
	
}
