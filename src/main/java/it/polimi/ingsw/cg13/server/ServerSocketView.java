package it.polimi.ingsw.cg13.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.client.InviaParametriPartita;
import it.polimi.ingsw.cg13.comandi.ChatMessage;
import it.polimi.ingsw.cg13.comandi.Exit;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.comandi.Query;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.mutatio.Mutatio;



public class ServerSocketView extends View implements Runnable {

	private Socket socket;
	private ObjectInputStream socketIn;
	private ObjectOutputStream socketOut;
	private GestoreGioco gestoreGioco;
	private static final Logger logger = LoggerFactory.getLogger(ServerSocketView.class);
	
	
	public ServerSocketView(Socket socket, GestoreGioco gestoreGioco) throws IOException {
		this.socket=socket;
		this.socketIn = new ObjectInputStream(socket.getInputStream());
		this.socketOut = new ObjectOutputStream(socket.getOutputStream());
		this.gestoreGioco=gestoreGioco;

	}

	@Override
	public void update(Mutatio change) {
		logger.info("SSV - (sending to the client): " + change);

		try {
			this.socketOut.reset();
			this.socketOut.writeObject(change);
			this.socketOut.flush();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
	

	@Override
	public void run() {
		
		try{
			while (!this.socket.isClosed()) {
				try{
					leggiEAgisci();
					
				}catch(IllegalArgumentException | CdQException e) {
				this.update(e);
				logger.error(e.getMessage(),e);
				}	
			}
		} catch ( ClassNotFoundException | IOException err) {
			this.update(err);
			logger.error(err.getMessage(),err);
		}	
		
		//Terminazione della ServerSocketView in corso
		
		//Unregister view dal modello e dal controller
		try {
			this.notifyObservers(this);
		} catch (RemoteException | CdQException e1) {
			logger.error(e1.getMessage(),e1);
		}
		
		//Chiusura dei flussi dati in ingresso e uscita
		try {
			this.socketIn.close();
			this.socketOut.close();
		} catch (IOException e) {
			this.update(e);
			logger.error(e.getMessage(),e);
		}

	}

	@Override
	public void update(Exception e) {
		logger.info("SSV - (exception sending to the client): " + e.getMessage(),e);

		try {
			this.socketOut.reset();
			this.socketOut.writeObject(e);
			this.socketOut.flush();
		} catch (IOException er) {
			logger.error(er.getMessage(),er);
		}
		
	}
	
	private void leggiEAgisci() throws CdQException, ClassNotFoundException, IOException{
		Object object = socketIn.readObject();
		if (object instanceof Azione) {
			Azione action=(Azione)object;
			logger.info("VIEW: received the action " + action);
			this.notifyObservers(action);
		}
		else if(object instanceof Login){
			Login loginPlayer=(Login)object;
			logger.info("SSV - received the player " + loginPlayer);
			this.gestoreGioco.aggiungiGiocatore(loginPlayer, this);
			this.gestoreGioco.checkFirstPlayer(this);
			this.gestoreGioco.checkNomeGiaUsato(loginPlayer, this);
		}
		else if(object instanceof Exit){
			logger.info("SSV - received query: "+object);
			Query query=(Exit)object;
			this.notifyObservers(query);
		}
		else if(object instanceof Query){
			logger.info("SSV - received query: "+object);
			Query query=(Query)object;
			this.notifyObservers(query);
		}else if(object instanceof InviaParametriPartita){
			InviaParametriPartita parametri=(InviaParametriPartita)object;
			this.gestoreGioco.setParametriPartita(parametri);
		}else if(object instanceof ChatMessage){
			logger.info("SSV - received query: "+object);
			ChatMessage messaggio=(ChatMessage)object;
			this.notifyObservers(messaggio);
	}
}

}
