package it.polimi.ingsw.cg13.server;

import it.polimi.ingsw.cg13.azioni.Azione;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableMVC;
import it.polimi.ingsw.cg13.observable.ObserverMVC;


public abstract class View extends ObservableMVC<Azione> implements ObserverMVC<Mutatio>
{

	//public abstract void setModel (Partita partita);
}
