package it.polimi.ingsw.cg13.server;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.cg13.client.ClientViewRemote;
import it.polimi.ingsw.cg13.comandi.Login;

@FunctionalInterface
/**
 * I metodi di questa interfaccia permettono di gestire la connessione in entrata degli utenti
 * @author Marco
 *
 */
public interface RMIRegistrationViewRemote extends Remote {
	public RMIGameViewRemote registerClient (ClientViewRemote client, Login login) throws RemoteException, AlreadyBoundException;
}
