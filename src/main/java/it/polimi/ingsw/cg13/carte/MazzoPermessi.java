package it.polimi.ingsw.cg13.carte;

import java.util.List;

import it.polimi.ingsw.cg13.mappa.Regione;

public class MazzoPermessi extends Mazzo<PermessoCostruzione> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4633724343225405777L;
	Regione regione;
	int permessiVisibili;
	
	/**
	 * Creo un mazzo con un nuovo arrayList vuoto
	 */
	public MazzoPermessi(){
		super();
	}
	/**
	 * 
	 * @param mazzoCarte
	 * @param permessiVisibili
	 * @throws IllegalArgumentException--> valori nulli
	 */
	public MazzoPermessi(List<PermessoCostruzione> mazzoCarte, int permessiVisibili){
		super(mazzoCarte);
		if(permessiVisibili>0)
			this.permessiVisibili=permessiVisibili;
		else
			throw new IllegalArgumentException("Nome del colore non valido!");
	}
	/**
	 * setta i permessi visibili del mazzo
	 * @param num
	 * @throws IllegalArgumentException-> num<=0
	 */
	public void setPermessiVisibili(int num){
		if(num>0)
			this.permessiVisibili=num;
		else
			throw new IllegalArgumentException("I per messi visibili devono essere maggiori di 0!");
	}
	
	/**
	 * Ritorna il numero dei permessi che puo mostrare il mazzo
	 * @return numero permessi visibili
	 */
	public int getPermessiVisibili(){
		return this.permessiVisibili;
	}

	/**
	 * {@inheritDoc}
	 * Ritorna la stringa del mazzo sottoforma della stringa delle carte visibili del mazzo
	 */
	@Override
	public String toString() {
		String mazzo="MazzoPermesso:\n";
		for(PermessoCostruzione carta:this.getCarteInCima(permessiVisibili)){
			mazzo+=carta.toString()+"\n";
		}
		mazzo+="carteMazzo="+this.sizeMazzo();
		return mazzo;
	}

	/**
	 * {@inheritDoc}
	 * Calcola l'hashCode per il mazzo permesso
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + permessiVisibili;
		result = prime * result + ((regione == null) ? 0 : regione.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 * Confronta se due mazzi permessi sono uguali confrontando la regione in cui si trova
	 * e i permessi che puo mostrare
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MazzoPermessi other = (MazzoPermessi) obj;
		if (permessiVisibili != other.permessiVisibili)
			return false;
		if (regione == null) {
			if (other.regione != null)
				return false;
		} else if (!regione.equals(other.regione))
			return false;
		return true;
	}

}
