/**
 * 
 */
package it.polimi.ingsw.cg13.carte;


import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

/**
 * @author Marco
 *
 */
public class CartaPoliticaSemplice extends CartaPolitica {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4628166127665728111L;
	private final Colore colore;

	/**
	 * Costruttore
	 * @param colore --> colore da assegnare alla carta
	 * @throws ColoreNotFoundException--> colore non valido 
	 */
	public CartaPoliticaSemplice(Colore colore) throws ColoreNotFoundException {
		if(colore!=null)
			this.colore=colore;
		else
			throw  new ColoreNotFoundException("Colore passato nullo!");
	}
	
	/**
	 * @return il colore della carta
	 */
	@Override
	public Colore getColor(){
		return colore;
	}
	
	
	@Override
	/**
	 * Metodo che controlla se la carta � dello stesso colore di un determinato consigliere
	 * @param consigliere --> consigliere da controllare con la carta
	 */
	public boolean match(Consigliere consigliere) {
		
		if(this.colore.equals(consigliere.getColor())){
			return true;
		}
		return false;
	}

	@Override
	/**
	 * Controlla se una carta � dello stesso colore di un' altra
	 * 
	 * @param carta --> carta da controllare 
	 * @throws IllegalArgumentException
	 */
	public boolean equals(CartaPoliticaSemplice carta){
		
		if(carta!=null){
			Colore coloreC=carta.getColor();
			return this.colore.equals(coloreC);
		}
		throw new IllegalArgumentException("Parametro nullo");
		
	}

	@Override
	/**
	 * return false se una carta politica semplice � stata controllata con un jolly
	 * 
	 * @param carta --> carta da controllare 
	 * 
	 */
	public boolean equals(CartaPoliticaJolly carta) {
		return false;
	}

	@Override
	/**
	 * toString della classe
	 * 
	 */
	public String toString() {
		return "("+colore + ")";
	}

	@Override
	/**
	 * Controlla se una carta � dello stesso colore di un' altra
	 * @param carta --> carta da controllare 
	 * @throws IllegalArgumentException
	 * 
	 */
	public <C extends CartaPolitica> boolean equals(C carta) {
		if(carta!=null){
		return carta.equals(this); 
		}
		else throw new IllegalArgumentException("CartaPolitica nulla");
	}

	@Override
	public int getCosto() {
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colore == null) ? 0 : colore.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartaPoliticaSemplice other = (CartaPoliticaSemplice) obj;
		if (colore == null) {
			if (other.colore != null)
				return false;
		} else if (!colore.equals(other.colore))
			return false;
		return true;
	}

	




}
