/**
 * 
 */
package it.polimi.ingsw.cg13.carte;

import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

/**
 * @author Marco
 *
 */
public class CartaPoliticaJolly extends CartaPolitica {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -407220040622482154L;


	/**
	 * Metodo che controlla se la carta e' dello stesso colore di un determinato consigliere
	 * @param consigliere --> consigliere da controllare con la carta
	 */
	@Override
	public boolean match(Consigliere consigliere) {
		return true;
	}

	@Override
	/**
	 * return false se una carta politica jolly � stata controllata con un semplice
	 * 
	 * @param carta --> carta da controllare 
	 * 
	 */
	public boolean equals(CartaPoliticaSemplice carta) {
		return false;
	}

	@Override
	/**
	 * return false se una carta politica jolly � stata controllata con un jolly
	 * 
	 * @param carta --> carta da controllare 
	 * 
	 */
	public boolean equals(CartaPoliticaJolly carta) {
		return true;
	}

	@Override
	/**
	 * toString della classe
	 * 
	 */
	public String toString() {
		return "(jolly)";
	}

	@Override
	/**
	 * Equals della classe
	 * @param carta
	 * @throws IllegalArgumentException--> valore nullo
	 */
	public <C extends CartaPolitica> boolean equals(C carta) {
		if(carta!=null){
		return carta.equals(this);
		}
		else throw new IllegalArgumentException("CartaPolitica nulla");
	}

	@Override
	/**
	 * Restituisce il costo della carta
	 * return 1;
	 */
	public int getCosto() {
		return 1;
	}

	/**
	 * Restituisce l' hashCode della carta
	 * @return hashCode
	 */
	@Override
	public int hashCode() {
		return 31;
	}

	/**
	 * Equals della cartaJolly
	 * @return true-> uguali / false->diverse
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
	/**
	 * Restituisce il colore della carta--> è un colore Fittizio
	 * @return Colore
	 */
	@Override
	public Colore getColor(){
		return new Colore("jolly", 0, 0, 0); //colore fittizio per la grafica... bruttissimo
	}
	
}
