/**
 * 
 */
package it.polimi.ingsw.cg13.carte;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import it.polimi.ingsw.cg13.bonus.OggettoBonusInput;
import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.eccezioni.CartePoliticheNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.PermessoNotFoundException;
import it.polimi.ingsw.cg13.mappa.Citta;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.partita.Partita;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public class PermessoCostruzione extends Carta implements OggettoBonusInput{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7763118167702368254L;
	private List<Citta> cittaCostruibili;
	private Set<Ricompensa> ricompense;
	private boolean isUsata=false;
	/**
	 * Costruttore
	 * @param citta--> lista di citt� da inserire
	 * @param ricompense-->set di ricompense
	 * @throws IllegalArgumentException--> valori nulli
	 */
	public PermessoCostruzione(List<Citta> citta,Set<Ricompensa> ricompense){
		if(citta!=null&&!citta.isEmpty()&&!citta.contains(null))
			this.cittaCostruibili=citta;
		else
			throw new IllegalArgumentException("Città passate non valide!");
		
		if(ricompense!=null&&!ricompense.isEmpty()&&!ricompense.contains(null))
			this.ricompense=ricompense;
		else
			throw new IllegalArgumentException("Ricompense passate non valide!");
		
	}
	
	/**
	 * Ritorna la lista di citt� in cui � �possibile costruire
	 * @return List<Citta> cittaCostruibili
	 */
	public List<Citta> getListaCitta(){
		return this.cittaCostruibili;
	}
	
	/**
	 * Ritorna la citt� della lista di Costruibili con un determinato indice
	 * @param index--> indice della citt�
	 * @return Citta 
	 */
	public Citta getCitta(int index){
		return this.cittaCostruibili.get(index);
	}
	
	/**
	 * Ritorna la citt� della lista di Costruibili con un determinato nome
	 * @param nomeCitta--> nome della citt�
	 * @return Citta 
	 * @throws CittaNotFoundException se non trova la citta' con quel nome
	 */
	public Citta getCitta(String nomeCitta) throws CittaNotFoundException{
		for(Citta citta:this.cittaCostruibili){
			if(citta.getNome().equals(nomeCitta)){
				return citta;
			}
		}
		throw new CittaNotFoundException(nomeCitta+" non trovata"); 
	}
	
	/**
	 * Assegna le ricompense ad un giocatore di una determinata partita 
	 * @param giocatore--> giocatore a cui assegnare le ricompense
	 * @param partita--> partita in corso	 
	 */
	public void assegnaRicompense(Giocatore giocatore,Partita partita){
		for(Ricompensa ricompensa:ricompense){
			ricompensa.assegnaRicompensa(giocatore, partita);
		}
	}

	@Override
	/**
	 * toString del metodo
	 */
	public String toString() {
		String permesso="PermessoCostruzione { cittaCostruibili=(";
		for(Citta citta:this.cittaCostruibili){
			permesso+=citta.getNome()+", ";
		}
		permesso+="), ricompense="+ricompense;
		return permesso;
	}
	
	
	@Override
	/**
	 * permette di inserire il permesso nella lista del Giocatore
	 * @param giocatore--> giocatore a cui assegnare il permesso
	 */
	public void acquistaDaMarket(Giocatore g) {
		this.setUsata(false);
		g.accumulaPermesso(this);
		
	}

	@Override
	/**
	 * permette di rimuovere il permesso dalla lista del Giocatore
	 * @param giocatore--> giocatore a cui rimuover il permesso
	 */
	public void rimuoviDaGiocatore(Giocatore g) throws PermessoNotFoundException {
		g.perdiPermesso(this);
		
	}
	
	/**
	 * {@inheritDoc}
	 * Aggiorna il permesso passato dal client
	 * Cerca il permesso tra le carte del giocatore
	 * e se lo trova lo rimuove perche ormai in vendita nel market
	 */
	@Override
	public void updateFromServer(Giocatore g,Inserzione inserzione)throws CdQException{
		List<PermessoCostruzione> permessi=new ArrayList<>();
		permessi.addAll(g.getPermessiCostruzione());
		permessi.addAll(g.getCartePermessoUsate());
		PermessoCostruzione permessoGiocatore=permessi.stream().
				filter(carta-> carta.equals(this)).findFirst().
				orElseThrow(()-> new CartePoliticheNotFoundException("Carta Permesso non riscontrata sul server"));
		g.rimuoviCartaPermesso(permessoGiocatore,g.getCartePermessoUsate());
		g.rimuoviCartaPermesso(permessoGiocatore,g.getPermessiCostruzione());
		inserzione.setOggettoInVendita(permessoGiocatore);
	}
	
	/**
	 * {@inheritDoc}
	 * riassegna il permesso giocatore nel caso quest'ultimo non venga comprato 
	 * da nessuno nella fase di market; controlla se e' stato gia usato oppure no 
	 * e lo assegna di conseguenza
	 */
	@Override 
	public void riassegnaAlProprietario(Giocatore g){
		if(this.isUsata())
			g.addPermessoCostruzione(this,g.getCartePermessoUsate());
		else g.addPermessoCostruzione(this, g.getPermessiCostruzione());
	}

	@Override
	/**
	 * permette di avere l' HashCode della' oggetto
	 * @return int result-->hashCode
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cittaCostruibili == null) ? 0 : cittaCostruibili.hashCode());
		result = prime * result + ((ricompense == null) ? 0 : ricompense.hashCode());
		return result;
	}

	@Override
	/**
	 * equals della classe
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermessoCostruzione other = (PermessoCostruzione) obj;
		if (cittaCostruibili == null) {
			if (other.cittaCostruibili != null)
				return false;
		} else if (!cittaCostruibili.equals(other.cittaCostruibili))
			return false;
		if (ricompense == null) {
			if (other.ricompense != null)
				return false;
		} else if (!ricompense.equals(other.ricompense))
			return false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 * Assegne le ricompense relative a questo permesso
	 */
	@Override
	public void assegnaBonusInput(Giocatore giocatore, Partita partita) {
		this.ricompense.forEach(ricompensa->ricompensa.assegnaRicompensa(giocatore, partita));
	}

	/*
	 * Non serve, la update del oggetto basta per verificare se l'oggetto passat va bene come parametro
	 * 
	@Override
	public boolean acceptParametroCorretto(RicompensaSpecial ricompensa,Giocatore giocatore, Partita partita) throws CdQException {
		return ricompensa.visitOggettoBonusInput(this,giocatore,partita);
	}*/

	/**
	 * Restituisce il set di ricompense
	 * @return ricompense
	 */
	public Set<Ricompensa> getRicompense() {
		return ricompense;
	}

	/**
	 * Ritorna se la carta e' stata gia usata dal giocatore
	 * @return true se la carta e' stata usata
	 */
	public boolean isUsata() {
		return isUsata;
	}

	/**
	 * Setta se una carta permesso e' stata usata per costruire o meno
	 * @param isUsata: usata 
	 */
	public void setUsata(boolean isUsata) {
		this.isUsata = isUsata;
	}
}
