/**
 * 
 */
package it.polimi.ingsw.cg13.carte;


import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CartePoliticheNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.oggetti.Inserzione;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;

/**
 * @author Marco
 *
 */
public abstract class CartaPolitica extends Carta{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1201799744962686229L;

	public abstract boolean match (Consigliere consigliere);
	public abstract boolean equals(CartaPoliticaSemplice carta);
	public abstract boolean equals(CartaPoliticaJolly carta);
	public abstract <C extends CartaPolitica> boolean equals(C carta);
	public abstract int getCosto();
	
	@Override
	/**
	 * il metodo permetti di aggiungere una carta politica alla lista del giocatore
	 * @param: Giocatore g--> giocatore a cui aggiungere la carta
	 */
	public void acquistaDaMarket(Giocatore g) {
		g.addCartaPolitica(this);
		
	}

	@Override
	/**
	 * il metodo permetti di rimuovere una carta politica dalla lista del giocatore
	 * @param: Giocatore g--> giocatore a cui rimuovere la carta
	 */
	public void rimuoviDaGiocatore(Giocatore g) {
		g.rimuoviCartaPolitica(this);
		
	}
	
	/**
	 * {@inheritDoc}
	 * Passata un inserzione dal client, viene aggiornata la carta in abse alle carti presenti 
	 * nella mano del giocatore
	 */
	@Override
	public void updateFromServer(Giocatore g,Inserzione inserzione)throws CdQException{
		CartaPolitica cartaGiocatore=g.getCartePolitiche().stream().
				filter(carta-> carta.equals(this)).findFirst().
				orElseThrow(()-> new CartePoliticheNotFoundException("Carta politica non riscontrata sul server"));
		g.rimuoviCartaPolitica(cartaGiocatore);
		inserzione.setOggettoInVendita(cartaGiocatore);
	}
	
	/**
	 * Riassegna la carta al giocatore passato come parametro
	 * @param g-> giocatore a cui assegnare la carta
	 */
	@Override 
	public void riassegnaAlProprietario(Giocatore g){
		g.addCartaPolitica(this);
	}
	
	public abstract Colore getColor();

}
