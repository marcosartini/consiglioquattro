 /**
 * 
 */
package it.polimi.ingsw.cg13.carte;

import java.util.List;

/**
 * @author Marco
 *
 */
public class MazzoPolitico extends Mazzo<CartaPolitica> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2295421127327935504L;

	/**
	 * @param mazzoCarte
	 * @throws IllegalArgumentException--> mazzo non valido
	 */
	public MazzoPolitico(List<CartaPolitica> mazzoCarte) {
		super(mazzoCarte);
	}
	
	/**
	 * Costruisci un mazzoPolitco con arrayList vuoto
	 */
	public MazzoPolitico(){
		super();
	}
	
	/**
	 * Costruttore che genera una copia distinta dell'oggetto ricevuto
	 * @param mazzoPolitico
	 * @throws IllegalArgumentException--> mazzo non valido
	 */
	public MazzoPolitico(MazzoPolitico mazzoPolitico) {
		super(mazzoPolitico);
	}

	
	
	
	
}
