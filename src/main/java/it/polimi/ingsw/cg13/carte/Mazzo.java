/**
 * 
 */
package it.polimi.ingsw.cg13.carte;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Iterator;
import java.util.List;

import it.polimi.ingsw.cg13.mutatio.PescaCartaMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;



/**
 * @author Marco
 *
 */
public abstract class Mazzo <C extends Carta> extends ObservableC<Mutatio> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7660615409937745830L;
	private List<C> mazzoCarte;
	
	
	
	/**
	 * @param mazzoCarte--> lista di carte da aggiungere al mazzo
	 * @throws IllegalArgumentException--> valori nulli
	 */
	public Mazzo(List<C> mazzoCarte) {
		if(mazzoCarte!=null && !mazzoCarte.isEmpty()){
			this.mazzoCarte = mazzoCarte;
			this.mescolaMazzo();
		}
		else
			throw new IllegalArgumentException("CartePermesso passate non valide!");
	}
	
	/**
	 * Costruttore Mazzo di carte vuoto
	 */
	public Mazzo(){
		this.mazzoCarte=new ArrayList<>();
	}

	/**
	 * Costruttore che genera una copia distinta dell'oggetto ricevuto
	 * @param mazzo
	 */
	public Mazzo(Mazzo<C> mazzo) {
		this.mazzoCarte=mazzo.mazzoCarte;
		this.mescolaMazzo();
	}
	
	/**
	 * Mescola il mazzo di carte
	 */
	public void mescolaMazzo(){
		Collections.shuffle( getMazzoCarte());
	}
	
	/**
	 * Ritorna il  numero di carte richeste dalla "cima" del mazzo senza rimuoverle
	 * @param numCarte--> numero di carte richieste
	 * @return le carte in cima al mazzo
	 */
	public List<C> getCarteInCima(int numCarte){
		List<C> carte=new ArrayList<>();
		int carteMazzo=this.mazzoCarte.size();
		if(carteMazzo==0)
			return carte;
		else if(carteMazzo<numCarte)
			numCarte=carteMazzo;
		carteMazzo--;
		int indice=carteMazzo;
		for(int i=0;i<numCarte;i++){
			//indice=indice-i;
			carte.add(this.mazzoCarte.get(indice-i));
		}
		return carte;
	}
	
	/**
	 * Prende la carta col numero passato tra le carte in cima del mazzo
	 * @param numCarta da prendere
	 * @param permessiVisibili da cui posso prendere le carte
	 * @return
	 */
	public C getCarta(int numCarta,int permessiVisibili){
		List<C> carteInCima=this.getCarteInCima(permessiVisibili);
		return carteInCima.get(numCarta);
		
	}
	
	/**
	 * Ritorna la prima carta del mazzo rimuovendola da esso
	 * @return Carta
	 */
	public C pescaCarta(){
		
		C carta=this.getMazzoCarte().remove(0);
		this.notifyObservers(new PescaCartaMutatio<>(this));
		return carta;
	}
	
	/**
	 * Restituisce la lista di carte contenute nel mazzo
	 * @return mazzoCarte--> List
	 */
	public List<C> getMazzoCarte() {
		return mazzoCarte;
	}
	
	
	/**
	 * Setta la lista di carte contenute nel mazzo	 * 
	 */
	public void setMazzoCarte(List<C> mazzoCarte) {
		this.mazzoCarte = mazzoCarte;
	}
	
	/**
	 * Restituisce la dimensione del mazzo di carte
	 * @return size-->int
	 */
	public int sizeMazzo(){
		return this.mazzoCarte.size();
	}
	
	/**
	 * Rimuove una lista di carte dal mazzo
	 * @param carteDaRimuovere--> lista di carte da rimuovere
	 */
	public void removeCarte(List<C> carteDaRimuovere){
		for(C cartaScelta:carteDaRimuovere){
			boolean trovata=false;
			Iterator<C> iteratorCarte=this.getMazzoCarte().iterator();
			C cartaGiocatore=null;
			while(!trovata && iteratorCarte.hasNext() ){
				cartaGiocatore=iteratorCarte.next();
				if(cartaScelta.equals(cartaGiocatore)){
					trovata=true;
				}
			}
			if(cartaGiocatore!=null && trovata){
				carteDaRimuovere.remove(cartaGiocatore);
				//this.getMazzoCarte().remove(cartaGiocatore);
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mazzoCarte == null) ? 0 : mazzoCarte.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mazzo other = (Mazzo) obj;
		if (mazzoCarte == null) {
			if (other.mazzoCarte != null)
				return false;
		} else if (!mazzoCarte.equals(other.mazzoCarte))
			return false;
		return true;
	}
	
	/**
	 * Permette di aggiungere una carta al mazzo
	 * @param carta
	 */
	public void aggiungiCarta(C carta){
		this.mazzoCarte.add(carta);
	}
	
	/**
	 * Permette di aggiungere una lista di carte al mazzo
	 * @param carta
	 */
	public void aggiungiCarta(List<C> carte){
		this.mazzoCarte.addAll(carte);
	}
	
}
