package it.polimi.ingsw.cg13.partita;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.polimi.ingsw.cg13.bonus.Ricompensa;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.file.FactoryOggetti;

public class Parametri implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3961197297755950702L;
	private final int numeroMaxGiocatori;
	private final int costoStrade;
	private final int numeroEmporiPerGiocatore;
	private final Map<Integer,Integer> costiAggiuntiviCorruzione;
	private final Map<String,Colore> coloriPolitici;
	private final List<Colore> coloriGiocatori;
	private final List<Colore> coloriCitta;
	private final List<Set<Ricompensa>> gettoni;
	private int durataMossa=90;
	private int configurazioneMappa;
	
	/**
	 * @param numeroMaxGiocatori > 0
	 * @param costoStrade > 0
	 * @param costiAggiuntiviCorruzione non vuoto
	 * @param coloriPolitici non vuoto
	 * @param coloriGiocatori non vuoto
	 * @param coloriCitta non vuoto
	 */
	public Parametri(int numeroMaxGiocatori, int costoStrade, Map<Integer, Integer> costiAggiuntiviCorruzione,
			Map<String,Colore> coloriPolitici, List<Colore> coloriGiocatori, List<Colore> coloriCitta, int numEmpGioc, List<Set<Ricompensa>> gettoni) {
	
		this.numeroMaxGiocatori = numeroMaxGiocatori;
		this.costoStrade = costoStrade;
		this.costiAggiuntiviCorruzione = costiAggiuntiviCorruzione;
		this.coloriPolitici = coloriPolitici;
		this.coloriGiocatori = coloriGiocatori;
		this.coloriCitta = coloriCitta;
		this.numeroEmporiPerGiocatore=numEmpGioc;
		this.gettoni = gettoni;
	}
	
	
	
	public Parametri(FactoryOggetti factoryOggetti) {
		this.numeroMaxGiocatori=factoryOggetti.getNumeroMaxGiocatori();
		this.costoStrade=factoryOggetti.getCostoStrade();
		//List<Integer> costi= factoryOggetti.getCostiAggiuntiviPerAzioni()
		
		this.costiAggiuntiviCorruzione=new HashMap<>();
		
		//riga seguente vede le cose da altro punto di vista, per il file
		//costi.forEach(c -> costiAggiuntiviCorruzione.put(costi.get(c), c));
		
		//da modificare nel file l'elenco, che inserisco a mano adesso:
		this.costiAggiuntiviCorruzione.put(1, 10);
		this.costiAggiuntiviCorruzione.put(2, 7);
		this.costiAggiuntiviCorruzione.put(3, 4);
		this.costiAggiuntiviCorruzione.put(4, 0);
		//^^^fine inserimento manuale
		
		this.coloriPolitici= new HashMap<>();
		List<Colore> coloriP=factoryOggetti.getColoriPolitici();
		coloriP.forEach(c -> this.coloriPolitici.put(c.getNome(), c));
		
		this.coloriGiocatori=factoryOggetti.getColoriGiocatori();
		this.coloriCitta=factoryOggetti.getColoriCitta();
		
		this.numeroEmporiPerGiocatore=factoryOggetti.getNumeroEmpori();
		
		this.gettoni= factoryOggetti.getGettoni();
	}

	
	public Parametri(FactoryOggetti factoryOggetti, int durataMosse, int configurazioneMappa) {
		this(factoryOggetti);
		this.durataMossa=durataMosse;
		this.configurazioneMappa = configurazioneMappa;
	}



	/**
	 * @return the costiAggiuntiviCorruzione
	 */
	public Map<Integer, Integer> getCostiAggiuntiviCorruzione() {
		return costiAggiuntiviCorruzione;
	}

	/**
	 * @return the coloriPolitici
	 */
	public Map<String,Colore> getColoriPolitici() {
		return coloriPolitici;
	}

	/**
	 * @return the coloriGiocatori
	 */
	public List<Colore> getColoriGiocatori() {
		return coloriGiocatori;
	}

	/**
	 * @return the coloriCitta
	 */
	public List<Colore> getColoriCitta() {
		return coloriCitta;
	}
	
	/**
	 * Dato il numero di carte, torna il costo aggiuntivo della corruzione
	 * @param key 
	 * @return l'oggetto con la chiave corrispondente
	 * @throws IllegalArgumentException se key non viene trovata
	 */
	public Integer getCostoCorruzione(Integer key) {
		Integer value=this.costiAggiuntiviCorruzione.get(key);
		if(value==null)
			throw new IllegalArgumentException("Chiave non presente nel dizionario");
		return value;
	}

	/**
	 * @return the numeroMaxGiocatori
	 */
	public int getNumeroMaxGiocatori() {
		return numeroMaxGiocatori;
	}

	/**
	 * @return the costoStrade
	 */
	public int getCostoStrade() {
		return costoStrade;
	}
	
	/**
	 * 
	 * @return il numero di empori per giocatore
	 */
	public int getNumeroEmporiPerGiocatore() {
		return numeroEmporiPerGiocatore;
	}
	
	/**
	 * Torna un colore politico
	 * @param nomeColore
	 * @return il colore corrispondente al nome ricevuto, se esiste
	 * @throws ColoreNotFoundException quando non e' presente quel nome di colore
	 */
	public Colore getColorePolitico(String nomeColore) throws ColoreNotFoundException{
		if(!this.coloriPolitici.containsKey(nomeColore))
			throw new ColoreNotFoundException("Colore non presente");
		return this.coloriPolitici.get(nomeColore);
	}
	
	public Set<Ricompensa> getGettone(int posizione) {
		if(!this.gettoni.isEmpty())
			return this.gettoni.remove(posizione);
		else throw new IllegalArgumentException("Bonus terminati");
	}
	
	/**
	 * Torna il numero di bonus disponibili nell'elenco
	 * @return un numero intero minimo 0
	 */
	public int getNumeroBonus() {
		return this.gettoni.size();
	}


	/**
	 * Torna il tempo della partita per fare una mossa
	 * @return tempo in secondi per fare una mossa
	 */
	public int getDurataMossa() {
		return durataMossa;
	}
	
	/**
	 * Torna la codifica delle mappa, rappresentata dal valore in base 10 delle combinazioni delle
	 * mappe in base 2 es: 0 se 000, 2 se 010 ecc...
	 * @return la codifica della configurazione
	 */
	public int getConfigurazioneMappa() {
		return configurazioneMappa;
	}
}
