package it.polimi.ingsw.cg13.partita;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import it.polimi.ingsw.cg13.bonus.GruppoBonusRe;
import it.polimi.ingsw.cg13.bonus.TesseraBonus;
import it.polimi.ingsw.cg13.bonus.TesseraBonusGruppoCitta;
import it.polimi.ingsw.cg13.bonus.TesseraBonusRegione;
import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.MazzoPolitico;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.AiutantiFinitiException;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ConsigliereNotFoundException;

import it.polimi.ingsw.cg13.file.FactoryOggetti;
import it.polimi.ingsw.cg13.mutatio.CambioMazziPoliticiMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.observable.ObserverC;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Consigliere;

public class Banco extends ObservableC<Mutatio> implements Serializable, ObserverC<Mutatio> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3321101697220495235L;
	private MazzoPolitico mazzoPolitico;
	private MazzoPolitico mazzoScarto;
	private final Queue<Aiutante> aiutanti;
	private final Map<Colore,Queue<Consigliere>> consiglieri;
	private final GruppoBonusRe gruppoBonusRe;
	private final List<TesseraBonus> tessereBonus;
	
	public Banco(FactoryOggetti factoryOggetti){
		this.mazzoPolitico=factoryOggetti.getMazzoPolitico();
		this.mazzoScarto=new MazzoPolitico();
		
		this.aiutanti=new ArrayDeque<>();
		this.aiutanti.addAll(factoryOggetti.getAiutanti());
	
		this.gruppoBonusRe=factoryOggetti.getGruppoBonusRe();
		// register obs
		this.tessereBonus=new ArrayList<>();
		this.consiglieri=new HashMap<>();
		int num=factoryOggetti.getNumeroConsiglieri();
		for(Colore colore: factoryOggetti.getColoriPolitici()){
			Queue<Consigliere> coda = new ArrayDeque<>();
			for(int i=0; i<num; i++)
				coda.add(new Consigliere(colore));
			this.consiglieri.put(colore, coda);
		}
		
		List<TesseraBonusGruppoCitta> bonusCitta = factoryOggetti.getTessereBonusCitta();
		List<TesseraBonusRegione> bonusRegione = factoryOggetti.getTessereBonusRegione();
		this.tessereBonus.addAll(bonusCitta);
		this.tessereBonus.addAll(bonusRegione);
		this.tessereBonus.forEach(bonus -> bonus.registerObserver(gruppoBonusRe));
		this.gruppoBonusRe.registerObserver(this);
		
		this.mazzoPolitico.registerObserver(this);
		this.mazzoScarto.registerObserver(this);
		
		
	}
	/**
	 * Genera un banco vuoto, con un GruppoBonusRe
	 * @param gruppoBonusRe
	 */
	public Banco(GruppoBonusRe gruppoBonusRe) {
		this.mazzoPolitico=new MazzoPolitico();
		this.mazzoScarto=new MazzoPolitico();
		this.aiutanti=new ArrayDeque<>();
		this.consiglieri=new HashMap<>();
		this.gruppoBonusRe= new GruppoBonusRe(gruppoBonusRe);
		this.tessereBonus=new ArrayList<>();
	}
	
	
	
	/**
	 * Genera un banco con mazzoScarto vuoto
	 * @param mazzoPolitico
	 * @param aiutanti
	 * @param consiglieri
	 * @param gruppoBonusRe
	 * @param tessereBonus
	 */
	public Banco(MazzoPolitico mazzoPolitico, Queue<Aiutante> aiutanti, Map<Colore, Queue<Consigliere>> consiglieri,
			GruppoBonusRe gruppoBonusRe, List<TesseraBonus> tessereBonus) {
		this.mazzoPolitico = mazzoPolitico;
		this.aiutanti = aiutanti;
		this.consiglieri = consiglieri;
		this.gruppoBonusRe = gruppoBonusRe;
		this.tessereBonus= tessereBonus;
		this.mazzoScarto=new MazzoPolitico();
	}



	/**
	 * @param mazzoPolitico
	 * @param mazzoScarto
	 * @param aiutanti
	 * @param consiglieri
	 * @param gruppoBonusRe
	 */
	public Banco(MazzoPolitico mazzoPolitico, MazzoPolitico mazzoScarto, Queue<Aiutante> aiutanti,
			Map<Colore, Queue<Consigliere>> consiglieri, GruppoBonusRe gruppoBonusRe, List<TesseraBonus> tessereBonus) {
		this.mazzoPolitico = mazzoPolitico;
		this.mazzoScarto = mazzoScarto;
		this.aiutanti = aiutanti;
		this.consiglieri = consiglieri;
		this.gruppoBonusRe = gruppoBonusRe;
		this.tessereBonus = tessereBonus;
	}
	
	/**
	 * Pesca una carta dal MazzoPolitico
	 * @return la carta politica in cima al mazzo
	 * 
	 */
	public CartaPolitica pescaCartaPolitica(){
		this.controllaMazzoPolitico();
		return this.mazzoPolitico.pescaCarta();
	}
	
	/**
	 * Controlla la dimensione del mazzo politico, e se e' esaurito, mescola il mazzo di scarto
	 * e li scambia
	 */
	public void controllaMazzoPolitico(){
		if(mazzoPolitico.sizeMazzo()==0){
			MazzoPolitico temp = this.mazzoPolitico;
			this.mazzoScarto.mescolaMazzo();
			this.mazzoPolitico=this.mazzoScarto;
			this.mazzoScarto=temp;
			this.notifyObservers(new CambioMazziPoliticiMutatio(mazzoPolitico,mazzoScarto));
		}
	}
	
	/**
	 * Torna ed toglie dal banco un consigliere del colore scelto
	 * @param colore non nullo
	 * @return un consigliere del colore indicato
	 * @throws NoSuchElementException se i consiglieri di quel colore sono finiti
	 * @throws ColoreNotFoundException se il colore richiesto non e' mappato
	 */
	public Consigliere getConsigliere (Colore colore) throws ColoreNotFoundException{
		if(!this.consiglieri.containsKey(colore))
			throw new ColoreNotFoundException("Colore non presente");
		return this.consiglieri.get(colore).remove();
	}
	
	/**
	 * Inserisce il consigliere nella coda di consiglieri del suo colore
	 * Se non c'e' il colore, lo aggiunge al dizionario e aggiunge il consigliere
	 * @param consigliere non nullo
	 */
	public void aggiungiConsigliere(Consigliere consigliere){
		Queue<Consigliere> queue=this.consiglieri.get(consigliere.getColor());
		if(queue==null){
			queue=new ArrayDeque<>();
		}
		queue.add(consigliere);
		this.consiglieri.put(consigliere.getColor(), queue);
			
	}
	
	/**
	 * Torna un aiutante del banco
	 * @return un aiutante
	 * @throws CdQException 
	 */
	public Aiutante getAiutante() throws CdQException{
		if(this.aiutanti.size()>0){
			return this.aiutanti.remove();
		}
		else throw new AiutantiFinitiException("Aiutanti del banco sono finiti");
	}
	
	/**
	 * Torna il numero di aiutanti richiesto, in una lista
	 * @param numero > 0
	 * @return una lista di aiutanti
	 */
	public List<Aiutante> getAiutante(int numero){
		List<Aiutante> aiutantiList=new ArrayList<>();
		for(int i=0;i<numero; i++){
			if(aiutanti.size()>0)
				aiutantiList.add(this.aiutanti.remove());
		}
		return aiutantiList;
	}



	/**
	 * @return the gruppoBonusRe
	 */
	public GruppoBonusRe getGruppoBonusRe() {
		return gruppoBonusRe;
	}


	
	/**
	 * Aggiunge la carta ricevuta al mazzo di scarto
	 * @param carta not null
	 */
	public void scartaCarta(List<CartaPolitica> carte){
			this.mazzoScarto.aggiungiCarta(carte);
	}
	
	@Override
	public String toString() {
		String stringa = "Banco:";
		
		stringa+= "\nAiutanti: "+this.aiutanti.size();
		stringa+= "\nConsiglieri: ";
		
		for(Entry<Colore, Queue<Consigliere>> entry : this.consiglieri.entrySet()){
			stringa+= " " +entry.getKey().getNome() +" = "+ this.consiglieri.get(entry.getKey()).size();
		}
		
		stringa+="\nMazzoPolitico: "+ this.mazzoPolitico.sizeMazzo() +" carte";
		stringa+="\nMazzoScarto: "+ this.mazzoScarto.sizeMazzo() +" carte";
		
		stringa+="\nBonus: ";		
		for(TesseraBonus tb: this.tessereBonus){
			stringa+= " "+ tb.toString();
		}
	
		return stringa;
	}
	
	/**
	 * Torna vero se gli aiutanti sono finiti, falso altrimenti
	 * @return se la coda di aiutanti e' vuota
	 */
	public boolean isEmptyAiutanti(){
		return this.aiutanti.isEmpty();
	}
	
	/**
	 * Aggiunge un aiutante alla coda di aiutanti disponibili del banco
	 */
	public void accumulaAiutante(){
		this.aiutanti.add(new Aiutante());
	}
	
	/**
	 * Aggiunge un consigliere alla coda dei consiglieri del suo colore, se il colore e' gia' presente
	 * @param consigliere non nullo
	 * @throws ConsigliereNotFoundException se si tenta di aggiungere un consigliere di un colore non previsto
	 */
	public void accumulaConsigliere(Consigliere consigliere) throws ConsigliereNotFoundException{
		Queue<Consigliere> queue=this.consiglieri.get(consigliere.getColor());
		if(queue==null){
			throw new ConsigliereNotFoundException("Non sono previsti consiglieri di questo colore");
		}
		queue.add(consigliere);
		this.consiglieri.put(consigliere.getColor(), queue);
	}
	
	@Override
	public void update(Mutatio change) {
		this.notifyObservers(change);
	}
	/**
	 * 
	 * @return l'elenco di tessere bonus
	 */
	public List<TesseraBonus> getTessereBonus() {
		return tessereBonus;
	}

	
	public Map<Colore, Queue<Consigliere>> getConsiglieri() {
		return consiglieri;
	}
	public MazzoPolitico getMazzoPolitico() {
		return mazzoPolitico;
	}
	public MazzoPolitico getMazzoScarto() {
		return mazzoScarto;

	}
	public void setMazzoPolitico(MazzoPolitico mazzoPolitico) {
		this.mazzoPolitico = mazzoPolitico;
	}
	public void setMazzoScarto(MazzoPolitico mazzoScarto) {
		this.mazzoScarto = mazzoScarto;
	}
}
