package it.polimi.ingsw.cg13.partita;

import java.io.Serializable;

import it.polimi.ingsw.cg13.file.FactoryOggetti;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.observable.ObservableC;
import it.polimi.ingsw.cg13.observable.ObserverC;
import it.polimi.ingsw.cg13.percorsi.PercorsoNobilta;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;

public class Percorsi extends ObservableC<Mutatio> implements Serializable, ObserverC<Mutatio> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4926571282790371437L;
	private final PercorsoNobilta nobilta;
	private final PercorsoRicchezza ricchezza;
	private final PercorsoVittoria vittoria;
	
	/**
	 * @param nobilta non vuoto
	 * @param ricchezza non vuoto
	 * @param vittoria non vuoto
	 */
	public Percorsi(PercorsoNobilta nobilta, PercorsoRicchezza ricchezza, PercorsoVittoria vittoria) {
		this.nobilta = nobilta;
		this.ricchezza = ricchezza;
		this.vittoria = vittoria;
		this.registraCaselle();
	}
	
	public Percorsi(FactoryOggetti factoryOggetti) {
		this.nobilta=factoryOggetti.getPercorsoNobilta();
		this.ricchezza=factoryOggetti.getPercorsoRicchezza();
		this.vittoria=factoryOggetti.getPercorsoVittoria();
		this.registraCaselle();
	}

	/**
	 * @return the nobilta
	 */
	public PercorsoNobilta getNobilta() {
		return nobilta;
	}

	/**
	 * @return the ricchezza
	 */
	public PercorsoRicchezza getRicchezza() {
		return ricchezza;
	}

	/**
	 * @return the vittoria
	 */
	public PercorsoVittoria getVittoria() {
		return vittoria;
	}

	@Override
	public void update(Mutatio change) {
		this.notifyObservers(change);
		
	}
	
	public void registraCaselle(){
		this.nobilta.getPercorso().forEach(casella->casella.registerObserver(this));
		this.ricchezza.getPercorso().forEach(casella->casella.registerObserver(this));
		this.vittoria.getPercorso().forEach(casella->casella.registerObserver(this));
	}
	
	
	
	
}
