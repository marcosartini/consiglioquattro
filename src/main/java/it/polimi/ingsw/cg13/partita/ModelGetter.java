package it.polimi.ingsw.cg13.partita;

import java.util.List;

import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ConsigliereNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.mappa.Mappa;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.percorsi.PercorsoNobilta;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.personaggi.King;


public interface ModelGetter {

	/**
	 * 
	 * @return il costo del pedaggio di percorrenza relativo alle strade
	 * del regno. Costo unico per tutte le strade
	 */
	public int getCostoStrade();
	
	/**
	 * Dato un numero di carte, viene chiesto il costo aggiuntivo da dover pagare
	 * @param numeroCarte numero delle carte utilizzate nell'azione
	 * @return il prezzo da pagare
	 */
	public int getCostoAggiuntivoAzione(int numeroCarte);
	
	/**
	 * 
	 * @return la mappa della partita
	 */
	public Mappa getMappaPartita();
	
	/**
	 * 
	 * @return il giocatore corrente nello stato del modello
	 */
	public Giocatore getGiocatoreCorrente();
	
	/**
	 * 
	 * @return la lista di giocatori iscritti alla partita
	 */
	public List<Giocatore> getGiocatori();
	
	/**
	 * 
	 * @return la carta in cima al mazzo politico
	 */
	public CartaPolitica pescaCarta();
	
	/**
	 * Mette la carta scartata nel mazzo di scarto
	 * @param carta
	 */
	public void scartaCarta(List<CartaPolitica> carte);
	
	/**
	 * 
	 * @return torna il percorso nobilta'
	 */
	public PercorsoNobilta getPercorsoNobilta();
	
	/**
	 * 
	 * @return il percorso ricchezza
	 */
	public PercorsoRicchezza getPercorsoRicchezza();
	
	/**
	 * 
	 * @return il percorso vittoria
	 */
	public PercorsoVittoria getPercorsoVittoria();
	
	/**
	 * 
	 * @param nomeRegione
	 * @return la regione con quel nome, se c'e'
	 * @throws RegioneNotFoundException
	 */
	public Regione getRegione(String nomeRegione) throws RegioneNotFoundException;

	/**
	 * 
	 * @param nomeCitta
	 * @return il nodo della citta con quel nome, se c'e'
	 * @throws CittaNotFoundException
	 */
	public NodoCitta getNodoCitta (String nomeCitta) throws CittaNotFoundException;

	
	/**
	 * 
	 * @param nomeColore
	 * @return il colore politico con quel nome, se c'e'
	 * @throws ColoreNotFoundException quando non trova il colore
	 */
	public Colore getColorePolitico(String nomeColore) throws ColoreNotFoundException;
	
	/**
	 * Prende un consigliere del colore specificato dai consiglieri del banco
	 * @param colore
	 * @return un consigliere di quel colore, se ce ne sono
	 * @throws ConsigliereNotFoundException quando non trova un consigliere di quel colore
	 * @throws ColoreNotFoundException quando non trova il colore ricevuto
	 */
	public Consigliere getConsigliere(Colore colore) throws ConsigliereNotFoundException, ColoreNotFoundException;
	/**
	 * 
	 * @return questa partita
	 */
	public Partita getPartita();
	
	/**
	 * 
	 * @return un aiutante, se ce ne sono ancora a disposizione
	 * @throws CdQException 
	 */
	public Aiutante getAiutante() throws CdQException;
	
	/**
	 * 
	 * @param numero
	 * @return una lista di aiutanti tale che \return.size()==numero
	 */
	public List<Aiutante> getAiutanti(int numero);
	


	
	/**
	 * 
	 * @return il re
	 */
	public King getKing();
}
	