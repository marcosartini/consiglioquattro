package it.polimi.ingsw.cg13.partita;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Random;

import it.polimi.ingsw.cg13.bonus.RicompensaSpecial;
import it.polimi.ingsw.cg13.carte.CartaPolitica;
import it.polimi.ingsw.cg13.carte.MazzoPermessi;
import it.polimi.ingsw.cg13.carte.PermessoCostruzione;
import it.polimi.ingsw.cg13.colore.Colore;
import it.polimi.ingsw.cg13.comandi.Login;
import it.polimi.ingsw.cg13.cronometro.Cronometro;
import it.polimi.ingsw.cg13.eccezioni.CdQException;
import it.polimi.ingsw.cg13.eccezioni.CittaNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ColoreNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ConsigliereNotFoundException;
import it.polimi.ingsw.cg13.eccezioni.ErrorUpdateServerException;
import it.polimi.ingsw.cg13.eccezioni.FileSyntaxException;
import it.polimi.ingsw.cg13.eccezioni.RegioneNotFoundException;
import it.polimi.ingsw.cg13.file.FactoryOggetti;
import it.polimi.ingsw.cg13.machinestate.State;
import it.polimi.ingsw.cg13.machinestate.StateInizio;
import it.polimi.ingsw.cg13.mappa.Consiglio;
import it.polimi.ingsw.cg13.mappa.ConsiglioRe;
import it.polimi.ingsw.cg13.mappa.ConsiglioRegione;
import it.polimi.ingsw.cg13.mappa.Mappa;
import it.polimi.ingsw.cg13.mappa.NodoCitta;
import it.polimi.ingsw.cg13.mappa.ProprietarioConsiglio;
import it.polimi.ingsw.cg13.mappa.Regione;
import it.polimi.ingsw.cg13.mutatio.CambioPermessiMutatio;
import it.polimi.ingsw.cg13.mutatio.MescolatoMutatio;
import it.polimi.ingsw.cg13.mutatio.Mutatio;
import it.polimi.ingsw.cg13.mutatio.PartitaMutatio;
import it.polimi.ingsw.cg13.mutatio.StateMutatio;
import it.polimi.ingsw.cg13.observable.ObservableMVC;
import it.polimi.ingsw.cg13.observable.ObserverC;
import it.polimi.ingsw.cg13.oggetti.Emporio;
import it.polimi.ingsw.cg13.percorsi.PercorsoNobilta;
import it.polimi.ingsw.cg13.percorsi.PercorsoRicchezza;
import it.polimi.ingsw.cg13.percorsi.PercorsoVittoria;
import it.polimi.ingsw.cg13.personaggi.Aiutante;
import it.polimi.ingsw.cg13.personaggi.Consigliere;
import it.polimi.ingsw.cg13.personaggi.Giocatore;
import it.polimi.ingsw.cg13.personaggi.King;

public class Partita extends ObservableMVC<Mutatio> implements ObserverC<Mutatio>, ModelGetter, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5600831102316015156L;
	private Parametri parametri;
	private Banco banco;
	private Percorsi percorsi;
	private Mappa mappa;
	private King king;
	private State statoPartita;
	private List<Giocatore> giocatori;
	private List<Giocatore> giocatoriOffline;
	private boolean isEnded = false;
	
	public Partita(String stringaMappa, int durataMosse, int configurazioneMappa) throws FileSyntaxException {
		//magari ricevera' il file di configurazione
				FactoryOggetti factoryOggetti = new FactoryOggetti("src/main/resources/CdQConfig"+stringaMappa+".xml");
				
				this.parametri= new Parametri(factoryOggetti,durataMosse, configurazioneMappa);
				this.banco = new Banco(factoryOggetti);
				this.percorsi = new Percorsi(factoryOggetti);
				this.mappa = new Mappa(factoryOggetti);
				this.king = factoryOggetti.getRe();
				this.giocatori=new ArrayList<>(this.parametri.getNumeroMaxGiocatori());
				this.giocatoriOffline=new ArrayList<>();

				
				//registrare partita su osservabili
				
				
				
	}
	
	public Partita() throws FileSyntaxException {
		//magari ricevera' il file di configurazione
				FactoryOggetti factoryOggetti = new FactoryOggetti("src/main/resources/CdQConfig0.xml");
				
				this.parametri= new Parametri(factoryOggetti);
				this.banco = new Banco(factoryOggetti);
				this.percorsi = new Percorsi(factoryOggetti);
				this.mappa = new Mappa(factoryOggetti);
				this.king = factoryOggetti.getRe();
				this.giocatori=new ArrayList<>(this.parametri.getNumeroMaxGiocatori());
				this.giocatoriOffline=new ArrayList<>();

				
				//registrare partita su osservabili
				
				
				
	}
	
	public Partita(List<Giocatore> giocatori) throws FileSyntaxException, ColoreNotFoundException  {
		//magari ricevera' il file di configurazione
		FactoryOggetti factoryOggetti = new FactoryOggetti("src/main/resources/CdQConfig0.xml");
		
		this.parametri= new Parametri(factoryOggetti);
		this.banco = new Banco(factoryOggetti);
		this.percorsi = new Percorsi(factoryOggetti);
		this.mappa = new Mappa(factoryOggetti);
		this.king = factoryOggetti.getRe();
		this.giocatori=giocatori;
		this.giocatoriOffline=new ArrayList<>();
		
		//registrare partita su osservabili
		
		
	}
	/**
	 * Inizializza la partita settando tutti i parametri
	 * @throws ColoreNotFoundException
	 */
	private void init() throws ColoreNotFoundException{
		this.king.setConsiglioKing(new ConsiglioRe(estraiConsiglieri(), this.king));

		for(Regione regione: this.mappa.getRegioni()){
			regione.setConsiglio(new ConsiglioRegione(estraiConsiglieri(), regione));
			regione.getConsiglio().registerObserver(this.mappa);
		}
		this.king.getConsiglio().registerObserver(this);
		
		this.banco.registerObserver(this);
		this.mappa.registerObserver(this);
		this.percorsi.registerObserver(this);
		this.king.registerObserver(this);
		
		//bonus sulle citta
		this.king.getPosizione().getCittaNodo().setRicompense(new HashSet<>());
		Random random = new Random();
		try{
			this.mappa.getGrafo().stream().filter(nodo -> !nodo.equals(king.getPosizione()))
						.forEach(nodo -> nodo.getCittaNodo().setRicompense(this.parametri.getGettone(random.nextInt(this.parametri.getNumeroBonus()))));
		}
		catch(IllegalArgumentException e){
			this.notifyObservers(e);
		}
		
		try {
			this.notifyObservers(new PartitaMutatio(this,"Configurazione partita per due giocatori in corso..."));
		} catch (Exception e) {
			this.notifyObservers(e);
		}
		
		//configurazione partita per due soli giocatori
		if(giocatori.size()==2){
			this.startPerDueGiocatori();
		}
	}
	
	/**
	 * INizio partita per due giocatori
	 */
	private void startPerDueGiocatori(){
		//giocatore fittizio che possiede gli empori della partita posizionati all'inizio
		Giocatore giocatore = new Giocatore("Partita", this.parametri.getColoriGiocatori().get(2), 2, this.mappa.getGrafo().size());
		Map<Regione,PermessoCostruzione> permessi= new HashMap<>();
		this.mappa.getRegioni().forEach(r -> permessi.put(r,r.getMazzoPermesso().pescaCarta()));
		permessi.forEach((r,p) -> p.getListaCitta().
				forEach(c -> c.costruisciEmporio(giocatore.usaEmporio())));
		permessi.forEach((r,p) -> r.getMazzoPermesso().aggiungiCarta(p));
		permessi.forEach((r,p) -> r.getMazzoPermesso().mescolaMazzo());
		
		for(Regione regione:this.mappa.getRegioni()){
			try {
				this.notifyObservers(new MescolatoMutatio(regione.getMazzoPermesso(),regione));
			} catch (RemoteException | CdQException e) {
				this.notifyObservers(e);
			}
		}
	}
	
	/**
	 * Estrae casualmente i consiglieri dal mazzo
	 * @return consiglieri
	 * @throws ColoreNotFoundException
	 */
	private Queue<Consigliere> estraiConsiglieri() throws ColoreNotFoundException{
		Queue<Consigliere> consiglieri = new ArrayDeque<>();
		List<Colore> colori = new ArrayList<>();
		colori.addAll(this.parametri.getColoriPolitici().values());
		
		Random random = new Random();
		int dimensione = colori.size();
		for(int i=0;i<4;i++){
			boolean ripeti;
			Consigliere consigliere;
			do{
				int posizione= random.nextInt(dimensione);
				try{
					consigliere=this.banco.getConsigliere(colori.get(posizione));
					consiglieri.add(consigliere);
					ripeti=false;
				}catch(NoSuchElementException e){
					//entrando qui significa che e' non ci sono giocatori del colore estratto, quindi va ripetuta la pescata
					ripeti=true;
				}
			}while(ripeti);
			
		}
		return consiglieri;
	}
	
	/**
	 * fa passare al turno successivo
	 */
	public void nextTurno(){
		List<Giocatore> giocatoriMustDoAction=new ArrayList<>();
		giocatoriMustDoAction.addAll(this.giocatori);
		Giocatore giocatore=giocatoriMustDoAction.remove(0);
		giocatore.addCartaPolitica(this.pescaCarta());
		List<RicompensaSpecial> ricompense=new ArrayList<>();
		this.statoPartita=new StateInizio(giocatoriMustDoAction,giocatore,ricompense);
		try {
			this.notifyObservers(new StateMutatio(statoPartita));
		} catch (CdQException | RemoteException e) {
			this.notifyObservers(e);
		}
	}
	
	/**
	 * Inizia il turno della partita
	 * @throws ColoreNotFoundException
	 */
	public void startTurnoPartita() throws ColoreNotFoundException{
		init();
		
		this.nextTurno();
		(new Thread(Cronometro.getIstanza(this))).start();
		
	}

	/**
	 * Restituisce il CostoStrade
	 * @return CostoStrade
	 */
	@Override
	public int getCostoStrade() {
		return this.parametri.getCostoStrade();
	}

	/**
	 * Restituisce la mappa
	 * @return mappa
	 */
	@Override
	public Mappa getMappaPartita() {
		return this.mappa;
	}

	/**
	 * Restituisce il giocatore corrente
	 * @return GiocatoreCorrente
	 */
	@Override
	public Giocatore getGiocatoreCorrente() {
		return this.statoPartita.getGiocatoreCorrente();
	}

	/**
	 * Restituisce una lista di giocatori
	 * @return giocatori
	 */
	@Override
	public List<Giocatore> getGiocatori() {
		return this.giocatori;
	}


	/**
	 * Restituisce il percorsoNobilta
	 * @return percorsoNobilta
	 */
	@Override
	public PercorsoNobilta getPercorsoNobilta() {
		return this.percorsi.getNobilta();
	}

	/**
	 * Restituisce il percorsoRicchezza
	 * @return percorsoRicchezza
	 */
	@Override
	public PercorsoRicchezza getPercorsoRicchezza() {
		return this.percorsi.getRicchezza();
	}

	/**
	 * Restituisce il percorsoVittoria
	 * @return percorsoVittoria
	 */
	@Override
	public PercorsoVittoria getPercorsoVittoria() {
		return this.percorsi.getVittoria();
	}

	@Override
	/**
	 * Restituisce il re
	 * @return il re
	 */
	public King getKing(){
		return this.king;
	}

	/**
	 * Restituisce la regione che ha come nome la stringa passata
	 * @return il re
	 */
	@Override
	public Regione getRegione(String nomeRegione) throws RegioneNotFoundException{
		return this.mappa.getRegione(nomeRegione);
	}


	@Override
	public NodoCitta getNodoCitta(String nomeCitta) throws CittaNotFoundException {
		return this.mappa.getNodoCitta(nomeCitta);
	}


	@Override
	public Colore getColorePolitico(String colore) throws ColoreNotFoundException {
		return this.parametri.getColorePolitico(colore);
	}

	@Override
	public Consigliere getConsigliere(Colore colore) throws ColoreNotFoundException{
		return this.banco.getConsigliere(colore);
	}

	@Override
	public void update(Mutatio change) {
		try {
			this.notifyObservers(change);
		} catch (CdQException | RemoteException e) {
			this.notifyObservers(e);
		}
	}

	@Override
	public int getCostoAggiuntivoAzione(int numeroCarte) {
		return this.parametri.getCostoCorruzione(numeroCarte);
	}

	@Override
	public CartaPolitica pescaCarta() {
		return this.banco.pescaCartaPolitica();
	}

	@Override
	public void scartaCarta(List<CartaPolitica> carte) {
		this.banco.scartaCarta(carte);
	}


	@Override
	public Partita getPartita() {
		return this;
	}

	
	@Override
	public Aiutante getAiutante() throws CdQException {
		return this.banco.getAiutante();
	}

	
	@Override
	public List<Aiutante> getAiutanti(int numero) {
		return this.banco.getAiutante(numero);
	}

	
	@Override
	public String toString() {
		String stringa=this.banco.toString()+
				"\n\n"+this.king.toString()+
				"\n\nMappa: "+this.mappa.toString()+"\n\n"+
				this.getPercorsoVittoria().toString()+"\n\n"+
				this.getPercorsoRicchezza().toString()+"\n\n"+
				this.getPercorsoNobilta().toString()+"\n\n"+
				this.statoPartita.toString()+
				"\n\nGiocatori in attesa del proprio turno: ";
		for(Giocatore giocatore: this.statoPartita.getGiocatoriMustDoAction()){
			stringa+=" "+giocatore.getNome()+" - ";
		}
		
		return stringa;
	}
	
	/**
	 * Aggiunge un nuovo giocatore alla lista dei giocatori di questa partita
	 * @param login
	 * @return il giocatore aggiunto
	 */
	public Giocatore addGiocatore(Login login){
		int numeroGiocatore=this.giocatori.size();
		int numeroAiutanti=numeroGiocatore;
		int numeroEmpori=this.parametri.getNumeroEmporiPerGiocatore();
		int ricchezza=10+numeroGiocatore;
		Giocatore nuovoGiocatore=new Giocatore(login.getNomeGiocatore(),numeroGiocatore,
				this.parametri.getColoriGiocatori().get(numeroGiocatore),numeroEmpori,numeroAiutanti,
				this.getPercorsoVittoria().getCasella(0),this.getPercorsoNobilta().getCasella(0),
				this.getPercorsoRicchezza().getCasella(ricchezza));
		nuovoGiocatore.getEmpori().forEach(this::registraOsservabiliTB);

		this.giocatori.add(nuovoGiocatore);
		nuovoGiocatore.addCartaPolitica(this.pescaCarta());
		nuovoGiocatore.addCartaPolitica(this.pescaCarta());
		nuovoGiocatore.addCartaPolitica(this.pescaCarta());
		nuovoGiocatore.addCartaPolitica(this.pescaCarta());
		return nuovoGiocatore;
	}
	
	/**
	 * 
	 * @return true se gli aiutanti sono finiti, false altrimenti
	 */
	public boolean isEmptyAiutanti(){
		return this.banco.isEmptyAiutanti();
	}
	
	/**
	 * Accumula un aiutante nelle disponibilita' partita
	 */
	public void accumulaAiutante(){
		this.banco.accumulaAiutante();
	}
	
	/**
	 * Restituisce alla disponibilita' della partita un consigliere
	 * @param consigliere
	 * @throws ConsigliereNotFoundException quando non c'e' una coda di consiglieri di quel colore
	 */
	public void accumulaConsigliere(Consigliere consigliere) throws ConsigliereNotFoundException{
		this.banco.accumulaConsigliere(consigliere);
	}
	
	/**
	 * Torna un consigliere locale come quello che riceve
	 * @param consigliere
	 * @return un consigliere tra le disponibilita' della partita simile al consigliere ricevuto
	 * @throws ColoreNotFoundException quando non c'e' un consigliere simile a quello ricevuto
	 */
	public Consigliere getConsigliere(Consigliere consigliere) throws ColoreNotFoundException{
		return this.getConsigliere(consigliere.getColor()); //dovrebbe lanciare eccezione se non trova, per server
	}
	
	/**
	 * Torna una regione locale
	 * @param regione
	 * @return una regione locale simile a quella ricevuta
	 * @throws RegioneNotFoundException quando la regione non viene trovata
	 */
	public Regione getRegione(Regione regione) throws RegioneNotFoundException{
		return this.mappa.getRegione(regione);
	}
	
	/**
	 * Torna un consiglio locale, corrispondente al consiglio ricevuto
	 * @param consiglio not null
	 * @return il consiglio corrispondente al consiglio ricevuto
	 * @throws ErrorUpdateServerException quando non esiste il proprietario locale del consiglio ricevuto
	 */
	public Consiglio getConsiglio (Consiglio consiglio) throws ErrorUpdateServerException{
		ProprietarioConsiglio proprietario = consiglio.getProprietario();
	
		if(proprietario.equals(king))
			return king.getConsiglio();
		else{
		Regione regioneConsiglio=this.mappa.getRegioni().stream().
					filter(regione -> regione.getConsiglio().getProprietario().equals(proprietario)).
					findFirst().orElseThrow(() -> new ErrorUpdateServerException("Consiglio non trovato"));
		return regioneConsiglio.getConsiglio();
		}
	}
	
	/**
	 * 
	 * @param mazzo
	 * @return il mazzo locale corrispondente al mazzo ricevuto
	 * @throws ErrorUpdateServerException quando il mazzo non e' tra quelli locali
	 */
	public MazzoPermessi getMazzoPermessi(MazzoPermessi mazzo) throws ErrorUpdateServerException{
		
		return this.mappa.getRegioni().stream().
					filter(regione -> regione.getMazzoPermesso().equals(mazzo)).
					findFirst().orElseThrow(() -> new ErrorUpdateServerException("Mazzo della regione non trovato")).
					getMazzoPermesso();
	
	}
	
	/**
	 * Registra su costruzione le tessere bonus come osservatori
	 * @param costruzione
	 */
	public void registraOsservabiliTB(Emporio emporio){
		this.banco.getTessereBonus().forEach(emporio::registerObserver);
	}

	/**
	 * Restituisce la lista di giocatori Offline
	 * @return giocatoriOffline
	 */
	public List<Giocatore> getGiocatoriOffline() {
		return giocatoriOffline;
	}	
	
	/**
	 * Restituisce un valore booleano per indicare la fine della partita
	 * @return true se la partita e' finita, false altrimenti
	 */
	public boolean isEnded(){
		return isEnded;
	}

	/**
	 * Restituisce lo stato corrente della partita
	 * @return statoPartita
	 */
	public State getStatoPartita() {
		return statoPartita;
	}

	/**
	 * Permette di settare lo stato della partita
	 * @param statoPartita
	 * @throws IllegalArgumentException--> parametro nullo!
	 */
	public void setStatoPartita(State statoPartita) {
		if(statoPartita!=null)
			this.statoPartita = statoPartita;
		else throw new IllegalArgumentException("Lo stato della partita non può essere nullo!");
	}
	
	/**
	 * Restituisce il banco
	 * @return banco
	 */
	public Banco getBanco() {
		return banco;
	}
	
	/**
	 * Aggiorna il giocatore passato
	 * @param giocatore
	 * @throws ErrorUpdateServerException
	 */
	public void updateGiocatore(Giocatore giocatore) throws ErrorUpdateServerException{
		int numeroGiocatore=this.getGiocatori().indexOf(giocatore);
		if(numeroGiocatore==-1){
			numeroGiocatore=this.getGiocatoriOffline().indexOf(giocatore);
			if(numeroGiocatore==-1)
				throw new ErrorUpdateServerException("Errore aggiornamento modello locale");
			else{
				this.getGiocatoriOffline().remove(numeroGiocatore);
				this.getGiocatoriOffline().add(numeroGiocatore, giocatore);
			}
		}
		else{
			this.getGiocatori().remove(numeroGiocatore);
			this.getGiocatori().add(numeroGiocatore, giocatore);
		}
	}

	/**
	 * setta il king della partita
	 * @param king
	 * @throws IllegalArgumentException--> king passato nullo
	 */
	public void setKing(King king) {
		if(king!=null)
			this.king = king;
		else throw new IllegalArgumentException("Il re passato non può essere nullo!");
	}

	/**
	 * Permette di settare il valore della variabile che indica se la partita è finita
	 * @param isEnded
	 */
	public void setEnded(boolean isEnded) {
		this.isEnded = isEnded;
	}

	public Parametri getParametri() {
		return parametri;
	}


}
