# README #

Progetto Ingegneria del Software A.A. 2015/16

Consiglio dei Quattro

## Avvio del gioco

### Server

La classe Server, situata nel package *server*, va lanciata semplicemente. Non richiede input.

Il Server resta in ascolto sulla porta **29999** con *socket* e sulla porta **44444** con *RMI*

### Client

La classe Client e' avviabile dal rispettivo package *client*.

Viene chiesto di scegliere il tipo di interfaccia da utilizzare: digitare **C** per CLI o **G** per GUI

#### CLI
Inserire i parametri richiesti dal programma:

1. Nome del giocatore: ad esempio *Marco*
2. Tipo di connessione preferita:
     * **S** per scegliere Socket;
     * **R** per RMI;
3. Indirizzo IP del server: **127.0.0.1** [se in locale]
4. Porta del server:
     * **29999** per Socket;
     * **44444** per RMI;

##### Primo Giocatore
Il primo giocatore connesso ha il privilegio di caratterizzare i parametri della partita.

Può infatti scegliere una tra le 8 combinazioni di plance. Nella CLI la scelta è a "scatola chiusa".

Indica anche il numero massimo di giocatori per quella partita e il timeout di durata delle mosse.

1. Numero massimo giocatori: il numero di giocatori desiderato, ad esempio **2**
2. Combinazione mappa: un numero tra **0** e **7** (inclusi)
3. Durata massima della mossa, in secondi, maggiore di 60: ad esempio **70**

Si attende quindi l'avvio della partita

##### Comandi
I comandi sono *case-sensitive*
+ Help: stampa l'elenco dei comandi utilizzabili e la loro sintassi
+ Print: stampa il modello

ESEMPIO: per effettuare una Azione Principale di cambio di consiglio della regione Mare aggiungendo un consigliere Rosso, digitare *CambioConsiglio mare rosso*

#### GUI
Inserire i parametri richiesti dal programma.
Gli stessi indicati nella sezione CLI.

##### Primo Giocatore
Come per la CLI, è possibile indicare i parametri di gioco.

Vengono presentate le 6 plance e si può selezionare quella preferita per ogni regione.

Viene mostrata un'anteprima della mappa che si sta scegliendo.

Completare i campi di numero giocatore e durata mossa.

Inviare i parametri.

Non resta che attendere l'avvio della partita.

##### Comandi
In generale, ciò che può essere parametro di un'azione è un ToggleButton, che va propriamente selezionato.

ESEMPIO: per cambiare un consiglio, selezionare quello desiderato cliccandoci sopra [verrà riquadrato e schiarito], poi nella Tab del *Banco* selezionare il consigliere da aggiungere. Cliccare infine sul pulsante dell'azione desiderata. *Compare una descrizione del pulsante se ci si posiziona sopra con il mouse*

## Notevole
Elenco delle qualità che vogliamo evidenziare

### Modello
+ Macchina a stati per la gestione dei turni
+ *Visitor Pattern* per l'implementazione delle transizioni tra stati
+ TessereBonus gestite con *Observable/Observer*
### GUI
+ JavaFX
+ Animazione nel *cambio del consiglio*
### Test
+ Utilizzo del framework *Mockito*
### Extra
+ Chat [sia in GUI sia in CLI]